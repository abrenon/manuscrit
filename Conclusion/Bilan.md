## Synthèse des résultats

### Les facettes de la disciplinarisation {.unnumbered .unlisted}

Les travaux exposés tout au long de ce manuscrit ont mis en lumière différentes
conséquences des changements subis par la Géographie entre le XVIII^ème^ et le
XIX^ème^ siècle, et notamment sa constitution en une véritable discipline
scientifique.

Cette disciplinarisation est marquée d'abord par une complexification des
articles de géographie en termes de richesse du vocabulaire, constitué en
moyenne de mots plus longs (voir la section \ref{sec:geo_contours}
p.\pageref{sec:geo_contours}). Ce phénomène est observable malgré une haute
concentration d'abréviations dans la plupart des entrées de la classe
*Géographie* qui décrivent brièvement un lieu sans donner plus de précision.
Mais ce seul fait ne doit pas occulter la finesse des mouvements contraires et
simultanés auquels la Géographie est sujette.

En premier lieu, la ressemblance entre la *Géographie* et les autres classe
s'accentue entre l'*EDdA* et *LGE*. Cela se manifeste par exemple au niveau de
la distribution du nombre de mot par article, bien plus similaire à celle des
autres domaines de connaissance dans la partie du corpus correspondant à *LGE*
(voir section \ref{sec:geography_edge_words_count}
p.\pageref{sec:geography_edge_words_count}). Des articles plus longs
apparaissent dans le domaine, particulièrement bref par contraste dans l'*EDdA*.
Une autre conséquence de ces rapprochements peut s'observer en considérant les
occurrences d'[@=EN] comme le fait la section \ref{sec:geo_named_entities}
(p.\pageref{sec:geo_named_entities}). Alors que les philosophes des Lumières
emploient surtout les [@=EN] dans les articles de la classe *Géographie*, le
profil d'utilisation des différents types relevés dans l'annotation
géo-sémantique des articles s'harmonise entre les disciplines. Les noms propres
de lieu en particulier (NP-Spatial) se propagent dans une certaine mesure aux
autres domaines.

Simultanément, la Géographie semble aussi se cloisonner. La très large majorité
des articles du domaine dans l'*EDdA* étaient extrêmements brefs et constitués
d'une longue phrase nominale. Cette tendance, propre au domaine, s'accroît dans
*LGE*, où ces articles se font plus nombreux et où le recours presque
systématique aux abréviations se double d'un «figement» des différents
compléments qui composent la phrase nominale, jusqu'à l'obtension de données
pour ainsi dire tabulaires. Par ailleurs, d'un domaine central et en quelque
sorte «refuge» pour de nombreuses entrées peu scientifiques (mythologie et
phénomènes naturels inexpliqués entre autres), les discours géographiques
s'épurent et s'affirment par contraste comme la description de territoires.

De manière surprenante, les passages de biographie ne disparaissent toutefois
pas entièrement des discours géographiques. Si les biographies clandestines
telles que celles écrites par Jaucourt n'ont plus lieu d'être dans les pages de
*LGE*, des articles de *Géographie* traitant de villes conservent quelques
mentions de personnages célèbres qui y ont vécu.

### Contributions {.unnumbered .unlisted}

#### Version numérique structurée de *LGE* {.unnumbered .unlisted}

Le premier résultat apporté par cette thèse est la définition d'un schéma
d'encodage XML-TEI pour représenter une encyclopédie. Le schéma qui se restreint
au module *core* sans utiliser les éléments du module *dictionaries* tient
compte des particularités de ces œuvres par opposition aux dictionnaires et
permet de représenter des développements arbitrairement longs et fortement
structurés. Cet encodage a été partiellement appliqué à *LGE* pour représenter
la structure de ses articles au-dessus du niveau des paragraphes.

Cette contribution se présente concrètement sous la forme d'un ensemble de
dépôts de code source et d'archives sur l'entrepôt de données Nakala
[@vigier_grande_2021] contenant les fichiers représentant les articles dans
différents formats, accompagnés de leurs métadonnées. Le logiciel central dans
le processus d'encodage est `soprano`[^soprano] qui opère la segmentation du
texte de *LGE* en articles et applique l'encodage défini. Au cours de cette
thèse, le logiciel a reçu plusieurs améliorations qui augmentent notamment la
précision de son algorithme de segmentation. Il s'accompagne d'autres dépôt
comme `Processing LGE`[^processing-lge] qui contient des métadonnées et des
scripts pour faciliter le traitement de l'œuvre dans son ensemble et `LGE
Meta`[^lge-meta] qui présente sous un format tabulaire les métadonnées présentes
au début de l'œuvre dont la liste des abréviations utilisées afin de faciliter
la prise en main des données à de futures équipes de recherches potentielles. 

[^processing-lge]:
    [https://gitlab.huma-num.fr/disco-lge/processinglge](https://gitlab.huma-num.fr/disco-lge/processinglge)

#### Un modèle de classification en domaine de connaissance {.unnumbered .unlisted}

La comparaison de modèles d'[@=AA] en vue de classer les articles par domaine de
connaissance a apporté trois résultats principaux. Le premier confirme la
supériorité de l'architecture Transformers et des modèles *BERT* préentraînés en
particulier en termes de qualité des résultats pour cette tâche de
classification. Le deuxième montre la pertinence de méthodes classiques qui
obtiennent des performances proches — et supérieures à celles des méthodes
d'[@=AP] autres que *BERT* — pour un coût en énergie bien moindre. L'application
des prédictions d'un modèle *BERT*, rectifiées pour un ensemble d'articles de
*Géographie* courts de *LGE* (identifiés comme traitant de communes et regroupés
au sein d'un sous-corpus), a non seulement rendu possibles les analyses
conduites au chapitre \ref{sec:contrasts} mais a surtout permis d'enrichir la
version numérique de *LGE* d'une métadonnée associant un domaine de connaissance
à chaque entrée.

D'autre part, l'analyse des erreurs d'une des architectures classiques ayant
obtenu d'assez bons résultats (celle associant un *TF-IDF* pour la vectorisation
et *SGD* pour la classification elle-même) a permis de visualiser les liens
entre la *Géographie* et les autres domaines dans l'*EDdA*. Les contenus
géographiques infusent de manière sélectives dans certains autres domaines au
centre desquels la *Géographie* fait en quelque sorte office de passerelle.
L'étude a en outre montré que ces ressemblances entre domaines n'étaient pas
seulement lexicales. L'ensemble de ces résultats a fait l'objet d'une
publication [@brenon_classifying_2022].

#### Révéler la biographie {.unnumbered .unlisted}

La troisième contribution présentée dans cette thèse concerne la relation
privilégiée qui existe entre les biographies et les discours géographiques. Les
spécialistes de l'*EDdA* connaissent bien les biographies déguisées en articles
de *Géographie* en utilisant le lieu de naissance de la personne décrite comme
prétexte. Le premier apport de la section \ref{sec:biographies} est de mettre en
évidence un autre filon de contenus biographiques dans l'*EDdA* au travers des
articles de *Philosophie*. En comparant le traitement de la philosophie de
Newton et de celles des autres philosophes, l'étude renverse la perspective sur
l'article WOLSTROPE en montrant que la question posée par cette entrée n'est pas
tant l'existence d'une biographie du savant anglais que le choix de son lieu de
naissance pour l'introduire.

Enfin la section apporte des éléments nouveaux pour comprendre la place des
biographies dans les articles de *Géographie* en distinguant entre différents
types de contenus biographiques dont la valeur illustrative varie d'un article à
l'autre. Après avoir défini deux critères pour caractériser ces contenus, elle
montre la survivance au XIX^ème^ siècle de ceux qui relèvent de l'anecdote et
sont principalement utilisés pour colorer la manière dont une ville est
présentée.

