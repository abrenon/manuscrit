## Perspectives

Au-delà des résultats discutés dans la section précédente, les présents travaux
ont également permis de dégager des pistes prometteuses pour de futures
recherches. Il s'agit pour une part d'améliorations techniques car les logiciels
peuvent bien sûr toujours être perfectionnées sans fin mais ces nouveaux chemins
à explorer ne se limitent pas à des détails d'implémentation. Les méthodes
utilisées dans le cadre de cette thèse pourraient trouver des applications sur
d'autres corpus et, de plus, les données produites pourraient intéresser des
études sur d'autres types de discours.

### Des améliorations techniques {.unnumbered .unlisted}

La recherche d'exemples dans des articles, nécessaire pour illustrer différents
points du propos, a été grandement facilitée par les interfaces de consultations
de l'*EDdA* mises en place par l'[@=ARTFL] et l'[@=ENCCRE]. De telles interfaces
permettent de naviguer rapidement dans les tomes de l'œuvre, de rechercher des
articles et d'opérer un retour au texte en les visualisant sur des photographies
des pages. Pour ces raisons, elles sont utiles d'une manière générale tant au
public qu'aux équipes de recherches. Les photos des pages numérisées et le texte
brut de *LGE* sont consultables sur le site de la BnF[^LGE-V2] mais si ce
dernier autorise la recherche de texte de manière indiscriminée, il ne permet
pas de trouver une vedette spécifique ni même de naviguer par article. L'état
actuel de la segmentation de *LGE* et la précision des métadonnées de navigation
qui comprennent le numéro des pages ont rendu faisables le même type de
recherches sans trop de friction lors de la rédaction de ce manuscrit mais
l'interface expérimentale déployée à cet effet n'est pas encore assez pratique
pour être publiée. Quelques efforts supplémentaires de développement restent
nécessaires pour la rendre plus intuitive, pour y implémenter la recherche par
vedette et pour trouver une solution pérenne pour l'héberger. 

Il a aussi été envisagé durant cette thèse d'étudier les variations thématiques
et discursives au sein des articles en utilisant les méthodes de classification
à l'échelle des paragraphes. La segmentation en paragraphes est disponible pour
l'*EDdA* mais pas encore assez robuste pour *LGE* (voir la section
\ref{lge_encoding_paragraphs} p.\pageref{lge_encoding_paragraphs}). Il est à
souhaiter que les prochains mois permettent de régler les derniers problèmes et
de réaliser un nouvel encodage de cette œuvre qui intègre la segmentation en
paragraphes. Des progrès attendent encore d'être faits pour comprendre la
grammaire des vedettes qui intègrent souvent plusieurs mots, parfois des
composantes en minuscules ou en petites majuscules et peuvent s'étendre sur
plusieurs lignes alors que l'encodage actuel ne conserve que le premier mot en
majuscules. Enfin, la résolution des renvois représente certainement un problème
délicat pour plusieurs raisons. L'imprécision de l'[@=OCR] rend un certain
nombre de «V.», abréviation de «voir», comme la séquence «l'» ce qui complique
leur détection. De plus, les vedettes formées de plusieurs mots augmentent
vraisemblablement les risques que la séquence utilisée pour un renvoi différe
légèrement de celle utilisée à l'article cible lui-même — sans compter l'usage
d'expressions référentielles en lieu et place de la vedette explicite telles que
«voir ce mot». À ces difficultés s'ajoute la distance entre un renvoi et sa
cible, potentiellement présente dans un tome distinct alors que le traitement
actuel par `soprano` considère les tomes séparément. La résolution de ces
problèmes et l'ajout d'une fonctionnalité pour suivre les renvois entre articles
apporterait pourtant encore bien plus d'intérêt à une interface web de
consultation de l'œuvre.

### Les discours disciplinaires d'autres encyclopédies {.unnumbered .unlisted}

La fin de cette thèse coïncide environ avec celle du projet GEODE. Cela signifie
qu'il ne reste guère de temps pour prolonger les travaux présentés ici dans le
cadre du projet, mais les thématiques étudiées restent pertinentes en elles-même
et il est à souhaiter que de futurs efforts soient entrepris pour caractériser
les évolutions des discours géographiques, dans ces encyclopédies ou dans
d'autres. En outre, si la Géographie est au centre des attentions dans cette
thèse, la très large majorité des méthodes et outils utilisés ne dépendent pas
de la discipline considérée.

Pour commencer, deux sous-corpus construits pour les besoins des présentes
analyses pourraient constituer des objets d'étude intéressants pour de futurs
travaux. Le sous-corpus Parallèle d'abord (voir la section
\ref{sec:parallel_corpus} p.\pageref{sec:parallel_corpus}) se prêterait aussi
bien à des améliorations techniques qu'à une exploitation dans d'autres
contextes. L'algorithme d'appariement utilisé reste pour l'instant assez naïf et
il y a sans doute des gains significatifs à obtenir en définissant des méthodes
de désambiguïsation plus fines pour permettre de considérer des vedettes proches
mais pas rigoureusement identiques, voire pour trouver des paires parmi des
groupes d'articles homonymes. La taille du sous-corpus Parallèle ne jouait pas
un rôle déterminant dans l'utilisation qui en est faite dans cette étude, mais
augmenter sa couverture ouvrirait la porte à des études plus quantitatives. Il
pourrait aussi s'avérer utile pour étudier les variations diachroniques au sein
d'autres domaines de connaissance.

Ensuite, le corpus des Communes, mis au jour presque accidentellement en
évaluant la qualité des prédictions du modèle BERT sur les articles de *LGE*
(voir la section \ref{sec:classifying_lge} p.\pageref{sec:classifying_lge})
possède plusieurs qualités notables. Sa taille d'abord est remarquable: avec
plus de 20 000 articles, il contient davantage d'articles qu'il n'y en a en tout
dans la classe *Géographie* de l'*EDdA* (même plus d'une fois et demie puisqu'il
y en a environ 13 000). Composé d'articles extrêmement brefs (50 tokens ou
moins) et tous construits sur le modèle d'une phrase nominale dense en
abréviations, sa grande régularité invite à une extraction automatique des
données.

De plus, les changements mis en évidence dans cette thèse ne concernent que les
deux premières œuvres du corpus de GEODE. Il apparaît naturel de se demander ce
que deviendraient les constats qui y sont faits au regard des deux autres œuvres
versées au corpus de GEODE, *Universalis* et *Wikipédia*, voire dans d'autre
encyclopédies encore. Les considérations diachroniques des présents travaux, qui
devait initialement inclure *Wikipédia*, ont dû se limiter au premier intervalle
du corpus faute de temps. Il serait donc profitable de prolonger les méthodes
utilisées dans cette thèse pour regarder si les tendances découvertes se
poursuivent ou bien se modifient. Les approches algébriques et celles basées sur
la théorie des graphes pour déterminer les influences entre domaines et leurs
proximités lexicales devraient trouver sans difficulté des applications à
d'autres encyclopédies ou d'autres domaines de connaissance et pourraient être
mises à profit pour réaliser une cartographie des domaines de connaissance d'une
œuvre donnée et la comparer avec les intentions déclarées par ses auteurs telles
que le «Systême figuré» des Lumières.

### *LGE*, un sujet riche pour les HN {.unnumbered .unlisted}

Enfin, il est difficile de travailler aussi longtemps au contact d'une œuvre
aussi fascinante que *LGE* sans apercevoir des passages qui éveillent la
curiosité et feraient probablement des sujets de recherches passionnants.

Les biographies occupent une part substantielle de l'œuvre mais sont à peine
mentionnées dans l'Avant-Propos (La Grande Encyclopédie, T1, p.XIII). Classées
dans ces travaux en *Histoire* ou bien en fonction de la discipline où la
personne décrite s'est illustrée, elles restent à ce jour non répertoriées. La
présence du prénom de la personne en lieu et place du désignant (voir la section
\ref{sec:knowledge_domains}) fournit un indice précieux pour les identifier et
pourrait sans doute également permettre de discerner le genre de la personne
décrite. Si ces articles traitent dans leur large majorité d'hommes célèbres,
certains sont consacrés à des femmes comme l'entrée sur LOVELING évoquée à la
même section (p.\pageref{fig:loveling_lge}). Un sous-corpus des biographies
classées par genre permettrait à n'en pas douter des analyses fructueuses: il
suffit de se saisir des premiers mots de la biographie de LOVELING pour trouver
un motif sûrement fécond. On trouve beaucoup de biographies de «femme de
lettres», mais aussi quelques occurrences où une femme est décrite par rapport à
son époux ou son père. Ainsi, les premiers mots sur lady FRANKLIN (La Grande
Encyclopédie, T18, p.66) la présentent comme la «seconde femme de sir John
Franklin», et sa vie n'est racontée dans le reste de l'article qu'en relation à
celle de son mari. Pour certains couples célèbres, les deux personnes ont leur
entrées. Alors que l'article sur ABAILARD (La Grande Encyclopédie, T1, p.14) ne
mentionne Héloïse qu'à la 22^ème^ ligne et s'étend sur 2 pages, celui sur
HÉLOISE (La Grande Encyclopédie, T19, p.1043) la présente d'emblée comme «amante
d'Abailard» (avec un renvoi vers l'article précédent qui, lui, n'en possède pas)
et ne consiste qu'en un bref paragraphe de 6 lignes. Mais cette asymmétrie en
faveur des hommes n'est pas systématique comme le démontre le traitement du
couple HOLLAND (La Grande Encyclopédie, T20, p.192). Lady Holland est certes
présentée d'emblée comme «femme du suivant» (renvoi implicite à l'article sur
son mari) alors que son mari est d'abord un «célèbre homme politique» mais son
entrée est plus brève et renvoie explicitement vers celle de son épouse. La
place des femmes dans *LGE* en général et plus particulièrement cette opposition
«femme de lettre / femme de» mérite certainement d'être examinée en détail et de
manière quantitative, ce que permet la version numérique de l'œuvre obtenue à
l'issue de cette thèse.

Un autre thème ressort nettement des pages de *LGE* et en particulier des
entrées géographiques. La construction du second empire colonial français
impulsée en 1830 par Charles X en Algérie est déjà bien avancée sous la III^ème^
république au moment de la publication de *LGE*: la présence française est
établie en Afrique subsaharienne (en Côte d'Ivoire depuis 1843 ou au Gabon
depuis 1839 entre autres), en Asie (Viêt Nam depuis 1858, première pierre de ce
qui deviendra l'Indochine en 1887 — 2 ans après le début de la parution de
*LGE*) et dans le Pacifique (dont Kanaky à partir de 1853). L'exploitation des
territoires, souvents décrits en termes des ressources qu'ils peuvent fournir
dans des sections réservées à la démographie et à l'économie des articles de
*Géographie* semble à première vue concerner autant des pays d'Europe (dont la
France elle-même) que des colonies mais la nature des statistiques rapportées
pourrait différer suivant les lieux. En revanche, la description des populations
colonisées se distingue de manière bien plus nette et révèle une vision du monde
basée sur une notion de «races» hiérarchisées. Les nombre d'habitants sont
souvent partitionnés suivant ce critère, par exemple aux articles PHILIPPEVILLE
(La Grande Encyclopédie, T26, p.676) — ancien nom de la ville de Skikda — et
ZANZIBAR (La Grande Encyclopédie, T31, p.1305). D'autres articles comme CANAQUES
(La Grande Encyclopédie, T8, p.1195) ou BAMBARA (La Grande Encyclopédie, T5,
p.192) prennent résolument le parti de caractériser les populations par des
qualités physiques ou psychologiques supposées inhérentes à tous les individus
d'un groupe ethnique. L'adhésion implicite aux forces coloniales transparaît
dans l'emploi de la première personne du pluriel, par exemple «notre
protectorat» à l'article BAMAKOU (La Grande Encyclopédie, T5, p.192) alors que
le recours à cette personne est très rare dans *LGE* par rapport à l'*EDdA*. Il
s'agirait indubitablement d'un travail pénible du fait de la violence de
certains textes, mais une étude lexicale précise basée sur des considérations
textométriques permettrait sans nul doute de rendre compte des représentations
et des théories de la fin du XIX^ème^ siècle et de montrer leur articulation
avec l'esprit républicain de l'époque qui transparaît par ailleurs le reste de
l'œuvre.

Il faut donc espérer que d'autres s'empareront des myriades de questions qui
subsistent dans les pages de *LGE*. Ces recherches rendront assurément les
efforts déployés au centuple, en émerveillement et en découvertes, comme elles
l'ont fait pour cette thèse.

