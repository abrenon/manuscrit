## (Méta)lexicographie {#sec:EdlA_lexicography}

En s'intéressant à l'encodage en XML-[@=TEI], la section précédente a montré
qu'un module spécifique était dédié aux dictionnaires et mis en évidence
plusieurs projets consacrés à l'étude de ce type d'ouvrages ou aux encyclopédies.
Au-delà du seul prisme des présents travaux, le genre lexicographique semble
occuper une place particulière dans les [@=HN] et jouer un rôle fondateur pour
la linguistique de corpus. La section qui débute tâche de cerner ces liens.

### Des objets conceptuellement riches {#sec:EdlA_lexicography_concept}

Dictionnaires et encyclopédies représentent, malgré leur apparence de simple
table associative entre termes et définitions, des objets intrinsèquement
complexes, dont la difficulté d'encodage a pu dérouter des spécialistes des
[@=HN] et de la lexicographie tels que @ide_encodingdictionaries_1995[p.177].
L'identification de ces complexités et leur prise en compte dans le schéma de la
[@=TEI] s'est montrée fructueuse non seulement pour ce genre mais aussi pour les
autres [@ide_encodingdictionaries_1995, *ibid*] ce qui constitue un premier
signe de l'importance du genre lexicographique en [@=HN].

La structure associative de ces ouvrages implique également une discontinuité
dans «le» texte. Un dictionnaire rassemble en réalité un *ensemble* de textes
distincts dont l'adjacence apparente n'est due qu'à l'arbitraire de l'ordre
alphabétique et qui ne se lisent pas linéairement du début à la fin
[@ide_encodingdictionaries_1995, p.167]. La dernière phrase d'un article ne
vient pas «avant» la première de l'article suivant; chacune existe dans son
propre espace.

Ces textes sont séparés mais pas indépendants: des renvois invitent à un
parcours transverse des entrées basé typiquement sur la synonymie dans les
dictionnaires et des proximités thématiques dans les encyclopédies
[@blanchard_systeme_2002, p.46]. Cette structure transversale se prête bien à
des études de graphes surtout pour les dictionnaires de synonymes
[@ploux_construction_1998]. D'autres approches basées seulement sur les
occurrences de mots dans les entrées de dictionnaires de langues bénéficient
également des apports de la théorie des graphes [@loiseau_dictionnaires_2011].

Mais à cette seule complexité structurelle s'ajoute une portée conceptuelle
extraordinaire, autant pour les dictionnaires de langues que pour ceux de
choses, pour suivre la distinction couramment faite. Les premiers, en ayant pour
but de définir les mots, constituent un relevé de signes: la vedette qui sert de
clef aux articles représente un signifiant qu'il faut en quelque sorte garder
abstrait à la lecture, son signifié étant apporté ensuite par la définition qui
forme le reste de l'entrée. Les dictionnaires de langue sont ainsi des espaces
isolés de la réalité sensible, les énoncés qu'on y trouve ne portent pas tant
sur le monde que sur le langage lui-même d'après @sinclair_beginning_1966[p.6].
Le choix du lexique lui-même reflète la façon de parler puisque «la nomenclature
d'ensemble est imposée par l'usage de la langue» [@rey_antoine_2006, p.39]. Les
définitions dans les dictionnaires de chose au contraire donnent «accès au
domaine à décrire», toujours selon @rey_antoine_2006[*ibid*] et, en cela,
concernent davantage le référent des signes. Dans les deux cas le dictionnaire
constitue ainsi en lui-même un «traité métaphysique sur le sens»
[@willinsky_wittgenstein_2001, p.189-190]\: le genre lexicographique intéresse
donc la sémantique la plus abstraite jusqu'à la logique et la philosophie. Le
problème du sens des mots représente un sujet de réflexion vertigineux.
D'Alembert avait déjà compris son ampleur en écrivant à l'article DICTIONNAIRE
(L'Encyclopédie, T4, p.958):

> \label{dalembert_dictionnaire}un dictionnaire de langues, qui paroît n'être
> qu'un dictionnaire de mots, doit être souvent un dictionnaire de choses quand
> il est bien fait : c'est alors un ouvrage très-philosophique.

Il n'est pas possible de définir les mots en demeurant au niveau du seul
langage, sous peine de produire des tautologies selon
@haiman_dictionaries_1980[p.330], ce qui l'amène à attaquer lui aussi la
distinction faite entre dictionnaires et encyclopédies.
@russell_inquiry_1941[p.25] en adoptant une approche parallèle à la déduction
mathématique basée sur des hypothèses, distingue des mots qui ne peuvent
s'expliquer qu'au moyen d'autres mots et des mots dont le sens provient au
contraire de l'expérience sensible du monde: les «mots-objets», qui ne
présupposeraient aucun autre mot [@russell_inquiry_1941, p26] et joueraient
ainsi le rôle d'axiomes du langage (tant du point de vue logique que de celui de
la chronologie individuelle puisqu'il écrit que ce seraient les premiers mots
appris par les enfants lors de l'acquisition du langage). S'il reste possible de
définir certains d'entre eux en intension (au prix d'une description
introduisant plusieurs nouveaux mots pour chacun) d'autres, typiquement ceux
rattachés à des qualia comme la couleur ne peuvent espérer recevoir qu'une
définition extensionnelle [@lehmann_lexicologie_2018, chap.2, §14]
nécessairement incomplète (il est possible d'énumérer *des* objets de couleur
rouge mais pas tous ces objets). Mais sans explications ni liens, une expérience
du monde ne constitue pas une définition, ce qui fait écrire à
@teubert_corpus_2005[p.137] que les descriptions des livres d'images pour enfant
jouent sans doute un rôle supérieur à celui des dictionnaires dans l'acquisition
du langage.

Élève de Russell, Wittgenstein ne se satisfait pas des «mots-objets» et refuse
le statut d'évidence que Russell accepte de leur concéder. Pour lui, le vrai
sens des mots échappe nécessairement aux définitions du dictionnaire car elles
établissent des frontières nettes entre eux alors que la réalité est bien plus
floue: les mots tirent selon lui leur signification de la manière dont ils sont
employés dans le langage [@willinsky_wittgenstein_2001, p.196]. À la suite de
Wittgenstein, @firth_synopsis_1957[p.11] base le sens des mots sur leur
relation avec la notion de *collocations*:

> You shall know a word by the company it keeps !

(«vous reconnaîtrez un mot à ses fréquentations»).
@zellig_distributional_1954[p.146] se propose même de quantifier ces
fréquentations en comptant les occurrences conjointes de différentes classes de
mots dans une approche distributionnelle. Cette approche ainsi que l'aphorisme
de Firth sont à la source de l'application de méthodes statistiques à la
linguistique et trouvent un écho jusque dans la façon de modéliser les mots dans
la plupart des outils actuels (voir les sections \ref{sec:EdlA_TAL} et
\ref{sec:EdlA_corpus_linguistics}). L'héritage de Firth, en passant par Halliday
puis Sinclair [@leon_histoire_2015, p.161] développe une pratique centrée sur
l'étude des collocations d'un point de vue probabiliste et distributionnel qui
aboutira plusieurs décennies plus tard à la conception de dictionnaires
radicalement nouveaux [@krishnamurthy_corpusdriven_2008, p.239].

### Dictionnaires et encyclopédies {#sec:EdlA_lexicography_dict_and_enc}

Identifier des collocations exige des corpus assez grands [@teubert_corpus_2005,
p.134] pour pouvoir observer assez d'occurrences des phénomènes étudiés pour
qu'elles puissent être significatives statistiquement, et c'est pourquoi
l'approche de ce qui restera connu sous le nom d'*École britannique* a pu
révolutionner la lexicographie une fois que les capacités des ordinateurs leur
ont permis de traiter automatiquement du texte. La section
\ref{sec:EdlA_TAL_history} revient plus en détail sur les grandes étapes qui ont
jalonné le XX^ème^ siècle dans ce domaine pour donner un sens plus précis à
l'expression «Traitement Automatique des Langues» ([@=TAL]) mais l'acronyme sera
utilisé dès la présente section pour désigner tous les traitements statistiques
de la langue opérés à l'aide d'ordinateurs.

Équipé de ces moyens nouveaux, @sinclair_collins_1990 concrétise en quelque
sorte les idées de Wittgenstein avec le projet COBUILD qui aboutit à la création
d'un dictionnaire dont les définitions sont fondées sur des observations
d'occurrences réelles trouvées dans des grands corpus de textes. Dans d'autres
pays aussi des projets similaires voient le jour.

En France, le *Trésor de la Langue Française*[^TLF] (TLF) est lancé au début des
années 1960 [@surmont_genese_2006, p.56] sur un corpus reprenant les données de
Frantext. Il comporte 270 000 définitions et 430 000 exemples et emploie dès ses
débuts des techniques de [@=TAL] et des ordinateurs Bull Gamma 60
[@surmont_genese_2006, p.57]. Les français restent toutefois à la traîne sur les
britanniques et les américains sur le versant informatique [@leon_histoire_2015,
p.159] et ces moyens ne sont déployés qu'en vue d'écrire le contenu du
dictionnaire dont la finalité est d'être imprimé, l'idée d'un dictionnaire comme
fichier étant reléguée à un futur relevant de la science-fiction
[@surmont_genese_2006, *ibid*]. L'informatisation du TLF (le TLFI)
n'interviendra que bien plus tard dans les années 1990, et il est frappant de
constater que la première version mise en ligne l'a été grâce à l'[@=ARTFL], en
1998 [@surmont_genese_2006, p.57]. En Allemagne, @teubert_corpus_2005[p.127]
rapporte l'existence d'un dictionnaire basé sur un corpus, le *Schlüsselwörter
der Wendezeit* paru en 1997.

[^TLF]: [http://atilf.atilf.fr/](http://atilf.atilf.fr/)

La linguistique de corpus nourrit et guide donc la conception de dictionnaires,
mais à l'inverse les dictionnaires sont des objets utiles pour le [@=TAL]. Il y
a donc deux mouvements contraires simultanés, mais en reprenant la distinction
faite par @lehmann_lexicologie_2018[p.255] il est même possible de diviser le
mouvement «retour» de l'apport des œuvres lexicographiques au [@=TAL] en deux
flux comme le montre la figure \ref{fig:lexicography_tal}: les produits de la
lexicographie sont nécessaires dans de nombreuses branches du [@=TAL], mais les
dictionnaires et les encyclopédies constituent également un genre dont les
œuvres sont étudiées pour elles-mêmes, ce que Lehmann nomme la
métalexicographie.

![La triple relation qu'entretiennent le TAL et les œuvres lexicographiques](figure/lexicographyTAL.png){#fig:lexicography_tal width=60%}

L'emploi de lexiques et de bases associatives de mots en [@=TAL] semble remonter
à ses origines. En 1959, alors qu'il est encore anachronique de parler de
[@=TAL] et que l'ensemble des activités qui relèveraient de ce domaine se résume
à la Traduction Automatique (TA), des objets comme la Machine de Trojanski
reposent sur des dictionnaires pour des tâches de traduction automatique depuis
et vers un langage symbolique servant de passerelle centrale entre les langues
naturelles [@leon_histoire_2015, p.99-100]. Plus récemment de nombreux outils
font usage de lexiques pour la segmentation du texte en tokens ou leur
étiquetage morphosyntaxique [@leon_histoire_2015, p.143]. C'est le cas par
exemple de la chaîne de traitement utilisée dans le projet PRESTO
[@diwersy_ressources_2017 p.33] et d'outils comme Talismane
[@urieli_apport_2013, p.191] ou Stanza[^stanza_lemmatizer] [@qi2020stanza]. Leur
emploi est tel que de nombreux efforts ont été déployés pour rendre
l'information contenue dans des dictionnaires accessible à des traitements
automatiques avec des succès parfois en dessous des attentes initiales selon
@ide_extracting_2000[p.9].

[^stanza_lemmatizer]:
    [https://stanfordnlp.github.io/stanza/lemma.html#improving-the-lemmatizer-by-providing-key-value-dictionary](https://stanfordnlp.github.io/stanza/lemma.html#improving-the-lemmatizer-by-providing-key-value-dictionary)

En ce qui concerne la métalexicographie, @zhu_discours_2022[§8] fait observer
que les dictionnaires constituent «un excellent terrain d'expérimentation» et
les encyclopédies intéressent par nature non seulement les nombreuses
disciplines des discours qui s'y déploient mais aussi la philosophie et
l'histoire des sciences pour ce qu'elles disent sur la vision du monde qui
prévalait au moment de leur parution. C'est dans ce rapport entre encyclopédies
et [@=TAL] que s'inscrivent les présents travaux.

Des projets de recherche couvrent les époques marquant des phases majeures dans
l'évolution des dictionnaires et des encyclopédies. @quemada_dictionnaires_1968
remonte ainsi jusqu'au *Dictionnaire françois-latin* de Robert Estienne, en
1539, premier relevé alphabétique de mots en français à offrir, outre leur
traduction latine, une définition rédigée en français
[@quemada_dictionnaires_1968, p.12]. L'apparition d'un genre encyclopédique en
tant que tel est un phénomène progressif mais un palier important a été atteint
avec le *Dictionnaire Universel* de Furetière en 1690 qui dépasse le cadre du
seul langage pour s'intéresser aux choses [@leca_tsiomis_dictionnaires_2009,
§§5-9]. Bien qu'on puisse lui reprocher certaines lacunes, sa deuxième édition,
parue en 1701 et évoquée à la section \ref{sec:EdlA_tei_applications}, achève
d'offrir au *Furetière* sa place dans l'histoire de l'encyclopédisme en Europe
et en fait un précurseur important de l'*EDdA*. Le mouvement se poursuit dans le
reste du XVIII^ème^ siècle, période très étudiée pour la richesse des
innovations dans les dictionnaires universels et les encyclopédies
[@leca_tsiomis_ecrire_1995, §10]. La première édition du *Dictionnaire Universel
François et Latin*, couramment appelé le *Dictionnaire de Trévoux* est publiée
par les jésuites (catholiques au contraire de Basnage qui était protestant) en
1704, peu après la parution du *Furetière* dont il reprend assez d'éléments pour
être accusé de plagiat [@le_guern_dictionnaire_1983, p.51;
@macary_dictionnaires_1973, p.152]. S'il y a tout de même quelques additions, il
s'agit en effet surtout de le purger de tout ce qui contredit la doctrine de
Rome [@le_guern_dictionnaire_1983, p.56]. Le *Trévoux* connaîtra au total 9
éditions jusqu'à 1771 au fil desquelles il fait l'objet d'augmentations
importantes et se démarque progressivement du *Furetière*
[@macary_dictionnaires_1973, pp.153-154]. Grand rival de l'*EDdA*, ses auteurs
se montrent de redoutables adversaires des philosophes des Lumières.

Au travers de ces quelques exemples glanés parmi une myriade d'œuvres et de
projets, une régularité se dégage: si les dictionnaires semblent intéresser le
[@=TAL] aussi bien comme outil que comme objet d'étude, en revanche les
encyclopédies ne sont exploitées que comme objet d'étude.

### L'Encyclopédie au centre des attentions

Parmi toutes les encyclopédies étudiées en [@=HN], l'*EDdA* se singularise par
le nombre de travaux qui y ont été consacrés dans de nombreux domaines. Il
existe même une revue en ligne spécialisée sur le sujet, «Recherches sur Diderot
et sur l'Encyclopédie»[^rde]. Des publications retracent l'histoire romanesque
de sa publication entravée de censure royale [@moureau2001]. D'autres
s'intéressent aux personnages centraux dans le projet
[@kafker_andre_francois_2016], en particulier à leurs motivations et à leurs
stratégies en tant que contributeurs de l'*EDdA* [@lojkine2013] ou bien au
contraire au reste de leur vie. @pruvost_regard_2022 montre ainsi la place
majeure qu'a occupée l'Académie française dans la vie de d'Alembert en dehors de
sa participation à l'*EDdA* et l'influence déterminante qu'il a eue sur la
cinquième édition du *Dictionnaire de l'Académie*.

[^rde]:
    [https://journals.openedition.org/rde/](https://journals.openedition.org/rde/)

Plusieurs études comparatives mettent en valeur l'importance de cette œuvre
centrale du siècle des Lumières au travers de ses liens avec les autres grandes
entreprises encyclopédiques du même siècle ou du suivant. @morin_diderot_1989
confronte ainsi l'*EDdA* à son prédécesseur et rival le *Dictionnaire de
Trévoux* en explicitant le dialogue qui existe entre les deux œuvres malgré une
ignorance feinte. Bien que les dernières éditions du *Trévoux* soient
contemporaines de l'*EDdA*, cette rivalité se double en réalité d'une relation
de filiation via la *Cyclopedia* anglaise de Chambers
[@leca_tsiomis_ecrire_1995] que Diderot et d'Alembert devaient initialement se
contenter de traduire. Le caractère innovant de son titre aurait suscité chez
son lectorat français des espoirs d'originalité infondés d'après
@leca_tsiomis_cyclopaedia_2023[p.437], et ce malgré l'avertissement de Chambers
lui-même dans sa préface. Le processus de traduction s'avère décevant pour les
encyclopédistes en révélant de nombreux emprunts parfois passés sous silence
[@leca_tsiomis_cyclopaedia_2023, p.441], notamment au *Dictionnaire de Trévoux*
[^plagiat] qui s'était lui-même servi copieusement chez Basnage. Certains
travaux donnent même des éléments pour identifier les versions des différentes
œuvres auxquelles ont pu avoir accès les contributeurs [@passeron_quelles_2006].
La lignée se prolonge après l'*EDdA* avec des œuvres comme la monumentale
*Encyclopédie Méthodique* que Panckoucke envisage d'abord comme une simple
amélioration de l'«ancienne Encyclopédie» [@groult_introduction_2019, p.8] mais
qui deviendra une collection de dictionnaires en plusieurs tomes pour chaque
discipline [@groult_ordre_2019, p.123] et dont la publication se poursuivra bien
après sa mort jusqu'en 1832 grâce à ses descendants
[@la_cotardiere_descendants_2019]. L'héritage de l'esprit encyclopédique passe
ainsi au XIX^ème^ et donnera lieu à d'autres tentatives d'actualisation des
connaissances de l'*EDdA* à travers des œuvres comme le *Grand Dictionnaire
Universel* de Pierre Larrousse et *LGE* [@jacquet_pfau_actualiser_2022], l'autre
encyclopédie du corpus auquel s'intéresse cette thèse.

[^plagiat]: ce qui vaudra aux encyclopédistes des accusations de plagiat de la
    part des mêmes jésuites qui avaient pourtant encensé la *Cyclopedia*

Naturellement, l'organisation de la connaissance dans l'*EDdA* fait également
l'objet d'études. @blanchard_systeme_2002 décrivent ainsi les renvois et la
composition hiérarchique des domaines de connaissance qui, en sus de l'ordre
alphabétique que les encyclopédies partagent avec les dictionnaires, structurent
les articles de l'*EDdA*.

Ces articles constituent ainsi une source d'information précieuse sur
l'évolution de nombreux domaines scientifiques. En s'intéressant à l'Histoire,
@hardestydoig_designant_2006 souligne la part d'arbitraire résidant dans tout
système d'organisation de la connaissance et montre que sa valeur ne se limite
pas à la vue synthétique qu'il offre mais qu'il possède aussi une composante
analytique des rapports entre sciences. L'appartenance d'un article à un domaine
est marqué par un ou plusieurs désignants, dont les combinaisons, loin d'être de
simples juxtapositions obéissent à des logiques révélatrices
[@cernuschi_designants_2006]. Les désignants peuvent ainsi être des lieux
d'innovation, faisant par exemple se rencontrer les termes «économie» et
«politique» dans des articles qui, s'ils n'inventent pas le domaine *ex nihilo*
semblent néanmoins fondateurs pour la discipline [@salvat_articles_2006].
Reflets de l'état de la connaissance au XVIII^ème^ siècle, les désignants
mettent en évidence des différences fondamentales dans le traitement de concepts
aujourd'hui compris comme proches, et qui expliquent les limites des théories de
l'époque, par exemple en Physique [@viard_lutilite_2006].

La Géographie est bien sûr concernée par ce type de travaux au même titre que
les autres sciences. @laboulais_geographie_2020 relate comment elle peine
d'abord à s'imposer dans les discours encyclopédiques puis à trouver sa place
parmi les autres domaines de connaissance, d'une origine purement mathématique à
une pratique bien plus ancrée dans les réalités humaines sous la plume d'auteurs
comme Robert de Vaugondy ou de Pâris de Meyzieu. L'importance des approches et
des styles personnels des différents auteurs est également discutée par
@laramee_production_2017 qui saisit le contraste édifiant entre la géographie de
Diderot et celle du chevalier de Jaucourt avant d'expliciter la vision du
continent américain qui ressort implicitement des pages de l'*EDdA*. Enfin, des
études contrastives entre l'*EDdA* et d'autres œuvres, de manière similaire à
celles mentionnées précédemment dans cette partie, existent aussi pour la
Géographie. @vigier_articles_2022 montrent par exemple la continuité dans les
profils discursifs des articles entre les dictionnaires de géographie
spécialisés et les encyclopédies, tout en soulignant les différences d'approches
entre le *Trévoux* et l'*EDdA*.

Après avoir présenté un format dans la section précédente et un objet d'étude
essentiel pour les [@=HN] dans la présente, il reste avant de pouvoir clore cet
état de l'art à s'intéresser à deux approches utilisées en [@=HN]. Pour bien les
comprendre et tenter de les nommer, il est utile de commencer par un bref
historique afin de montrer leurs origines communes.

