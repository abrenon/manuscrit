Les travaux présentés dans ce manuscrit reposent sur des considérations issues
d'un spectre de disciplines centré sur l'informatique (encodage, complexité
algorithmique, [@=AA]) mais allant des mathématiques (statistiques, théorie des
graphes) à la linguistique (syntaxe, coocurrences). Cette pratique
pluridisplinaire constitue une illustration représentative des [@=HN], un
courant interdisciplinaire né à la fin du XX^ème^ siècle, ou au moins constitué
sous ce nom à cette époque à partir de la pratique en développement depuis les
années 1960 consistant à appliquer des techniques de calcul automatique à des
thématiques des humanités [@beaudouin_retour_2016, p.17]. Cette rencontre entre
l'informatique d'une part et les sciences humaines et sociales d'autre part a
rendu possible l'émergence de projets ambitieux, capables d'aborder de nouveaux
objets d'études et de révéler ceux déjà connus sous un jour nouveau.

L'encodage des données pour permettre leur manipulation par des outils
informatisés, rappelé par l'adjectif «Numérique» dans l'expression «Humanités
Numériques» est un point commun à tous les projets du domaine. La première
section de ce chapitre s'intéresse aux efforts entrepris pour standardiser les
formats utilisés dans le but de rendre les fichiers produits facilement
exploitables, interopérables et réutilisables. La place privilégiée qu'occupent
les ouvrages lexicographiques dans les [@=HN] et leur lien étroit avec les
questionnements linguistiques sont ensuite traités à la section
\ref{sec:EdlA_lexicography}. Des travaux existants sur les dictionnaires et les
encyclopédies y sont présentés, notamment sur l'*EDdA*, œuvre majeure qui
intéresse de nombreuses branches des [@=HN]. Puis la section
\ref{sec:EdlA_TAL_history} dresse un bref historique du pan informatique de la
discipline et, enfin, les sections \ref{sec:EdlA_TAL} et
\ref{sec:EdlA_corpus_linguistics} présentent les appareils conceptuels, formats
et outils déployés respectivement dans les chapitres
\ref{sec:domains_classification} (classification automatique) et
\ref{sec:contrasts} (linguistique de corpus).

