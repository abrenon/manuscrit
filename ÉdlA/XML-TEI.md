## Structuration et encodage des données {#sec:EdlA_encoding}

Les formats de fichiers les plus manipulés au cours de cette thèse appartiennent
principalement à deux familles présentes très fréquemment en [@=HN]: ceux
structurés en tableaux et ceux possédant des structures arborescentes
représentées par un langage de balisage.

Les métadonnées des fichiers du corpus ont été représentées par des formats
tabulaires de la famille CSV, un format très simple en usage depuis le début des
années 1970 et formalisé bien plus tard [@rfc4180]. L'acronyme signifie
*Comma-Separated Values* («des valeurs séparées par des virgules») qui décrit la
manière d'organiser les cellules au sein d'une ligne (les lignes elles-mêmes
sont séparées par un caractère «nouvelle ligne» `'\n'`). De nombreuses variantes
de ce format utilisant différentes conventions sont en usage, si bien qu'on
parle également parfois de «SSV», *Something-Separated Values* («des valeurs
séparées par quelque chose») pour désigner plus généralement cette famille où le
caractère délimiteur peut varier. La variante utilisée en pratique dans les
présents travaux est [@=TSV] qui se sert du caractère tabulation (`'\t'`) pour
séparer les cellules.

L'autre format principal dans cette thèse est le format XML-[@=TEI], un langage
à balises (*markup language*, c'est l'origine de la séquence «ML» dans les noms
de langage mentionnés dans cette partie). Ce format possède une histoire bien
plus riche ponctuée d'étapes importantes de normalisation  et de standardisation
qu'il faut expliciter pour donner un sens aux travaux exposés dans la section
\ref{sec:corpus_lge_encoding}. Il joue un rôle majeur actuellement en [@=HN] et
c'est à lui que s'intéresse cette section.

### Les formats XML {#sec:xml_formats}

Les langages de balises constituent des outils précieux pour représenter des
informations structurées et détaillées. Dans cette famille, la lignée du langage
SGML — dont les origines remontent via COCOA et GML jusqu'aux années 1960 — se
distingue pour sa grande fertilité en ayant donné naissance à un très grand
nombre de formats dont une partie est visible à la figure
\ref{fig:markup_languages}. Ces formats trouvent des applications dans des
domaines très variés comme le dessin vectoriel pour SVG, la messagerie
instantanée pour XMPP et bien sûr les pages web pour le format HTML.

![Quelques membres éminents de la famille SGML](figure/markupLanguages.png){#fig:markup_languages width=40%}

SGML [@sgml_iso_8879] est en fait un métalangage, c'est-à-dire qu'il décrit une
syntaxe pour définir des structures de fichier plutôt que de donner des règles
sur un format précis. Il se caractérise par l'emploi de chevrons (ouvrant `'<'`
et fermant `'>'`) et d'une barre oblique (`'/'`) pour donner un sens spécial à
une séquence de caractères, en vert sur la figure \ref{fig:sgml_example}. C'est
ainsi qu'il est possible de créer des *balises*, ouvrantes (figure
\ref{fig:sgml_example_open}) et fermantes (\ref{fig:sgml_example_close}), dont
le nom est défini par la séquence entre chevrons (et éventuellement la `'/'`
pour une balise fermante). Une balise ouvrante et une balise fermante de même
nom s'associent pour former ensemble l'équivalent paramétrisable d'une paire de
parenthèses servant à délimiter une portion du document: un *élément* comme
celui visible à la figure \ref{fig:sgml_example_pair}. Il est en outre possible
d'ajouter des propriétés aux balises à l'aide d'attributs qui associent des
*valeurs* à des *clefs*. Sur la même figure \ref{fig:sgml_example_pair}, la clef
`id` est ainsi associée à la valeur `doc0` (le symbole `=` est syntaxique et ne
fait pas partie de la clef ni de la valeur, de même que les guillemets à
l'intérieur desquels doit être placée la valeur).

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.25\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figure/openMarkup.png}
    \caption{Une balise ouvrante (dont le nom est «text»)}
    \label{fig:sgml_example_open}
  \end{subfigure}
  \hspace{.05\textwidth}
  \begin{subfigure}[b]{0.25\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figure/closeMarkup.png}
    \caption{Une balise fermante (dont le nom est «author»)}
    \label{fig:sgml_example_close}
  \end{subfigure}
  \hspace{.05\textwidth}
  \begin{subfigure}[b]{0.25\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figure/markupPair.png}
    \caption{Une paire de balises «p» délimitant le texte «Contenu» et dont l'attribut «id» vaut «doc0»}
    \label{fig:sgml_example_pair}
  \end{subfigure}
  \caption{Des balises SGML}
  \label{fig:sgml_example}
\end{figure}

Quand un élément ne contient ni texte ni aucun autre élément — un cas qui peut
se rencontrer assez régulièrement, un élément vide reste utile par rapport à son
élément parent ou pour ses attributs — il est également possible d'utiliser une
syntaxe dite «autofermante» qui permet de le représenter en une seule balise.
Ainsi un élément `<p></p>` peut se noter `<p/>`. C'est cette notation qui sera
utilisée dans toute la thèse pour désigner les éléments afin de distinguer la
balise et son nom. Ainsi `<p/>` pourra être utilisé pour parler de l'entité
abstraite que constitue un élément (la classe de tous les éléments XML qu'il est
possible de construire avec des balises dont le nom est «p») aussi bien que pour
une de ses instances particulières («l'élément racine de la figure
\ref{fig:sgml_example_pair} est un `<p/>`»).

Le langage XML [@xml_spec_2008] forme un sous-ensemble de SGML qui ajoute des
contraintes pour rendre le langage plus simple à analyser automatiquement mais
il reste lui aussi au niveau de la structure. Pour définir un langage concret
utilisable dans un cadre précis pour représenter un certain type de données, il
faut décrire un ensemble de balises, d'attributs et de règles indiquant quelles
balises peuvent en inclure combien de quels autres types, et quels attributs
elles peuvent recevoir (affectés de quelles valeurs). Ces informations
constituent un *schéma* de données, qui est spécifié historiquement sous la
forme d'une *Document Type Definition* [@xml_spec_2008, §2.8], format de
référence des schémas défini en même temps que XML lui-même. D'autres formats
plus récents et plus expressifs ont vu le jour pour décrire des schémas: XSD
[@xsd_spec_2012] publié également par le consortium *W3C* à l'origine de XML,
Schematron [@jelliffe_information_nodate] ou encore RELAX NG
[@relax_ng_specification].

Deux formats XML définis par des schémas particuliers, ALTO et [@=TEI], sont
principalement utilisés dans les présents travaux. Le format ALTO (*Analyzed
Layout and Text Objects*, c'est-à-dire «Mise en page analysée et objets
textuels») est publié par la Librairies du Congrès des États-Unis d'Amérique
[@alto_specification_2023]. Il sert à représenter des pages numérisées après
l'application de systèmes de reconnaissance optique de la disposition (*Optical
Layout Recognition* ou [@=OLR]) puis de reconnaissance optique de caractères
(*Optical Character Recognition* ou [@=OCR]). Il contient essentiellement des
éléments de structure à différents niveaux (page, bloc de texte, ligne, mot)
comme le montre l'extrait de la figure \ref{fig:alto_sample}. L'imbrication de
ces éléments modélise la structure du document représenté, et leurs attributs
donnent des informations sur leurs coordonnées pour les positionner sur la page
(`HPOS` et `VPOS`) et sur les séquences de caractères lues (`CONTENT` pour le
contenu lui-même et `WC` — *word confidence* ou «confiance dans le mot» — qui
donne un indice de qualité d'après l'[@=OCR]). Le standard, qui a atteint sa
version 4.4 est spécifié par un schéma XSD[^alto_xsd]. Il est utilisé par
exemple pour encoder des journaux [@gutehrle_processing_2022] et sert de format
de référence pour des outils développés dans le but d'améliorer la qualité
d'[@=OCR] [@maurer_nautilus_2023].

[^alto_xsd]:
    [https://www.loc.gov/standards/alto/v4/alto.xsd](https://www.loc.gov/standards/alto/v4/alto.xsd)

![Un court extrait d'un document XML-ALTO](figure/ALTO.png){#fig:alto_sample width=100%}

Dans le cadre de cette thèse, XML-ALTO représente le format d'entrée dans lequel
une des deux œuvres du corpus, *LGE*, est représentée. C'est à partir de ce
format qu'il a fallu travailler pour gagner l'accès au texte de l'œuvre,
opération décrite à la section \ref{sec:corpus_preprocessing_lge}, en vue de
pouvoir en produire un encodage dans le format de référence pour le corpus
d'étude: XML-[@=TEI].

### Le format XML-TEI {#sec:xml_tei}

De la même façon qu'ALTO, XML-[@=TEI] est donc un dialecte du XML obtenu en
définissant un schéma. Celui du format [@=TEI] est publié et maintenu par la
*Text Encoding Initiative*[^tei-c], un collectif de volontaires issus du monde
de la recherche. Cette organisation a été créée en 1988
[@ide_encodingstandards_1995, §5; @huitfeldt_multidimensional_1994, p.237] suite
aux principes adoptés en novembre 1987 à la conférence de Vassar
[@ide_text_1995, p.2]. La [@=TEI] publie ses premières recommandations en
novembre 1990 [@ide_text_1995, p.11], la [@=TEI] P1 (*Proposal 1*, «proposition
n°1»). Les années suivantes voient se poursuivre l'essentiel des travaux de
normalisation et deux autres versions (P2 et P3) sont publiées, respectivement
en avril 1992 et à l'automne 1993 [@ide_text_1995, p.12]. La [@=TEI] est alors
stabilisée pour l'essentiel et le rythme de sortie des recommandations diminue
fortement.

[^tei-c]: [https://tei-c.org/](https://tei-c.org/)
[^4.8.0]:
    [https://tei-c.org/Vault/P5/current/doc/tei-p5-doc/readme-4.8.0.html](https://tei-c.org/Vault/P5/current/doc/tei-p5-doc/readme-4.8.0.html)

Fondée dès l'origine sur le standard SGML, la [@=TEI] est reformulée en termes
de XML peu après sa normalisation (les premières versions de XML datent de
1996), conduisant à la [@=TEI] P4 publiée en juin 2002. L'étape suivante, la
[@=TEI] P5, sortie le 2 novembre 2007 est toujours la version officielle du
consortium [@=TEI] et continue d'être mise à jour (la version 4.8.0[^4.8.0] a
été publiée le 8 juillet 2024).

Un des objectifs à l'origine du projet est de proposer un format unifié pour
favoriser la réutilisabilité des données de recherches et éviter l'apparition
d'encodages incompatibles au fil des projets, une démarche en accord avec ce qui
sera par la suite formalisé sous le nom des principes FAIR[^FAIR] pour des
données découvrables, accessibles, interopérables et réutilisables (*Findable*,
*Accessible*, *Interoperable* and *Reusable*). Dès ses débuts, le consortium
vise un format unique pour représenter tous types de documents — nativement
numériques, imprimés, manuscrits ou même épigraphes — dans des genres allant du
théâtre aux écrits religieux en passant par les dictionnaires et la
correspondance, en mêlant potentiellement plusieurs langues à la fois. Les
documents peuvent être représentés avec une grande finesse, en marquant par
exemple des passages rayés ou reformulés ou en faisant coexister dans un même
document des versions alternatives d'un texte à des fins de comparaisons. Il est
également possible d'enrichir un document d'annotations compilées dans le cadre
des recherches comme des parties de discours, des liens entre les entités
mentionnées dans le texte et une base de connaissance voire des informations
géo-sémantiques [@moncla_perdido_2023]. Ces couches additionnelles donnent une
profondeur supplémentaire aux textes et permettent leur analyse suivant
différents axes, allant au-delà de l'unidimensionalité imposée par le format
numérique [@huitfeldt_multidimensional_1994, p.236].

[^FAIR]: [https://www.go-fair.org/fair-principles/](https://www.go-fair.org/fair-principles/)

Pour rendre cela possible sans trop accroître la complexité du schéma, la
[@=TEI] opte pour une approche modulaire combinant des composantes très
génériques communes à tout type de documents tels que les modules *core* et
*tei* et d'autres au contraire très spécialisées et adaptées aux besoins
spécifiques de certaines disciplines. La [@=TEI] P5 comporte ainsi 587 éléments
répartis en 21 modules et définit 84 attributs pour ces éléments[^guidelines].
Cette modularité est implémentée en morcelant le schéma en plusieurs fichiers
indépendants qui peuvent être inclus ou non sans remettre en cause la cohérence
de l'ensemble. Le consortium [@=TEI] a mis en ligne l'interface ROMA[^ROMA] pour
permettre de générer des schémas personnalisés adaptés aux besoins de chaque
projet et il existe même une telle personnalisation du schéma [@=TEI] dont le
but est d'aider à en écrire d'autres directement dans un éditeur XML sans avoir
besoin de ROMA [@bauman_customization_2019]. Des schémas alternatifs au schéma
standard de la [@=TEI] sont repartagés au sein de la communauté et rencontrent
une certaine popularité comme le *Comic Book Markup Language* (CBML, le «langage
à balises pour les bandes dessinées») de @walsh_comic_2012 ou la TEI Lex-0
[@romary_lex0_2018] pour les ouvrages lexicographiques.

Un module est présent depuis la [@=TEI] P3 [@sperbergmcqueen_tei_1999;
@ide_encodingdictionaries_1995]: le module *dictionaries*. Il comprend 33
éléments et se focalise sur les entrées, niveau à partir duquel les ouvrages
lexicographiques diffèrent des autres types d'œuvres
[@ide_encodingdictionaries_1995, p.168]. Il permet également de représenter les
sous-entrées, objets utilisés par les lexicographes pour rendre compte de la
polysémie des mots. Ce module a servi de point de départ à la détermination d'un
encodage pour représenter *LGE* (travail présenté à la section
\ref{sec:corpus_lge_encoding}).

[^guidelines]:
    [https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html)
[^ROMA]: [https://roma.tei-c.org/](https://roma.tei-c.org/)

Les besoins de la [@=TEI] en outillage spécifique restent modestes: s'appuyant
sur des technologies standards (SGML puis XML) pour lesquelles existent de
nombreux outils, la communauté a accès à un riche panel d'outils pour travailler
sur ses fichiers. Il existe ainsi déjà des éditeurs de texte spécialisés pour
les langages de balisage, des moteurs de validation vérifiant la conformité d'un
document à un schéma donné ainsi que des feuilles de transformation XSLT
[@kay_xsl_2017] permettant d'appliquer des règles de réécriture automatique pour
changer la structure d'un document et éventuellement le convertir vers ou à
partir d'autres formats XML ou HTML. D'autres outils de conversion comme
pandoc[^pandoc] peuvent prendre en entrée d'autres formats que des dialectes
SGML et permettent de produire de la [@=TEI] automatiquement à partir de
fichiers dans de nombreux formats tels que Markdown, HTML, LaTeX et même ODT
(format de traitement de texte).

[^pandoc]: [https://pandoc.org/](https://pandoc.org/)

### Une communauté active {#sec:EdlA_tei_applications}

La communauté qui s'organise autour du format [@=TEI] possède sa propre
revue, le *Journal of the Text Encoding Initiative* (JTEI)[^JTEI] depuis le
début des années 2010 [@schreibman_editorial_2011]. Le volume de données
encodées en XML-[@=TEI] a connu une croissance rapide au cours des années 1990,
pour dépasser les centaines de millions de mots [@ide_text_1995, p.5].

[^JTEI]: [http://jtei.revues.org/](http://jtei.revues.org/)

Le format s'est imposé comme un standard incontournable dans de nombreux
logiciels en [@=HN]. C'est par exemple le format utilisé en interne par le
logiciel d'analyse textométrique TXM [@heiden_txm_2010] pour représenter les
textes d'un corpus. Le format d'entrée pour importer les corpus dans le
Lexicoscope [@kraif_lexicoscope_2016] est également basé sur le format
XML-[@=TEI] sans en respecter exactement le schéma: les fichiers ne possèdent
pas l'en-tête minimal requis et utilisent certains éléments non conformes comme
`<doc/>` ou `<meta/>`. De plus, au lieu de représenter directement un texte, les
éléments `<meta/>` et `<s/>` contiennent des «îlots» d'autres formats de
données, respectivement [@=TSV] et CoNLL-U (voir la section
\ref{sec:EdlA_tal_pos} à propos de ce format). Enfin, des interfaces de
visualisation existent pour exposer des corpus encodés en [@=TEI] sur le web. Le
projet TAPAS[^TAPAS] permet ainsi de parcourir des fichiers dans ce format en
alternant entre différents modes d'affichage, de l'édition diplomatique au code
XML des fichiers. Philologic[^Philologic][@allen_philologic4_2013] développé à
l'[@=ARTFL] permet non seulement d'afficher les fichiers d'un corpus au format
[@=TEI] mais offre en outre la possibilité d'effectuer des requêtes sur leurs
contenus et de visualiser les résultats dans un concordancier simple avec un
contexte autour des motifs trouvés.

[^TAPAS]: [https://tapasproject.org/](https://tapasproject.org/)
[^Philologic]:
    [https://artfl-project.uchicago.edu/philologic4](https://artfl-project.uchicago.edu/philologic4)

La version de l'*EDdA* de l'[@=ARTFL] est encodée en [@=TEI] et publiée à l'aide
de Philologic4. D'autres projets portant sur des ouvrages lexicographiques ont
utilisé le même format pour représenter leurs corpus. C'est le cas par exemple
de Nenufar[^nenufar] qui s'intéresse à plusieurs éditions du *Petit Larousse*
[@bohbot_projet_2019] qu'il permet de comparer dans une interface dédiée ou de
BASNUM[^basnum] qui s'intéresse ainsi au *Dictionnaire Universel* de Furetière,
ou plutôt à sa seconde édition, améliorée par Henri Basnage de Beauval
[@williams2017; @galleron_tenir_2022]. Le format est répandu pour des textes de
différentes époques et l'énumération qui suit se contente de citer quelques
exemples parmi la grande variété qui existe. Le format [@=TEI] a ainsi été
utilisé pour encoder des textes du moyen-âge comme les «Chroniques rimées du
Mont Saint-Michel»[^chroniques] [@de_saint_pair_chronique_2022] ou la Base du
Français Médiéval[^BFM], mais aussi des textes de la Renaissance avec le corpus
Epistemon[^Epistemon] qui utilise Philologic4 et la version web de TXM pour
proposer des interfaces publiques de parcours et de requête des textes. Dans
d'autres langues que le français, @salgado_morais_2024 ont publié une version en
TEI Lex-0 du *Diccionario da Lingua Portugueza* d'António de Morais Silva (fin
du XVIII^ème^ siècle); il existe également une édition de la correspondance de
Mary Hamilton[^MH]. L'*Oxford Text Archive*[^OTA] des *Bodleian Libraries*
rassemble des textes dans plus de 25 langues différentes sur plus de 2 000 ans.
Enfin, le projet Perseus[^Perseus] présente la spécificité d'intégrer les œuvres
qu'il étudie dans un système d'information géographie (GIS) à l'aide d'une
couche d'annotation géo-sémantique riche.

[^nenufar]:
    [http://nenufar.huma-num.fr/presentation/](http://nenufar.huma-num.fr/presentation/)
[^basnum]:
    [https://anr.fr/Projet-ANR-18-CE38-0003](https://anr.fr/Projet-ANR-18-CE38-0003)
[^chroniques]:
    [http://catalog.bfm-corpus.org/ChronSMichelBo](http://catalog.bfm-corpus.org/ChronSMichelBo)
[^BFM]: [https://txm-bfm.huma-num.fr/txm/](https://txm-bfm.huma-num.fr/txm/)
[^Epistemon]:
    [https://www.bvh.univ-tours.fr/Epistemon/index.asp](https://www.bvh.univ-tours.fr/Epistemon/index.asp)
[^MH]:
    [https://www.maryhamiltonpapers.alc.manchester.ac.uk/edition/overview/](https://www.maryhamiltonpapers.alc.manchester.ac.uk/edition/overview/)
[^OTA]:
    [https://ota.bodleian.ox.ac.uk/repository/xmlui/](https://ota.bodleian.ox.ac.uk/repository/xmlui/)
[^Perseus]:
    [https://www.dlib.org/dlib/july00/crane/07crane.html](https://www.dlib.org/dlib/july00/crane/07crane.html)

\label{sec:EdlA_tei_limits}Malgré ces succès et une large adoption, certaines
réserves ont toutefois été émises à l'encontre de la [@=TEI] en général et du
module *dictionaries* en particulier. Ainsi, @ide_background_1998[p.6] déplore
que le haut degré de généricité de la [@=TEI] entraîne une forte abstraction qui
complique l'encodage. De plus, la très grande flexibilité de la [@=TEI] offre
souvent une multiplicité de solutions pour représenter un phénomène donné dans
une œuvre. Si cette souplesse peut être vue comme un avantage certain, elle
accroît les risques que des projets différents utilisent des conventions
incompatibles. Le reproche sur la complexité est partagé par
@tutin_electronic_1998[p.364] dans leurs travaux sur l'encodage du *Petit
Larousse Illustré*.

Enfin malgré l'expressivité de la boîte à outils que représente la [@=TEI] dans
son ensemble, il est à noter que la version de l'*EDdA* encodée par l'[@=ARTFL]
n'utilise pas le module *dictionaries*. Ce point ne constitue pas en soi une
critique mais conduit à s'interroger sur l'expressivité du module *dictionaries*
et sa pertinence pour les œuvres encyclopédiques. Quelques angles morts
demeurent dans ses règles, empêchant parfois l'utilisation d'éléments simples
dans certains contextes, un point que n'hésitent pas à reconnaître
@ide_encodingdictionaries_1995[p.172-173], encourageant même à ne pas respecter
tout à fait le schéma du module quand cela est nécessaire. Il est ainsi
impossible d'utiliser l'élément `<p/>` représentant les paragraphes dans une
définition (`<def/>`) comme le rapportent @hagen_twentytwo_2020[p.6]. Parmi les
autres projets cités précédemment, Nenufar est moins concerné par ces limites
d'expressivité car le *Petit Larousse* qu'il traite et encode est un
dictionnaire et ne possède pas de développements structurés sur plusieurs
niveaux comme ceux des encyclopédies. Le projet prend le parti d'utiliser
l'élément `<note/>` pour représenter les quelques paragraphes présents dans
certaines entrées (voir par exemple l'entrée AIR[^air]) en dépit de la
sémantique qui lui est associée par la documentation. Le projet BASNUM le
rejoint sur ce choix qui, avec les problèmes d'expressivité du module
*dictionaries* en général, seront développés à la section
\ref{sec:corpus_lge_encoding}.

[^air]: [http://nenufar.huma-num.fr/?article=1211](http://nenufar.huma-num.fr/?article=1211)

