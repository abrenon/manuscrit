## Traitement Automatique de la Langue {#sec:EdlA_TAL}

La section qui s'ouvre présente les différents outils employés pour une part
lors de la préparation du corpus (voir chapitre \ref{sec:corpus}) mais plus
encore dans les travaux de classification (présentés au chapitre
\ref{sec:domains_classification}). Comme le laissent présager les remarques
historiques de la section précédente, l'[@=AA] y occupe une part importante.

Les méthodes basées sur des réseaux de neurones travaillent sur des entrées sous
forme numérale. Pour pouvoir travailler sur des textes, ils doivent
nécessairement à un moment de leur traitement être représentés par des nombres
ou plutôt des tableaux de nombres: des vecteurs.

### Vectorisation {#sec:EdlA_vectorization}

Pour vectoriser un document, une première approche consiste à le considérer
comme un «sac de mots» [@salton1986introduction]. L'ensemble des mots dans un
corpus (tokens ou lemmes selon les implémentations) constitue le vocabulaire
utilisé comme base d'un espace vectoriel de très grande dimension dans lequel
chaque document est représenté. Pour chaque vecteur de cette base (correspondant
donc à un mot unique du corpus), la composante associée dans le vecteur qui
représente un document est un entier positif ou nul égal au nombre d'occurrences
du mot (sa fréquence) dans ce document. Le terme anglophone consacré est «*Bag
of Words*», et bien que ce terme soit le seul pour lequel l'équivalent français
s'emploie, l'acronyme *BoW* sera également utilisé dans ce qui suit par soucis
d'homogénéité avec les autres méthodes. Cette approche produit des
représentations vectorielles très grandes (beaucoup de nombres) et avec peu de
valeurs non nulles (la plupart valent 0) car chaque document n'utilise qu'une
fraction de l'ensemble du vocabulaire défini par le corpus entier. On parle de
représentation *creuse* (*sparse* en anglais).

La deuxième approche, TF-IDF (*Term Frequency - Inverse Document Frequency*,
«fréquence du terme - fréquence inverse de document») est un type de
représentation en *BoW* qui au lieu de simplement pondérer les mots par leur
fréquence dans le document considéré tempère ce nombre en le divisant par la
proportion de documents du corpus qui contiennent ce mot. De la même manière que
les *BoW* «purs», les vecteurs produits par cette méthode sont creux, ce qui
tend à dégrader les performances des algorithmes d'[@=AA] en augmentant leurs
complexités spatiales et temporelles.

\label{edla_word_embeddings} Par contraste avec les deux approches précédentes,
les plongements de mots produisent des vecteurs de plus petites dimensions et
denses en coefficients non nuls. Ils constituent une famille de méthodes
fondamentalement différentes des deux précédentes du fait qu'elles capturent le
contexte des mots dans leurs représentations vectorielles. Il y a deux approches
pour entraîner des plongements de mots: CBOW (*Continuous Bag of Words*, «sacs
de mots continus») et *skip-gram* («fenêtres à trou»). La première consiste à
prédire un mot en fonction de son contexte. À l'inverse, une architecture
*skip-gram* apprend à prédire le contexte à partir d'un mot. Initialement pensée
au niveau des mots individuels, *Word2Vec* [@mikolov2013efficient] permet déjà
de représenter un texte en combinant les vecteurs des mots qu'il contient (par
exemple en faisant leur moyenne). Peu adapté aux textes longs, on lui préfère
souvent la méthode voisine *Doc2Vec* [@le2014distributed] sur ce type d'entrées.
Ces méthodes de plongements nécessitent un entraînement sur le corpus sur lequel
elles vont être utilisées car elles ne peuvent pas gérer de mots spécifiques à
ce corpus et qu'elles n'auraient jamais rencontrés. D'autres telles que
*FastText* [@bojanowski2017enriching] sont disponibles préentraînées car elles
considèrent des fenêtres de quelques caractères plutôt que des mots entiers, ce
qui leur permet de calculer un vecteur même pour des termes absents de leur
vocabulaire d'entraînement. On parle pour ces méthodes de plongement «statique»
parce qu'elles produisent un vecteur unique pour représenter un mot, en
combinant tous les différents contextes dans lesquels il peut apparaître. Plus
récemment, *BERT* [@devlin2018bert] — *Bidirectional Encoder Representations
from Transformers* («Représentations d'Encodeur Bi-directionnels à partir de
Transformeurs», voir p.\pageref{deep_learning_classifiers}) — utilise un
plongement contextuel, où la représentation de chaque mot dépend du contexte
dans lequel il apparaît dans une phrase, pour la phase de vectorisation qu'il
intègre. *BERT* utilise des réseaux de neurones de type *transformer* et le
concept de masque pour prédire les mots qui complètent une amorce de phrase
donnée.

### Classification {#sec:EdlA_classification}

La classification de documents est un problème général en [@=IA] qui consiste à
regrouper ensemble des objets jugés plus proches ou ressemblants pour une
certaine sémantique ou une certaine métrique par opposition à ceux qui
aboutissent dans les autres groupes. Il existe dès le début des années 1970 une
littérature fournie portant sur les stratégies de classification et les mesures
de similarités utilisées que @cormack_review_1971 entreprend d'organiser.

Les problèmes de classification couvrent un grand nombre d'applications
pratiques variées. On trouve des études portant sur la classification de sons
[@nogueira_transformers_2022], d'images [@adriyanto_classification_2022] — même
hors du spectre visible [@yu_simplified_2020] — ou bien d'événements sur un
système informatique en vue de détecter des attaques [@gavari_detection_2022].

#### Classer des textes {#sec:EdlA_TAL_text_classification}

En [@=TAL], classer un texte peut signifier lui attribuer par exemple un sujet
(«infrastructure» ou «politique étrangère»), un type de contenu («dépêche» ou
«publicité») voire une sensibilité politique («gauche» ou «droite») dans le cas
d'une étude de @peterson_classification_2018.

\label{artfl_classification}Appliquées aux encyclopédies, les méthodes de
classification permettent de prédire les domaines de connaissance des articles
(voir à ce sujet la section \ref{sec:structuring_knowledge}
p.\pageref{sec:structuring_knowledge}). Dans le cadre des travaux de l'[@=ARTFL]
sur l'*EDdA*, @horton2009mining ont ainsi testé la classification Bayesienne
dite «naïve» ([@=Naive Bayes]) pour prolonger la classification des auteurs de
l'œuvre sur les entrées laissées sans domaine par les auteurs. L'Encyclopedia
Britannica a également été étudiée par le
Nineteenth-Century Knowledge Project[^19cProject] qui a utilisé des méthodes
d'[@=AA] mais aussi des approches basées sur des règles pour indexer 400 000
articles à travers 4 éditions de l'œuvre [@grabus_representing_2019].

[^19cProject]: [https://tu-plogan.github.io/](https://tu-plogan.github.io/)

#### Classification supervisée

Dans tous les cas évoqués ci-dessus, l'ensemble de classes à attribuer aux
documents est défini en amont de l'étude. Il n'est pas pour autant indépendant
du corpus mais correspond au contraire à un axe d'analyse que l'on suppose
pertinent: un partitionnement suivant une sensibilité politique peut avoir un
intérêt pour classer des discours parlementaires ou des professions de foi
électorales, mais serait à priori beaucoup moins adapté à des prévisions
météorologiques ou des recettes de cuisine.

À partir de l'ensemble de classes, un échantillon des documents doit être annoté
manuellement pour servir de référence pour l'entraînement du modèle de
classification: il s'agit de classification supervisée. Plus précisément, on
distingue un jeu d'entraînement qui aide directement à ajuster les coefficients
du modèle et un jeu de test qui permet d'en évaluer les performances une fois
l'entraînement réalisé et avant d'appliquer le modèle à l'ensemble des documents
à classer.

\label{deep_learning_classifiers}Les méthodes les plus simples utilisées pour la
classification apparaissent à la suite du renouveau de l'[@=AA] dans les années
1990: *[@=Naive Bayes]* [@domingos1997optimality], les *Support Vector Machines*
(*SVM*) de @christianini2000support puis les *Random Forests*
[@breiman2001random] et la *Logistic Regression* [@kleinbaum2002logistic].
D'autres telles que les [@=RNN] et les [@=CNN] mentionnés précédemment (voir
p.\pageref{sec:statistical_methods_return}) reposent sur de nombreuses couches
de neurones qui abstraient progressivement le signal d'entrée et amènent à
parler d'Apprentissage Profond ([@=AP]). Les *CNN*s sont basés sur deux
opérations: la convolution et un sous-échantillonnage des entrées gardant les
valeurs maximales (*max-pooling*). La convolution extrait des éléments
caractéristiques des données et le max-pooling compresse le résultat, permettant
à ces architectures d'analyser des images avec succès. Les [@=RNN], davantage
adaptés à l'analyse de données séquentielles, trouvent un prolongement dans les
*LSTM* (*Long Short-Term Memory*, «mémoire à court-terme longue») qui permettent
de mieux capturer des dépendances éloignées dans les séquences d'entrées
[@hochreiter_lstm_1997], ce que les [@=RNN] ne parviennent pas à faire en
pratique. Une autre technique d'[@=AP] pour garder accès aux premiers éléments
d'une séquence d'entrée, nommée l'«attention», est au centre de l'architecture
des transformeurs, un autre modèle d'[@=AP] qui définissait l'état de l'art au
début de cette thèse. Un modèle tel que *BERT*, basé sur des transformeurs,
permet à la fois la vectorisation et la classification de ses entrées. Son très
grand nombre de paramètres requiert des ressources de calcul considérables pour
entraîner un tel modèle à partir de rien. Puisque de tels moyens ne sont à
l'heure actuelle disponibles que sur des infrastructures dédiées au calcul dont
l'accès est coûteux et restreint, le modèle est distribué préentraîné sur des
tâches générales et ne doit subir qu'un réentraînement superficiel sur quelques
époques pour l'adapter à une tâche en particulier — on parle de *fine-tuning* en
anglais et s'il est tentant de proposer la traduction «spécialisation» par
analogie avec la pédagogie et l'apprentissage humain, là encore le terme anglais
semble s'être imposé en français.

#### Topic-modeling

Au contraire des méthodes de classification précédentes, d'autres approches
tâchent de révéler la structure propre à l'ensemble de documents qu'il s'agit de
classer sans à priori en les groupant par «clusters». Certaines sont basées
exclusivement sur des mesures de distance dans un espace métrique où sont
représentés les objets [@lance_general_1967], d'autres comme l'Allocation
Latente de Dirichlet ([@=LDA]) créent des clusters de mots ce qui permet de
mettre automatiquement en évidence les différents thèmes présents dans les
documents du corpus. Ces méthodes permettent d'approcher les données avec moins
de préconçus mais demandent davantage de travail dans l'interprétation des
résultats.

La modélisation de thèmes par [@=LDA] connaît une certaine popularité pour
déterminer les contenus saillants dans de vaste ensemble de données textuelles.
Ce succès s'explique sans doute au moins en partie par le fait qu'elle ne
demande pas de posséder au préalable des métadonnées ni de créer des annotations
pour l'entraînement car c'est une méthode non supervisée. Dans son analyse de
discours prononcés au parlement britannique, @guldi_parliaments_2019 emploie le
[@=Topic Modeling] pour mener une «recherche critique» des «tensions et des
moments charnière» dans les débats politiques du Royaume-Uni.
@barron_individuals_2018 utilisent le [@=Topic Modeling] comme un point d'entrée
à partir duquel mesurer la «nouveauté» et le caractère plus ou moins «éphémère»
des discours prononcés lors des premières années de la Révolution Française.
L'intérêt de cette approche est qu'elle ne requiert pas de métadonnées sur
chaque discours outre celles disponibles à la constitution du corpus d'étude et
utilisées pour le partitionner (dans le cas précédent, le nom du député à qui il
est attribué et la date à laquelle il a été prononcé).

Après les travaux de classification sur [@=Naive Bayes] évoqués plus haut, une
autre étude menée à l'[@=ARTFL] a consisté à utiliser la [@=LDA] pour constituer
automatiquement des groupes d'articles qui ont ensuite été analysés en les
comparant aux classes originales des encyclopédistes [@roe_discourses_2016]. À
l'issue de ces recherches, ils concluent que les thèmes identifiés par la
[@=LDA] peuvent être compris en termes de discours sous-tendant l'*EDdA* de part
en part et qui ne sont pas en correspondance simple avec les classes originales.

En utilisant la [@=LDA] ainsi que d'autres modèles d'[@=AA], Underwood a examiné
l'histoire et l'instabilité de genres littéraires [@underwood2018historical;
@underwood_life_2016; @underwood2020machine]. Il trouve les méthodes
computationnelles utiles en ce qu'elles peuvent «détecter et comparer des
ressemblances de familles floues qu'il serait difficile de définir avec des
mots sans être simpliste» [@underwood_life_2016, p.6]. De telles approches de la
classification de texte, quantitatives et prédictives offrent aux recherches en
[@=HN] la possibilité de comprendre les résultats dans un cadre interprétatif
nouveau.

#### Reconnaissance Optique de Caractères

Enfin, avant de clore complètement cet aperçu des travaux en Classification
Automatique, il est intéressant de mentionner un champ de recherche qui utilise
des techniques de classification sans s'y réduire. Si la localisation des
caractères sur la page et la compréhension des liens qu'ils entretiennent est un
préalable essentiel aux tâches de reconnaissance de caractères
[@sayre_machine_1973, p.213], la classification des zones identifiées en
caractères semble bien plus complexe. Les moyens classiques semblent inefficaces
pour produire des résultats généralisables sur des données jamais rencontrées
par l'algorithme et dans la même étude @sayre_machine_1973[p.216 et seq] explore
donc des méthodes statistiques pour la classification.

En implémentant les [@=CNN], @lecun_backpropagation_1989 parviennent à créer des
modèles capables de lire les codes postaux écrits à la main sur des enveloppes.
De nombreux systèmes récents sont basés sur les *LSTM* [@wick_comparison_2018,
p.79], architecture utile en classification ce qui souligne encore la parenté
entre les deux tâches. On distingue généralement cette tâche encore plus
difficile de reconnaissance de caractères tracés à la main — donc moins
réguliers — sous le nom de *Handwritten Text Recognition* (HTR) par rapport au
problème plus général d'[@=OCR]. Pour favoriser l'évaluation de système de HTR,
@chague_htr_2021 propose la diffusion de jeux de données pouvant servir de
vérité terrain. Aujourd'hui, tous les systèmes d'[@=OCR] sont basés sur des
méthodes d'[@=AA].

Les travaux de classification effectués dans cette thèse n'ont pas porté sur
cette tâche mais elle représente un prérequis absolu à leur existence, puisqu'on
lui doit l'existence d'[@=OCR] capables de fournir automatiquement des versions
numériques des textes d'encyclopédies. L'existence de systèmes d'[@=OCR] rend
donc possible cette étude ainsi que d'autres semblables qui travaillent sur de
grandes masses de données imprimées comme le projet NewsEye[^newseye] qui
s'intéresse à la presse imprimée des siècles passés [@doucet_newseye_2020].

[^newseye]: [https://www.newseye.eu/](https://www.newseye.eu/)

### Étiquetage morphosyntaxique et syntaxique {#sec:EdlA_tal_pos}

Une autre tâche très commune en [@=TAL] concerne l'annotation automatique des
textes en morphosyntaxe et en syntaxe: bien qu'avec une approche souvent
différente de celle de la linguistique classique, le [@=TAL] s'intéresse aussi
aux mots. D'abord il s'agit de les délimiter lors d'une phase de *segmentation*
en unités lexicales (on trouve, particulièrement en [@=TAL], l'emploi du mot
«*token*» pour rendre compte du caractère artificiel et conventionnel de ces
unités). Ensuite, il faut identifier à quelle classe grammaticale ils
appartiennent lors de l'*annotation morphosyntaxique*. On parle aussi pour
désigner les classes grammaticales de «partie de discours» (en anglais *part of
speech* — [@=POS]). Enfin l'*annotation syntaxique* établit leurs relations au
sein des phrases. En rendant faisables en pratique de telles annotations sur de
larges volumes de texte, les méthodes automatiques constituent des outils
précieux pour les linguistiques de corpus (voir section
\ref{sec:EdlA_corpus_linguistics}). De même que pour l'[@=OCR], les présents
travaux ne contiennent pas de contribution sur ces tâches mais elles sont
indispensables pour comprendre les phases de préparation et d'analyse du corpus.

La question de la seule segmentation du texte constitue déjà un problème
«délicat» [@diwersy_ressources_2017, p.29] qui ne relève en rien de l'évidence.
Pour commencer, il n'existe pas de caractère qui délimite les mots sans aucune
ambiguïté: l'espace qui apparaît comme un délimiteur évident fait pourtant
partie de mots composés comme «compte rendu» [@lebart_analyse_2019, p.44], les
caractères de ponctuation comme l'apostrophe peuvent parfois séparer deux mots
(«l'ami») et parfois faire partie d'un seul mot («aujourd'hui») de même que le
point marquant une fin de phrase ou bien un mot abrévié
[@habert_instruments_2005, p.13].

La tâche d'annotation morphosyntaxique qui vient ensuite peut s'apparenter au
problème de classification décrit dans la sous-section précédente en ce qu'elle
associe à chaque mot en entrée une classe possible (sa [@=POS]) parmi celles
prédéfinies. Il existe plusieurs jeux d'étiquettes morphosyntaxiques de
référence comme celui du Penn Treebank [@marcus_building_1993] mais le jeu à
utiliser doit être déterminé en fonction des besoins de l'étude que l'on
souhaite mener. Ainsi @vigier_autour_2017[p.100] recourt à une catégorie «G»
qui regroupe des participes présents, adjectifs verbaux et gérondifs pour tenir
compte du fait que ces objets ne sont pas encore bien différenciés sur les états
de langues les plus anciens de son corpus d'étude.

Toutefois, à la différence des problèmes de classification habituels,
l'annotation en [@=POS] est fortement compliquée par une dépendance contextuelle
aiguë. Des mots différents, auxquels il faut donc attribuer des [@=POS]
différentes, peuvent revêtir la même forme et nécessiter le contexte des autres
mots de la phrase pour être distingués. Ce peut être le cas par exemple pour des
raisons d'homonymie : «été» peut être le nom d'une saison ou bien une forme du
verbe «être». Puisqu'il s'agit de «reconnaître» les mots, cette phase d'analyse
est généralement l'occasion d'associer aussi à chaque mot une forme normalisée,
son «lemme». L'existence d'amalgames tels que «des» dans «le temps des cerises»
ou «aux» dans «une tarte aux prunes» (qui rassemblent en un seul mot en réalité
deux mots — «de» et «les» pour le premier, «à» et «les» pour le second) vient
complexifier les choses en montrant que la segmentation ne peut coïncider tout à
fait avec le fonctionnement de la grammaire. Une stratégie pour annoter les
amalgames peut consister à leur associer une combinaison des lemmes et des
[@=POS] des mots qui les constituent comme le fait @vigier_autour_2017[p.101].
Il existe de nombreux algorithmes pour calculer la [@=POS] des mots dont les
implémentations ont donné lieu à des outils comme le Brill Tagger
[@brill_simple_1992] ou TreeTagger [@schmid_improvements_1995]. Ces outils
produisent généralement des formats tabulaires comme [@=TSV] (voir section
\ref{sec:EdlA_encoding} p.\pageref{sec:EdlA_encoding}) qui représentent un token
par ligne, stockant dans différentes colonnes la forme précise rencontrée dans
le texte, le lemme et la [@=POS] correspondante.

La dernière phase, celle d'analyse en syntaxe, révèle la structure des phrases
sous une forme arborescente. On distingue généralement deux approches: l'analyse
en constituants qui descend des théories de @chomsky_syntaxe_1965 et celle en
dépendance qui est davantage fondée sur la vision de @tesniere_elements_1959. La
première s'inscrit dans la lignée des grammaires non contextuelles qui procèdent
par réécritures successives de règles. Dans un tel système, une partie des
symboles dits «terminaux» représente les mots du vocabulaire tandis que les
autres symboles modélisent les différentes structures abstraites, certaines
récursives, disponibles pour construire des phrases. Ces éléments non terminaux
comprennent entre autres les syntagmes nominaux et verbaux ou les propositions
relatives qui peuvent elles-mêmes renfermer des éléments valides au niveau d'une
phrase complète. Les nœuds des arbres obtenus avec cette approche contiennent
les différentes parties qu'il est possible d'isoler dans une phrase: la phrase
dans sa totalité, généralement notée S, se trouve à la racine de l'arbre, qui
peut avoir par exemple deux nœuds enfants NP et VP pour représenter un syntagme
nominal sujet d'un syntagme verbal comme c'est le cas à la figure
\ref{fig:syntax_trees_constituents} qui montre une analyse en constituants de la
phrase «Les cellules nerveuses appartiennent au type multipolaire» extraite de
l'article SYMPATHIQUE (La Grande Encyclopedie, T30, p.759). Dans l'analyse en
dépendance au contraire, les mots sont directement en relation les uns avec les
autres et ce sont eux qui peuplent les nœuds de l'arbre syntaxique. Les arêtes
entre nœuds expriment les fonctions syntaxiques: un adjectif peut se trouver
sous un nom commun, son arête portant la fonction «amod» (qui signifie que
l'adjectif modifie le sens du nom) comme le montre la figure
\ref{fig:syntax_trees_dependencies} qui présente une analyse en dépendances de
la même phrase.

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.54\textwidth}
    \includegraphics[width=\textwidth]{figure/syntax/constituents.png}
    \caption{Analyse en constituants}
    \label{fig:syntax_trees_constituents}
  \end{subfigure}
  \begin{subfigure}[b]{0.44\textwidth}
    \includegraphics[width=\textwidth]{figure/syntax/dependencies.png}
    \caption{Analyse en dépendances}
    \label{fig:syntax_trees_dependencies}
  \end{subfigure}
  \caption{Deux analyses syntaxiques d'une phrase de l'article SYMPATHIQUE, \textit{LGE}, T30, p.759}
  \label{fig:syntax_trees}
\end{figure}

Ces théories apparues lors de l'âge d'or rationaliste (voir la section
\ref{sec:EdlA_TAL_history}) s'accordent naturellement très bien avec des
méthodes symboliques. Elles ont donné lieu à de nombreux formalismes
grammaticaux — grammaires d'arbres adjoints [@joshi_tree_1997], grammaires
catégorielles abstraites [@de_groote_towards_2001] — ainsi qu'à des algorithmes
tels que Cocke-Kasami-Younger [@moll_introduction_1988, p.64] pour les
grammaires non contextuelles ou celui de @nivre_deterministic_2004 pour de
l'analyse en dépendances. Des outils comme Talismane [@urieli_apport_2013]
intègrent toutes les couches d'analyse, de la segmentation à l'analyse
syntaxique conduite par un algorithme hybride qui combine apprentissage
automatique et règles expertes.

Cependant, comme pour la classification, l'état de l'art de l'analyse syntaxique
est désormais défini par des méthodes d'[@=AA]. Cela explique l'importance dans
le domaine des *treebanks*, ces «arboretums» syntaxiques rassemblant de grands
nombres d'exemples de phrases annotées par des spécialistes et pouvant servir de
vérité terrain pour entraîner et évaluer les modèles de langues sur cette tâche.
Il existe des treebanks pour de nombreuses langues, souvent même plusieurs pour
les langues les plus étudiées comme le French Treebank [@abeille_building_2003],
Sequoia [@candito_corpus_2012] ou encore GSD [@guillaume_conversion_2019]
amélioré et converti à partir du French Treebank en utilisant Grew
[@bonfante_application_2018] un outil de réécriture de graphes. Ces grandes
collections de données que sont les treebanks sont rendues interopérables grâce
à un formalisme commun pour l'annotation syntaxique, les «dépendances
universelles» (*Universal Dependencies* ou [@=UD]) introduites par
@nivre_universal_2017. Ce formalisme qui s'inscrit comme son nom l'indique dans
l'analyse en dépendances de Tesnière fournit des jeux d'étiquettes pour les
[@=POS] et pour les relations syntaxiques créés dans le but de pouvoir couvrir
toutes les langues naturelles. Les annotations en UD sont généralement
représentées en utilisant le format CoNLL-U [@buchholz_conll_2006] qui
s'apparente à un format [@=TSV] enrichi pour pouvoir représenter les séparations
entre phrases, des commentaires ainsi que les «sous-tokens» nécessaires pour
rendre compte des amalgames. Des outils tels que Stanza [@qi2020stanza] — retenu
pour annoter en syntaxe le corpus de cette thèse — ou HOPS
[@grobol_analyse_2021], tous deux basés sur de l'[@=AA], permettent d'analyser
un texte en UD et produisent en sortie des fichiers au format CoNLL-U. Le modèle
utilisé par HOPS prédit l'existence de liens dans un graphe dont les nœuds
représentant les mots d'une phrase. Cette stratégie, mise en regard du fait
qu'il est également possible d'utiliser des règles de réécriture de graphe pour
annoter en syntaxe [@guillaume_dependency_2015 ; @bonfante_application_2018,
chap.6] souligne la proximité thématique qui existe entre la théorie des graphes
et l'analyse syntaxique (bien que les arbres syntaxiques, qui n'admettent pas de
boucles, ne soient que des cas particuliers plus simples de graphes).

