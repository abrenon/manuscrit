## Histoire et terminologie {#sec:EdlA_TAL_history}

Il est difficile de trouver un terme pour regrouper ces deux approches sans
risquer d'en effacer une en donnant l'impression qu'elle n'est qu'un cas
particulier de l'autre. Le terme «[@=TAL]» a été utilisé dans la section
précédente (p.\pageref{sec:EdlA_lexicography_dict_and_enc}) mais il vaut
mieux bien comprendre ce qu'il recouvre avant de l'adopter. Pour cette raison,
on se contente pour l'instant de désigner ces familles de méthodes par ce
qu'elles ont en commun: une approche plus statistique que symbolique du langage.
Pour bien cerner cette distinction, il est utile de revenir à la chronologie
esquissée dans la section \ref{sec:EdlA_lexicography} et de la développer.

### Une compétition stimulante

Aux origines de cette lignée, il est d'abord question de «Traduction
Automatique» (TA) et le domaine de recherche est surtout exploré pour ses
applications potentielles, notamment militaires [@leon_cnrs_2002, §5]. Ainsi ça
n'est pas «[@=TAL]» mais seulement «TA» qu'il faut reconnaître dans «ATALA», le nom
de l'Association pour l'étude et le développement de la Traduction Automatique
et de la Linguistique Appliquée[^ATALA] créée en 1959 [@leon_cnrs_2002, §1].

[^ATALA]: [https://www.atala.org/](https://www.atala.org/)

Tout comme le terme plus général d'Intelligence Artificielle ([@=IA]) dont il
constitue un champ, le [@=TAL] ne faisait initialement aucune hypothèse quant au
moyen d'automatiser le traitement de la langue. Depuis ses origines, le domaine
a d'après @church_pendulum_2011 [p.2] connu une alternance de phases empiristes
(axées sur des méthodes statistiques) et rationalistes (axées sur des méthodes
symboliques). Après des succès rapides, l'accroissement des difficultés amenait
un ralentissement des progrès pour l'approche dominante permettant à l'autre
approche de reprendre l'initiative.

Au sortir de la guerre, l'[@=IA] est saisie d'un véritable engouement pour les
réseaux de neurones artificiels. @mcculloch_logical_1943 montrent comment ils
peuvent être utilisés pour implémenter des fonctions logiques, puis
@minsky_neural_1952 met leurs idées en pratique en construisant une machine
(physique) sur ces principes: le SNARC. Avec le perceptron de
@rosenblatt_perceptron_1958, un réseau de neurones monocouche, ces travaux
établissent les fondations de l'Apprentissage Automatique ([@=AA]), dont les
perspectives semblent alors illimitées.

Dans les années 1960 Chomsky, Pierce et même Minsky lui-même ont critiqué les
gains techniques sans vision scientifique d'ensemble de ces méthodes empiristes
et statistiques. Le rapport ALPAC [@alpac_1966] — auquel Pierce a d'ailleurs
contribué [@church_pendulum_2011, p.16] — est à l'origine d'une réduction des
financements pour ces approches, un «hiver» [@church_pendulum_2011, p.4] au
profit des rationalistes. C'est à cette époque que fleurissent les travaux sur
les grammaires formelles, décrites et catégorisées notamment par
@chomsky_three_1956.

### L'avènement des méthodes statistiques {#sec:statistical_methods_return}

Au cours des décennies suivantes, l'augmentation exponentielle des puissances de
calcul disponibles selon la loi de @moore_cramming_1998 a permis de traiter de
plus grandes sources de données ce qui a graduellement redonné l'avantage aux
empiristes. Il s'est ainsi écoulé une trentaine d'années entre les premières
solutions statistiques mentionnées ci-dessus et l'apparition d'architectures
vraiment utilisables en pratique telles que les Réseaux de Neurones Récurrents
([@=RNN]), décrits par @werbos_phdthesis_1974 puis implémentés séparément par
@rumelhart_learning_1986 et @lecun_learning_1986 ou les Réseaux de Neurones
Convolutionnels ([@=CNN]), descendants du Neocognitron de
@fukushima_neocognitron_1980.

Mais depuis 2010, le balancier semble cassé si l'on en croit
@church_pendulum_2011[p.2], qui s'inquiète de ne pas voir les méthodes
rationalistes faire leur retour à l'issue de la période d'une vingtaine d'années
qui séparait les pics d'activités dans chacune des deux approches lors des
oscillations précédentes. Loin de revenir dans les recherches pour combler les
lacunes des méthodes statistiques, elles se retrouvent éclipsées jusque dans les
contenus pédagogiques [@church_pendulum_2011, p.19 et seq.].

Plus d'une décennie plus tard, ses inquiétudes semblent on ne peut plus fondées:
l'approche empiriste occupe plus que jamais le devant de la scène et le terme
«[@=IA]» est désormais utilisée de manière interchangeable avec «[@=AA]», voire
parfois seulement pour désigner des modèles génératifs. L'Association pour
l'Avancement de l'Intelligence Artificielle [^AAAI] semble ainsi ne plus
s'occuper que d'[@=AA], contrairement à ses débuts. Hors du monde académique
l'[@=IA] est devenue un «catalyseur de croissance» [@verdegem_dismantling_2024]
à l'origine d'un type d'économie, le «capitalisme d'IA» [@dyer_inhuman_2019]. Le
mot, devenu un but en soi, est suffisant pour définir le cap de politiques
publiques [@aghion_ia_2024].

[^AAAI]: [https://aaai.org/conference/aaai/](https://aaai.org/conference/aaai/)

Les travaux conduits dans cette thèse s'inscrivent dans l'approche empiriste.
Ils utilisent des outils produits par ce courant aux deux époques où le
«pendule» de Church oscillait de son côté, et qui correspondent à deux
mouvements distincts dans la pratique de l'informatique linguistique. Après
avoir identifié leurs origines communes, il reste à caractériser ce qui les
sépare pour pouvoir convenir d'une dénomination à utiliser dans le reste de
cette thèse.

### Convention de nommage {#sec:EdlA_history_names}

La linguistique n'a pas attendu l'arrivée de réseaux de neurones artificiels
utilisables pour que sa pratique bénéficie des apports des méthodes
statistiques. Avant la période rationaliste autour des années 1970, les
linguistes ont déjà vu leur boîte à outils s'étoffer au fil d'une
mathématisation progressive de la discipline dans les deux décennies qui ont
suivi la 2^nde^ guerre mondiale [@leon_histoire_2015, p.6].

Au départ, ces nouvelles méthodes représentent seulement un moyen d'accélérer
les recherches en décuplant la capacité des linguistes à aller trouver des
exemples. En effet à partir des années 1960 la lecture attentive de textes pour
repérer des occurrences de motifs, par exemples syntaxiques, est jugée trop
inefficace [@leon_cnrs_2002, §63] et les équipes s'emploient à automatiser leur
détection. Le projet de Wittgenstein de fonder le sens des mots sur leur emploi
peut enfin être réalisé en étudiant leur «vrai» usage dans de grands
échantillons de productions des locuteurs plutôt que de se satisfaire
d'expériences de pensée [@willinsky_wittgenstein_2001, p.197]. L'école
britannique, de Firth à Sinclair, s'inscrit naturellement tout à fait dans cette
approche. En France aussi, mais de façon tout à fait différente, les
mathématiques s'intéressent à la linguistique à cette époque et vont apporter
énormément à la discipline à travers les contributions de chercheurs comme
Benzécri.

Il ne s'agit donc pas d'un mouvement homogène mais bien d'une constellation de
pratiques, ce qui complique déjà le choix d'un nom satisfaisant pour cette seule
sous-partie du domaine et amène @leon_histoire_2015 [p.157] et
@nazarenko_hal_00619268 à parler de «linguistique**s** de corpus» au pluriel.
C'est cet usage qui a été retenu pour ce manuscrit car il possède en effet une
qualité précieuse: en faisant de «linguistique» le noyau du syntagme nominal, il
rappelle qu'il s'agit d'une «automatisation des sciences du langages» pour
reprendre le titre de @leon_histoire_2015. Il s'agit certes de l'utilisation de
techniques nouvelles, mais par des linguistes et dans le cadre d'une pratique
centrée sur la linguistique.

Pour revenir au terme «[@=TAL]» sur lequel s'ouvrait cette brève section, il est
maintenant possible de lui donner un sens. Il semblerait parfait pour désigner
l'ensemble du domaine : l'arrivée des machines dans les tâches linguistiques
initiée dans les années 1960 et mentionnée ci-dessus consistait effectivement
déjà à traiter le langage de manière automatique. Toutefois, le terme reste peu
employé par les linguistes et semble en réalité bien plus populaire sur l'autre
versant de la discipline : dans une pratique de l'informatique dont la
linguistique représente un objet d'application. Il y est revendiqué autant par
des (rares, désormais) héritiers de Chomsky établissant de nouveaux modèles
formels de la langue que par des empiristes développant de nouveaux outils
statistiques.

Au vu des succès écrasants des approches empiristes, le sens de «[@=TAL]» tend à
se réduire à «application de méthodes d'[@=AA] à des problèmes linguistiques».
C'est pourtant le sens plus général d'«informatique appliquée à la linguistique»
sans hypothèse sur le type de méthodes utilisée qui a été retenu dans ces pages,
non pas par refus d'une évolution du langage mais pour ne pas contribuer à
occulter une réalité qui a été productive par le passé et pourrait l'être à
nouveau. S'il fallait un terme pour distinguer la pratique du [@=TAL] restreinte
à l'[@=AA], il serait alors tentant d'exploiter la remarque faite à la section
\ref{french_please} (p.\pageref{french_please}) en employant «NLP» (*Natural
Language Processing*) tant l'emploi de l'acronyme anglais équivalent est massif
même chez des francophones quand il s'agit d'[@=AA].

Ainsi, à l'issue des considérations historiques et réflexions précédentes la
place relative des approches couvertes dans les deux sections suivantes de cet
état de l'art devient claire. La section \ref{sec:EdlA_TAL} s'intéresse au
versant informatique de la discipline, majoritairement à des méthodes empiristes
mais sans exclure quelques techniques rationalistes. La section
\ref{sec:EdlA_corpus_linguistics} sera au contraire centrée sur les pratiques
linguistiques enrichies d'outils informatiques, héritières des travaux des
écoles britannique et française initiés dans les années 1960.

