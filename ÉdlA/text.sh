#!/bin/sh

source ./chapter.sh "Un instantané des Humanités Numériques {#sec:EdlA}"

cat ÉdlA/Introduction.md
cat ÉdlA/XML-TEI.md
cat ÉdlA/Lexicographie.md
cat ÉdlA/Historique.md
cat ÉdlA/TAL.md
cat ÉdlA/Linguistique_de_corpus.md
