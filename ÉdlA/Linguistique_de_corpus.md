## Linguistiques de corpus {#sec:EdlA_corpus_linguistics}

La branche la plus ancienne des méthodes statistiques présentées à la section
\ref{sec:EdlA_TAL_history} (voir p.\pageref{sec:EdlA_TAL_history}) est née bien
avant que des réseaux de neurones ne soient utilisables en pratique. On pourrait
donc la croire éteinte: cette dernière section entend montrer qu'il n'en est
rien. Puisant aux sources des mathématiques et de la statistique, les linguistes
n'ont pas attendu ces technologies pour mobiliser des approches très variées sur
leurs problématiques de recherche.

### Du texte au corpus

Un dénominateur commun à toutes ces approches existe dans la notion de corpus,
qui peut en première intention se comprendre comme une «collection» de textes.
Avant de décrire dans le reste de cette partie les différentes manières de
réunir un tel ensemble, il est utile de commencer par s'interroger sur la notion
de «texte» puisque c'est par rapport à elle que sera définie celle de corpus.

\label{text_polysemy}Le terme est assez fortement polysémique puisqu'il peut
être utilisé tant pour désigner une unité d'analyse comme c'est le cas ici que
pour signifier une production écrite (par opposition à orale) ainsi que la
«matière» qu'étudient certains linguistes. @rastier_textes_1996[p.19] définit la
notion comme «une suite linguistique empirique attestée, produite dans une
pratique sociale déterminée, et fixée sur un support quelconque». La définition
semble étrangement longue et complexe: l'objet apparaît évident et est bien peu
discuté dans la littérature sur la linguistique appliquée par rapport à celle de
domaines comme la sémiotique [@rastier_semiotique_2001]. La linguistique semble
lui préférer comme objet d'étude les systèmes complets que représentent les
langages dans leur ensemble ou bien les productions langagières jusqu'au niveau
de la phrase, celui où se déploie la syntaxe, «niveau ultime» selon Benveniste
[@rastier_genres_1999, p.83]. Parmi les quelques sens attribués au mot «texte»
au début de ce paragraphe, si c'est bien le premier qui intéresse cette section,
le dernier — le texte comme «matière» — joue un rôle important dans le reste de
cette thèse et il faut en dire quelques mots.

Les deux emplois sont presque contraires; car le texte comme unité se définit
implicitement par ses contours, par contraste avec les autres textes qui en sont
distincts, alors qu'en considérant le texte comme une matière à travailler, la
notion de frontière ne s'y applique plus. D'ailleurs, dans le premier cas
«texte» est dénombrable alors que dans le second, traité véritablement comme un
continu, il se retrouve manipulé comme une quantité indénombrable:
@rastier_genres_1999[p.85] marquent cette opposition entre *des* et *du*
texte(s). L'emploi indénombrable convient assez bien à l'école londonienne qui
cherche le sens des mots dans une nasse d'emplois la plus grande possible
[@krishnamurthy_corpusdriven_2008, p.232] : «faites confiance au texte» («Trust
the text») exhorte @sinclair_trust_2004 — l'emploi indénombrable est ici clair,
personne ne se demande «auquel ?». Cet emploi est encore plus répandu dans les
méthodes d'[@=AA] traitées à la fin de la section \ref{sec:EdlA_TAL} où «texte»
ne désigne souvent plus que la nature des données en entrée — par opposition à
«image» ou «audio» — souvent même qualifiées de texte «brut» (voir la section
\ref{sec:text_format} à ce sujet).

Une distinction semblable se retrouve dans la manière dont le texte ou les
textes peuvent être assemblés pour former un corpus d'étude.
@leon_histoire_2015[p.159] oppose ainsi les mouvements *corpus-based* et
*corpus-driven* déjà présents au sein de l'école britannique. Le premier
correspond à l'approche de Quirk ou de Leech par exemple [@leon_histoire_2015,
p.163] qui assemblent des textes choisis dans un but précis: l'étude est *basée
sur un corpus*. À l'inverse, l'approche *corpus-driven* de Firth, Sinclair et
l'école londonienne avec eux requiert des textes intégraux par soucis
d'objectivité et d'authenticité des productions étudiées: c'est ce qui émerge du
corpus qui oriente l'analyse.

Au XXI^ème^ siècle, la notion de corpus demeure plurielle, s'appliquant à des
objets, des usages, des domaines de la linguistique et même des méthodes de
traitement très différentes [@cori_presentation_2008, p.6]. Parmi ces méthodes,
la textométrie accorde une importance décisive au corpus, qui doit être composé
avec soin et équipé de métadonnées pertinentes en vue de la conduite d'analyses
précises [@pincemin_semantique_2022, p.9]. @pincemin_heterogeneite_2012
[p.13-14] affirme qu'il ne suffit pas de regrouper des données pour obtenir un
corpus, qui doit selon elle se fonder entre autres sur des critères de
cohérence, de représentativité, de complétude et d'exploitabilité. Elle
singularise la textométrie par rapport à la linguistique de corpus pour laquelle
le paramètre majeur serait le volume de données [@pincemin_heterogeneite_2012,
p.15]. La profusion de textes en libre accès sur internet permet en effet
l'apparition d'approches assez opposées comme le *Web as Corpus* (WAC[^WAC]) où
toute ressource est bonne à prendre pourvu qu'elle augmente la quantité de
données d'une étude, partant du principe que cela la rendra plus
robuste — argument rapporté mais critiqué par
@pincemin_heterogeneite_2012[*ibid*].

[^WAC]: l'acronyme n'est plus à la mode mais la pratique est en filiation
    directe avec celle utilisée pour entraîner les *Large Language Models* une
    décennie après la remarque de Pincemin.

Dans tous les cas, le corpus joue un rôle assez déterminant dans la discipline
pour justifier l'appellation *Corpus Linguistics*, «linguistique de corpus»,
apparue en 1984 [@leon_histoire_2015, p.159]. Elle est parfois opposée à
*Computational linguistics*, «linguistique computationnelle», préférée par les
américains [@leon_histoire_2015, p.130] non seulement pour les approches
statistiques, en particulier dans les applications de l'[@=AA] à la linguistique
évoquées à la section \ref{sec:EdlA_TAL}, mais également pour les approches
symboliques. L'opposition semble dépasser le cadre de la terminologie,
@wilks_corpus_2010[p.408] appelant à dépasser des «malentendus», ce qui
n'empêche pas Leech d'utiliser l'hybride *Computer Corpus Linguistics*
[@leon_sources_2008, p.26] — «linguistique de corpus par ordinateur».
@leon_histoire_2015[p.13] parle quant à elle d'une véritable «guerre des
dénominations» [@leon_histoire_2015, p.130]: en plus des traductions de ces deux
termes on trouve également en français linguistique «quantitative», «appliquée»
et «outillée»…

### Les outils de la linguistique

Ces termes recouvrent de nombreuses pratiques communes et semblent refléter
autant des véritables différences de positions scientifiques que des traditions
académiques. Elles partagent une variété de méthodologies dont la plupart
connaissent des implémentations dans les outils courants du domaine tels que TXM
[@heiden_txm_2010], IRaMuTeQ[^iramuteq] ou encore Hyperbase
[@brunet_computer_1989]. Un état des lieux très complet des différents types de
traitements et d'analyses utilisés est déjà dressé par
@pincemin_semantique_2022[{2.2}, pp.2-7] mais cette sous-section s'applique à
les replacer dans leur contexte historique et disciplinaire.

[^iramuteq]: [http://www.iramuteq.org/](http://www.iramuteq.org/)

La notion de collocation occupe une place centrale depuis les débuts
britanniques de la discipline, chez Firth par exemple [@leon_histoire_2015,
p.161]. Le sens du mot a changé progressivement mais il reste étroitement lié à
celui de cooccurrence, c'est-à-dire l'apparition conjointe de deux termes dans un
même empan textuel. Motivé par la recherche de termes fréquemment associés en
vue de caractériser le sens des mots dans le cadre de la production de
dictionnaires (voir section \ref{sec:EdlA_lexicography_concept}), le concept de
collocation en est progressivement venu à désigner des cooccurrences
particulièrement fréquentes jusqu'à créer une «attente mutuelle» chez les
locuteurs [@leon_sources_2008, p.16]. Caractériser cette attente demande une
mesure statistique précise de leur surreprésentation [@lafon_analyse_1981].

En pratique, l'implémentation du calcul des cooccurrences le rapproche de celui
des spécificités comme le fait remarquer @pincemin_semantique_2022[p.4]. Avant
d'expliciter la notion de spécificité, il est utile de remarquer que la plupart
des outils utilisés dans les linguistiques de corpus reposent sur des décomptes
de différents objets avec une approche statistique. En creux, ces décomptes
supposent un partitionnement: il s'agit de recenser les occurrences d'un
phénomène dans une partie du corpus par rapport aux autres pour dégager des
critères quantitatifs objectifs qui caractérisent cette partie. Cette approche
contrastive est fondamentale dans l'ensemble de la discipline.

Guiraud le premier, à la tête des stylisticiens [@leon_histoire_2015, p.129]
remarque que «la linguistique est la science statistique type»
[@brunet_lexicometrie_2014]. Bientôt rejoint par Muller, ils forment dans les
années 1960 un courant que @beaudouin_retour_2016[p.21] nomme la statistique
lexicale et dont les travaux sont initialement basés sur des décomptes de
fréquence. Il s'agit seulement au départ d'établir des concordanciers
répertoriant les occurrences des mots dans leur contexte, de dénombrer ces
occurrences, leur distribution et d'établir un vocabulaire caractéristique des
textes [@brunet_lexicometrie_2014, p.21]. On parle aussi pour cette branche de
lexicométrie. L'ordinateur n'est pas encore devenu indispensable dans cette
pratique de la linguistique car les calculs restent très simples, accessibles à
une calculette [@brunet_lexicometrie_2014, p.17]. Le premier grand débat qui
agite le champ concerne l'utilisation de formes lemmatisées ou non
[@beaudouin_retour_2016, p.30; @pincemin_semantique_2022, p.14]. Muller prendra
parti en faveur de la lemmatisation [@brunet_muller_2009, p.1].

Cependant, considérer les seules fréquences souffre de limites intrinsèques
comme le fait de ne pas pouvoir saisir les absences [@pincemin_semantique_2022,
p.3]: impossible en effet de compter des «non occurrences» de phénomènes qui ne
se réalisent pas dans une partie d'un corpus par rapport à la normale définie
par les autres textes avec lesquelles elle serait mise en contraste. En
rapportant les fréquences observées à la taille des différentes parties,
@lafon_variabilite_1980 définit la spécificité d'une forme comme une mesure de
la probabilité que la distribution observée résulte d'une répartition purement
aléatoire au sein du corpus. En ce sens, la spécificité quantifie la surprise
que constitue la surreprésentation d'un motif dans une partie du corpus. Avec la
loi de Zipf, ce point constitue un deuxième lien entre la lexicométrie et les
travaux menés en théorie de l'information dès les années 1950 notamment par
Shannon ou Markov [@leon_histoire_2015, chap.4 et en particulier p.56].

\label{textometry_specificity}Différentes lois ont été utilisées pour modéliser
la distribution des mots en vue de calculer des spécificités comme la loi du
Khi^2^. La distribution des mots dans un langage étant fortement inhomogène (la
fréquence d'un terme étant inversement proportionnelle à son rang selon la loi
de Zipf), il a également été proposé pour mieux modéliser leur répartition
d'utiliser une loi normale pour les formes les plus communes combinée à une loi
de Poisson pour les plus rares. C'est finalement pour une loi hypergéométrique
qu'opte @lafon_variabilite_1980, p.128] et c'est sur ce choix qu'est basée
l'implémentation dans le logiciel TXM [@heiden_txm_2010]. Dans ce logiciel, la
spécificité est exprimée sur une échelle logarithmique: une valeur de
spécificité de 2 signifie qu'il y a 1 chance sur 100 ($10^{2} = 100$) que le
hasard puisse expliquer la distribution observée. On considère généralement que
le seuil à partir duquel une spécificité devient significative est de 3
(c'est-à-dire au plus 1 chance sur 1 000 que le hasard explique le phénomène).
Pour des raisons pratiques d'affichage, le logiciel TXM «sature» et n'affiche
pas plus de 1 000 comme valeur pour une spécificité. Peu importe la valeur
réelle dans ce cas puisque cela correspond déjà à des distributions ayant
seulement au mieux 1 chance sur $10^{1000}$ de se produire par hasard, donc
l'affichage de cette valeur dans TXM suffit en pratique à exclure totalement une
coïncidence.

La deuxième contribution majeure des mathématiques à l'école française de
linguistique vient un peu plus tard, à partir de la fin des années 1960 avec le
courant de l'analyse des données fondée par Benzécri [@pincemin_semantique_2022,
p.1]. Reprochant les approches trop centrées sur le vocabulaire de la
statistique lexicale de Guiraud et Muller, il propose l'Analyse Factorielle des
Correspondances — [@=AFC] — [@benzecri__analyse_1973] qui est une technique de
réduction de dimension. L'[@=AFC] permet de détecter les proximités et les
différences entre les parties d'un corpus en représentant les associations qui
existent entre le vocabulaires et les textes dans un espace à deux dimensions
[@beaudouin_retour_2016, p.20]. L'emploi conjoint de statistiques et de
techniques d'algèbre peut faire penser à un précurseur des méthodes plus
récentes utilisées en [@=AA] comme les plongements de mots. Il est intéressant
de noter qu'il n'en est rien: le positionnement théorique de Benzécri est opposé
non seulement à la linguistique de Chomsky mais également à l'[@=IA]
[@beaudouin_retour_2016, p.18]. L'[@=AFC] construit une projection dans un
espace abstrait qu'il appartient à la personne qui conduit les recherches
d'interpréter. Pour Benzécri, l'ordinateur n'est pas «intelligent»; bien au
contraire le fait qu'il ne le soit pas est vu comme un gage d'objectivité car
l'ordinateur n'a dès lors pas d'à priori et ne peut pas mentir
[@beaudouin_retour_2016, p.22]. Cette conception est diamétralement opposée aux
promesses d'intelligence des architectures les plus récentes de modèles d'[@=AA]
comme les *Large Language Models* dont les «hallucinations» sont assez établies
et documentées pour qu'une littérature leur soit consacrée
[@farquhar_detecting_2024; @chelli_hallucination_2024; @benazha_measuring_2024].

### Études contrastives

Forte de ces outils, la linguistique est à même de décrire quantitativement les
contrastes qui peuvent exister entre différents textes réunis au sein d'un même
corpus. Plusieurs travaux comparent ainsi des genres textuels
[@pamuksac_distinguer_2023] y compris dans des domaines spécifiques comme la
justice [@diotparvaz_heterogeneite_2020] ou sur des thématiques particulières
comme des questions sociétales et environnementales [@dang_brouillard_2020].

De nombreuses études explorent les différences à l'échelle d'auteurs
individuels. Une des promesses des stylisticiens est bien de réussir à
caractériser le style de différents auteurs [@muller_statistique_1988;
@labbe_distance_2000]. @pincemin_heterogeneite_2012[p.19] mentionne également
des travaux plus récents de textométrie faisant la synthèse des approches de la
statistique lexicale et de l'analyse des données pour opposer différents genres
présents au sein du corpus Frantext (voir section
\ref{sec:EdlA_lexicography_dict_and_enc}) ou dans l'œuvre d'un seul
auteur — Victor Hugo — pour une étude de @brunet_hugometrie_2002. Il est
intéressant de noter la ressemblance avec les applications de la classification
automatique pour déterminer le sujet d'un texte ou l'orientation politique de
son auteur (voir la section \ref{sec:EdlA_TAL_text_classification}). Cependant,
les deux démarches sont en réalité tout à fait opposées : alors que la finalité
des tâches de classification réside complètement dans la production d'une
partition des textes, ces études textométriques s'attachent au contraire à
identifier les critères qui permettent de rattacher un texte à une des
catégories pour rendre explicite ce qui les caractérise.

À ce titre, le domaine politique semble particulièrement bénéficier des apports
de la textométrie qui permet de révéler les positionnement thématiques et
idéologiques qui transparaissent dans les discours. Un corpus regroupe les vœux
prononcés traditionnellement au nouvel an par les présidents de la cinquième
république. Mis à jour au fil des élections, il est distribué avec TXM et
alimente de nombreuses études [@pincemin_semantique_2022;
@mayaffre_explorer_2019]. Les discours des syndicats font aussi l'objet
d'analyses contrastives [@brugidou_discours_2000]. Des époques plus reculées
comme l'entre-deux-guerres ne sont pas ignorées [@mayaffre_poids_2000]. Enfin,
la taille des intervalles de temps considérés permet la mise en place d'étude de
productions d'une seule personnalité politique où les contrastes sont envisagés
selon la dimension temporelle. @labbe_françois_1983 étudie ainsi le discours
public de François Mitterrand dans la quinzaine d'années avant son élection au
poste de président de la république puis celui de De Gaulle sur un peu plus
d'une décennie [@labbe_diachronie_2010].

De telles études, dites en diachronie, permettent de capturer des évolutions à
des échelles variées selon la durée considérée, les changements les plus
profonds dans le langage nécessitant davantage de temps pour se produire.
Suivant le genre textuel considéré et l'ancienneté des époques, la finesse et la
régularité des «graduations» sur l'axe temporel peut être dure à atteindre car
il devient difficile de trouver assez de texte d'un genre considéré pour chaque
intervalle du corpus. @vigier_autour_2017, avec une segmentation par décennie,
réussit à mettre en lumière les changements dans l'emploi des prépositions en
français sur quatre siècles et demie. Quand le découpage du corpus en périodes
distinctes est assez précis, il devient possible d'observer quand et sous quelle
influence un changement a lieu, comme le montrent @hilpert_quantitative_2016
[p.50]. Les approches récentes les plus gourmandes en données comme le WAC
favorisent naturellement ce genre d'études, par exemple en sociolinguistique,
permettant d'établir le cheminement d'une innovation linguistique.
@tarrade_detecting_2022 décrivent ainsi les différentes phases de propagation
d'une innovation linguistique entre des locuteurs sur un réseau social, jusqu'à
ce qu'elle devienne un changement persistant dans la langue ou bien soit
abandonnée.

D'autres genres se prêtent naturellement moins à ce genre d'étude diachronique
où le maillage temporel est contrôlé finement puisque certains types d'ouvrages
notamment scientifiques ou lexicographiques ne peuvent être produits au même
rythme et en même quantité que les messages de microblog. Pour les étudier, il
faut se contenter des œuvres disponibles, parfois une seule par époque, en
gardant à l'esprit les lacunes dans la couverture temporelle et le risque de
mesurer des particularités propres à une œuvre donnée plus qu'à son époque. En
s'appuyant de cette manière sur des manuels de physique, @mouhouche_etude_2014
parvient à révéler les changements profonds dans la compréhension du phénomène
physique de résonnance au travers de la terminologie employée pour en parler, de
son origine accoustique jusqu'à ses applications modernes aux planètes pour
véhiculer les notions d'accord et de transfert d'énergie.
@blumenthal_encyclopedie_2017 étudie le recours croissant aux locutions
prépositionnelles dans l'Encyclopédie Universalis par rapport à l'*EDdA*. S'il
ne peut pas dater finement les changements, il lui est tout de même possible de
mettre en évidence les différences entre les deux époques (XVIII^ème^ et XX^ème^
siècles).

Les analyses diachroniques posent des défis techniques qui leur sont propres.
@diwersy_ressources_2017 ainsi que @vigier_autour_2017 montrent par exemple les
difficultés quant à l'ordre et à la segmentation des mots, et même quant à la
graphie qu'implique le choix de rassembler dans un même corpus — Presto dans les
deux cas — des états de langue aussi différents que le français des XVI^ème^ et
XX^ème^ siècles. Cette question illustre elle aussi le fait qu'il n'existe pas
de barrière étanche entre les approches computationnelles plus récentes
considérées à la section précédente (\ref{sec:EdlA_TAL}) et celles décrites dans
la présente section. Les méthodes de plongement utilisées classiquement en
[@=AA] pour représenter le sens des mots sont gênées par les glissements
sémantiques qui surviennent dans les données d'entraînement. Étudier le sens des
mots en diachronie avec ces méthodes nécessite donc des adaptations
particulières [@hamilton_diachronic_2016; @kutuzov_diachronic_2018;
@tahmasebi_survey_2019].

L'état de l'art qui s'achève avec ce chapitre a permis d'identifier trois axes
principaux qui structurent l'ensemble de cette thèse et la situent sans
ambiguïté en [@=HN]. Le premier est bien sûr celui des questions d'encodage qui
sous-tendent tout le chapitre \ref{sec:corpus}. La section
\ref{sec:EdlA_encoding} a décrit les formats utilisés, principalement le
XML-[@=TEI], et soulevé des difficultés qui seront discutées en particulier à la
section \ref{sec:corpus_lge_encoding}. La section \ref{sec:EdlA_lexicography} a
ensuite montré le lien étroit qui unit les œuvres lexicographiques et les
travaux conduits en linguistique au cours du XX^ème^ siècle. Les quelques
réflexions faites sur leur structure et sur les études déjà menées dans ce
domaine alimenteront naturellement le chapitre \ref{sec:corpus}, mais la section
était également nécessaire pour amorcer un historique des contacts entre
linguistique et informatique. Poursuivi à la section \ref{sec:EdlA_TAL}, cet
historique a aidé à comprendre d'où venaient les outils conceptuels mobilisés
dans les présents travaux et qui définissent les deux autres axes de l'étude.
Les applications de l'[@=AA] aux tâches de classification permettent d'enrichir
le corpus d'étude en associant aux articles un ensemble d'étiquettes
représentant les domaines de connaissance dont ils relèvent. Cette métadonnée
partage les textes suivant l'axe des disciplines en plus celui du temps,
constitutif du corpus assemblé pour cette étude diachronique et qui distingue
par construction les articles du XVIII^ème^ siècle issus de l'*EDdA* de ceux du
XIX^ème^ provenant de *LGE*. En annotant les textes en morphosyntaxe et en
syntaxe à l'aide d'autres méthodes d'[@=AA], ce double partitionnement rend
possible la conduite d'analyses contrastives. C'est ainsi que les méthodes des
linguistiques de corpus décrites dans la présente section
\ref{sec:EdlA_corpus_linguistics} sont utilisées au chapitre \ref{sec:contrasts}
pour identifier et caractériser les changements survenus dans les discours
géographiques.

