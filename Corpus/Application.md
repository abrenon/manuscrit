## Des textes au corpus {#sec:corpus_application}

Après avoir décrit les œuvres puis les manières de les représenter par des
formats numériques ainsi que les choix opérés sur cette question, il est temps
pour conclure ce chapitre de montrer comment ces étapes préparatoires
convergent. Elles permettent de constituer un corpus qui structure les données
et facilite leur emploi avec différents outils pour les besoins de cette thèse.
Cette section sera également l'occasion de faire une remarque méthodologique sur
l'obtention et la maintenance du corpus et des métadonnées qu'il contient.

### Différents formats pour différents usages {#sec:corpus_application_formats}

Deux formats de départ différents sont utilisés pour représenter les articles du
corpus en amont de tout traitement. Le premier est un format XML-[@=TEI]
utilisant les éléments principaux du schéma (module *core*, par opposition au
module *dictionaries*, pour les raisons présentées à la section
\ref{sec:dictionaries_vs_encyclopedias}
p.\pageref{sec:dictionaries_vs_encyclopedias}). Le choix de balises diffère
légèrement entre l'*EDdA* (pour laquelle l'[@=ARTFL] utilise des éléments
`<div1/>` pour les articles dont les métadonnées sont stockées dans des éléments
`<index/>`) et *LGE* (pour laquelle les rares métadonnées accessibles sont
stockées directement comme attribut de l'élément `<div/>` choisi pour
représenter les articles). Le second est un format textuel utilisant les choix
d'encodage décrits à la sous-section \ref{sec:text_format}
(p.\pageref{sec:text_linear_format}).

Le second peut s'obtenir du premier en extrayant le contenu de certaines balises
précises dans l'encodage des fichiers. C'est de cette façon qu'a été obtenue la
version texte des articles de l'*EDdA*. Pour *LGE* en revanche, la version
textuelle peut être extraite directement à partir des pages en XML-ALTO,
indépendamment de la version XML-[@=TEI], les deux correspondant à des modes de
sortie de `soprano` (la génération des métadonnées décrite à la section
\ref{sec:corpus_structuring_metadata} constitue également un mode séparé).

#### Format textuel {#sec:corpus_application_formats_text}

Le format textuel est en pratique le plus utilisé dans les différentes études
conduites au cours de cette thèse. La plupart des outils de [@=TAL] attendent en
entrée un flot d'unités indépendantes de texte (avec toutes les réserves émises
à ce sujet à la section \ref{sec:text_format} p.\pageref{sec:text_format})
débarrassées des contingences liées à leur représentation (retours à la ligne au
milieu d'une phrase, divisions au mileu des mots coupés par un retour à la
ligne…). Pour analyser les phrases en syntaxe et morphosyntaxe, un outil doit
voir au moins toute une phrase d'un coup (il peut en voir plusieurs au sein d'un
même paragraphe mais donnera une analyse erronée s'il ne reçoit qu'une phrase
partielle); pour classifier un article, un modèle doit avoir accès à tout le
texte qu'il contient.

Puisque le choix fait pour représenter les articles dans des fichiers texte
consiste à garder la mise en page des lignes telles qu'elles apparaissent dans
les encyclopédies du corpus, il faut d'abord «nettoyer» le texte avant de
pouvoir appeler dessus une chaîne de classification, un analyseur syntaxique ou
un annotateur d'entités nommées. Pour éviter les redondances de traitement et
s'assurer que tous les outils ont reçu la même entrée, la phase d'extraction a
été factorisée, et une version linéarisée des articles est produite et stockée
dans des fichiers texte utilisant d'autres paramètres d'encodage textuel (voir
p.\pageref{sec:text_lines_paragraph} et seq.).

Le format textuel rempli donc deux rôles dans ces travaux. Il y a d'abord un
format «patrimonial» dont le but est de fournir une représentation relativement
fidèle des articles et de leur mise en page (l'exception majeure étant les
paragraphes, qui sont séparés par des lignes vides pour limiter les ambiguïtés).
De ce format patrimonial est tiré un format texte «linéaire» dont le but est
d'être consommable directement par des outils de [@=TAL]. Ce deuxième format se
caractérise par les paramètres d'encodage textuels suivants: les fichiers
utilisent le même encodage UTF-8 que pour le texte patrimonial, les fins de
lignes typographiques sont ignorées et les mots divisés sont recollés (à l'aide
d'une heuristique simpliste par rapport aux remarques faites à la section
\ref{sec:text_linear_format} en particulier p.\pageref{sec:text_glueing_lines},
ce point est discuté dans la sous-section \ref{sec:shs_reproducibility}). Enfin,
la séparation des paragraphes par des lignes vides, bien qu'inutile en soi
suite au recollement, est conservée pour favoriser la lisibilité de ces fichiers
par des humains à des fins de contrôle de la qualité des données.

Les outils de classification automatique utilisés tout au long du chapitre
\ref{sec:domains_classification} prennent ainsi en entrée du texte linéaire et
produisent pour chaque texte une valeur symbolique, le nom d'une classe dont
relève le texte d'après le modèle. Cette information est stockée dans des
fichiers de métadonnées tabulaires (voir la sous-section
\ref{sec:corpus_structuring_metadata}). L'analyse syntaxique en dépendances
universelles (*Universal Dependencies*[^UD], [@=UD] dans le reste de cette
thèse) est effectuée avec la librairie python Stanza[^stanza][@qi2020stanza],
qui retourne une annotation en  des articles au format CoNLL-U[^conllu]. Cette
annotation comprend une étiquette morphosyntaxique pour des tokens identifiés,
exprimée avec les parties de discours ([@=POS]) propres aux [@=UD], les
UPOS[^UPOS].

[^stanza]:
    [https://stanfordnlp.github.io/stanza/](https://stanfordnlp.github.io/stanza/)

[^UD]:
    [https://universaldependencies.org/](https://universaldependencies.org/)

[^conllu]:
    [https://universaldependencies.org/format.html](https://universaldependencies.org/format.html)

[^UPOS]:
    [https://universaldependencies.org/docs/u/pos/](https://universaldependencies.org/docs/u/pos/)

#### Format XML-TEI

Le format XML-[@=TEI] (voir la section \ref{sec:xml_tei}), au contraire du
format texte discuté ci-dessus permet de conserver plus d'information sur les
pages tout en distinguant le rôle de chaque objet et sa relation aux autres. Les
éléments péritextes par exemple, présentés à la section
\ref{sec:encyclopedia_anatomy} p.\pageref{sec:encyclopedia_anatomy},
introduiraient beaucoup d'ambiguïté dans des fichiers texte, à moins d'en
complexifier encore l'encodage. L'option `-k` (`--keep` en version longue) de
`soprano` permet de choisir quel type d'éléments conserver et permet de produire
des fichiers ne contenant que le texte des articles, mais par défaut, tous les
éléments sont conservés (en particulier les changements de page, les légendes
d'images, etc.), ce qui prend tout son sens avec le format XML-[@=TEI].

Il constitue donc une version plus riche, cruciale pour la diffusion des données
et leur réutilisation dans le cadre d'autres études. C'est ce qui a permis aux
présents travaux de bénéficier des résultats des efforts des projets [@=ARTFL]
et [@=ENCCRE]. Le projet GEODE comporte une dimension patrimoniale et met à
disposition le texte de *LGE* au format XML-[@=TEI]. Il est à souhaiter que ces
données connaissent le même destin que celles de l'*EDdA*. Certains thèmes de
recherche un peu trop éloignés de la problématique initiale et qui n'ont pas pu
être suivis au long de cette thèse pourraient en effet intéresser de nombreux
spécialistes des [@=HN]. Le rapport entre géographie et colonisation suggéré à
la Préface (La Grande Encyclopédie, T1, p.I) — voir la section
\ref{sec:knowledge_domains} p.\pageref{lge_preface_domains}, ou le traitement
des notices biographiques de femmes et d'hommes par exemple mériteraient sans
doute une exploration soigneuse.

Les logiciels de textométrie utilisés dans les présents travaux prennent en
entrée des fichiers au format XML-[@=TEI]. Le logiciel TXM [@heiden_txm_2010]
permet ainsi d'explorer directement le corpus à partir de la version XML-[@=TEI]
des deux œuvres et de faire des mesures de nombre de tokens, des œuvres ou de
leur partitions (notamment par domaine de connaissance, voir la section
\ref{sec:geo_size_metrics} à partir de la page \pageref{sec:geo_size_metrics}).
L'extension intégrant l'étiqueteur TreeTagger[^treetagger] permet même d'accéder
à des étiquettes morphosyntaxiques pour les tokens. En pratique, cette extension
n'a pas été utilisée dans cette étude puisque l'annotation a été faite avec
Stanza sur les fichiers texte, et la sortie en CoNLL-U a été extraite pour
produire une version XML-[@=TEI] intégrant en plus les informations
morphosyntaxiques en UPOS. En ce qui concerne le Lexicoscope
[@kraif_lexicoscope_2016] qui utilise un format dual, conservant du contenu
CoNLL-U dans un environnement de balises XML-[@=TEI] ordinaires (des paragraphes
`<p/>` groupant des éléments `<s/>` dont chacun contient l'annotation CoNLL-U de
la phrase qu'elle représente plutôt que du texte directement lisible pour des
humains), les fichiers XML-[@=TEI] ont été entièrement synthétisés à partir des
sorties de Stanza.

[^treetagger]:
    [https://cis.uni-muenchen.de/~schmid/tools/TreeTagger/](https://cis.uni-muenchen.de/~schmid/tools/TreeTagger/)

### Principe de structuration {#sec:corpus_structuring_metadata}

Un corpus n'est pas constitué que de données, quelle que soit la qualité du
format qui les représente. Une part importante de l'effort d'organisation de
cette thèse a été employée à mettre en regard des données des métadonnées qui
soient pertinentes pour les analyses et pratiques à utiliser techniquement.

#### Des désignants à des ensembles de domaines {#sec:domains_build_classes}

La notion de domaine de connaissance introduite en détail à la section
\ref{sec:knowledge_domains} (p.\pageref{sec:knowledge_domains}) joue un rôle
central dans cette thèse. Disposer d'un ensemble de valeurs à associer aux
articles pour représenter leur appartenance à un domaine a demandé un travail
important de normalisation. Le chapitre \ref{sec:domains_classification} décrit
en détail les techniques utilisées pour identifier le domaine de chaque article
mais il s'agit ici, en amont de ces opérations, de déterminer les valeurs à
utiliser pour la classification (c'est-à-dire le codomaine des fonctions
implémentées par les différents classifieurs).

D'Alembert avait prédit que non seulement une encyclopédie ne serait jamais
complète mais que son organisation même pourrait être renouvelée par
l'expérience de son lectorat. Dans son *Discours préliminaire* (L'Encyclopédie,
T1, p.xv), il offre du contexte au «Systême» (voir la figure
\ref{fig:systeme_figure} p.\pageref{fig:systeme_figure}) si rigide en apparence
en affirmant que «la forme de l'arbre encyclopédique dépendra du point de vûe où
l'on se mettra pour envisager l'univers littéraire. On peut donc imaginer autant
de systèmes différens de la connoissance humaine, que de Mappemondes de
différentes projections […]».

Il n'est pas surprenant pour un projet de l'envergure de l'*EDdA* et s'étendant
sur autant d'années (21 au total) que les désignants qui matérialisent les
domaines de connaissance au sein des articles comptent plus de 7 000 formes
uniques différentes. La plupart sont dues à des variations sur les abréviations,
l'orthographe ou la ponctuation utilisée. Une très grande partie de ces formes
uniques n'ont que très peu d'occurrences, et un système automatique n'a pas de
compréhension sémantique des regroupements qu'il pourrait être légitime de
faire. Utiliser ces désignants tels quels n'aurait que peu de sens pour
constituer des classes dans l'optique d'études contrastives. Il faut donc déjà
normaliser les formes variées prises par les désignants, ce qui a fait l'objet
de travaux à la fois par l'[@=ARTFL] et l'[@=ENCCRE]. Dans l'exemple de
l'article EVIAN présenté à la figure \ref{fig:edda_evian}, le désignant «Géog. mod.»
se normalise en «Géographie moderne» ce qui permet de l'identifier à celui
de COMMERCY à la figure \ref{fig:edda_commercy} qui est «Géograph. mod.» et
diffère donc en toute rigueur.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/EDdA/evian_t6_p146.png}
        \caption{Article EVIAN dans l'\textit{EDdA}, T6, p.146}
        \label{fig:edda_evian}
    \end{subfigure}
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/EDdA/commercy_t3_p700.png}
        \caption{Article COMMERCY dans l'\textit{EDdA}, T3, p.700}
        \label{fig:edda_commercy}
    \end{subfigure}
    \caption{Deux entrées de «Géographie moderne» dans l'\textit{EDdA}}
    \label{fig:edda_geogr_mod}
\end{figure}

\label{sec:domain_groups}L'[@=ARTFL] a réduit ce nombre à 2 620 classes
normalisées [@horton2009mining, §3] qui correspondent dans l'encodage
XML-[@=TEI] de l'œuvre à l'attribut `normclass`. À partir des mêmes données,
l'[@=ENCCRE] a créé manuellement une liste de 2 160 «désignants explicités» ,
prenant également en compte les tournures évoquées précédemment à la section
\ref{sec:knowledge_domains}  («en termes de», etc., voir
p.\pageref{sec:edda_domain_expressions}). L'[@=ENCCRE] a ensuite cherché à
identifier les thématiques proches et rassemblé ces classes en 312 domaines,
puis sur un deuxième niveau en 44 «ensembles de domaines». Une altération minime
de ces ensembles de domaines a été faite dans le cadre du projet GEODE pour
rassembler au sein d'une seule classe *Métiers* les différentes branches de
professions qui dans le jeu de l'[@=ENCCRE] ont chacune un ensemble de domaines à
part. Ce choix définit un ensemble de 38 *domaines regroupés* visibles à la
figure \ref{fig:distribution_grouped_domains} et utilisés dans la comparaison de
méthodes de classification automatique conduite au chapitre
\ref{sec:domains_classification}.

![Distribution des nombres d'articles par *domaine regroupé*](figure/histogram/corpus/domainGroup_frequencies.png){#fig:distribution_grouped_domains width=100%}

Dans ce nouveau système, les deux articles de la figure \ref{fig:edda_geogr_mod}
se retrouvent classés en *Géographie*, le domaine regroupé qui inclut la
*Géographie moderne* et comporte 13 289 articles. La figure
\ref{fig:distribution_grouped_domains} montre qu'il s'agit du groupe le plus
fréquent parmi les articles pour lesquels une classification des auteurs est
connue, et que le suivant, *Droit - Jurisprudence* compte quasiment deux fois
moins d'articles (6 901).

Ce jeu des «domaines regroupés» posait encore quelques difficultés de
dimensionnement pour les études contrastives présentées au chapitre
\ref{sec:contrasts}. De plus, la réduction du déséquilibre entre classes permet
d'obtenir des prédictions de meilleures qualités sur les classes moins
représentées. Pour cette raison un nouveau jeu de classes plus simple a été créé
pour représenter les domaines de connaissances dans l'*EDdA*. Ce jeu ne comprend
plus que 17 classes qui ont été construites manuellement par Katherine
\textsc{McDonough}, historienne et membre du projet GEODE, en vue de rendre
compte de manière concise des thématiques principales abordées dans l'*EDdA* et,
au contraire des regroupements de classes opérés par l'[@=ENCCRE], en assemblant
des «branches» et des «sous-branches» à différents niveaux dans le «Systême
figuré» (voir \ref{fig:systeme_figure}). Le jeu de classes, appelées par
convention dans le projet les «superdomaines» (c'est le nom qui sera retenu dans
le reste de cette thèse), a donc été conçu dès l'origine comme un regroupement
non pas d'*ensemble de domaines* ou de *domaines regroupés* mais directement de
désignants normalisés. Il est constitué des classes suivantes:

- Agriculture
- Beaux-arts
- Belles-lettres
- Chasse
- Commerce
- Droit Jurisprudence
- Géographie
- Histoire
- Histoire naturelle
- Médecine
- Métiers
- Militaire
- Musique
- Philosophie
- Physique
- Politique
- Religion

#### Structure des métadonnées

Le premier besoin pour attacher des informations additionnelles aux textes sans
devoir dupliquer leur contenu est la détermination d'une information qui
identifie de manière unique les fichiers, ce qui constitue une clef primaire
dans le jargon des bases de données. En distinguant les articles des propriétés
qu'on souhaite pouvoir leur associer, c'est l'approche la plus modulaire
puisqu'elle permet de croiser entre elles plusieurs métadonnées issues de
traitements différents ou de sélectionner des sous-corpus, par opposition à une
approche en «jeu de données» ou *datasets* dans laquelle toutes les informations
requises, texte et métadonnées, seraient groupés ensemble de manière autonome en
un objet unique.

Les présents travaux optent pour une clef primaire simple basée sur le rang des
articles dans chaque tome des œuvres du corpus. Elle est ainsi constituée d'un
triplet comprenant un code pour l'œuvre (une des deux valeurs symboliques `EDdA`
ou `LGE`[^work]) d'un numéro de tome et d'un numéro d'article qui est son rang
dans l'œuvre. Il avait également été envisagé d'utiliser à la place du rang un
identifiant basé sur la vedette de l'article (complétée par un entier pour
garantir unicité malgré les homonymies potentielles), de manière à obtenir un
système plus résistant aux redécoupages alors que la segmentation de *LGE* était
en cours d'amélioration (ainsi, un identifiant avait plus de chance de demeurer
identique alors que le rang de tous les articles survenant dans un tome après un
article où la segmentation avait été corrigée se retrouve modifié). Cette idée a
été abandonnée suite à la difficulté d'obtenir une représentation unifiée d'un
système à l'autre de caractères accentués[^nfc].

[^work]: les valeurs `Universalis` et `Wikipedia` étaient également possible
    pour le reste du corpus GEODE mais n'ont pas été utilisées en pratique

[^nfc]: dans l'encodage UTF-8, utilisé de manière assez générale sur de nombreux
    systèmes modernes, différentes conventions de normalisation existent (on
    distingue les formes normales composées — NFC — et décomposées — NFD) et
    tous les systèmes n'optent pas pour la même normalisation. Ainsi, un 'é' (e
    accent aigu) peut être réalisé directement en NFC (il lui correspond le code
    U+00e9, représenté par la séquence d'octets "`0xc3` `0xa9`"), ou bien
    «assemblé» en NFD en combinant un 'e' simple (de code U+0065 représenté par
    l'octet `0x65`) avec U+0301, un code représentant un accent aigu à combiner
    avec une autre lettre, et qui se représente lui même par la séquence "`0xcc`
    `0x81`"

Contrairement aux différentes étapes de traitement et d'annotation qui
représentent chacune les articles dans un format différent, les métadonnées
constituent des propriétés permanentes à associer aux textes (un auteur, un
domaine de connaissance…). Elles sont stockées dans des fichiers au format
[@=TSV], conçus pour faciliter l'accès par des moyens programmatiques (voir la
section \ref{sec:EdlA_encoding}). La structure des métadonnées relève donc déjà
davantage du code que des données, ce pourquoi les colonnes sont nommées en
anglais, *lingua franca* pour les codes sources. Ainsi, le tableau
\ref{tab:primary_key_example} représente les métadonnées minimales nécessaires
pour référencer par exemple les deux articles de la figure
\ref{fig:edda_geogr_mod}, c'est-à-dire leur clefs primaires.

\begin{table}[h]
    \centering
    \input{table/metadataSamples/primaryKey.tex}
    \caption{Clefs primaires des entrées COMMERCY et EVIAN dans l'\textit{EDdA}}
    \label{tab:primary_key_example}
\end{table}

À cette clef primaire doivent s'ajouter des colonnes pour assurer la navigation
et le retour au plein texte. La plus évidente est la vedette (un humain cherche
un article à propos d'EVIAN, pas le 3184ème article de l'*EDdA*). Pour
outrepasser le problème d'homonymie des vedettes soulevé ci-dessus, une version
normalisée et rendue unique de cette vedette est également stockée, bien qu'elle
n'ait finalement pas été retenue pour constituer la clef primaire. Enfin, le
numéro de la page auquel apparaît un article est stocké dans ces métadonnées de
navigation pour retrouver l'article dans les pages numérisées. Avec ces
informations supplémentaires, les deux articles précédents sont représentés par
les métadonnées visibles au tableau \ref{tab:navigation_example}.

\begin{table}[h]
    \centering
    \input{table/metadataSamples/navigation.tex}
    \caption{Métadonnées de navigation pour les entrées COMMERCY et EVIAN dans l'\textit{EDdA}}
    \label{tab:navigation_example}
\end{table}

Ensuite, d'autres dimensions supplémentaires peuvent être ajoutées (domaines,
auteurs, taille des textes…). Leur stockage dans des fichiers distincts fournit
le plus de flexibilité, permettant par exemple de mettre à jour le domaine
prédit pour un article suite à une amélioration dans la chaîne de traitement de
classification automatique, sans devoir par exempler regénérer tous les fichiers
qui dépendent de traitements prenant seulement en compte les tailles. Cette
approche permet de cerner au plus près les dépendances entre données au fil des
différents traitements, au seul prix de la répétition des clefs primaires dans
chaque fichier, bien moins contraignante que la répétition des textes entiers
des articles eux-mêmes. Il peut arriver de devoir grouper plusieurs métadonnées
pour une étude particulière, ce qui peut s'effectuer par une simple jointure sur
la clef primaire des articles. Le résultat d'une telle jointure peut s'observer
dans les sources du présent manuscrit[^manuscrit], par exemple dans le fichier
`data/corpus/metadata.tsv` où les métadonnées utiles à la génération des figures
du manuscrit ont été groupées en un seul fichier pour réduire la place utilisée
par le dépôt (les outils de versionnement sont surtout faits pour travailler sur
de petits fichiers, limiter la taille du dépôt permet d'éviter la dégradation
des performances).

#### Structure des dossiers et des fichiers

Un corpus, en tant qu'association structurée de données et de métadonnées
rassemblées dans un but d'étude précis, est représenté numériquement par un
dossier contenant à la fois les textes du corpus et leurs métadonnées. Les
différents fichiers associés à chaque métadonnée sont stockés à la racine du
corpus. Chaque format correspondant à un état des textes (une étape de
traitement ou un type d'encodage) est stocké dans un sous-répertoire dédié du
corpus (par exemple `Text/` ou `TEI/` pour les formats évoqués à la section
\ref{sec:corpus_application_formats} qui contiennent les textes en amont de tout
traitement, respectivement aux formats textuels ou XML-[@=TEI]).

Le triplet de la clef primaire assure non seulement le lien entre les
métadonnées mais se manifeste aussi dans chacune des arborescences correspondant
à un état donné des textes. À l'intérieur de chaque sous-dossier, les fichiers
sont en effet organisés par œuvre (dans un répertoire dont le nom est la valeur
de l'attribut `work` de l'article). Chaque dossier d'œuvre (`EDdA/` ou `LGE/`
donc) contient un répertoire par tome, nommé en préfixant la valeur de
l'attribut `volume` d'un 'T' (t majuscule): `T1/`, `T2/`… jusqu'à `T17/` dans
`EDdA/` et `T31/` dans `LGE/`. À l'intérieur de chaque dossier de tome, chaque
article est nommé par son rang, suffixé de l'extension idoine (`.txt`, `.xml`,
`.conllu`…). Ainsi, il est extrêmement facile d'accéder aux contenus des
articles en itérant sur leurs métadonnées, puisque pour un état donné leurs
chemins relativement au répertoire correspondant à cet état s'obtiennent par une
simple opération de concaténation.

### Reproductibilité {#sec:reproducibility}

Sans constituer une spécification précise, les deux sous-sections précédentes
\ref{sec:corpus_application_formats} et \ref{sec:corpus_structuring_metadata}
montrent la logique qui a présidé aux choix techniques de conception du corpus
utilisé dans le reste de la thèse. De même que la description des idées clefs
d'un algorithme fournit une aide précieuse pour comprendre une de ses
implémentations dans un langage en particulier, elles tentent de documenter les
données diffusées à l'issue du projet pour aider à s'en emparer. Avant de
refermer complètement ce long chapitre, quelques remarques sur la notion de
reproductibilité donneront une perspective aux choix opérés.

Cette thèse n'est pas reproductible. Au-delà de ce seul constat hélas vrai pour
la plus grande partie des thèses soutenues à ce jour, cette sous-section se
propose de donner un sens à cette assertion dans le contexte disciplinaire qui
lui est propre avant d'aller voir quels efforts ont tout de même été entrepris
en direction de la reproductibilité, comment cette notion s'intègre à la
méthodologie esquissée par la structure du corpus décrite dans le reste de cette
section et quels progrès restent nécessaires.

#### De la reproductibilité en SHS ? {#sec:shs_reproducibility}

Le premier obstacle à la reproductibilité de ces travaux réside dans
l'impossibilité de partager une moitié du corpus. Les pages scannées de *LGE* et
toutes les données extraites à partir de ces pages à l'issue du projet DISCO-LGE
sont bien sûr diffusées sur la plateforme Nakala[^nakala] et placées sous
licence libre Creative Commons By-Nc-Sa[^cc], ce qui signifie qu'elles peuvent
être utilisées librement aux seules conditions de signaler leur origine, de ne
pas en faire un usage commercial et de ne les repartager — elles ou toute donnée
nouvelle créée à partir d'elles — que sous les mêmes conditions. Cela est
naturellement compatible avec une diffusion pour d'autres travaux de recherche.
En revanche, en ce qui concerne l'*EDdA*, les données utilisées ont été
communiquées par l'[@=ARTFL] sous condition de non-divulgation et il est donc
impossible de les repartager. L'[@=ARTFL] accepterait vraisemblablement de
partager les tomes de l'*EDdA* en XML-[@=TEI] avec d'autres équipes qui en
feraient la demande, mais il est impossible de comparer cette autre hypothétique
version des données avec celle utilisée dans le cadre du projet GEODE: il se
pourrait tout à fait que l'[@=ARTFL] ait fait des améliorations dans ses chaînes
de traitements et qu'elles ne produisent plus exactement les mêmes données.

[^cc]:
    [https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)

Si des spécialistes du XIX^ème^ siècle estiment surprenante la proportion
d'articles de géographie dans *LGE* trouvée dans les présents travaux au vu de
leur connaissance préalable de l'œuvre, il leur sera possible de reprendre les
données et les outils utilisés pour la mesure afin de recalculer séparément le
résultat; éventuellement d'inspecter les états intermédiaires pour trouver la
source de l'erreur ou confirmer le résultat. Si un doute semblable devait
survenir sur les données de l'*EDdA*, il serait possible de réappliquer les
traitements, mais pas de s'assurer que les données d'entrées seraient les mêmes.
Si un résultat différent venait à être trouvé, il ne serait pas possible de
déterminer l'origine de l'erreur.

Au-delà de ce seul exemple hypothétique, il est légitime de s'interroger sur la
pertinence d'une démarche reproductible dans le cadre des [@=HN] ou, plus
généralement, des Sciences Humaines et Sociales. Les problématiques de
reproductibilité ont émergé dans des domaines où l'informatique occupe une place
centrale, depuis la théorie des langages et la compilation où
@thompson_reflections_1984 démontrait en 3 pages la difficulté à s'assurer de ce
que faisait un programme — même en ayant accès à son code source (!) — jusqu'au
Calcul Hautes-Performances [@courtes_reproduire_2021] où la reproductibilité
joue un rôle clef pour contrôler la cohérence de logiciels déployés sur des
plateformes variées et s'assurer de la validité des résultats qui y sont
obtenus. Dans un contexte où le logiciel occupe une part croissante des travaux
dans de nombreux domaines jusqu'à devenir un élément central de la notion de
preuve, la reproductibilité apporte des garanties de confiance essentielles pour
la recherche. Mais les [@=HN] ne constituent pas un domaine où les résultats
s'obtiennent par application d'un simple calcul sur un ensemble d'entrées. Au
contraire, elles passent par de nombreuses étapes exploratoires réalisées dans
des logiciels dédiés. Ces interfaces (dans le cadre des présents travaux, TXM et
le Lexicoscope sont des exemples évidents) sont conçues pour présenter les
données mais requièrent des interactions humaines pour guider l'analyse. Il
serait donc vain de vouloir automatiser de bout en bout une chaîne de traitement
en [@=HN] jusqu'à la rendre reproductible et vérifiable de manière automatique
sur une plateforme d'intégration continue; toutefois, la reproductibilité peut
constituer un atout précieux pour les [@=HN] et les présents travaux doivent
reconnaître ses apports.

#### Principes {#sec:principles}

Puisque la rediffusion de la totalité des données de cette thèse est
inaccessible, il faut revenir aux définitions des concepts élémentaires en
reproductibilité pour savoir ce qu'il est possible d'atteindre. La
«reproductibilité» représente la possibilité de reproduire le plus exactement
possible une expérience, en appliquant la «même» analyse aux «mêmes» données.
Malheureusement, la difficulté à s'accorder sur le sens à donner à ces deux
identités a pour conséquence une inhomogénéité de la terminologie
[@plesser_reproducibility_2017]. Pour [@rougier_sustainable_2017], «reproduire»
une expérience signifie la reconstituer exactement, en utilisant exactement les
mêmes outils et logiciels sur un jeu de données identique jusqu'au dernier
octet, alors que «répliquer» tolère des données équivalentes. Cette définition
est en contradiction avec celle des sciences expérimentales mais tend à
s'imposer dans les sciences computationnelles.

Les deux concepts sont dans une relation ambivalente. Il est plus difficile
techniquement et donc plus contraignant de concevoir une expérience en
garantissant qu'elle sera reproductible, ce qui témoigne d'un contrôle plus fin
sur la totalité de ses paramètres. Cependant une expérience réplicable peut
posséder une plus grande force de preuve: un résultat peut apparaître simplement
à cause d'une erreur dans les données, d'une intéraction particulière entre code
et données, ou tout simplement de vraies données tout à fait particulières et
qui ne se généralisent pas. En appliquant les mêmes analyses sur de nouvelles
données indépendantes, une seconde équipe qui réplique une expérience augmente
la confiance que la communauté scientifique peut accorder à un résultat.
@schloss_identifying_2018 distingue aussi la «robustesse» qui à l'inverse
consiste à appliquer des analyses différentes aux mêmes données. Enfin, une
expérience est «généralisable» quand il est possible de parvenir à des
conclusions similaires en appliquant des analyses différentes de l'expérience
initiale à d'autres données. Ces quatre critères, résumés au tableau
\ref{table:reproducibility_4_flavours} ne s'excluent pas mais sont idéalement à
cumuler pour produire des recherches dignes de confiance.

Les seules expériences de cette thèse pouvant viser à la reproductibilité sont
celles concernant *LGE* exclusivement. Toutes celles qui intègrent l'*EDdA* ne
peuvent par construction viser qu'à la réplicabilité. En pratique, étant donnée
la part cruciale qu'a jouée l'*EDdA*, notamment dans l'entraînement des modèles
de classification automatique (voir le chapitre \ref{sec:domains_classification}
dans son ensemble) préalable aux analyses contrastives menées au chapitre
\ref{sec:contrasts}, il faut viser à la réplicabilité sur l'ensemble des
travaux. Il serait intéressant de tester la robustesse des résultats produits
sur *LGE* en appliquant d'autres méthodes de classification aux articles de
cette encyclopédie.

::: {}

                     mêmes données   données différentes
------------------- --------------- --------------------
même analyse         Reproductible   Réplicable
analyse différente   Robuste         Généralisable

: Quatre notions différentes de la reproductibilité
\label{table:reproducibility_4_flavours}

:::

Mais la reproductibilité (au sens large à nouveau, hors de celui défini par le
tableau \ref{table:reproducibility_4_flavours}) peut également apporter aux
phases d'investigation des [@=HN]. La nature irrégulière des objets d'étude gêne
le développement de traitements s'appliquant parfaitement à toutes les données.
À l'échelle mésoscopique du présent corpus — la taille proverbialement grande
des encyclopédies empêche une étude qualitative d'une des œuvres en détail, à
fortiori de deux, mais les volumes de données en jeu restent très largement
inférieurs aux mégadonnées générées par les collectes automatiques de
dispositifs numériques — il faut considérer que tout ce qui peut arriver
arrivera. En ce qui concerne les désignants par exemple, il suffit quasiment
d'envisager l'existence d'une régularité et de tenter de la capturer dans un
motif pour qu'il soit contredit par une donnée du corpus, qu'elle ait réellement
été produite telle quelle par un choix ou une erreur humaine ou qu'elle émerge à
cause des bruits générés par les imperfections des différentes étapes de
traitement (vieillissement du papier et de l'encre, qualité de la numérisation,
performances de l'[@=OCR]…). Ainsi, dans ce type d'étude il s'agit en permanence
de placer un curseur entre ce qui est acceptable pour répondre à une question
précise et ce qui est perfectible en vue d'études futures. Trouver cet équilibre
engendre un va-et-vient continu entre traitement des données et analyses. Pour
cette raison, les choix faits dans la représentation du corpus et en particulier
dans la conception des métadonnées favorisent la plus grande flexibilité, en
tâchant de minimiser l'effort nécessaire pour relancer les traitements affectés
par un changement dans une donnée. Cette stratégie repose sur le constat
empirique qu'il est vain d'espérer que la première version sera la bonne et
qu'il vaut mieux considérer toutes les données en aval dans la chaîne de
dépendance comme temporaires, susceptibles de mises à jour et donc jetables. La
détermination d'un ensemble de domaines de connaissance présentée à la
sous-section \ref{sec:domains_build_classes}
p.\pageref{sec:domains_build_classes} d'une façon très linéaire résulte en
réalité de ce type de négociation entre code et données. Naturellement, pour que
ces allers et retours conduisent à un processus d'amélioration continue plutôt
qu'à tourner en boucle, il est crucial de pouvoir contrôler finement ce qui
change ou pas et de pouvoir s'assurer qu'un seul paramètre du traitement a été
modifié à chaque nouvelle tentative pour mesurer son effet propre et pas par
exemple celui d'un changement dans une librairie dont dépend le programme
utilisé. Cela permet de valider une amélioration et de garantir qu'il n'y a pas
de régression dans la qualité des données. C'est ce type de garanties que peut
apporter la reproductibilité des logiciels.

Si des «instantanés» de *LGE* à plusieurs étapes clefs des traitements sont
distribués via Nakala pour faciliter la prise en main des données, toutes
peuvent être regénérées localement (et donc vérifiées) en appliquant les mêmes
traitements sur les pages [@=OCR]isées au format XML-ALTO, seules réelles
entrées de toute la partie du corpus qui concerne cette œuvre. Le logiciel
`soprano`[^soprano] utilisé pour extraire les articles et les encoder en format
texte ou en XML est distribué sous licence libre (BSD 3) et empaqueté pour le
gestionnaire de paquets reproductible Guix[^guix]. Un autre dépôt[^outillage]
contient tous les outils utilisés pour les différentes analyses et la production
des formats enrichissant les données (classification, annotation syntaxique,
etc.). Le développement de ces outils a aussi donné lieu à plusieurs librairies
qui sont bien sûr elles aussi distribuées sous licence libre (GNU GPL v3 pour
`ghc-geode`[^ghc-geode] et `geopyck`[^geopyck]). Les outils spécifiques à
l'*EDdA*, développés antérieurement au projet GEODE sont pour rappel (voir la
section \ref{sec:corpus_preprocessing_edda}) aussi disponibles[^edda-clinic].
L'ensemble de ces logiciels est empaqueté pour Guix et ces paquets sont
distribués dans un canal Guix spécifique au projet[^geode-packages] afin de les
regrouper et de faciliter leur installation. Enfin, les mêmes efforts de
reproductibilité ont été appliqués à ce manuscrit lui-même. Ses sources sont
ouvertes et disponibles[^manuscrit] (sous licence GNU GPL v3). Le dépôt permet
de recompiler une version PDF du texte que vous êtes en train de lire mais
surtout, il contient les données utilisées pour tracer la plupart des tableaux
et figures (celles utilisant un thème de couleur mauve ou une palette
arc-en-ciel pour les domaines de connaissance). Il est ainsi possible de
vérifier les résultats annoncés et d'étudier les codes utilisés pour les
générer.

[^guix]:
    [https://guix.gnu.org/](https://guix.gnu.org/)

[^outillage]:
    [https://gitlab.liris.cnrs.fr/abrenon/outillage](https://gitlab.liris.cnrs.fr/abrenon/outillage)

[^ghc-geode]:
    [https://gitlab.liris.cnrs.fr/geode/ghc-geode](https://gitlab.liris.cnrs.fr/geode/ghc-geode)

[^geopyck]:
    [https://gitlab.liris.cnrs.fr/abrenon/geopyck](https://gitlab.liris.cnrs.fr/abrenon/geopyck)

[^geode-packages]:
    [https://gitlab.liris.cnrs.fr/geode/geode-packages](https://gitlab.liris.cnrs.fr/geode/geode-packages)

La longueur de ce chapitre reflète dans une large mesure le temps consacré dans
cette thèse au traitement des données en vue de constituer le corpus d'étude. À
partir des œuvres, dont la lecture et l'étude directe ont constitué un point de
départ utile pour le reste des travaux, une réflexion a été développée sur les
points communs structurels des articles dans le corpus et la notion clef de
domaine de connaissance. Les besoins spécifiques à *LGE*, notamment en matière
d'encodage, ont amené à identifier les différences intrinsèques qui séparent les
encyclopédies des dictionnaires et à formuler des choix pour représenter les
articles dans des fichiers numériques. Ces décisions ont été mises en
perspective de l'utilisation des données qui est faite aux chapitres
\ref{sec:domains_classification} et \ref{sec:contrasts} et de la démarche
reproductible qui sous-tend l'ensemble de cette thèse.

