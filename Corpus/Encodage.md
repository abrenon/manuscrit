## Encodage des fichiers {#sec:corpus_lge_encoding}

### Des différences entre Encyclopédies et Dictionnaires {#sec:dictionaries_vs_encyclopedias}

Les tomes obtenus de l'[@=ARTFL] sont encodés dans le format XML-TEI (voir la
section \ref{sec:EdlA_encoding}). Pour *LGE*, puisque la version numérique a été
créée à partir d'une ré-OCRisation de l'œuvre, il a été nécessaire d'encoder les
articles. Il est naturel, à la suite de l'ARTFL de diffuser le texte de *LGE*
encodé dans le format XML-TEI (voir la section \ref{sec:EdlA_encoding}
p.\pageref{sec:EdlA_encoding}). Mais ce choix ne suffit pas à définir
directement un encodage car les recommandations publiées contiennent un très
grand nombre de balises aux sémantiques précises et aux règles de composition
complexes. Le module *dictionaries* identifié à la section \ref{sec:xml_tei}
n'est ainsi pas utilisé par l'ARTFL dans son encodage de l'*EDdA*. Il apparaît
naturel d'utiliser le même schéma d'encodage mais la sous-section
\ref{sec:corpus_preprocessing_edda} vient de montrer qu'il n'est pas sans
défauts, c'est pourquoi une étude systématique a été menée pour choisir un
schéma d'encodage pour les articles de *LGE*.

Ainsi, il y a par exemple plusieurs balises utilisées pour délimiter une unité
structurelle de texte. La plus générale est `div` mais elle n'a été ajoutée qu'à
partir de l'itération 0.6 de la version P5 des recommandations de la TEI.
Historiquement il y avait une collection de variantes de cette balise intégrant
une notion de profondeur fixe: `div1`, `div2`, etc. jusqu'à `div7`. Les bonnes
pratiques recommandent d'utiliser soit seulement des `div` (sans numéro) à
toutes les profondeurs soit seulement leurs équivalents numérotés mais de ne pas
mélanger les deux. Les tomes de l'*EDdA* obtenus via l'*ARTFL* utilisent encore
des balises numérotées `div1` etc. mais la profondeur importante de certains
articles de *LGE* est un argument contre l'emploi d'un jeu de balises dont la
profondeur est limitée car avant de disposer d'une version numérique il est
impossible de savoir quel article a la structure la plus profonde et si cette
profondeur est bien inférieure à 7. Cette partie, en reprenant et étendant des
travaux exposés dans @brenon_encoding_2024, explore les possibilités et les
écueils mis en évidence page \ref{sec:EdlA_tei_limits} à la section
\ref{sec:EdlA_tei_applications} pour définir un schéma d'encodage approprié à
*LGE*.

#### Outils pour une exploration systématique {#sec:graph_tools}

La version de la [@=TEI] utilisée pour cette étude (4.3.0, correspondant à la
révision [b4f72b1ff](https://github.com/TEIC/TEI/commit/b4f72b1ff)) contient 590
éléments[^587], documentés sur le site web du consortium. Avec une moyenne de
presque 80 éléments pouvant être inclus sous un élément donné (79.91), parcourir
manuellement les pages de la documentation peut se révéler très long et
difficile à faire de manière exhaustive à cause de l'explosion combinatoire du
nombre de chemins possibles à partir d'un nœud à chaque étape supplémentaire.

[^587]: la section \ref{sec:xml_tei} p.\pageref{sec:xml_tei} fait état de 587
    éléments: depuis la version 4.3.0, 6 éléments ont été retirés des
    spécifications, et 3 nouveaux ajoutés: `<eventName/>`, `<gender/>` et
    `<post/>`. Ces éléments sans lien avec les encyclopédies n'appartiennent pas
    au module *dictionaries*  et ne remettent donc pas en cause les résultats
    présentés.

Aussi, pour aller plus loin que les remarques préliminaires de la section
\ref{sec:EdlA_tei_applications} sur les difficultés prévisibles pour encoder une
encyclopédie à l'aide du module *dictionaries*, le problème peut être
avantageusement transformé en un graphe pour bénéficier des nombreux résultats
existants sur ces objets. L'ensemble des éléments de la TEI est ainsi représenté
par un graphe orienté dont les nœuds sont les éléments XML disponibles en [@=TEI]
et dont les arêtes représentent la possibilité d'inclure un élément
correspondant à leur nœud de destination directement sous un élément
correspondant à leur nœud source. Il est alors possible d'utiliser des méthodes
classiques et éprouvées comme l'algorithme de @dĳkstra59 qui permet de calculer
le plus court chemin entre deux nœuds d'un graphe et de savoir s'ils ne sont pas
connectés pour tester de manière systématique toutes les manières dont les
éléments peuvent être combinés sans risque d'oubli.

Il n'y a évidemment aucune garantie que le plus court chemin ait un sens en
termes de schéma d'encodage mais l'algorithme fournit au moins un moyen efficace
de vérifier s'il est seulement possible d'inclure un élément sous un autre et
donne une longueur minimale nécessaire pour réaliser l'inclusion. Cette méthode
trouve donc tout son intérêt pour obtenir des résultats négatifs. L'absence
totale de chemin entre deux nœuds prouve l'impossibilité d'une inclusion entre
eux; une profondeur anormalement élevée est un indice sérieux qu'une inclusion
ne devrait pas être possible et n'est qu'un accident émergeant des règles du
schéma sans sens réel en termes d'encodage.

Le graphe du schéma XML-[@=TEI] est donc défini formellement de la manière
suivante. Un nœud est créé pour chacun des 590 éléments présents dans les
spécifications. Puis une arête est créée d'un nœud source `a` vers un nœud
destination `b` si et seulement si le schéma stipule qu'un élément de la classe
représentée par `b` peut être contenu directement dans un élément de celle
représentée par `a`. De manière équivalente, il est possible de dire que les
arêtes du graphe représentent la relation "admet pour enfant direct",
c'est-à-dire que — toujours sous l'hypothèse d'une arête d'un nœud `a` vers un
nœud `b` dans le graphe — l'arbre XML de la figure \ref{fig:xml_sample_ab} est
valide selon le schéma.

![Traduction sous forme d'arbre XML de l'existence d'une arête `a` $\rightarrow$ `b` sur le graphe](figure/ab.png){#fig:xml_sample_ab width=10%}

Le mot «élément» est ici employé dans le même sens que dans la documentation de
la TEI pour désigner l'appareil conceptuel caractérisé par un nom de balise tel
que `p` ou `div` pris pour la classe de tous les éléments ayant ce nom, et non
pas une de ses instances particulières qui pourrait se trouver dans un document
précis. La figure \ref{fig:dictionaries-subgraph} en montrant le graphe obtenu
par cette transformation restreint au seul module *dictionaries* laisse imaginer
la complexité de la totalité des spécifications du format XML-TEI.

![Le sous-graphe du module *dictionaries*](figure/dictionaries.png){#fig:dictionaries-subgraph}

##### Définitions

Une des premières notions définies en théorie de graphe est celle de chemin
entre deux nœuds, obtenue en considérant la fermeture transitive de la relation
binaire représentée par les arêtes: formellement, on dit que deux nœuds `a` et
`b` sont reliés par un chemin s'il y a une arête entre les deux ou bien s'il y a
une arête de `a` à un troisième nœud `c` tel que `c` et `b` sont reliés par un
chemin. Étant donnée la relation choisie pour définir le graphe de la TEI, cette
notion de chemin traduit la possibilité d'inclure un élément sous un autre
directement ou indirectement.

Les nœuds visités sur le graphe le long d'un chemin représentent les éléments
XML intermédiaires qui sont nécessaires pour produire un arbre XML valide suivant le
schéma TEI. Au vu de la sémantique descendante associée à ces arbres, il est
pertinent d'appeler la longueur d'un tel chemin d'inclusion sa *profondeur*. La
possibilité d'inclure un élément directement sous lui-même correspond à la
présence de boucle sur le graphe (c'est-à-dire une arête d'un nœud vers
lui-même) et trouve une illustration dans la TEI avec l'élément `<abbr/>`: une
`<abbr/>` (abréviation) peut directement en contenir une autre.

En généralisant l'idée à des chemins d'inclusion de n'importe quelle longueur
supérieure à 1 on obtient la notion de *cycle*. L'élément `<address/>` en fournit
un exemple: bien qu'une `<address/>` ne puisse pas en contenir directement une
autre, elle peut en revanche contenir un `<geogName/>` qui, à son tour, peut
contenir une nouvelle `<address/>`. D'un point de vue de théorie des graphes, on
dira que cet élément admet un cycle d'inclusion de longueur 2.

##### Applications

Intuitivement, la pertinence du plus court chemin entre deux nœuds diminue quand
la distance entre deux éléments dans le graphe augmente, mais elle peut s'avérer
utile pour fournir des suggestions lorsque l'on découvre les modules de la TEI.
En effet la documentation présente une vision locale des nœuds du graphe car la
page d'un élément décrit les éléments admissibles immédiatement au-dessus ou
en-dessous du premier mais ne peut donner que quelques exemples d'arbres XML qui
le contiennent (la combinatoire rend impossible l'intégration d'une liste
exhaustive d'exemples d'utilisation même sur seulement deux niveaux). Si ceux
fournis ne correspondent pas à la structure que l'on souhaite encoder, il peut
donc être difficile de comprendre comment un élément peut être placé sous un
autre.

C'est dans ce type de situation que l'algorithme du plus court chemin permet de
«découvrir» que pour inclure une `<pos/>` (pour représenter une «partie de
discours» ou [@=POS] en anglais) dans une `<entry/>` représentant un article de
dictionnaire, il faut passer par une balise comme `<form/>`, `<cit/>`,
`<dictScrap/>` ou `<gramGrp/>` mais que cela ne peut pas se faire directement.
En effet, le parcours exhaustif de tous les chemins reliant les deux nœuds
correspondants sur le graphe ramène les chemins `entry-form-pos` et
`entry-gramGrp-pos`. Il revient à la personne en charge de l'annotation de
choisir le chemin le plus approprié en fonction de sa pertinence dans le
contexte.

Un dernier exemple éclairant sur l'usage de cette méthode peut être trouvé en
recherchant le plus court chemin d'inclusion d'un `<pos/>` sous le `<body/>`
d'un document: il passe directement par un `<entryFree/>` (c'est un chemin de
longueur 2, `body-entryFree-pos`) qui, à la différence d'`<entry/>` l'accepte
directement sans nœud intermédiaire. Ce pourrait ne pas être le chemin attendu,
selon que d'autres éléments grammaticaux dans l'article tels qu'un `<case/>` ou
un `<gen/>` justifient le recours à un `<gramGrp/>`, mais en augmentant le
périmètre de recherche jusqu'aux chemins de longueur 3, on trouve comme attendu
le chemin passant par `<entry/>`, parmi d'autres. Pour qui découvrirait ces
balises, une image commencerait à apparaître au vu de ces chemins: `<pos/>`
n'est pas un élément très profond, il peut apparaître près de la «surface» des
articles, dont les «points d'entrée» possibles depuis le reste du graphe sont
les balises `<entry/>` et sa version moins contrainte `<entryFree/>`.

#### Limites du module *dictionaries* {#sec:dictionaries_limits}

Les encyclopédies se distinguent des dictionnaires par un certain nombre de
traits comme la présence de désignants marqués typographiquement, le découpage
en paragraphe rendu nécessaire par des articles parfois très longs — l'article
EUROPE (La Grande Encyclopédie, T16, p.782) s'étend ainsi sur 64 pages comme
l'illustre la figure \ref{fig:europe_span} — et très structurés. Les outils
d'exploration systématique de graphes mis en place ci-dessus permettent
d'identifier plusieurs faiblesses dans le module *dictionaries* pour encoder les
éléments caractéristiques des encyclopédies identifiés ci-dessus.

![Article EUROPE dans *LGE*, T16 s'étendant de la p.782 à la p.846](figure/text/LGE/europe_t16.png){#fig:europe_span}

##### Désignants {#sec:encoding_designants}

Introduits à la section \ref{sec:encyclopedia_anatomy}, les désignants sont
utilisés pour situer les articles dans un espace organisé de connaissances. Dans
les œuvres du corpus, ils sont marqués formellement en apparaissant
immédiatement après la vedette entre parenthèses et souvent abréviés. Comme cela
a été montré à la figure \ref{fig:loveling_lge}, le même dispositif
typographique est utilisé pour les entrées biographiques dans *LGE* afin de
donner le prénom de la personne dont elles traitent. Cependant, tous les emplois
ne se ramènent pas à l'un de ces deux cas d'usage. L'article BALAGRUS (La Grande
Encyclopédie, T5, p.48) reproduit à la figure \ref{fig:lge_balagrus} et extrait
de la même page que la figure \ref{fig:anatomy_outside_top} de la section
\ref{sec:encyclopedia_anatomy} p.\pageref{fig:anatomy_outside_top} montre ainsi
un exemple où le mot entre parenthèses n'est pas le prénom de la personne décrite
par l'entrée mais une répétition du nom qui sert de vedette dans son alphabet
grec d'origine: «BALAGRUS (Βάλαγρος)». Il existe potentiellement d'autres
articles pour lequel le «désignant» typographique sert à donner encore une autre
information.

![Article BALAGRUS dans *LGE*, T5, p.48](figure/text/LGE/balagrus_t5_p48.png){#fig:lge_balagrus width=60%}

Pour représenter ces éléments (les vrais désignants et les autres qui prennent
leur place), aucune solution parfaite n'émerge. Le module *dictionaries* fournit
`<usg/>` dont la sémantique correspond bien à la fonction première des
désignants et qui a été utilisée par exemple par @salgado_morais_2024 [p.139].
En revanche, utiliser cette balise pour représenter des prénoms comme le fait
*LGE* nécessiterait l'emploi des valeurs `comp` (complément typique de la
vedette) ou `colloc` (collocation typique de la vedette) pour l'attribut `type`
si l'on considère que le prénom d'un personnage historique représente seulement
une information que l'on apprend à associer directement à son nom par un
mécanisme mémoriel semi-volontaire. Cela supposerait tout de même de réussir à
distinguer les différents cas de manière automatique (voir la section
\ref{sec:automatic_processing_constraints} plus loin, en particulier à partir de
la page \pageref{sec:automatic_processing_constraints}). En l'absence d'une
liste exhaustive des domaines ou des prénoms apparaissant dans ses pages et
compte tenu de la subtilité des sémantiques associées à certains objets (le fait
par exemple que «Βάλαγρος» à la figure \ref{fig:lge_balagrus} ci-dessus ne soit
pas un prénom mais l'orthographe originelle du nom en grec), cette opération
semble pour le moins délicate à réaliser de manière satisfaisante et sans
erreurs. Il serait également possible de ne pas chercher à distinguer les cas et
d'utiliser systématiquement la valeur `hint` pour cet attribut mais en ce cas,
autant employer directement une balise traduisant simplement le choix
typographique plutôt qu'une sémantique précise. Hors du module, l'élément
`<domain/>`, malgré son nom, ne peut se trouver que dans l'en-tête d'un document
et couvre seulement le contexte social d'un texte et pas son sujet. Le nom
d'`<interp/>` peut aussi prêter à confusion puisqu'il ne sert pas à étiqueter un
groupe de mots comme une interprétation possible donnant un contexte (ce qu'il
serait défendable d'argumenter pour un désignant qui, placé en tête d'article,
oriente l'esprit de la personne qui le lit vers un sujet particulier). Au
contraire, la documentation montre clairement qu'il s'agit d'un outil
d'annotation pour ajouter du texte qui n'est pas présent dans le document
original mais qui est le résultat d'une analyse effectuée dans le cadre de
l'annotation.

##### Structures imbriquées

Les encyclopédies se caractérisent également par des développements parfois
longs et très structurés avec des sections et sous-sections imbriquées jusqu'à
des profondeurs importantes. *LGE* n'échappe pas à la règle, le discours se
structurant sur au moins 5 niveaux par exemple dans l'article EUROPE. Certains
passages contiennent même un niveau supplémentaire sous la forme d'énumérations,
comme l'illustre la figure \ref{fig:europe_list}.

![Énumération dans l'article EUROPE, *LGE*, T16, p.783](figure/text/LGE/europe_t16_liste.png){#fig:europe_list width=60%}

La figure \ref{fig:lge_construction} extraite de l'article CONSTRUCTION (La
Grande Encyclopédie, T12, p.732) montre ainsi le début de la partie «III.
Jurisprudence» de l'article, dans lequel s'ouvre un «I. \textsc{Droit
d'accession}» à l'intérieur duquel on trouve un «1°». Ce dernier, bien que la
typographie ne le marque que faiblement, sans paragraphe séparé, sans titre et
survenant au milieu d'une phrase après des «:» constitue pourtant bien une unité
structurelle du texte à la portée extra-phrastique puisqu'on voit sur la figure
\ref{fig:lge_construction_start} cette phrase se terminer, et que le «2°»
visible sur la figure \ref{fig:lge_construction_end} n'apparaît que plus bas sur
la même page après plusieurs autres phrases. Cette question de la typographie
est d'ailleurs importante puisque les paragraphes peuvent autant servir à
marquer une sous-division du texte qu'à simplement aérer un passage long ou à
marquer la succession de plusieurs idées comme le fait le 3^ème^ paragraphe
visible sur la figure \ref{fig:lge_construction_start} ou même celui de la
figure \ref{fig:action}. Même les listes comme celles visible à la figure
\ref{fig:europe_list} bien qu'elles ne soient pas écrites verticalement avec un
élément par ligne (pour des raisons évidentes de compacité de l'information
requise par les encyclopédies) pourraient tout à fait être encodées à l'aide de
la balise TEI `<list/>` dont l'attribut `@rend` inclut la valeur `inline` pour
rendre compte de ce choix de présentation.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/LGE/construction_t12_start.png}
        \caption{Début de la section III. Jurisprudence}
        \label{fig:lge_construction_start}
    \end{subfigure}
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/LGE/construction_t12_end.png}
        \caption{Fin du 1° et début du 2° quelques lignes plus bas}
        \label{fig:lge_construction_end}
    \end{subfigure}
    \caption{Extrait du début de la section III. Jurisprudence de l'article CONSTRUCTION dans \textit{LGE}, T12, p.734}
    \label{fig:lge_construction}
\end{figure}

Or, malgré plusieurs exemples de connexions entre le module *dictionaries* et le
reste de la XML-TEI, en particulier vers le module *core* (l'élément `<ref/>` en
est un exemple), le module *dictionaries* apparaît dans une certaine mesure
isolé d'éléments structurels importants tels que `<head/>` ou `<div/>`. En
effet, en calculant par un parcours systématique tous les chemins vers ce
dernier depuis les éléments `<entry/>` ou `<sense/>` dont la longueur est
inférieure ou égale à 5, tous les chemins trouvés (au nombre de 8 943 et 38 649
respectivement en excluant les cycles pour éviter les doublons) contiennent soit
un élément `<floatingText/>` soit un élément `<app/>`. Le premier comme son nom
le suggère est utilisé pour encoder du texte qui n'a pas sa place dans le flot
normal du document, par exemple dans le contexte de récits enchâssés. Les deux
exemples fournis dans la documentation en ligne montrent un élément `<body/>`
directement sous le `<floatingText/>`, séparant de manière nette son contenu de
ce qu'il y a à l'extérieur de la balise. La finalité du deuxième, bien que son
nom — abréviation d'«apparatus» — soit moins clair, est d'agréger ensemble
plusieurs versions d'un même extrait par exemple pour dénoter plusieurs lectures
possibles d'un groupe de mots obscur d'un manuscrit ou quand le processus
d'encodage tente de rassembler une version unique d'une œuvre à partir de
plusieurs sources qui diffèrent sur un passage. Dans les deux cas, il semble
clair que ces éléments n'ont pas à être présents systématiquement dans
l'encodage d'un article d'encyclopédique simplement pour pouvoir structurer ses
sections et sous-sections. Ainsi, malgré une connectivité interne plutôt élevée,
le module *dictionaries* ne parvient pas non plus à fournir de quoi représenter
des structures imbriquées récursivement avec `<div/>`.

Les structures enchâssées mises en évidence guident la recherche d'éléments
imbricables pour les représenter. Plus précisément, elles imposent plusieurs
contraintes sur les éléments XML à utiliser: il s'agit de trouver une paire
d'éléments, le premier — représentant tout découpage en section et
sous-section — devant pouvoir contenir lui-même et le deuxième élément, sur
lequel la seule contrainte est d'avoir une sémantique compatible avec la
représentation d'un titre de section. De plus, le premier élément doit aussi
pouvoir contenir plusieurs éléments `<p/>`, la balise de référence pour encoder
les paragraphes d'après la documentation XML-[@=TEI].

En filtrant le contenu du module pour ne retenir que les éléments qui vérifient
les conditions ci-dessus et peuvent être inclus sous un élément `<entry/>` il ne
reste aucun candidat. Relâcher cette contrainte additionnelle en remplaçant
`<entry/>` par `<entryFree/>`, un élément de sémantique similaire mais bien plus
flexible pour s'adapter à des structures de dictionnaire inhabituelles n'apporte
aucune amélioration à ce résultat. Ce manque de candidat impose d'alléger encore
les critères de recherche sur les éléments utilisables. Il se pourrait ainsi
qu'un élément intermédiaire soit nécessaire entre le niveau le plus haut de
l'article (l'élément `<entry/>` dans le scénario initial) et l'élément encodant
les sections et sous-sections. De même, l'imbrication récursive aussi pourrait
nécessiter un élément «compagnon» pour fonctionner (l'élément représentant une
sous-section s'insérant non plus directement sous celui pour une section mais
sous un nouvel élément intermédiaire). En termes de théorie des graphes, il
s'agit de relâcher la contrainte sur l'existence d'une boucle pour considérer
aussi les cycles d'une certaine longueur assez faible (il s'agit quand même de
représenter une inclusion assez directe).

En fixant leur longueur à 3, c'est-à-dire en acceptant un seul élément
intermédiaire dans le chemin d'inclusion entre l'élément qui représentera les
sections et lui-même représentant une sous-section, seuls 21 éléments sont
possibles. Aucun d'entre eux n'apparaît cependant comme une bonne solution
évidente: tous les chemins pour inclure `<p/>` depuis n'importe quel élément du
module *dictionaries* contiennent une `<figure/>` (dont la sémantique montre
clairement son inadéquation: une sous-section n'est pas une figure à l'intérieur
du texte), ou un `<stage/>` (réservé au théâtre) ou bien un `<state/>`
(traduisant une qualité temporaire d'une personne ou d'un endroit, une fois de
plus sans lien avec le contexte d'utilisation requis). Les chemins d'inclusion
vers `<head/>` ou `<title/>` sont aussi décevants. Et, là encore, le
remplacement de `<entry/>` par `<entryFree/>` n'apporte aucune amélioration.
Sans constituer une preuve absolue qu'aucun des éléments envisagés ne pourrait
convenir, il n'en demeure pas moins vrai qu'aucune solution satisfaisante
n'apparaît dans le module, ce qui incite à poursuivre les recherches ailleurs.

Les contraintes sont relâchées encore un peu pour inclure les éléments hors du
module *dictionaries* pour représenter les sections et sous-sections, sous les
mêmes conditions que précédemment, en particulier que l'ensemble représentant
l'article puisse apparaître sous un élément `<entry/>`. Cette nouvelle recherche
ne rapporte que 3 candidats: `<figure/>`, `<metamark/>` et `<note/>`.

Le premier ne convient évidemment pas, comme cela a déjà été remarqué
précédemment. Le but de `<metamark/>` est de transcrire les marques d'éditions
qui apparaissent sur un exemplaire d'une source primaire pour altérer le flot
normal du texte et suggérer une lecture alternative (suppression, insertion,
réordonnancement, toute note laissée à cet effet par un humain sur un livre
physique). Cela n'a pas de rapport avec une section d'un article d'encyclopédie.
Le premier élément qui pourrait ressembler à un candidat sérieux est `<note/>`.
Il est fait pour contenir du texte, dans le but d'expliquer quelque chose et
semble assez général (il n'est pas rattaché à un genre spécifique ni à la
présence d'un objet particulier sur la page). Sa sémantique demeure toutefois un
peu éloignée des contraintes: sa documentation le décrit comme un commentaire
additionnel («*additional comment*»), ce qui semble convenir mais qui apparaît
hors du flot de texte principal («*out of the main textual stream*») alors que
les développements des articles sont précisément la matière des textes
encyclopédiques, pas des remarques en marge ou en bas de page.

Ainsi, le module *dictionaries* semble équipé avec un élément satisfaisant mais
difficile à utiliser pour les désignants (`<usg/>`), mais il n'inclut aucun
élément pour les titres de sections et ne permet pas non plus d'avoir des
structures imbriquées récursivement avec une sémantique satisfaisante, même
encodées par des éléments qui auraient été choisis hors de ce module.

#### Une notion de sens mal définie

Mais au-delà des seules contraintes typographiques, d'autres problèmes
apparaissent en s'intéressant à la sémantique des entrées. Il peut être
difficile de définir des mots sans fournir d'entrées tautologiques
[@haiman_dictionaries_1980, p.330], mais pour certains mots, et en particulier
les noms propres, la notion de sens paraît encore plus insaisissable: il est
possible d'écrire des sommes d'information, même sur plusieurs tomes, sans pour
autant avoir «défini» un sujet (au sens où tout ce texte se rattache à l'objet
mais ne le caractérise pas ni ne fixe ses limites). Il y a ainsi dans les
encyclopédies en général et dans *LGE* en particulier des articles historiques
et des biographies. Si la notion de «sens» peut sembler mal à-propos pour le
récit d'événements historiques, il reste possible d'argumenter qu'il y a bien
mise en relation du concept qui les rassemble avec le nom sous lequel
l'événement est connu. Mais quand il s'agit du récit de la vie d'une personne,
de décrire son rôle dans des événements historiques et ses relations à d'autre
personnes la notion de sens convient encore moins. Une entrée telle que SANJO
SANETOMI (La Grande Encyclopédie, T29, p.441) dont le début est visible à la
figure \ref{fig:sanjo} ne constitue en aucun cas une *définition*.

![Début de l'article SANJO SANETOMI dans *LGE*, T29, p.441](figure/text/LGE/sanjo_t29.png){#fig:sanjo width=60%}

De plus, les encyclopédies, du fait de leur héritage philosophique des
*Lumières*, ne sont pas uniquement des espaces d'assertivité mais intègrent
aussi une composante dialectique intrinsèque. Certains articles posent les bases
requises pour comprendre la complexité d'un problème et susciter à leur lecture
des réflexions sur le sujet sans apporter de réponse définitive. Cela peut aller
jusqu'à l'emploi de points d'interrogation comme dans l'article ACTION (La
Grande Encyclopédie, T1, p.489) dont la figure \ref{fig:action} reproduit un
extrait.

![Extrait de l'article ACTION dans *LGE*, T1, p.495](figure/text/LGE/action_t1.png){#fig:action width=60%}

Dans cet extrait l'auteur invente une situation hypothétique pour montrer à quel
point la démarcation entre deux sous-catégories d'actions juridiques qu'on
pourrait croire mutuellement exclusives est en fait ténue. La motivation
profonde du passage est de transmettre l'idée que le terme est difficile à
définir sans ambiguïtés et nécessite une part d'interprétation. L'encoder dans
un élément `<sense/>`, ou pire un élément `<def/>` serait véritablement un
contrensens. Leur emploi pour encoder des articles encyclopédiques en général et
ceux de *LGE* en particulier n'est donc pas approprié.

Trois caractéristiques fondamentales des encyclopédies au moins rendent donc le
module *dictionaries* impropre à les encoder: la présence de désignants, les
longs développements très structurés et le fait que la notion de «sens» y joue
un rôle moins central que dans les dictionnaires. Même un encodage composite
utilisant des éléments hors de ce module sous une balise `<entry/>` ne permet
pas de remplir les conditions requises.

### Proposition d'un schéma d'encodage {#sec:encoding_proposal}

Pour ces raisons, il faut chercher ailleurs de quoi encoder les articles de
*LGE*. La quête n'est pas longue puisque le module *core* fournit évidemment ce
qu'il faut à travers les balises `<div/>`, `<head/>` et `<p/>` qui ont en plus
l'avantage d'avoir une charge sémantique moindre que `<sense/>` et `<def/>`.
C'est pourquoi l'encodage retenu pour représenter *LGE* se contente d'utiliser
ces balises.

Pour illustrer ce choix sur un cas concret, il est appliqué à l'article CATHÈTE
reproduit à la figure \ref{fig:lge_cathete}. Par souci de concision et de
cohérence avec les remarques ci-dessus, les exemples se limiteront à ce qui se
passe au niveau de l'article, c'est-à-dire à partir de la balise immédiatement
sous l'élément `<body/>` du document. Tout ce qui est lié aux métadonnées se
trouve comme il convient dans un fichier TEI sous l'élément `<teiHeader/>` qui a
tout ce qu'il faut pour les représenter.

![Article CATHÈTE dans *LGE*, T9, p.849](figure/text/LGE/cathète_t9.png){#fig:lge_cathete width=60%}

#### Le schéma complet

En restant à l'intérieur du module *core* pour la structure, tous les éléments
utiles sont disponibles et le schéma d'encodage retenu paraphrase pour
l'essentiel la documentation officielle. Chaque article est représenté par une
balise `<div/>`. Il est conseillé de définir dessus un attribut `xml:id` avec un
identifiant unique pour l'entrée à l'échelle du corpus complet d'étude afin de
pouvoir ensuite gérer les renvois et manipuler les articles sans ambiguïté dans
les différents outils d'exploitation du corpus. Le système d'identification des
articles retenu dans le cadre du projet consiste à simplement les numéroter
séquentiellement par œuvre et par tome, et à concaténer les trois
composantes — en base 10 pour celles qui sont numériques — séparées par le
caractère `_` (tiret bas).

![L'élément conteneur `div` pour l'article CATHÈTE](figure/text/LGE/cathète_0.png){#fig:cathete-xml-0}

À l'intérieur de cet élément doit se trouver un `<head/>` contenant la vedette
de l'article. Un élément `<hi/>` à l'intérieur permet le cas échéant de
traduire le fait qu'une typographie particulière est utilisée pour la mettre en
valeur (comme du gras ou des petites majuscules).

Aucun élément satisfaisant n'a été trouvé pour représenter les désignants. Le
meilleur candidat est la balise `<usg/>` dans le module *dictionaries* mais elle
ne peut pas apparaître sous un élément `<head/>`[^head] d'après le schéma de la
TEI. Tous les chemins d'inclusion de longueur inférieure ou égale à 3
contiennent des éléments inappropriés (`<cit/>`, `<figure/>`, `<castList/>` et
`<nym/>`) et doivent donc être écartés. Les autres possibilités incluent
`<term/>` (imprécis) et `<rs/>` («referring string» soit «chaîne de caractères
faisant référence», une formulation très générale qui pourrait convenir — après
tout les désignants font référence à un domaine de connaissance — mais tous les
exemples fournis dans la documentation sont des entités nommées ou des chaînes
de référence, des contextes assez différents de l'usage souhaité pour
représenter des objets abstraits apparaissant seulement une fois en début
d'entrée). Le meilleur compromis est de s'en tenir à des caractéristiques
objectives quand elles sont présentes (souvent abréviées, un élément `<abbr/>`
peut être approprié). Pour les prénoms de personnes remplaçant dans *LGE* les
désignants pour les notices biographiques, l'élément `<persName/>` convient. La
figure \ref{fig:cathete-xml-1} représente l'en-tête de l'article CATHÈTE en
tenant compte de ces remarques.

[^head]: le désignant fait partie de l'en-tête de l'article et c'est pourquoi le
    choix privilégié exploré ici est de le place sous la balise `<head/>`, mais
    un `<usg/>` ne peut pas apparaître non-plus ni dans un `<p/>` ni même
    directement dans un `<div/>`

![L'encodage de la vedette et du désignant de l'article CATHÈTE](figure/text/LGE/cathète_1.png){#fig:cathete-xml-1}

Ensuite, chaque sens est encodé par un élément `<div/>` dont l'attribut `type`
est défini à la valeur `sense` en référence à l'élément `<sense/>` du module
*dictionaries* qui aurait été employé. Cette balise devrait être numérotée à
partir de 0 en fonction de son rang parmi les différents sens couverts par
l'article et présente même si la vedette n'en a qu'un seul comme c'est le cas
pour l'article CATHÈTE à la figure \ref{fig:cathete-xml-2}.

![La structure vide permettant de représenter le seul sens du mot CATHÈTE](figure/text/LGE/cathète_2.png){#fig:cathete-xml-2}

De plus, chaque ligne de l'article doit commencer par un élément `<lb/>`, y
compris avant l'élément `<head/>` comme le présente la figure
\ref{fig:cathete-xml-3}, une façon de souligner l'importance sémantique d'un
retour chariot séparant deux articles dans la mise en page dense des
enyclopédies où la place est rare. Marquer explicitement les débuts de ligne
permet de reconstruire un facsimile mais a également l'avantage de mettre en
avant le fait que la vedette, bien qu'isolée dans sa propre balise, se trouve
sur la même ligne que le reste de l'entrée, une caractéristique que les
encyclopédies partagent avec les dictionnaires.

Pour compléter la structure, les sections et sous-sections apparaissant dans le
corps de l'article sont encodées de la façon habituelle avec des `<div/>`
contenant des `<p/>` pour les paragraphes, qui peuvent éventuellement contenir
des `<head/>` locaux pour représenter leurs titres.

![Un encodage complet de l'article CATHÈTE](figure/text/LGE/cathète_3.png){#fig:cathete-xml-3}

Certains articles comme BOUMERANG (La Grande Encyclopédie, T7, p.704) comportent
des figures avec des légendes (voir la figure \ref{fig:boumerang-photo}) qui
sont encodées de la manière standard avec respectivement les éléments
`<figure/>` et `<figDesc/>` comme dans la figure \ref{fig:boumerang-xml}.

![Figure de l'article BOUMERANG dans *LGE*, T7, p.704](figure/text/LGE/boumerang_t7.png){height=300px #fig:boumerang-photo}

![Encodage de la figure dans l'article BOUMERANG et de sa légende](figure/text/LGE/boumerang.png){#fig:boumerang-xml}

Un autre problème dû à l'abandon de la balise `<entry/>` pour représenter les
articles est que l'élément `<xr/>` ne peut être utilisé dans aucun des éléments
du module *core* du schéma d'encodage choisi. Or, cet élément est utile pour
encoder les renvois, présents dans les encyclopédies comme dans les
dictionnaires, dont un est visible à la figure \ref{fig:gelocus_photo} montrant
l'article GELOCUS (La Grande Encyclopédie, T18, p.699). Il faut donc utiliser à
la place l'élement `<ref/>`, disponible à l'intérieur d'un `<p/>`. Son attribut
`target` doit avoir pour valeur le `xml:id` de l'article vers lequel il pointe,
préfixé d'un dièse (`#`) comme l'illustre la figure \ref{fig:gelocus-xml}. Cette
solution a été préférée au fait d'introduire un élément `<dictScrap/>` pour
pouvoir placer un `<xr/>`, parce que cela suggérerait qu'il s'agit d'un insert
de contenu dictionnairique dans un contexte qui ne l'est pas, alors que la
lourdeur d'encodage ajoutée est purement artificielle et ne provient que de
l'impossibilité d'utiliser le module *dictionaries*.

![Article GELOCUS dans *LGE*, T18, p.699](figure/text/LGE/gelocus_t18.png){#fig:gelocus_photo width=60%}

![Encodage des renvois dans l'article GELOCUS](figure/text/LGE/gelocus.png){#fig:gelocus-xml}

Les éléments péritextes évoqués précédemment à la section
\ref{sec:encyclopedia_anatomy} et qui apparaissent sur les pages d'encyclopédies
notamment pour apporter de l'information sur la position de la page (son rang et
la plage de vedettes qu'elle couvre) peuvent être encodés par des balises
`<fw/>` (pour «forme work»). Leurs attributs `place` et `type` devraient être
renseignés pour les positionner sur la page et identifier leur fonction si
celle-ci a pu être déterminée (ce qui n'est pas toujours le cas car ces éléments
brefs et placés sur les bords des pages sont justement ceux qui subissent le
plus souvent des dégâts quand le papier s'abîme et sont en conséquence mal lus
par l'[@=OCR]).

Enfin, d'autres éléments TEI s'avèrent utiles pour représenter des «événements»
qui surviennent au fil du texte comme le début d'une nouvelle colonne de texte
ou d'une nouvelle page. La figure \ref{fig:alcala-photo} montre ainsi le haut de
la gauche de la dernière page du 1^er^ tome de *LGE* (visible dans son ensemble
à la figure \ref{fig:lge_last_page_photo} dans la section
\ref{sec:corpus_preprocessing_lge} p.\pageref{fig:lge_last_page_photo}) qui
comporte des éléments péritextes et constitue un début de page. Les éléments
habituels (`<pb/>` pour le début de page, `<cb/>` pour le début de colonne)
peuvent et doivent être utilisés comme le propose la figure
\ref{fig:alcala-xml}.

![Extrait de l'article ALCALA-DE-HÉNARÈS dans *LGE*, T1, p.1200](figure/text/LGE/last_page_top_left_t1.png){width=70% #fig:alcala-photo}

![Encodage d'un début de page dans l'article ALCALA-DE-HÉNARÈS](figure/text/LGE/alcala.png){#fig:alcala-xml}

#### Les contraintes d'un traitement automatique {#sec:automatic_processing_constraints}

L'encodage dont il est ici question a été développé pour être appliqué par
`soprano` pour encoder les articles extraits du flot brut des pages. Cette
opération ne pouvait bien sûr qu'être automatisée devant le grand nombre
d'objets à traiter: des 37 797 pages, `soprano` extrait 134 820 articles. Le
standard XML-TEI est un outil très large permettant des usages très divers mais
certains de ses éléments comme `<unclear/>` ou `<factuality/>` permettent de
représenter des informations sémantiques très abstraites (la deuxième touche à
la notion de vérité même) nécessitant une compréhension très fine de
l'intégralité du texte et sur laquelle même des experts humains pourraient être
en désaccord. Il n'est bien sûr pas souhaitable que de telles considérations
soient laissées à un processus automatique.

Pour cette raison, le choix d'un schéma d'encodage pour *LGE* a été
principalement contraint par le besoin de ne prendre en compte que des
caractéristiques objectives du texte pouvant être extraites sans intervention
humaine. Des traits comme la position relative des blocs et l'usage d'une
typographie particulière comme le gras ou les majuscules suffisent par exemple à
repérer les vedettes d'articles.

Le cas des renvois est un peu particulier et pourrait apparaître comme un
contre-exemple à ce principe. Pourtant, la pratique consistant à établir des
liens entre articles est tellement fréquente (autant dans les dictionnaires que
dans les encyclopédies) que le processus échappe dans une certaine mesure au
reste du discours et prend une forme particulière et codifiée, entre parenthèses
et après un token précis qui invite à effectuer soi-même la redirection lors de
la lecture. Dans *La Grande Encyclopédie* la très large majorité des renvois a
ainsi lieu entre parenthèses, et le renvoi est suggéré par le verbe «voir»
raccourci à son initiale «V.» comme cela était déjà visible sur la figure
\ref{fig:gelocus_photo}. Quelques très rares exceptions conservent la forme
«voir», «voy.» ou même «voyez» (avec ou sans majuscule) mais il s'agit dans la
plupart des cas de renvois externes, vers d'autres œuvres que *LGE*. Lorsqu'il
s'agit bien de renvois entre articles, c'est que le contexte particulier de ce
renvoi exige une formulation plus explicite qui sort du «lien» codifié
typographiquement pour revenir à une tournure plus construite : par exemple une
reprise anaphorique pour éviter une répétition — «Voy. ce mot» à l'article
COMBATTANT (La Grande Encyclopédie, T11, p.1153) — ou un renvoi multiple — «voir
les mots \textsc{Poste} et \textsc{Télégraphe}» à l'article AFFRANCHISSEMENT (La
Grande Encyclopédie, T1, p.702).

À ces rares exceptions près, il semble donc possible d'utiliser cette structure
très codifiée pour détecter et encoder les renvois, bien que cela n'ait pas
encore été implémenté dans `soprano`. Renseigner correctement l'attribut
`target` de la balise `<ref/>` pourrait en revanche se révéler plus difficile et
nécessiter de traiter les articles en plusieurs étapes, pour d'abord découvrir
toutes les entrées existantes — et ce faisant associer des vedettes aux
identifiants uniques d'articles — avant d'essayer de résoudre les mots après ces
«V.» entre parenthèses. Puisque la segmentation des articles a lieu
indépendamment pour les différents tomes et que les renvois ont une portée qui
dépasse le tome où ils sont utilisés, il n'est pas possible pour `soprano` de
simplement construire cette table associative en mémoire avant de produire les
fichiers et il faudra donc envisager un traitement en plusieurs passes.

La résolution des renvois, si elle doit être implémentée un jour ne devrait donc
pas remettre en question une caractéristique principale de l'encodeur utilisé.
Si des lexicographes pourront reprocher à l'encodage proposé une trop grande
superficialité, il a l'avantage de ne pas nécessiter le stockage de structures
de données trop complexes en mémoire pour une durée longue. L'algorithme
responsable de l'encodage dans `soprano` produit ainsi les articles dès qu'il
peut sans attendre d'avoir une représentation complète de l'article en mémoire.
Dès qu'un bloc est prêt, il est encodé et le XML-TEI correspondant est écrit
dans le fichier de destination. Cela permet de conserver une utilisation de la
mémoire vive raisonnable (de l'ordre de 3Go), ce qui permet de traiter plusieurs
tomes simultanément sur la plupart des machines récentes. Ainsi même en prenant
environ 4min par tome, le temps total requis pour segmenter et encoder la
totalité de *LGE* sur une machine de 16Go de RAM peut rester voisin de 40min en
lançant 3 processus en parallèle (au lieu des $31 \times 4 = 124min = 2h04$
totales nécessaires sans parallélisation).

#### Dans l'implémentation actuelle

L'implémentation de référence pour ce schéma d'encodage est le programme
soprano[^soprano]. Ce logiciel a déjà servi à créer deux versions mais il
n'implémente pas encore toutes les spécifications de l'encodage proposé
précédemment. La figure \ref{fig:cathete-xml-current} montre ainsi l'état actuel
de l'article CATHÈTE à la fin de la phase d'encodage.

![Encodage actuel de l'article CATHÈTE produit par `soprano`](figure/text/LGE/cathète_current.png){#fig:cathete-xml-current}

La détection des vedettes ne permet pas encore de reconnaître les désignants et
c'est pourquoi ils apparaissent à l'extérieur de la balise `<head/>`. Aucun
travail n'est non plus fait pour résoudre les abréviations (ici, par exemple,
retrouver qu'«Archit.» correspond à «Architecture») et les encoder comme telles.
Cela serait pourtant faisable grâce à la liste des abréviations présente au
début du premier tome et qui a été encodée sous un format numérique dans le
cadre du projet[^lge-meta]. Il serait également intéressant de distinguer les noms de
domaines des prénoms de personnes puisque l'ambiguïté existe dans *LGE* pour ce
qui tient typographiquement la place de désignant.

[^lge-meta]: Le fichier correspondant à ces données est distribué dans le dépôt
    [https://gitlab.liris.cnrs.fr/geode/lge-meta.git](https://gitlab.liris.cnrs.fr/geode/lge-meta.git)

\label{lge_encoding_paragraphs}De la même manière, puisque la détection des
titres au début des sections n'est pas complète, aucune analyse de la structure
n'est effectuée pour l'instant sur le texte à l'intérieur de chaque article qui
est gardé sans structure, directement sous l'élément `<div/>` correspondant à
l'entrée plutôt que dans des `<div/>` imbriquées. Les paragraphes ne sont pas
encore identifiés et pour cette raison pas encodés non plus.

En revanche, `soprano` gère déjà correctement les figures et leurs légendes
quand il y en a. L'encodeur garde la trace des lignes, pages et colonnes en
cours et insère les éléments vides correspondants (`<lb/>`, `<pb/>` et `<cb/>`)
lorsqu'une de ces structures s'ouvre. Les pages sont numérotées en fonction de
leur rang dans le document numérique tel qu'il est disponible, qui diffère de
celui inséré par les éditeurs. Il permet donc de savoir à quelle photo de page
se rendre, pas de connaître le nombre écrit en haut de la page correspondante.
La suite des numéros de page est en effet surprenamment difficile à suivre.
D'abord la numérotation ne démarre pas à la première page, les premières pages
avant et après le frontispice n'ont pas de numéro. De plus les cartes
géographiques présentes dans l'œuvre sont imprimées sur des feuilles distinctes
des cahiers en sextodecimo contenant le texte proprement dit avec lesquels elles
sont simplement reliées. Elles ne comportent pas de numéro de page, et la
numérotation se met donc en quelque sorte en pause sur les quelques pages qui
les contiennent. Si elle n'incrémente donc pas simplement de 1 à chaque page en
entrée du processus, il n'est pas non plus facile de la lire directement sur
chaque page sans contexte: en tant qu'élément péritexte, les caractères sont
souvent malmenés par l'[@=OCR] et ne ressemblent parfois pas à des entiers. Pour
les obtenir, il faudrait donc pouvoir croiser plusieurs informations (les
numéros lus sur toutes les pages pour s'assurer que la suite est croissante ou
les marques péritextes des cahiers sextodécimo entre autres) pour espérer
synthétiser une donnée assez fiable. Cela n'est pour l'instant pas implémenté.

### Le mythe du «texte brut» {#sec:text_format}

Parler d'encodage se rapporte généralement — comme c'est le cas dans toute la
sous-section précédente \ref{sec:encoding_proposal} — à des formats hautement
structurés comme les différents dialectes du XML, le JSON ou bien le YAML entre
autres. On oppose fréquemment ces formats à un autre censé représenter le
contenu textuel sans mise en forme aucune, auquel on réfère le plus souvent par
l'expression «texte brut», et qu'on enregistre par convention dans des fichiers
portant l'extension `.txt`[^txt]. Le format texte étant employé pour certaines
études de cette thèse (voir la section \ref{sec:corpus_application_formats}), il
est important de mettre en lumière que, comme les autres formats cités
précédemment, il peut en fait faire l'objet de choix décisifs requiérant des
interprétations sémantiques radicales et l'adoption de conventions: une forme
d'encodage.

[^txt]: on trouve aussi du «text brut» sous d'autres formes, par exemple dans
    une cellule d'un fichier tableur, ou dans une valeur au sein d'un fichier
    JSON; le terme «fichier» (textuel) sera employé par soucis de simplicité
    mais les remarques les remarques faites dans cette section s'appliquent
    aussi à ces autres méthodes de stockage de texte

Pour revenir à la distinction faite à la section \ref{text_polysemy}
(p.\pageref{text_polysemy}), parler de format renvoie au texte en tant que
matière, que quantité indénombrable plutôt que comme une unité sémantique au
sein d'un corpus d'étude. @rastier_textes_1996[p.18] refuse de réduire la notion
de texte à sa seule représentation numérique — une «chaîne de caractères» — mais
la définition qu'il propose à la page 19 s'avère tout de même éclairante sur
cette composante qui représente sa «substance graphique». S'il refuse de limiter
l'usage du mot texte aux seules productions écrites il requiert qu'une «suite
linguistique» soit «fixée sur un support» pour pouvoir parler de texte. Une
version numérique constitue une trace qui permet de fixer un texte et de le
diffuser (partage, copie, sauvegarde).

Le deuxième aspect de la définition de @rastier_textes_1996[*ibid*] insiste sur
l'existence d'un texte dans le cadre d'un pratique sociale. Quel que soit le
support utilisé pour fixer un texte, c'est rarement la trace elle-même mais
plutôt l'objet abstrait qu'elle dénote qui intéresse son lectorat ou les
linguistes. Avant même de considérer quelque niveau d'analyse que ce soit sur le
texte — ni sémantique, ni syntaxique ni même la lemmatisation — plusieurs
particularités physiques de son support sont écartées pour considérer un texte.
Le choix des propriétés du texte à ignorer et la manière dont cette abstraction
a lieu dépend de la pratique sociale dans le cadre de laquelle il a été
produite.

Ainsi on considérera la plupart du temps qu'un texte est «le même» bien qu'il
soit prononcé par deux personnes différentes (avec une voix différente), mais
dans le cadre d'une œuvre où ces voix auraient été associées à des personnages
différents, le changement de voix pourrait alors revêtir une importance. À
l'écrit, la mise en page d'un texte ou la fonte utilisée pour l'imprimer est
généralement ignorée, même si certaines formes littéraires jouent précisément
sur ces paramètres. En termes physiques, et de manière tout à fait analogue avec
la réduction décrite à la section \ref{sec:corpus_preprocessing_lge} du format
XML-ALTO à un flot linéaire de texte, on peut voir cette mise de côté de
certaines propriétés comme une projection visant à réduire les dimensions d'un
texte pour obtenir un objet fondamentalement unidimensionnel. En termes
mathématiques, il s'agit plutôt de classes d'équivalence: la pratique sociale
associée à un texte définit une relation selon laquelle deux traces représentent
«le même» texte. Derrière l'expression «texte brut», il y a la croyance qu'un
«fichier texte» pourrait constituer une trace textuelle canonique du texte qu'il
représente [^2]. Cette section se propose de montrer que cette croyance est
infondée.

[^2]: et donc qu'on pourrait assimiler un texte à cette trace particulière, de
    la même façon qu'on peut parfois noter par abus «$2$» la classe
    d'équivalence de tous les nombres congrus à $2$ pour un certain modulo $p >
    2$, en identifiant cette classe à l'entier $2$ qui en fait partie et
    constitue un représentant fort commode

#### Lignes et paragraphes {#sec:text_lines_paragraph}

La notion d'encodage dans le contexte de format textuel désigne plutôt
d'habitude celui des caractères: l'association à un glyphe d'un code symbolique
et numérique, et d'une séquence d'octets pour le représenter. Ainsi le concept
de la lettre «A», première lettre de l'alphabet latin telle qu'on la trace en
majuscule, reçoit le code arbitraire 65, et est représenté par une séquence d'un
unique octet, `0x41` — représentation électronique de la valeur 65, notée en
base hexadécimale par convention. Il existe différents encodages, incompatibles
entre eux (UTF-8, ISO-8859-1, EUC-JP…), utilisés couramment sur diverses
plateformes mais ce n'est pas de cette distinction dont il s'agit ici. En effet,
il est aisé de préciser l'encodage d'un fichier qu'on distribue (ils sont tous
nommés et spécifiés très précisément), et normalement un seul format est utilisé
au sein d'une même plateforme de traitement. Mais même en supposant une
convention d'encodage universellement acceptée, des problèmes pratiques
demeurent.

La plupart des formats d'encodage étendent en effet le format ASCII
(c'est-à-dire que tout fichier ASCII est par exemple aussi un fichier UTF-8 ou
ISO-8859-1 valide), regroupant la plupart des signes typographiques communs, les
chiffres et les lettres de l'alphabet latin sans diacritiques: tous les glyphes
sont donc représentés par la même séquence d'octets quel que soit l'encodage
utilisé (et en l'occurrence, toutes ces «séquences» ne comportent en réalité
qu'un seul octet, ce pourquoi on identifiera dans ce qui suit le code d'un
caractère et l'octet le représentant). Or, ce sous-ensemble commun représenté
par ASCII est déjà suffisant pour requérir des choix cruciaux dans la
représentation des fichiers texte. Le jeu de caractères ASCII contient en effet
plusieurs façons de séparer les mots: l'espace (`' '`, code `0x20`) mais
également la tabulation (`'\t'`, code `0x09`) et la nouvelle ligne (`'\n'`, code
`0x0a`). En pratique suivant le système d'exploitation les lignes dans un
fichier peuvent être terminées par la séquence `"\n"` (un seul `'\n'`), `"\r"`
(un seul caractère de code `0x0d`) ou `"\r\n"`, ce pourquoi il sera question ici
de «fin de ligne» pour désigner cette séparation sémantique sans préoccupation
pour sa réalisation exacte (qui constituerait un niveau de choix supplémentaire
dans ce qui suit mais sera ignoré car fixé sur la plateforme sur lequel le
traitement a lieu).

Le texte lorsqu'il est affiché pour un humain (comme c'est le cas de ces lignes
dans le PDF ou la version papier que vous êtes en train de lire) est réparti en
lignes d'une largeur raisonnable pour aérer le texte et coïncider avec la taille
des formats de papier usuels. Il est possible dans un fichier texte de former au
contraire des lignes arbitrairement longues, mais ce n'est pas sans présenter
des inconvénients techniques (difficultés pour lire et se déplacer dans le
fichier, lenteur potentielle suivant l'éditeur de texte utilisé). En pratique,
des caractères séparateurs seront utilisés pour isoler des unités indépendantes:
phrases, paragraphes, sections… le niveau auquel se fera cette séparation et la
manière dont elle sera réalisée constituent un premier ensemble de «paramètres
d'encodage textuel» qui sont autant de degrés de liberté pour déterminer la
façon de représenter un texte sous forme numérique. Un premier choix peut
consister par exemple à réserver les fins de ligne à la séparation des
paragraphes, et à conserver tout le texte d'un paragraphe sur une même ligne (ce
qui n'est pas sans poser d'autres difficultés, voir ci-dessous). Un autre choix
peut consister à créer des lignes d'une largeur maximale fixe, comme c'est le
cas à la figure \ref{fig:lge_alpam_text}. Dans ce deuxième cas, la largeur à
utiliser constitue un deuxième paramètre, le nombre de «Caractères Par Ligne»
([@=CPL]), souvent lié au matériel sur lequel ces fichiers sont traités plus
qu'à une intention de la personne qui les édite: les largeurs de lignes
utilisées historiquement sont de l'ordre de 80 [@=CPL], mais la figure
\ref{fig:lge_alpam_text} montre une sorte de facsimile textuel de l'article tel
qu'il apparaît sur la page imprimée de *LGE* (visible juste au-dessus à la
figure \ref{fig:lge_alpam_photo}) dont les colonnes de texte font 50 caractères
sans compter la ponctuation et les espaces fines d'après l'Avant-Propos (La
Grande Encyclopédie, T1, p.XI). Quel que soit le nombre de [@=CPL] choisi, le
contenu réel sauvé dans le fichier se voit encombré de caractères non
significatifs, introduits pour des raisons techniques mais ignorés dès que cela
sera possible à la lecture ou à l'analyse. Sur la figure
\ref{fig:lge_alpam_bin}, il s'agit des caractères `'\n'` (nouvelle ligne)
présents dans la suite d'octets qui représente l'article, surlignés en mauve.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/LGE/alpam_t2_p437.png}
        \caption{Photo de l'article}
        \label{fig:lge_alpam_photo}
    \end{subfigure}
    \begin{subfigure}[b]{0.60\textwidth}
        \verbatiminput{src/article/LGE/alpam.txt}
        \caption{Texte extrait par \texttt{soprano}}
        \label{fig:lge_alpam_text}
    \end{subfigure}
    \begin{subfigure}[b]{0.8\textwidth}
        \includegraphics{figure/text/LGE/alpam_t2_p437_bin.png}
        \caption{Contenu du texte détaillé par octet}
        \label{fig:lge_alpam_bin}
    \end{subfigure}
    \caption{Article ALPAM dans \textit{LGE}, T2, p.437}
    \label{fig:lge_alpam}
\end{figure}

Le choix précédent a ensuite des répercussions sur la séparation entre
paragraphes: en effet, si les fins de lignes ne leur sont pas réservées, il faut
trouver une façon de désambiguïser un nouveau paragraphe et une ligne qui
commencerait simplement par un mot en majuscule. En pratique dans l'*EDdA* comme
dans *LGE*, les paragraphes ne sont pas séparés par des lignes vides mais sont
indentés d'une largeur d'environ 2 caractères. Un choix «facsimile» existe donc
encore pour ce deuxième paramètre: reproduire l'indentation, mais comme pour la
largeur des lignes, cette décision peut donner lieu à plusieurs implémentations
en pratique. La bataille des conventions d'indentation est un lieu commun des
langages de programmation: on peut trouver l'emploi de la tabulation ('`\t`'),
ou d'un nombre fixe d'espaces (généralement 2, 4 voire 8). Rester au plus près de
la mise en page dans le corpus exigerait sans doute le choix d'une tabulation ou
de deux espaces.

#### Récupérer un texte linéaire {#sec:text_linear_format}

En fonction des choix de paramètres d'encodage textuel décrits ci-dessus, un
même texte peut donc donner lieu à différentes traces textuelles. Récupérer un
texte linéaire à partir d'une de ses traces n'est jamais évident et demande
toujours un travail, qu'un humain réalise à la lecture sans y penser mais qui
nécessite des règles précises basées sur la connaissance du choix d'encodage
textuel pour des algorithmes qui tenteraient d'y accéder. Ainsi, le lectorat
voit naturellement une espace entre «*Wallichii*» et «R.» sur la figure
\ref{fig:lge_alpam_text} (alors qu'il n'y en a pas, il n'y a qu'une fin de
ligne), et ignore en revanche le tiret et la fin de ligne pour former le mot
«Aristolochiacées» à la ligne suivante alors que cette séquence n'est pas
présente telle quelle dans le fichier.

\label{sec:text_glueing_lines}Ce problème du recollement des lignes au sein d'un
paragraphe est bien plus difficile qu'il n'y paraît. En première approche, pour
joindre deux lignes il suffit d'ajouter une espace quand le dernier caractère de
la première n'est pas un tiret (`'-'`) et de simplement concaténer la première
privée de son `'-'` final à la deuxième dans le cas contraire. Mais en pratique,
l'élimination du tiret dépend du contexte: il fait partie de certains mots
composés comme «peut-être» ou «pis-aller» et permet de rattacher les enclitiques
au mot qui les précède comme dans «celui-là» ou «puis-je», ce qui interdit sa
suppression. Si des ressources lexicales peuvent aider dans le premier cas (mais
avec des risques de faux-positifs, que faire sans analyse sémantique fine d'un
hypothétique nom propre «Pisaller» ne tenant pas sur le reste d'une ligne et qui
aurait donné lieu à la séquence «Pis-`'\n'`aller» dans un fichier ?), la grande
variété d'éléments combinables dans le second cas n'incite pas à opter pour une
simple vérification d'appartenance à une liste préétablie d'exceptions connues.
Dans tous les cas, une analyse fine des dynamiques au niveau au moins
morphosyntaxique sinon syntaxique semble donc requise pour déterminer avec
certitude le sort d'un tiret survenant juste avant une fin de ligne.

Loin de constituer une base commune pour représenter un texte, le format `.txt`
représente donc en fait un impensé de l'encodage. Dans le cadre des présents
travaux, les paramètres d'encodage textuel utilisés pour représenter les
articles correspondent à ce qui est présenté à la figure
\ref{fig:lge_alpam_text}. Les fichiers utilisent l'encodage UTF-8 et le
caractère `'\n'` (nouvelle ligne) pour représenter les fins de ligne. Des fins
de ligne sont insérées là où il y en avait dans l'édition de *LGE* dont est
extraite l'article, de manière à conserver la largeur originelle du texte. Les
paragraphes sont en revanche séparés entre eux par deux fins de ligne
(c'est-à-dire, visuellement dans le fichier, par une ligne vide). Les
indentations ne sont pas marquées. Ce choix n'est pas nécessairement meilleur
qu'un autre: il retarde le recollement des mots divisés entre plusieurs lignes
pour ne pas introduire d'erreur définitive à cette étape (suite à la remarque du
paragraphe précédent), mais rend de ce fait l'accès au texte un peu plus
complexe.

À l'issue de cette section, l'ensemble des paramètres clefs pour représenter les
textes du corpus ont été identifiés et discutés. La confrontation des besoins
d'encodage définis par l'observation des pages de *LGE* et des structures
définies par le schéma d'encodage *TEI*, a montré que le module *dictionaries*
n'était pas adapté à la représentation d'une encyclopédie telle que *LGE*. Un
encodage alternatif utilisant des éléments plus généraux de la *TEI* — tout
comme l'avait fait l'[@=ARTFL] dans sa version de l'*EDdA*, mais en opérant des
choix légèrement différents — a été démontré sur quelques articles et une
portion de cet encodage est implémentée dans l'outil `soprano`, permettant de
générer automatiquement des articles au format XML-[@=TEI] à partir des pages en
ALTO livrées par la BnF. Cet outil permet également de produire des fichiers en
format texte, dont il a été montré que le contenu faisait également l'objet de
choix précis qui ont été explicités pour cette version.

