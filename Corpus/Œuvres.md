## Les œuvres du corpus

Des quatre encyclopédies présentes dans le corpus du projet GEODE, cette thèse
se concentre sur les deux premières dans l'ordre chronologique: l'*EDdA* et
*LGE*. Ces deux encyclopédies couvrent respectivement la deuxième moitié du
XVIII^ème^ siècle et la fin du XIX^ème^ siècle, la publication de *LGE* débutant
en 1885, 113 ans après la fin de celle de l'*EDdA* qui s'achève en 1772.

La toute première étape des présents travaux a consisté à rassembler des
versions numériques de ces textes et à les homogénéiser assez pour pouvoir leur
appliquer les mêmes traitements dans les phases ultérieures. En effet, alors que
sa position de symbole du siècle des *Lumières* a fait de l'*EDdA* un objet
d'étude fréquent et facilement disponible en format numérique, *LGE* reste
encore bien plus confidentielle avec une seule version imparfaite disponible au
début de cette thèse. Cette section dresse l'état des lieux des versions des
deux œuvres et retrace les étapes qui ont été nécessaires pour les constituer en
corpus d'étude.

### Contextes historiques {#sec:historical_context}

#### L'Encyclopédie de Diderot et d'Alembert {#sec:corpus_edda}

Dans le langage courant, les termes de «dictionnaire» et d'«encyclopédie» sont
souvent utilisés de manière assez interchangeables pour désigner des livres qui
regroupent de nombreuses connaissances et les ordonnent en listes de définitions
alphabétiques. Leur similarité est visible jusque dans le titre complet de
l'*EDdA*, «Encyclopédie ou dictionnaire raisonné». Si le mot «encyclopédie» fait
aujourd'hui partie de notre vocabulaire, il était bien plus surprenant et même
controversé quand Diderot et d'Alembert ont choisi de l'utiliser pour leur
œuvre.

Il ne s'agit pourtant pas d'un néologisme: le terme est déjà en usage au
XVI^ème^ siècle, par exemple chez Rabelais qui l'emploie pour faire déclarer à
Thaumaste que Panurge lui a ouvert «le vray puys et abisme de Encyclopédie». À
cette époque, le terme renvoie encore principalement au concept abstrait de
maîtrise simultanée de toutes les connaissances. C'est cette définition proche
de l'étymologie grecque du terme que donne @basnage_dictionnaire_1702 [p.760]
dans la deuxième édition du *Dictionnaire Universel*, dit «de Furetière» \: un
enchaînement de toutes les connaissances, de *κύκλος*, «cercle», et *παιδεία*,
"connaissance". Furetière semble critiquer sa poursuite en tant qu'une forme
d'hubris («C'est une témérité à un homme de vouloir posséder l'Encyclopédie»)
mais le présente comme désuet et humoristique («plus en usage que dans le
burlesque»). Il a pourtant été repris par Chambers qui en a fait le titre de son
propre dictionnaire universel — «Cyclopedia» — dont l'*EDdA* ne devait à
l'origine être qu'une traduction [@kafker_andre_francois_2016].

Au-delà du reproche moral, le concept qui plaisait tant à Rabelais était quelque
peu daté à la fin du XVII^ème^ siècle et faisait l'objet de la même critique
dans le *Dictionnaire de Trévoux* (voir section
\ref{sec:EdlA_lexicography_dict_and_enc}
p.\pageref{sec:EdlA_lexicography_dict_and_enc}). L'entrée *Encyclopédie* reste
inchangée dans les quatres éditions parues entre 1721 et 1752, moquant le terme
et déconseillant à son lectorat d'essayer de le cultiver. Dans ce but, l'article
cite un poème de Pibrac qui encourage les gens à se spécialiser dans une seule
discipline sans quoi ils risqueraient de ne jamais parvenir à atteindre la
perfection dans aucun, avec une argumentation qui n'est pas sans rappeler le
proverbe «qui trop embrasse mal étreint». Il est d'autant plus intéressant de
constater que la définition demeure inchangée jusqu'à 1752, un an après la
parution du premier volume de l'*EDdA*. Les Jésuites, éditeurs du *Dictionnaire
de Trévoux*, désapprouvaient le projet de l'*EDdA* et ont réussi à le faire
interdire la même année par le Conseil d'État en l'accusant de chercher à
détruire l'autorité royale, à inspirer la rebellion et à corrompre la moralité
de manière générale. Le combat idéologique ne s'arrêtait bien sûr pas aux mots,
mais la volonté d'effacer le mot lui-même est tout à fait cohérent avec leur
combat contre les philosophes des Lumières.

L'attaque n'est pas ignorée par Diderot qui fait commencer la définition même du
mot «Encyclopédie» dans l'*EDdA* par une réfutation ferme. Il commence par
rejeter les inquiétudes formulées dans le *Dictionnaire de Trévoux* qui ne
seraient selon lui que l'expression du manque de confiance de ses auteurs en
leurs propres capacités avant de laisser l'argument principal à une citation
latine du chancelier Bacon [@lojkine2013, p.5] qui explique qu'une collaboration
permet d'accomplir bien plus qu'il n'est possible à quiconque quel que soit son
talent: ce qui ne serait sans doute pas à la portée d'une seule personne au cours
d'une seule vie pourrait l'être à un effort commun sur plusieurs générations.

L'Histoire semble indiquer que les adversaires de Diderot ont pris sa défense de
la faisabilité du projet très au sérieux. Six ans après la reprise de la
publication, ils sont parvenus à faire révoquer le privilège de l'*EDdA* une
deuxième fois [@moureau2001]. Le résultat de cette interdiction fut que les dix
tomes de texte restant ont dû être publiés illégalement jusqu'en 1765, ce qui a
été rendu possible par la protection secrète de Malesherbes qui — malgré sa
place à la tête de la censure royale — a sauvé les manuscrits de la destruction.
Leur impression a eu lieu en cachette, en dehors de Paris, et les livres ont été
(faussement) estampillés comme venant de Neufchâtel. Suite à la forte demande
des libraires qui craignaient de perdre l'argent qu'ils avaient investi dans le
projet, un privilège spécial fut émis pour les volumes de planches qui ont été
publiés entre 1762 et 1772.

Malgré le tour romanesque qu'a dû prendre la publication de l'*EDdA*, dans leur
dernière édition de 1771 les auteurs du *Dictionnaire de Trévoux* n'avaient plus
d'autre choix que de reconnaître le succès des projets encyclopédiques du
XVIII^ème^ siècle. Dans cette version la définition a été entièrement
retravaillée, pour se contenter de reconnaître que les bonnes encyclopédies sont
difficiles à écrire du fait de la somme de connaissances requises et de travail
nécessaire pour se tenir à jour des avancées scientifiques. Il n'est plus
possible de tourner l'idée même en ridicule. L'entrée reconnaît que la
*Cyclopædia* de Chambers constitue une tentative décente avant de mentionner
sans les nommer — mais de manière suffisament explicite — le projet de Diderot
et d'Alembert, désignant le collectif par «Une Société de gens de Lettres» (un
renvoi clair au sous-titre du projet) et précisant qu'il a débuté en 1751. Mais
juste à côté de cette reconnaissance à demi-mot du succès du projet, deux
nouvelles entrées font leur apparition: une pour un adjectif, «encyclopédique»
et une autre pour le nom «encyclopédiste», témoignages éloquents de la portée de
l'entreprise encyclopédique et de l'influence qu'elle a eu non seulement sur son
époque mais sur la relation à la connaissance elle-même. Cette reconnaissance a
bien entendu un retentissement particulier dans les pages du *Trévoux* mais elle
ne s'y limite pas. Le terme connaît une évolution similaire dans le
*Dictionnaire de l'Académie* [@pruvost_regard_2022, p.115-117].

Il existe au moins trois versions numériques de l'*EDdA* disponibles en ligne
publiquement sous différentes conditions. L'Édition Numérique Collaborative et
CRitique de l'Encyclopédie ([@=ENCCRE] dans le reste de ce manuscrit) édite un
site web[^ENCCRE] offrant de nombreuses fonctionnalités dont un parcours
linéaire du texte par tome qui associe à la version numérique du texte une
photographie de la page dont il est extrait, ainsi qu'une interface de recherche
permettant d'accéder directement à un article donné. Des métadonnées
répertorient des informations objectives trouvées dans chaque article — auteur,
présence de renvois, etc. — qui sont ensuite commentées par un ensemble de notes
pouvant renvoyer à des fiches thématiques. Une deuxième source est l'American
and French Research on the Treasury of the French Language (dorénavant
[@=ARTFL]) qui diffuse également sa version du texte[^ARTFL] au moyen du
logiciel Philologic4[^philologic]. L'outil permet des recherches avancées
incluant un filtrage des résultats suivant certaines métadonnées. Le texte,
lisible depuis l'interface web générée par Philologic4 contient des liens vers
des photos des pages pour le retour au texte. Enfin, Wikisource dispose
également d'une version[^wiki], partiellement corrigée par sa communauté, sous
forme d'un ensemble de pages qu'il est possible de consulter mais sans outil de
recherche et sans métadonnées.

[^ENCCRE]:
    [https://enccre.academie-sciences.fr/](https://enccre.academie-sciences.fr/)
[^ARTFL]:
    [https://artflsrv04.uchicago.edu/philologic4.7/encyclopedie0922/](https://artflsrv04.uchicago.edu/philologic4.7/encyclopedie0922/)
[^philologic]:
    [https://artfl-project.uchicago.edu/philologic4](https://artfl-project.uchicago.edu/philologic4)
[^wiki]:
    [https://fr.wikisource.org/wiki/L%E2%80%99Encyclop%C3%A9die/1re_%C3%A9dition](https://fr.wikisource.org/wiki/L%E2%80%99Encyclop%C3%A9die/1re_%C3%A9dition)

\label{edda_existing_versions}Si ces interfaces de consultation permettent une
navigation facile des textes, l'application systématique d'outils de traitement
requiert un accès à des fichiers contenant à la fois le plein texte et des
métadonnées exploitables. L'[@=ARTFL] a accepté de partager ses données avec les
membres du projet GEODE et c'est donc sa version qui a servi de base à cette
étude pour le contenu textuel des articles. Chacun des 17 tomes de l'œuvre est
représenté par un fichier encodé au format XML-[@=TEI]. Chaque article au sein
d'un tome contient les métadonnées accessibles depuis l'interface de
Philologic4. Dans le cadre de certains travaux du projet, elles ont été
recoupées avec celles accessibles depuis l'interface de l'[@=ENCCRE]. Toutes les
photos d'article de l'*EDdA* proviennent du site de l'[@=ENCCRE]. Le texte
comprend au total 74 198 articles et 23 786 713 tokens.

#### La Grande Encyclopédie {#sec:corpus_lge}

*La Grande Encyclopédie, Inventaire raisonné des Sciences, des Lettres et des
Arts par une Société de savants et de gens de lettres* fut publiée en France
entre 1885 et 1902 par une équipe de plus de deux cent spécialistes organisés en
onze sections. Ce texte comprend 31 tomes d'environ 1200 pages chacun et fut,
d'après @jacquet_pfau2015 la dernière entreprise encyclopédique française
majeure à marcher dans les traces de l'ancêtre prestigieux que fut l'*EDdA*,
publiée environ 130 ans plus tôt.

Le titre complet de l'œuvre montre déjà la volonté de filiation avec l'*EDdA* de
ses auteurs dans leur choix de sous-titre [@jacquet_pfau_actualiser_2022, p.96].
La préface décrit la France comme une précurseure des grandes entreprises
encyclopédiques, rattrapée depuis par les grandes puissances occidentales et qui
n'a pas su se mettre à jour depuis l'*EDdA*. Les éditeurs mettent en avant les
illustrations qu'ils placent dans la «tradition des encyclopédistes du siècle
dernier», malgré l'évolution technique qu'elles apportent en étant intégrées au
texte par contrastes avec les volumes de planches séparées du textes de
l'*EDdA*. Le choix visionnaire des philosophes des Lumières de traiter des
métiers, techniques et outils est présenté comme précurseur de la «société
industrielle» du XIX^ème^ siècle.

À l'issue d'un historique qui remonte jusqu'à Démocrite en passant par Aristote,
l'*EDdA* fait l'objet d'une anecdote de Voltaire rapportant un souper au
Trianon. Dans ce récit, la cour expose au roi l'intérêt d'une encyclopédie et la
qualité de l'*EDdA* en particulier, ce qui l'amène à revenir sur sa décision
d'interdire l'œuvre. La préface s'achève par une réflexion sur la vie
nécessairement «éphémère» des encyclopédies et le besoin de les renouveler pour
les adapter aux progrès des sciences et aux évolutions du langage. Le tout
dernier paragraphe prend la forme d'un souhait, celui de marquer son époque et
de s'inscrire dans la lignée du «souvenir de Diderot et d'Alembert».

Cet espoir a été déçu dans une large mesure puisque *LGE* tombe rapidement dans
l'oubli [@vigier_lesprit_2022, p.18]. Le projet connaît des difficultés
financières dès les dernières années de sa publication [@jacquet_pfau2015,
p.90]. La Librairie Larousse, qui édite le *Grand Dictionnaire Universel*,
rachète *LGE*, non pas pour développer une offre plus scientifique mais bien
pour se débarasser d'un concurrent. Elle disparaît peu après malgré sa grande
rigueur et ses qualités scientifiques — des articles de *LGE* sont parfois cités
dans des publications [@jacquet_pfau2015, p.91] et les plus longs, véritables
dossiers sur un sujet, connaissent une vie hors de ses pages en étant publiés
sous formes de monographies [@jacquet_pfau_actualiser_2022, p.99].

Une version numérique de cette œuvre a été réalisée par la BnF et mise en
ligne[^LGE-V1] en 2007. Basée sur une réimpression non datée de l'édition
originale, elle comprend une image par page de l'œuvre, numérisée en niveau de
gris à une résolution de 300x300 pixels par pouce. De ces fichiers images a été
tirée une version partielle du texte par application d'un programme de
reconnaissance optique de caractères ([@=OCR]). Cette version présente un
certains nombre de limites empêchant de mener une étude intégrale du texte
par des moyens automatiques comme la textométrie et faisait écrire à
@jacquet_pfau2015[p.88] qu'«il n'existe pas de version numérique complète et
structurée en format texte de *LGE*».

[^LGE-V1]: [http://catalogue.bnf.fr/ark:/12148/cb377013071](http://catalogue.bnf.fr/ark:/12148/cb377013071)

D'abord, le texte est incomplet: si un grand nombre de tomes ont été OCRisés,
certains comme par exemple les tomes 5 ou 18 n'ont pas été traités et aucun
texte n'est disponible pour ces volumes sur le site de
Gallica[^LGE-V1-liste-des-tomes]. Cet état rend impossible une étude exhaustive
mais rien ne suggère qu'une étude basée sur les tomes disponibles serait sujette
à un biais particulier, puisque les tomes OCRisés ne semblent pas avoir été
choisis selon une logique précise, ceux qui manquent ne sont par exemple pas
contigus ni au début ni à la fin de l'œuvre. Ensuite, cette version en «texte
brut» (en réalité une page HTML avec un balisage minimal) ne comporte qu'une
annotation très superficielle et n'est en particulier pas segmentée en articles.
Cela est un obstacle majeur pour les présents travaux, puisque l'unité d'étude
de notre corpus est l'article, permettant de mettre en évidence des régularités
propres à un domaine de connaissance ou à un auteur précis en vue d'analyses
contrastives. Enfin, des erreurs dans la détection de l'organisation de la page
([@=OLR]) obscurcissent significativement le texte en opérant des permutations
locales de son contenu qui viennent parfois mélanger des morceaux d'articles
entre eux. Cela complique nettement la segmentation du texte en articles et
vient dans tous les cas endommager la structure des phrases. Ces nouvelles
erreurs se répercutent à leur tour sur les phases ultérieures d'analyse, causant
des problèmes dans les annotations morphosyntaxiques et syntaxiques qu'il est
nécessaire d'appliquer au texte pour faire de la textométrie.

[^LGE-V1-liste-des-tomes]: [https://gallica.bnf.fr/ark:/12148/bpt6k246407#](https://gallica.bnf.fr/ark:/12148/bpt6k246407#)

Dans le but de pallier ces défauts, le projet CollEx Persée
DISCO-LGE[^DISCO-LGE] (2019-2021) a entrepris de renumériser cette encyclopédie
en partenariat avec la BnF et d'en produire une version encodée en XML-TEI.
Cette nouvelle version a été réalisée à partir de photographies d'un exemplaire
original[^LGE-V2] situé à la Bibliothèque de l'Arsenal à Paris[^bib-arsenal].

[^LGE-V2]: [https://catalogue.bnf.fr/ark:/12148/cb41651490t](https://catalogue.bnf.fr/ark:/12148/cb41651490t)
[^bib-arsenal]: [https://www.bnf.fr/fr/arsenal](https://www.bnf.fr/fr/arsenal)

Ce projet a pris fin en août 2021 et a abouti à la publication sur
Nakala[^nakala], le dépôt de données de la Très Grande Infrastructure de
Recherche Huma-Num, d'une nouvelle version de l'œuvre sous différents formats,
dont des segmentations par article en XML-TEI et en format texte (`.txt`). Cette
version compte 229 475 articles pour un total de 54 936 266 tokens.
L'identification automatique des débuts d'article y a causé davantage de faux
positifs (séquence incorrectement considérée comme un début d'article donnant
lieu à un surdécoupage du vrai article en plusieurs) que de faux négatifs
(débuts d'article non reconnu par le système causant la fusion de deux vrais
articles) et ce chiffre semble donc (largement ?) surestimé.

[^nakala]:
    [https://nakala.fr/10.34847/nkl.74eb1xfd](https://nakala.fr/10.34847/nkl.74eb1xfd)

### Organiser les connaissances {#sec:structuring_knowledge}

La problématique de structuration de la connaissance, comme cela a été décrit
à l'introduction (voir la section \ref{sec:intro_strategy} page
\pageref{sec:intro_strategy}), est au cœur des présents travaux. Elle a bien sûr
une forte valeur intrinsèque pour les auteurs de chacune des encyclopédies du
corpus mais, dans le cadre de cette thèse, elle est ce qui permet de
singulariser et de problématiser la Géographie. Elle est la raison pour laquelle
des efforts sont déployés au chapitre \ref{sec:domains_classification} pour
appliquer une classification automatique aux articles, et elle fonde la démarche
contrastive adoptée au chapitre \ref{sec:contrasts}. Pour commencer, il s'agit
de définir plusieurs concepts fondamentaux liés à la notion et à sa
matérialisation dans les articles.

#### Anatomie d'un article d'encyclopédie {#sec:encyclopedia_anatomy}

Les premiers de ces concepts sont purement typographiques. En vue de pouvoir
évoquer les différentes parties d'une encyclopédie, il est nécessaire de bien
définir ce qui constitue les articles et leur environnement sur les pages. La
figure \ref{fig:anatomy_samples} offre un aperçu des éléments pouvant apparaître
dans un article (non représentatif, les deux articles ont été choisis pour leur
concentration élevée).

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/EDdA/bromelia_t2_p434.png}
        \caption{Article BROMELIA dans l'\textit{EDdA}, T2, p.434}
        \label{fig:anatomy_samples_edda}
    \end{subfigure}
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/LGE/alectrurus_t2_p68.png}
        \caption{Article ALECTRURUS dans \textit{LGE}, T2, p.68}
        \label{fig:anatomy_samples_lge}
    \end{subfigure}
    \caption{Les différents constituants possibles d'un article d'encyclopédie}
    \label{fig:anatomy_samples}
\end{figure}

Dans une encyclopédie, une entrée associe un texte à un mot ou un groupe de
mots, la «vedette», qui sert tout à la fois de «titre» à l'article et de point
de référence dans l'ensemble du texte puisque c'est vers ces vedettes que
pointent les renvois. Tout article d'encyclopédie comporte nécessairement une
vedette, typographiquement marquée en majuscule (au moins partiellement), comme
c'est le cas pour les deux articles de la figure \ref{fig:anatomy_samples} où
elles sont surlignées en violet. La vedette a une existence grammaticale à part
du reste de l'article, ce qui confirme son rôle de «clef» dans la gigantesque
table associative constituée par une encyclopédie: quand elle n'est pas séparée
de la première phrase par un point comme c'est le cas de la figure
\ref{fig:anatomy_samples_lge}, elle est souvent simplement apposée et tout à
fait optionnelle, séparée d'une virgule du reste de la première phrase qui est
alors le plus souvent nominale, déportant tout syntagme verbal dans une
subordonnée relative comme le montre la figure \ref{fig:anatomy_samples_edda}.
Elle peut être suivie optionnellement d'un «désignant» entre parenthèses (en
bleu sur la figure \ref{fig:anatomy_samples}) qui sert à situer l'entrée dans un
espace structuré de connaissances (notion centrale de cette sous-section et qui
est développée plus bas dans le segment \ref{sec:knowledge_domains}). Un
désignant peut ainsi référer à un ou plusieurs domaines de connaissances,
souvent abréviés.

Les articles ne constituent pas une collection d'informations isolées mais sont
au contraire liés entre eux par un système de renvoi, qui constitue un deuxième
mode d'organisation en sus du système de domaines de connaissances
[@blanchard_systeme_2002, p.46]. Les articles peuvent ainsi contenir des
renvois, figurés en jaune sur la figure \ref{fig:anatomy_samples} . Ils sont
constitués du verbe «voir» à l'impératif ou à l'infinitif jussif et de la
vedette d'un autre article (elle est bien en majuscule, éventuellement dans une
fonte plus petite) et possèdent une valeur illocutoire en invitant le lectorat à
tourner les pages du livre ou à aller chercher un autre tome en vue de
poursuivre la lecture dans l'article ainsi ciblé. À l'origine une simple phrase
parmi les autres au milieu du texte, avec le verbe écrit en toutes lettres
(cette forme reste plus fréquente dans l'*EDdA*), les renvois ont évolué et pour
prendre la forme stylisée visible dans les deux exemples, où le verbe n'existe
plus qu'au travers de son initiale, et où le renvoi dans son ensemble peut être
mis entre parenthèses comme dans la figure \ref{fig:anatomy_samples_lge}, ce qui
n'est pas sans préfigurer des dispositifs modernes tels que les liens
hypertextes. On trouve parfois aussi une formulation réflexive pour éviter une
répétition («V. ce mot»), établissant implicitement le lien entre l'occurrence
du mot dans la phrase précédente à la vedette correspondante. Il semble que
cette tournure s'accompagne plus fréquemment d'une forme complète du verbe
«voir» ou «voyez» que la forme directe où la vedette est explicite.

Enfin, les articles sont parfois signés (en vert sur la figure
\ref{fig:anatomy_samples}). La signature de l'article BROMELIA (figure
\ref{fig:anatomy_samples_edda}) n'est pas une initiale, ce 'I' entre parenthèses
est utilisé par Louis-Jean-Marie \textsc{Daubenton}[^daubenton]. Les articles
peuvent mentionner occasionnellement leurs sources, en orange sur la même
figure. Celles de *LGE* sont particulièrement fréquentes et riches, faisant
l'objet d'un dernier paragraphe marqué systématiquement par l'abréviation
«\textsc{Bibl}.» (visible sur la figure \ref{fig:anatomy_samples_lge}) et
pouvant parfois s'étendre sur plusieurs colonnes de texte comme c'est le cas
pour l'article COLONISATION (La Grande Encyclopédie, T11, p.1066) — 3 colonnes
et demie de bibliographie, s'étendant de la page 1117 à la page 1119.

[^daubenton]: https://artflsrv04.uchicago.edu/philologic4.7/kafker/navigate/1/33

Pour une encyclopédie imprimée sur un support papier, les articles apparaissent
les uns à la suite des autres dans l'ordre alphabétique (sans tenir compte des
accents et autres diacritiques) sur une ou plusieurs colonnes (2 colonnes dans
l'*EDdA* comme dans *LGE*). Autour des articles se trouvent divers éléments
«péritextes» mis en évidence à la figure \ref{fig:anatomy_outside_top}. Le haut
d'une page comporte généralement le numéro de la page dans le tome courant,
surligné en vert sur la figure \ref{fig:anatomy_outside_top}. Pour aider à la
recherche d'un article donné, des indicateurs visibles en bleu sur la figure
\ref{fig:anatomy_outside_top} représentent l'intervalle de l'index alphabétique
couvert par la page courante, en donnant les positions de l'article auquel
appartiennent la première et la dernière ligne de la page. Dans l'*EDdA* (figure
\ref{fig:anatomy_outside_top_edda}), il s'agit seulement des trois premières
lettres des vedettes de ces articles, dans *LGE* (figure
\ref{fig:anatomy_outside_top_lge}) des vedettes complètes. L'évolution des
techniques d'imprimerie permet de positionner des figures au milieu des colonnes
de texte dans *LGE* accompagnées de leurs légendes (en mauve sur la figure
\ref{fig:anatomy_outside_top_lge}). Dans l'*EDdA*, les figures et leurs légendes
sont présentes mais confinées aux tomes de planches.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics{figure/text/EDdA/naturel_t11_p47.png}
        \caption{Haut de la page 47 du tome 11 de l'\textit{EDdA}}
        \label{fig:anatomy_outside_top_edda}
    \end{subfigure}
    \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics{figure/text/LGE/balaeniceps_t5_p48.png}
        \caption{Haut de la page 48 du tome 5 de \textit{LGE}}
        \label{fig:anatomy_outside_top_lge}
    \end{subfigure}
    \caption{Les différents éléments péritextes en haut et en cours de pages}
    \label{fig:anatomy_outside_top}
\end{figure}

Les techniques de reliure imposent l'impression de ces encyclopédies par
«cahiers» (de 8 pages pour l'*EDdA* — *in octavo* — et de 16 pages pour
*LGE* — *in sextodecimo*). Chacun de ces cahiers porte une inscription qui
rappelle le tome auquel il appartient (en jaune sur la figure
\ref{fig:anatomy_outside_bottom}) et la position du cahier dans l'ordre de
reliure (en orange sur la figure \ref{fig:anatomy_outside_bottom}). Cette
position est exprimée par une lettre dans l'*EDdA* (ici un 'G') et par un nombre
entier dans *LGE*. Par construction, ces inscriptions reviennent régulièrement:
toutes les 8 pages dans l'*EDdA*, environ toutes les 16 pages dans *LGE* (la
présence de cartes qui sont imprimées à part et insérées entre les pages des
cahiers vient perturber cette périodicité).

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics{figure/text/EDdA/navet_t11_p49.png}
        \caption{Bas de la page 49 du tome 11 de l'\textit{EDdA}}
        \label{fig:anatomy_outside_bottom_lge}
    \end{subfigure}
    \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics{figure/text/LGE/balaklava_t5_p49.png}
        \caption{Bas de la page 49 du tome 5 de \textit{LGE}}
        \label{fig:anatomy_outside_bottom_edda}
    \end{subfigure}
    \caption{Les différents éléments péritextes en bas de page}
    \label{fig:anatomy_outside_bottom}
\end{figure}

Ces définitions posées, il devient possible de s'intéresser au concept principal
de cette sous-section qui est étroitement lié à la notion de désignant.

#### Notion de domaine de connaissance {#sec:knowledge_domains}

Comprendre l'organisation de la connaissance est un champ de recherche à
l'intersection de plusieurs disciplines [@foucault_order_1970;
@bowker_sorting_2000; @blair_too_2010; @wellmon_organizing_2015]. Les
historiens, par exemple, étudient quels types de documents contiennent des
connaissances, comment la connaissance est divisée en catégories et quelles
relations ces catégories entretiennent [@groult_lencyclopeou_2003;
@holmberg_maurists_2017]. Dans certains documents historiques, cette
classification est visible: entre le XVII^ème^ et le XIX^ème^ siècle des
encyclopédies ont ainsi commencé à la matérialiser au début des articles.

Les systèmes de classification choisis par les éditeurs d'une encyclopédie
reflètent le contexte spécifique des débats intellectuels contemporains de sa
publication, qui peut transparaître par exemple dans l'emploi d'un désignant
plutôt qu'un autre comme le montre @viard_lutilite_2006[§33]. Les encyclopédies
sont donc un terrain particulièrement intéressant pour étudier comment la
classification de la connaissance change au cours du temps. Dans le cadre de
cette thèse, la notion de classification permet surtout d'accéder en première
approximation aux discours géographiques qui sont l'objet de cette thèse puisque
tant l'*EDdA* que *LGE* considèrent la géographie comme un domaine de
connaissance parmi les autres.

Là où certains philosophes tentent de trouver une organisation des connaissances
satisfaisantes en elle-même en tant que description du monde dans sa totalité,
formant des systèmes à priori dans lesquels n'importe quelle
connaissance — contemporaine du système ou ultérieure — est censée pouvoir
ensuite trouver sa place, d'autres approches plus empiriques sont également
possibles. Ainsi, alors qu'il répertorie et ordonne deux années de
communications scientifiques dans les *Philosophical Transactions of the Royal
Society*, @royal_society_great_britain_philosophical_1665 [p.405] propose une
organisation tripartite. Il distingue une «Histoire naturelle» tout à fait
générale, une catégorie des «Singularités» — tout ce qui échappe à la norme
définie par la première catégorie rendant par construction le système
complet — et une troisième catégorie méta qui traite des outils pour étudier les
catégories précédentes. Une telle organisation à postériori est par construction
plus facile à équilibrer mais elle a peu de chances de résister à l'épreuve du
temps ou d'être adaptée à plusieurs œuvres encyclopédiques. Il est intéressant
de noter que cette classification sommaire, présentée comme «a more Natural
Method» sert surtout initialement à se libérer de l'arbitraire de l'ordre
alphabétique.

Dans l'*EDdA*, l'organisation de la connaissance intègre non seulement les
sciences mais également les arts et les métiers. Elle est fondée sur une autre
partition en trois, celle proposée par Bacon qui rattache tout savoir aux
facultés humaines de la mémoire, l'imagination et la raison [@jaquet_bacon_2010,
§10]. Cette distinction est fondamentale dans la démarche des encyclopédistes:
revendiquée par d'Alembert dès le Discours Préliminaire (L'Encyclopédie, T1,
p.xvj), elle est aussi visible dans les trois colonnes qui partagent le «Systême
figuré des connoissances humaines» reproduit à la figure
\ref{fig:systeme_figure}. Elle témoigne d'une structuration à priori des
sciences bien plus ambitieuse que la division très pragmatique de Lowthorpe. Sa
catégorie des Singularités semble tout de même trouver un écho dans la branche
des «Écarts de la Nature» du «Systême» (voir à gauche de l'arbre de la figure
\ref{fig:systeme_figure} dans la moitié supérieure) mais c'est bien là la seule
ressemblance. Il ne faut néanmoins pas croire que les efforts d'organisation des
auteurs de l'*EDdA* s'arrêtent à cette taxonomie. Un vaste réseau de renvois
structure transversalement les articles et suggère des navigations de l'œuvre,
véritables liens «hypertextes» avant l'heure [@blanchard_systeme_2002, p.47].
Les encyclopédistes ne commettent pas non plus l'erreur de prendre le «Systême»
pour plus qu'il n'est: une déclaration d'intention et non pas une «grille fixe»
[@cernuschi_designants_2006, §3] dans lequel les articles devraient à tout prix
trouver leur place. Des branches de l'arbre demeurent sans articles et à
l'inverse des articles sont affectés à des domaines qui ne sont pas mentionnés
par le «Systême figuré» [@leca_tsiomis_tentative_2006, §4].

![Le «Systême figuré des connoissances humaines», une taxonomie au cœur de l'*EDdA*](figure/systême_figuré.png){#fig:systeme_figure}

\label{sec:edda_domain_expressions}En pratique, les domaines de connaissance se
manifestent la plupart du temps au niveau des articles sous la forme des
désignants présentés dans la partie précédente de cette section
(p.\pageref{sec:encyclopedia_anatomy}). Certains articles n'ont pas de désignant
sous cette forme marquée typographiquement (entre parenthèses, en tête
d'article, souvent abréviée), mais reçoivent tout de même une indication quant à
leur domaine d'appartenance, une *marque de domaine*. Une tournure telle que «en
termes de» est très utilisée à cet effet, suivie du nom d'une science — par
exemple à l'article DISQUE «en termes de Botanique» (L'Encyclopédie, T4,
p.1045) — ou d'une profession — comme dans l'article AMBRISE «en termes de
Fleuriste» (L'Encyclopédie, T1, p.326). On trouve aussi parfois simplement «en»
suivi du nom d'une science comme l'illustre l'article ÉLASTICITÉ — «en Physique»
(L'Encyclopédie, T5, p.444). Une troisième tournure possible est *PRÉPOSITION*
suivi de «les» («chez les», «dans les», «parmi les»…), qui peut accepter
différents types de groupes humains, qu'ils soient déterminés par une activité
comme à l'article A A A visible à la figure \ref{fig:aaa_t1} — «chez les
chimistes» (L'Encyclopédie, T1, p.5) — des pratiques par exemple dans l'entrée
CALVAIRE — «chez les Chrétiens» (L'Encyclopédie, T2, p.565) — ou une simple
appartenance civilisationnelle — on pourra penser à «chez les Latins» de
l'article PAVE (L'Encyclopédie, T12, p.192). Outre «marques de domaine»,
l'[@=ENCCRE] qualifie également ces expressions de «désignants *non marqués*» ou
«*flottants*»[^enccre-politique-editoriale], le terme étant employé au moins par
@boussuge_description_2019[p.237]. Leur usage s'est répandu à la fin du
XVII^ème^ siècle dans le dictionnaire de Richelet avant d'accéder au statut de
«tradition lexicographique» [@quemada_dictionnaires_1968, p.307] une fois repris
dans le *Furetière* et le *Trévoux*. Leur présence ou non n'obéit à aucune règle
évidente, et les formulations restent très variables. Pour trouver un semblant
d'ordre, on peut observer que les différents groupes répertoriés précédemment
offrent en quelque sorte un dégradé de valeurs classifiantes, qu'il est possible
de jauger en tentant de les reformuler. Alors que «chez les chimistes» est un
équivalent assez clair du désignant «Chimie», il n'y a pas de domaine
«Chrétienté» mais la position de ce «chez les Chrétiens» en début d'article,
apposé à la vedette avant le verbe de la phrase évoque encore beaucoup l'usage
précédent. Il ne s'agit déjà plus d'un désignant puisque l'article en est par
ailleurs pourvu — «(Hist. ecclés.)» — mais une reformulation possible de
l'expression en «Religion chrétienne» montre encore une certain proximité de cet
emploi avec un désignant. Pour «chez les Latins», en revanche, il n'y a plus
qu'une valeur circonstancielle malgré sa position relativement en début
d'article: le terme défini (pavé) n'est pas constitutif d'un ensemble de
pratiques qui définirait le groupe comme ce pouvait être le cas avec les
chimistes et les chrétiens.

[^enccre-politique-editoriale]:
    [http://enccre.academie-sciences.fr/encyclopedie/politique-editoriale/?s=31&](http://enccre.academie-sciences.fr/encyclopedie/politique-editoriale/?s=31&)

![Article A A A dans l'*EDdA*, T1, p.5](figure/text/EDdA/aaa_t1_p5.png){#fig:aaa_t1 width=60%}

À la suite de @leca_tsiomis_tentative_2006[§15], on réservera le terme de
«désignant» aux formes marquées typographiquement, placées entre parenthèse en
début d'article. Les désignants sont en toute rigueur un cas particulier de
marques de domaine mais on utilisera les deux termes en opposition, pour
distinguer les marques de domaine qui ne sont pas des désignants. Les désignants
résultent d'une simplification et d'une codification au fil du temps des marques
de domaine, leurs «ancêtres» d'après @leca_tsiomis_tentative_2006[§8]. Ils sont
en effet absents d'œuvres antérieures comme le *Dictionnaire Universel* de
Basnage ou même de la 3ème édition du *Dictionnaire de  Trévoux* (celle de
1743). Dans ces œuvres en revanche, des occurrences de «en termes de» se
rapprochent des débuts d'articles là où se trouverait un désignant. C'est dans
la *Cyclopedia* de Chambers que le mot «terme» disparaît, restant sous-entendu
puisque l'œuvre est un recueil de terminologie [@leca_tsiomis_tentative_2006,
§12], laissant apparaître le nom de la discipline seule. Dans l'*EDdA*, le
concept de désignant est tout à fait né et des marques de domaine se trouvent
même parfois entourées de parenthèses quand elles apparaîssent à la même place,
comme c'est le cas aux articles BIZEGLE — «(chez les Cordonniers)»
(L'Encyclopédie, T2, p.268) — ou CANON — «(terme de Rubannier)» (L'Encyclopédie,
T2, p.618). La présence de la majuscule comme dans les désignants vient
également rompre la lecture normale de l'expression en transformant le nom de
profession en une sorte d'objet symbolique, le nom d'une classe et plus le mot
dans son acception courante. Enfin ces expressions semblent avoir quasiment
disparu de *LGE*. «En termes de» présente 2433 occurrences dans l'*EDdA* contre
seulement 51 dans *LGE* (pourtant 2.3 fois plus volumineuse d'après les chiffres
présentés dans la sous-section \ref{sec:historical_context}). Cela confère au
motif une spécificité d'au moins 1 000 dans l'*EDdA* (voir au sujet de la
saturation de l'affichage des spécificités dans TXM la section
\ref{textometry_specificity} p.\pageref{textometry_specificity}). «Chez les»
suivi d'un mot commençant par une majuscule est plus difficile à exploiter
puisque d'après la remarque précédente la séquence n'a pas toujours la même
valeur qu'un désignant mais dans tous les cas elle est également moins
représentée: 713 occurrences dans l'*EDdA* contre 882 dans *LGE*, le motif
obtient donc une spécificité de 33 dans l'*EDdA* ce qui est suffisant pour
écarter une répartition seulement dûe au hasard.

\label{lge_preface_domains}Dans *LGE*, il n'y a pas d'arborescence des
connaissances mais la préface accorde une place majeure à la notion de domaine.
Dès sa première page (La Grande Encyclopédie, T1, p.I), ses auteurs après avoir
justifié le bien-fondé de leur entreprise par le manque d'encyclopédie française
à jour en termes de connaissances scientifiques établissent une longue liste de
domaines de connaissances pour montrer la grande variété de sujets que *LGE* se
propose de couvrir («en un mot, tout ce qui est de nature à jeter la lumière sur
le monde physique et sur le monde intellectuel»). Plus que réellement des noms
de sciences, il s'agit plutôt de thématiques choisies pour mettre en valeur les
intérêts de l'époque — ce qui permet la mention des «applications nouvelles de
l'électricité» alors que la physique et la chimie dont elles relèvent pourtant
figurent déjà dans la liste. La géographie quant à elle n'est même pas
mentionnée. Elle n'apparaît que plus bas sur la même page, ses découvertes étant
associées au «développement colonial» pour intégrer les cartes, «gravées
spécialement pour» *LGE*, dans un paragraphe qui met en valeur l'apport des
illustrations dans l'œuvre. La place importante qu'y occupent les cartes prend
un sens tout particulier quand on sait la volonté de l'œuvre de s'inscrire dans
l'héritage de l'*EDdA* à la lumière de ce qu'écrit Diderot dans l'article
ENCYCLOPÉDIE (L'Encyclopédie, T5, p.635) qui justifie la sécheresse reprochée à
sa géographie, s'en tenant à «la seule connoissance géographique […] qui suffira
à la postérité pour construire de bonnes cartes de nos tems». Alors que l'*EDdA*
ne comporte en effet pas de cartes, *LGE* incarne cette postérité qui dresse des
cartes.

Mais plus encore, la notion de domaine de connaissance est décrite dans les
pages qui suivent comme primordiale dans tout projet encyclopédique au-delà de
*LGE* seule. Après un incontournable rappel historique qui évoque
chronologiquement les étapes majeures qui ont mené au concept moderne
d'encyclopédie depuis l'étymologie grecque du terme, la préface revient au
*Discours préliminaire* de l'*EDdA* écrit par d'Alembert pour souligner
l'importance de l'organisation et de la structuration de la connaissance dans
les encyclopédies, présentées comme des traits caractéristiques qui les
distinguent des dictionnaires (la sous-section
\ref{sec:dictionaries_vs_encyclopedias} de la section suivante explore plus en
détail cette problématique et met en évidence d'autres différences mais il est
intéressant de voir que cette séparation était déjà perçue au XVIII^ème^ siècle
avant que le terme d'encyclopédie ne réussisse à s'imposer). D'après cette
partie de la préface, la démarche encyclopédique consiste à révéler les liens
qui relient tous les savoirs humains et à les organiser, partant des faits
concrets pour généraliser vers l'abstraction et établir ainsi ce qui constitue
les «lois naturelles». Mais à la suite de d'Alembert qui comme le fait remarquer
@hardestydoig_designant_2006[§1] reconnaît la part d'arbitraire inhérente à tout
«système» (toujours dans le *Discours préliminaire*), les auteurs ont bien
conscience qu'il n'y a pas de preuve que l'ensemble des connaissances admette un
ordre. La volonté d'y trouver une logique pourrait simplement s'expliquer par la
façon dont fonctionne l'esprit humain. C'est à ce moment que l'encyclopédisme
s'écarte de la science en nécessitant un acte de foi: les auteurs de *LGE*
affirment que faire une encyclopédie revient à décider qu'une telle structure
existe et à tenter d'y parvenir. On assiste ainsi à un certain renversement de
la problématique de départ puisque la classification ne serait pas un simple
aspect de la conception d'une encyclopédie mais au contraire *le* point de
départ d'un projet fondamentalement positiviste et *la* question centrale à
laquelle elle prétend répondre en proposant un système d'organisation des
connaissances.

Une fois posé ce principe, le reste de la préface est bien plus pragmatique: il
ne s'agit plus que de choisir un ensemble de domaines pour classer les articles.
Fidèle à l'esprit de l'*EDdA*, les auteurs de *LGE* ancrent eux aussi leur
recherche d'un système de classification dans les travaux de Bacon. Ils tiennent
compte des tentatives de partition des sciences effectuées au XIX^ème^ siècle et
mentionnent celle d'Ampère basée sur la dichotomie entre nature et esprit
(opposition *cosmologique*/*noologique*), celle d'Auguste Comte fondée sur
l'ordre historique entre les sciences et celle de Herbert Spencer qui s'attache
à la division entre science abstraite et concrète. Conscients de l'impossibilité
d'«arrêter une classification définitive» les auteurs de *LGE* optent pour un
système inspiré de l'approche historique d'Auguste Comte remise à jour.

L'avant-propos poursuit ce travail en fixant un objectif de taille à l'ensemble
du projet avant d'accorder place à chacun des domaines (La Grande Encyclopédie,
T1, p.XI). C'est la première et seule apparition de la liste des 14 domaines
retenus plus un domaine «Matières diverses» pour absorber tout élément qui
mettrait en défaut ces domaines, témoin s'il était besoin du pragmatisme de
l'approche à postériori de leur système — un outil pratique tout au plus mais
sans illusions sur une quelconque valeur particulière d'un point de vue
philosophique. Ces choix donnent le tableau présenté figure
\ref{fig:lge_editors_domains}, d'aspect presque trivial par contraste avec le
profond travail de réflexion épistémologique qui le précède.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics{figure/text/LGE/avant-propos_domaines_1.png}
        \caption{Première partie du tableau dans \textit{LGE}, T1, p.XI}
        \label{fig:lge_editors_domains_1}
    \end{subfigure}
    \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics{figure/text/LGE/avant-propos_domaines_2.png}
        \caption{Deuxième partie du tableau dans \textit{LGE}, T1, p.XII}
        \label{fig:lge_editors_domains_2}
    \end{subfigure}
    \caption{Tableau de répartition du volume de texte prévisionnel de \textit{LGE} entre les différents domaines de connaissance}
    \label{fig:lge_editors_domains}
\end{figure}

Les articles de *LGE* comportent parfois des désignants marqués
typographiquement — la figure \ref{fig:anatomy_samples_lge} à la page
\pageref{fig:anatomy_samples_lge} en présentait déjà un exemple. Ils semblent
toutefois plus rares que dans l'*EDdA*: là où environ 70% des articles de
l'*EDdA* présentaient un désignant, cette proportion est (probablement très)
inférieure à 45% dans *LGE*. Il n'est pas encore possible d'accéder de manière
fiable aux désignants dans cette encyclopédie, mais cette borne supérieure a été
obtenue de la manière suivante. Sur les 134 820 articles identifiés (voir la fin
de la sous-section \ref{sec:corpus_preprocessing_lge}
p.\pageref{lge_segmentation}), seuls 76 786 possèdent des parenthèses sur leur
première ligne; mais pour 16 024 d'entre eux au moins ces parenthèses
contiennent un renvoi. Il y a bien, en toute rigueur, quelques articles
possédant à la fois un désignant et un renvoi comme l'article GELOCUS (La Grande
Encyclopédie, T18, p.699) mais la conjonction des deux est assez rare pour ne
pas perturber ce chiffre donné sans décimales et constituant de toute façon un
majorant grossier. Les 60 762 articles restants sont à l'origine de ce chiffre
de 45%. En réalité, dans *LGE* le dispositif typographique des désignants n'est
pas utilisé seulement pour attribuer un domaine de connaissance. Une
particularité majeure de cette œuvre réside dans la présence de notices
biographiques, exclues pour des raisons éditoriales des pages de l'*EDdA*. Dans
les articles de biographie, le prénom de la personne suit son nom de famille qui
sert de vedette à l'article, accolé entre parenthèses en tout début d'article
exactement comme les désignants, ce que montre la figure \ref{fig:loveling_lge}
où le prénom «Rosalie», entre parenthèses, suit le nom de famille «LOVELING».
Difficile donc de distinguer les deux sans posséder une liste exacte ou bien des
prénoms apparaissants ou bien des domaines utilisés (car les désignants ne
suivent pas les 14 domaines présentés à la figure \ref{fig:lge_editors_domains}
qui ne fournissent qu'un cadre catégorique assez vaste). Un rapide parcours de
la liste de ces 60 762 mot obtenus en position de désignant suffit pour se
rendre compte que les prénoms sont extrêmement fréquents, sans doute
majoritaires sur les noms de domaines abréviés. Un filtrage sur le caractère '.'
(caractéristique des désignants abréviés et normalement absent des noms de
personnes sauf certains titres ou erreurs d'[@=OCR]) ne ramène en effet que
12 062 articles: il y a sans doute légèrement plus de vrais désignants au total,
mais même une proportion de 50% par rapport aux prénoms paraît exagérément
élevée (même en admettant cette proportion, il y aurait alors $0.5 \times 60 762
= 30 381$ articles avec un désignant, soit seulement 22.5% de l'ensemble des
articles). Il y a donc un net recul de l'emploi des désignants depuis l'*EDdA*.

![Article LOVELING dans *LGE*, T22, p.699](figure/text/LGE/loveling_t22_p699.png){#fig:loveling_lge width=60%}

En revanche, un autre mécanisme est à l'œuvre pour situer le texte de *LGE* dans
un système de domaines: les articles les plus longs font l'objet d'une
construction complexe en sections et sous-sections (ce thème est également
développé dans la section \ref{sec:dictionaries_limits}) et au premier niveau
ces sections permettent de traiter la notion désignée par la vedette suivant ses
emplois dans différents domaines. Il n'est ainsi pas rare de trouver des
articles où les titres des sections feraient des désignants tout à fait
acceptables comme par exemple l'article CHEMIN (La Grande Encyclopédie, T10,
p.1025) dont un extrait est reproduit à la figure \ref{fig:chemin_lge}. Sa
section III. s'intitule «Droit» et ne contient qu'un renvoi à l'article VOIRIE,
la section IV. «Marine» ne comporte qu'un paragraphe (ces deux exemples montre
que le découpage en section ne correspond pas nécessairement à un besoin de
répartition harmonieux du volume de texte mais bien à une logique structurelle
propre) et la section V., «Art militaire» semble bien plus longue puisqu'elle
s'ouvre sur une sous-section (non-numérotée) «\textsc{Chemin couvert}». Cette
approche diffère de celle suivie dans l'*EDdA* où les termes ayant une existence
dans plusieurs domaines faisaient fréquemment l'objet de plusieurs entrées
distinctes — avec donc une répétition de la vedette à chacune — que seuls les
désignants différenciaient, ce qu'illustre la figure \ref{fig:air_edda}. De même
que certains «désignants» typographiques sont en réalité des prénoms, il n'est
pas aisé de relier systématiquement ces titres de sections à des domaines de
connaissance. Ainsi, les I. et II. du même article CHEMIN discuté plus haut sont
respectivement intitulés Voirie et Administration; le terme «voirie» ne renvoie
pas de manière évidente à une des grandes catégories présentées à la figure
\ref{fig:lge_editors_domains}, ou alors sans doute à la même catégorie
qu'«administration», montrant que la division en sections ne constitue pas une
partition en domaines de l'article. Certains articles tels ARGIOPE (La Grande
Encyclopédie, T3, p.868) possèdent des sections qui ne sont pas du tout liées à
un domaine de connaissance mais désambiguïsent un terme qui a été adopté aussi
bien pour parler d'une araignée (section I.) que d'un mollusque (section II.).

![Extrait de l'Article CHEMIN dans *LGE*, T10, p.1025](figure/text/LGE/chemin_t10_p1025.png){#fig:chemin_lge width=60%}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/EDdA/air_theol_t1_p236.png}
        \caption{Article AIR en Théologie dans l'\textit{EDdA}, T10, p.236}
        \label{fig:air_edda_theol}
    \end{subfigure}
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/EDdA/air_peinture_t1_p237.png}
        \caption{Article AIR en Peinture dans l'\textit{EDdA}, T10, p.237}
        \label{fig:air_edda_peinture}
    \end{subfigure}
    \begin{subfigure}[b]{0.60\textwidth}
        \includegraphics{figure/text/EDdA/air_musique_t1_p237.png}
        \caption{Article AIR en Musique dans l'\textit{EDdA}, T10, p.237}
        \label{fig:air_edda_musique}
    \end{subfigure}
    \caption{Trois entrées de l'\textit{EDdA} sur la vedette AIR}
    \label{fig:air_edda}
\end{figure}

#### La place de la Géographie {#sec:domains_geography}

Au milieu de ces domaines, la Géographie occupe une place particulière. Elle
semble du fait de ses très grandes applications pratiques — situer les villes,
permettre les voyages et donc le commerce — relever d'une sorte d'évidence qui
fait que sa place aux côtés des autres sciences est discutée au XVIII^ème^
siècle (voir la section \ref{sec:geo_intro} p.\pageref{sec:geo_intro}). Le choix
revendiqué par les éditeurs de l'*EDdA* de l'inclure dans ses pages s'impose
évidemment par la suite et cette place n'est plus discutée dans *LGE*. Il n'en
demeure pas moins troublant que la Géographie soit omise dans la première
énumération de domaines au début de la préface de l'œuvre.

Dans le «Systême» des encyclopédistes (voir figure \ref{fig:systeme_figure}), la
Géographie est présentée aux côté de l'*Uranographie* et de l'*Hydrographie*
sous la *Cosmographie* au sein des sciences mathématiques. À un siècle où les
techniques de mesure issues de la géométrie permettent des grandes avancées
métrologiques telle que la détermination de la position des méridiens et de leur
mesure, ce choix peut paraître relever de réalités pratiques de l'époque. Il
renvoie pourtant à des considérations plus théoriques pour inscrire l'*EDdA*
dans la filiation de Bacon et de Locke [@laboulais_geographie_2020, p.3]. Une
autre approche, celle de Robert de Vaugondy — géographe ordinaire du roi et
contributeur de l'*EDdA* — rend compte d'une Géographie plus diverse comme le
fait également remarquer @laboulais_geographie_2020 [p.2]. À la fin de l'article
GÉOGRAPHIE (L'Encyclopédie, T7, p.608) dont il est l'auteur, il propose de
mettre la géographie astronomique à part, reconnaissant sa dette envers
l'Astronomie et partitionne le reste de la Géographie en *naturelle*,
*historique*, *civile* ou *politique*, *sacrée*, *ecclésiastique* et enfin
*physique*. Cette division est un pas certain vers la position moderne de la
Géographie au sein des Sciences Humaines et Sociales, et sa relation privilégiée
avec l'Histoire. D'ailleurs, dès le préambule de l'*EDdA*, un paragraphe associe
*Chronologie* et *Géographie*, dressant un parallèle entre une «science des
tems» (sic) et une «des lieux» mais les encyclopédistes n'identifient pas
*Chronologie* et *Histoire*, puisqu'ils font de celle-ci la mère de celle-là,
attribuant en parallèle la même relation de parenté à la *Géographie* et à
l'*Astronomie* respectivement. @laboulais_geographie_2020[p.5] montre pourtant
un autre lien entre les deux disciplines, mis en avant par un autre contributeur
de l'*EDdA*, Paris de Meyzieu, pour des motifs pédagogiques: à l'article ÉCOLE
MILITAIRE (L'Encyclopédie, T5, p.311), il présente la Géographie comme un
prérequis à l'Histoire, dont elle servirait à situer les grands événements. En
ce sens, il déroge à la classification du «Systême figuré» pour sortir la
Géographie de la branche de l'arbre consacrée à la raison et la placer sur celle
de la mémoire. Dans *LGE*, le rapprochement est consommé puisque les deux
disciplines sont associées et leurs articles décomptés ensemble dans le tableau
des répartition des articles entre différents domaines présenté à la figure
\ref{fig:lge_editors_domains}. De plus, si la liste des collaborateurs du projet
n'associe pas chacun à une discipline précise, il est à noter que sur les 12
membres du projet dont la qualité contient le mot «géographie», la moitié
exactement d'entre eux sont des enseignants, agrégés ou professeurs, à la fois
d'histoire et de géographie. Cette observation est cohérente avec le rôle bien
connu joué par l'association de ces deux disciplines à l'école dans la
constitution d'une identité nationale, qui trouve son impulsion dans la
Révolution Française et n'a été remise en cause ni par les empires ni par les
républiques ultérieures pour culminer dans les célèbres lois «Jules Ferry»
[@chevalier_geographie_2013, §2 et 3].

Au travers des analyses présentées dans cette section, la notion de domaine de
connaissance apparaît donc bien comme fondamentale. Elle joue un rôle central
dans la structuration des encyclopédies et se matérialise sous plusieurs formes
plus ou moins rigides, des désignants aux simple tournures de phrases en passant
par des titres de section. La Géographie apparaît comme un prérequis à plusieurs
autres disciplines, ce qui lui donne une place plutôt avantageuse dans l'ordre
d'«enchaînement» des connaissances que les encyclopédistes — tant au XVIII^ème^
qu'au XIX^ème^ siècle — s'efforcent de saisir. Elle semble occuper en quelque
sorte un rôle de pivot en permettant l'articulation de sciences aussi diverses
que les Mathématiques et l'Histoire. Cette position spécifique constitue un
première confirmation de la pertinence du choix de singulariser cette discipline
au sein des encyclopédies dans les présents travaux.

### Prétraitements {#sec:corpus_preprocessing}

Après ce premier tour d'horizon de la matière textuelle disponible dans le
corpus, il est temps d'évoquer quelques étapes de traitements nécessaires pour
pouvoir exploiter ces données.

#### L'Encyclopédie de Diderot et d'Alembert {#sec:corpus_preprocessing_edda}

L'unité d'étude de cette thèse est l'article. En effet, il s'agit de la plus
grande unité pensée par les auteurs d'un dictionnaire ou d'une encyclopédie pour
être lue de manière continue du début à la fin. Les ouvrages lexicographiques ne
sont pas faits pour une lecture linéaire de la lettre A à la lettre Z, ils
constituent une collection de textes (voir la section
\ref{sec:EdlA_lexicography}) auxquels on peut se référer indépendamment: les
articles. C'est au niveau des articles qu'interviennent les différents appareils
de navigation de l'œuvre: renvois pour des parcours chaînés, désignants pour des
parcours thématiques. Or, les fichiers livrés par l'[@=ARTFL] représentent
chacun un tome. La première tâche à conduire sur ces données est donc la
segmentation des tomes en articles.

Mais l'utilisation d'outils habituels pour manipuler des arbres XML est gênée
par le fait que l'encodage des fichiers transmis n'est pas tout à fait valide.
Certains éléments comme `sc`, `blockquote` ou `page` sont utilisés alors qu'ils
n'existent pas dans le schéma [@=TEI]. Plus préoccupant, d'autres ne sont pas
complets: plusieurs éléments `<index/>` utilisés pour représenter les
métadonnées des articles au moyen de leurs attributs (sans contenus donc, ils
sont censés être auto-fermants) n'ont pas leur `/` final, ce qui syntaxiquement
signifie qu'ils ne se ferment jamais et englobent tout le reste du fichier après
eux. Enfin, certains sont tout simplement mal formés: on trouve des balises
pourvues d'attributs mais pas d'un tag (comme `<XREEF="Incorrupticoles">`, sans
`<` ouvrant avant leur tag, ainsi que quelques erreurs dans les entités XML.
Mais le problème le plus spectaculaire réside dans les quelques occurrences de
`&amp`;, qui représenterait en XML le caractère '&' (esperluette), si seulement
son dernier caractère était un ';' (point virgule): il s'agit malheureusement en
réalité du caractère UTF-8 `U+037e` utilisé en grec pour marquer l'interrogation
à la place de notre '?', quasi identique visuellement. L'irrégularité de ces
erreurs et en particulier l'homographie en jeu dans la dernière incite à penser
que l'encodage de ces fichiers a été réalisé par des opérateurs humains.

Pour corriger ces erreurs ainsi que pour procéder à la division des tomes
proprement dite tout en extrayant les métadonnées des articles, quelques scripts
ont été écrits. Ils constituent la suite `EDdA-Clinic`[^edda-clinic] dont le
code source est librement accessible sous license BSD-3 et hébergé sur
l'instance `gitlab` d'Huma-Num. À l'aide des outils de cette suite, la division
des tomes est réalisée selon la requête XPath `/TEI/text/body/div1` dont le sens
est le suivant: les articles correspondent aux éléments XML `<div1/>` placés
immédiatement sous le `<body/>` qui est sous le `<text/>`, commun à tout un tome
de l'encyclopédie, et l'ensemble est contenu dans une balise `<TEI/>`
conformément à la spécification du consortium [@=TEI].

[^edda-clinic]:
    [https://gitlab.huma-num.fr/alicebrenon/eddaclinic](https://gitlab.huma-num.fr/alicebrenon/eddaclinic)

Le contexte de la requête, c'est-à-dire le reste de l'arbre traversé pour
arriver au nœud `<body/>` contenant tous les `<div1/>` est conservé et
réappliqué autour de chaque article, de manière à ce que chaque fichier
d'article individuel conserve l'ensemble des métadonnées présentes sur le tome
dont il est issu, comme l'illustre la figure \ref{fig:edda_split}. À l'issue de
cette phase, le texte de l'*EDdA* se répartit en 74 198 fichiers. Il est à noter
que les tomes 1 à 8 comportent un frontispice qui rappelle le titre de l'œuvre
entre les avertissements des auteurs et le début des articles. Dans les fichiers
de l'[@=ARTFL] ces frontispices sont également encodés par une balise `div1`
comme les articles. Ces 8 frontispices deviennent donc 8 fichiers, mais le choix
d'encodage très différent fait par l'[@=ARTFL] pour représenter ces parties qui
ne sont pas à proprement parler textuelles comme peuvent l'être le discours
préliminaire ou les avertissements des différents tomes empêche d'en récupérer
les informations habituellement associées aux articles (auteur, domaine de
connaissance…). Par conséquent, ils se retrouvent absents des fichiers de
métadonnées et donc écartés dans les présents travaux, du fait de l'emploi
systématique des métadonnées pour manipuler les textes. On peut donc considérer
qu'il y a plutôt 74 190 articles, et des trous dans la numérotation (pas
d'article 4 dans les tomes 1,2,4,5,6 et 7, pas d'article 7 dans le tome 3, pas
d'article 3 dans le tome 8).

\begin{figure}
    \centering
    \begin{minipage}{.8\textwidth}
        \begin{subfigure}[m]{0.25\textwidth}
            \includegraphics[width=\textwidth]{figure/preprocessing/1_file_per_tome.png}
            \caption{Un fichier par tome}
            \label{fig:edda_split_tome}
        \end{subfigure}
        \hspace{.8cm}\rightarrow\hspace{.2cm}
        \begin{subfigure}[m]{0.75\textwidth}
            \centering
            \begin{minipage}{\textwidth}
                \hspace{.08\textwidth}
                \includegraphics[width=.33\textwidth]{figure/preprocessing/1_file_per_article.png}
                \hspace{.1\textwidth}
                \includegraphics[width=.33\textwidth]{figure/preprocessing/1_file_per_article.png}
                \vspace{.4cm}
            \end{minipage}
            \includegraphics[width=.33\textwidth]{figure/preprocessing/1_file_per_article.png}
            \caption{Un fichier par article}
            \label{fig:edda_split_article}
        \end{subfigure}
    \end{minipage}
    \caption{Le contexte XML est dupliqué pour chaque article}
    \label{fig:edda_split}
\end{figure}

#### La Grande Encyclopédie {#sec:corpus_preprocessing_lge}

La publication de la première version numérique de *LGE* réalisée à l'issue du
projet DISCO-LGE[^DISCO-LGE] repose en grande partie sur un logiciel développé
spécifiquement dans le but de traiter les données fournies par la BnF au format
XML-ALTO (voir la section \ref{sec:xml_formats}). Ce logiciel, nommé
`soprano`[^soprano], permet d'obtenir des articles à partir du contenu des
pages. Cette opération complexe fait intervenir trois étapes majeures. Comme
l'ordre de lecture détecté par l'[@=OLR] appliqué en même temps que l'[@=OCR]
est parfois erroné, un réordonnancement des blocs est d'abord appliqué. Puis, en
fonction de leur position sur la page et de leur contenu, les blocs sont
étiquetés pour distinguer leur rôle et différencier le corps du texte
constituant les articles des autres éléments qui apparaissent fréquemment dans
une page d'encyclopédie et sont décrits à la sous-section
\ref{sec:encyclopedia_anatomy} p.\pageref{sec:encyclopedia_anatomy} (en
particulier, les éléments identifiés en bleu, vert et mauve sur la figure
\ref{fig:anatomy_outside_top_lge}). Enfin le flot linéaire de texte est déduit
des deux étapes précédentes et une opération de segmentation tâche de repérer
les débuts d'articles pour pouvoir les répartir dans des fichiers distincts et
associer à chacun la liste des métadonnées correspondantes.

[^soprano]: [https://gitlab.inria.fr/abrenon/soprano](https://gitlab.inria.fr/abrenon/soprano)

Avant de pouvoir effectuer tous ces traitements, la première fonctionnalité de
`soprano` est d'appliquer un filtrage pour enlever de la sortie des éléments
ALTO repérés manuellement: le plus souvent des «scories», faux-positifs de
l'[@=OCR] qui a cru reconnaître un caractère dans une tâche sur le papier ou
dans une courbe d'une des gravures, mais aussi des données tabulaires, des
équations ou des formules chimiques, abondantes dans les pages de *LGE* et
susceptibles de gêner les analyses linguistiques faites sur le texte, notamment
l'annotation en syntaxe. Le repérage manuel a été fait à l'aide du logiciel
`chaoui`[^chaoui], également développé dans le cadre du projet DISCO-LGE. Cet
outil complémentaire de `soprano` permet aussi de visualiser la structure et
l'ordre des blocs tels qu'ils sont représentés par les fichiers ALTO. La figure
\ref{fig:lge_last_page} montre ainsi la dernière page du premier tome de *LGE*
entièrement dédiée à la ville d'Alcala-de-Hénarès (avec une gravure pleine page
représentant le palais archiépiscopal visible sur la figure
\ref{fig:lge_last_page_photo}) et l'ordre dans lequel l'[@=OLR] appliqué à cette
page a placé les différents blocs qu'il a identifiés sur la page (figure
\ref{fig:lge_last_page_chaoui}). Les numéros représentent l'ordre des éléments
XML correspondant dans le fichier ALTO qui encode la page: ils se trouvent
effectivement au bon endroit sur la page mais l'ordre des numéros ne correspond
pas à l'ordre de lecture. Le problème en jeu ici réside dans la difficulté à
concilier un objet bidimensionnel — la page — et un objet unidimensionnel — le
texte.

En effet une page possède une hauteur et une largeur; les signes imprimés dessus
peuvent se positionner les uns par rapport aux autres selon un axe horizontal
(l'un est à la gauche de l'autre) mais aussi selon un axe vertical (l'un est
au-dessous de l'autre). Le texte quant à lui est une abstraction intrinsèquement
linéaire, au moins dans ses réalisations concrètes (avant toute analyse
syntaxique ou sémantique): il est formé d'un flot de morphèmes constituant un
flot de mots qui s'assemblent en un flot de phrases (pouvant à leur tour
éventuellement former un flot de paragraphes, sections, chapitres suivant la
complexité de la production, ou d'articles dans le cadre de cette thèse).
Chacune de ces unités peut seulement venir «avant» ou «après» une autre: ni
«au-dessus/en-dessous» ni «à gauche/à droite» — une propriété sans doute issue
du langage oral qui se déploie dans la seule dimension temporelle: au-delà des
différences structurelles propres qui peuvent distinguer l'oral de l'écrit,
toute production écrite peut donner lieu à une production orale (en la lisant)
et doit donc s'inscrire dans cette unidimensionalité. À l'échelle d'un bloc de
texte, ces notions se traduisent par la lecture des lignes de haut en bas et, à
l'intérieur d'une ligne, des mots de gauche à droite. Mais entre blocs,
l'évidence d'ordre s'estompe: les pages comme celle présentée à la figure
\ref{fig:lge_last_page} barrée d'une figure se lisent de gauche à droite en
commençant par les deux colonnes au-dessus de la figure (blocs 8 puis 9) avant
de poursuivre sous la figure (blocs 2 puis 3). Mais dans des pages sans figure
pleine page, il faut lire tous les blocs de la colonne de gauche de haut en bas
puis continuer en haut de la colonne de droite. Cette règle est assez simple à
implémenter mais la figure \ref{fig:lge_last_page_chaoui} montre que l'[@=OLR]
utilisé par la BnF ne l'applique pas. À cette règle simple s'ajoute la nécessité
de séparer tous les éléments péritextes (voir sous-section
\ref{sec:encyclopedia_anatomy}) parfois intégrés à tort dans un des blocs de
texte. Le meilleur modèle du texte d'une encyclopédie est un flot linéaire de
texte entouré de satellites qui existent hors de lui (les éléments péritextes ne
sont pas «à lire» à un moment particulier du texte).

[^chaoui]:
    [https://gitlab.huma-num.fr/alicebrenon/chaoui](https://gitlab.huma-num.fr/alicebrenon/chaoui)

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics{figure/text/LGE/lge_t1_p1200.png}
        \caption{Une photo de la page}
        \label{fig:lge_last_page_photo}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics{figure/preprocessing/cuts.png}
        \caption{L'ordre des blocs inféré par l'OLR pour cette page}
        \label{fig:lge_last_page_chaoui}
    \end{subfigure}
    \caption{Dernière page du premier tome de \textit{LGE}}
    \label{fig:lge_last_page}
\end{figure}

\label{lge_segmentation}La valeur principale de `soprano` réside cependant dans
sa capacité à détecter les débuts d'article afin de séparer les entrées. C'est
cette fonctionnalité qui a fait l'objet des améliorations les plus importantes
au cours de cette thèse par rapport à la version obtenue à l'issue du projet
DISCO-LGE. Une fois de plus, le problème abstrait est excessivement simple: les
débuts d'articles correspondent aux vedettes, qui sont repérables
typographiquement par leur indentation et leur casse haute. Malheureusement, la
présence de scories dans les marges ainsi que la très légère inclinaison des
colonnes gênent la mesure précise de l'indentation des lignes. Il faut des
conditions très laxes, que vérifient parfois des éléments qui ne sont pas des
débuts de paragraphe (des signatures de contributeur, des titres dans un tableau
ou même une légende de figure intégrée à tort dans un bloc de texte par
l'[@=OLR] et qui n'a pas été repérée et corrigée par `soprano`). Les majuscules
ne permettent guère de gagner en précision: les erreurs de l'[@=OCR] peuvent les
transformer arbitrairement en chiffres ou en signes de ponctuation, ce qui
empêche également de pouvoir bénéficier de l'information capitale qu'elles
doivent constituer une suite croissante selon l'ordre alphabétique. À la place,
il est nécessaire de procéder avec la plus grande tolérance, en vérifiant des
ressemblances graphiques entre caractères pour ne pas rejeter des vedettes et
sous-segmenter, c'est-à-dire laisser au sein du même fichier plusieurs articles.
Devant la difficulté à vérifier l'ordre alphabétique des entrées, une règle
simple mais pragmatique a été ajoutée depuis la version issue du projet
DISCO-LGE: `soprano` permet de préciser la vedette du premier et du dernier
article et ne vérifie que l'appartenance des vedettes potentielles à cet
intervalle pour prendre la décision de créer un nouvel article. Avec cette régle
le nombre précédent de 229 475 articles tombe à 134 820, nombre d'articles
retenu pour les présents travaux. Un rapide échantillonnage montre qu'il reste
des problèmes de sous-segmentation (des articles groupés au sein du même
fichier), vraisemblablement plus que de sur-segmentation (un article éclaté en
plusieurs fichiers). Les auteurs de *LGE* en mentionnent 200k quand ils écrivent
l'article ENCYCLOPÉDIE (La Grande Encyclopédie, T15, p.1014), mais la fin du
projet est encore assez lointaine alors et quand on tient compte des difficultés
financières qu'a connu le projet à sa toute fin [@jacquet_pfau2015, p.90], il
paraît possible que cette prévision n'ait pas été tout à fait atteinte. Le
nombre réel d'article dans *LGE* demeure donc inconnu, mais pourrait être de
l'orde de 150k à 200k articles, soit environ du double au triple de celui de
l'*EDdA*. Il faut pourtant poursuivre, car la quête d'une qualité parfaite est
sans fin et des erreurs subsistent toujours, même dans les sources les plus
fiables. Ainsi la version de l'*EDdA* de l'[@=ARTFL] contient aussi au moins un
cas de sous-segmentation, l'entrée pour MÉLER UN CHEVAL (L'Encyclopédie, T10,
p.313) contient également un article de *Géographie ancienne*, MÉLÈS qui décrit
une rivière d'Asie.

Les techniques présentées ci-dessus ne constituent que la première étape pour
accéder au texte. Son encodage a nécessité une étude séparée pour déterminer les
éléments appropriés à la représentation de ses constituants et à sa
structuration. Cette étude, qui n'a concerné que *LGE* puisque l'*EDdA* était
déjà disponible en format XML, fait l'objet de la section suivante
\ref{sec:corpus_lge_encoding}.

