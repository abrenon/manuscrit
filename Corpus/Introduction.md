La notion de corpus occupe une place centrale dans les [@=HN], représentant
l'objet autour duquel s'articulent de nombreux projets. Ce chapitre présente non
seulement les tâches qui ont été réalisées le plus tôt dans cette thèse mais
également celles qui ont requis le plus de travail. Toutefois, sa place avant
les autres chapitres ne reflète pas un ordre strict de dépendance : au
contraire, de nombreux allers et retours ont été nécessaires entre les données
et les outils utilisés pour les exploiter: ce chapitre est une tentative de
saisir un état satisfaisant d'un effort en réalité continu et pouvant se
poursuivre sans fin.

Il commence par présenter les objets de l'étude et introduire les concepts
requis pour décrire ces encyclopédies et les articles qu'elles contiennent, au
premier rang desquels la notion de domaine de connaissance qui sous-tend cette
thèse de part en part. Puis, la section \ref{sec:corpus_lge_encoding}
s'intéresse aux conséquences des différences entre encyclopédies et
dictionnaires sur l'encodage des fichiers, qu'elle aborde à l'aide de méthodes
d'exploration systématiques de graphes. Enfin, la section
\ref{sec:corpus_application}, en accord avec la remarque ci-dessus sur
l'impossibilité d'arrêter jamais l'amélioration de la qualité du corpus, tente
de consigner les leçons apprises au fil du projet pour structurer les données
de la manière la plus pratique et la plus flexible possible.

