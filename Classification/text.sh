#!/bin/sh

source ./chapter.sh 'Classification automatique en domaines de connaissances {#sec:domains_classification}'

cat Classification/Introduction.md
cat Classification/Models.md
cat Classification/Relations.md
cat Classification/Application.md
