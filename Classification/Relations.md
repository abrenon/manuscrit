## Relations entre *Géographie* et autres domaines de connaissance {#sec:geo_relations}

Après avoir tâché de tirer les meilleures performances des différents modèles de
classification à la section précédente (\ref{sec:classifiers_comparison}), cette
section s'intéresse au contraire aux erreurs qui subsistent dans leurs
prédictions. Faisant en effet l'hypothèse que les erreurs commises par le modèle
reflètent dans une certaine mesure les proximités qui existent entre les
domaines de connaissance elle se propose ainsi de comprendre les relations qui
lient la *Géographie* aux autres domaines de connaissance.

Deux tendances gouvernent le choix d'un modèle dont étudier les erreurs. Il faut
qu'elles soient assez nombreuses pour fournir matière à observation et donc ne
pas prendre le meilleur, mais à l'inverse il est vraisemblable que les
prédictions d'un classifieur obtenant de trop mauvais résultats recèlent
davantage de bruit que de vérités profondes sur les contenus des articles. Pour
ces deux raisons, c'est le modèle *SGD+TF-IDF* entraîné sur tous les articles
disponibles (colonne n°3 du tableau \ref{tab:result_1} p.\pageref{tab:result_1})
qui a été retenu pour cette étude et sera utilisé tout au long de cette section.
Ce modèle parvient à classer correctement 9 630 des 11 702 articles, soit 82%.
Ce sont les 2 072 autres, les erreurs, qui intéressent cette étude.

### Des erreurs éloquentes {#sec:model_errors}

Pour comprendre les facteurs amenant à ces erreurs il faut identifier clairement
les paramètres en jeu, représentés schématiquement par la figure
\ref{fig:classification_errors_situation}.

![Les paramètres en jeu lors de la classification automatique d'un article](figure/errors.png){#fig:classification_errors_situation width=50%}

Premièrement trois entités différentes (les ellipses sur la figure
\ref{fig:classification_errors_situation}) associent des classes aux articles.
La première et la seule qui puisse constituer une vérité indiscutable est le
choix des éditeurs de l'*EDdA* au travers de l'emploi de désignants.
L'[@=ENCCRE] les normalise et, en leur absence, émet parfois sa propre
classification basée sur des indices présents dans le texte (notamment les
*marques de domaine* décrites à la section \ref{sec:knowledge_domains}
p.\pageref{sec:knowledge_domains}). À la différence de ces deux sources, le
modèle est une troisième origine de classes pour les articles qui, elle,
peut — et doit —  être remise en cause puisqu'il s'agit d'accorder ses sorties
aux deux sources de vérité terrain utilisées pour l'entraîner puis l'évaluer.
Les erreurs étudiées dans cette partie correspondent aux configurations
possibles de différences entre ces trois origines des classes, sur un ou
plusieurs domaines.

Après la question des origines, un deuxième paramètre s'ajoute dans l'équation.
Tous les travaux de la section \ref{sec:classifiers_comparison} considèrent en
effet un problème de classification monoclasse : pour les 5% d'articles assignés
à plusieurs domaines par les encyclopédistes (voir la section
\ref{classification_datasets} p.\pageref{classification_datasets}), une
seule classe a été retenue pour entraîner le modèle (correspondant à la cellule
en vert sur la figure). Or d'une part il ne semble pas y avoir d'ordre
d'importance entre les désignants présents en tête d'un article, et d'autre part
les combinaisons de classes apparaissant ensemble au sein de ces 5% possèdent
une certaine cohérence. Il y a ainsi plusieurs articles relevant à la fois de
l'*Histoire Naturelle* et de la *Botanique*[^bromelia] mais aucun relevant à la
fois de la *Pharmacie* et du *Spectacle*. Malgré le choix d'une classe parmi
plusieurs pour simplifier la tâche du classifieur automatique, les combinaisons
de classe existantes sont également une source d'information intéressante.

[^bromelia]: l'article BROMELIA (L'Encyclopédie, T2, p.434) présenté à la figure
    \ref{fig:anatomy_samples_edda} p.\pageref{fig:anatomy_samples_edda} dans la
    description des parties d'un article d'encyclopédie en offre un exemple

Ce problème posé par le choix d'une classe unique à partir d'un ensemble de
classes existe non seulement du côté de la vérité terrain mais également du côté
de la prédiction. Techniquement, un classifieur fait ses prédictions en
attribuant à chaque classe du jeu d'étiquettes une probabilité d'être correcte
et en retenant la classe qui obtient la plus élevée (en bleu sur la figure
\ref{fig:classification_errors_situation}). Or, pour plus de la moitié des
erreurs du modèle — 1 082, soit 9% du total — la classe qu'il fallait prédire a
tout de même obtenu la deuxième plus haute vraisemblance, et pour les 990
autres, elle apparaît souvent, même avec un rang plus élevé (3^ème^ ou 4^ème^).
Ainsi, même quand il se trompe, le modèle ne donne pas des réponses entièrement
absurdes et il y a donc un intérêt à considérer ses erreurs pour comprendre les
similarités et les différences entre les groupes de domaines.

À la lumière des remarques ci-dessus, et pour mieux comprendre les erreurs du
modèle, il peut donc être intéressant de confronter aux deux sources de vérités
possibles que sont les encyclopédistes et l'[@=ENCCRE] non seulement la
prédiction retenue par le modèle (celle de plus haute probabilité) mais aussi
les suivantes, tout en tenant compte du fait que certains articles relèvent en
réalité de plusieurs domaines à la fois. Cela permettrait d'apporter de
l'information sur la nature intriquée de l'organisation de la connaissance dans
l'*EDdA* et permettrait d'«aller au-delà du système de classification originel
des éditeurs pour commencer à tracer les contours des pratiques discursives à
plusieurs niveaux qui contribuent à la riche texture dialogique de l'*EDdA*»
[@roe_discourses_2016, p.10].

En un sens, cette étude conduite sur la seule *EDdA* s'inscrit entièrement dans
la perspective de d'Alembert citée à la section \ref{sec:domains_build_classes}
en considérant la classification automatique comme une projection utile pour
visualiser les articles: ceux dont la classe prédite correspond à la classe qui
leur avait été assignée par les éditeurs et ceux pour lesquels elle ne
correspond pas qui sont tout aussi intéressants. Mais il y a une tension
indépassable entre la tâche d'évaluer une classification générée automatiquement
et l'accès à une compréhension qu'il n'y a pas forcément une classification
unique et «correcte» pour un article donné.

#### Les erreurs autour de la *Géographie* {#sec:geography_errors}

Un point de départ simple pour cette étude consiste à étudier les faux négatifs
et les faux positifs des articles de *Géographie*. Sur les 2 621 articles de
cette classe présents dans le jeu de test, 191 ont été mal classés par le
modèle.

##### Faux négatifs

Seuls 39 articles étiquetés *Géographie* ont été classés à un autre domaine que
*Géographie* par le modèle. Ils constituent donc des faux négatifs du modèle
*SGD+TF-IDF* pour cette classe, dont la répartition est visible à la figure
\ref{fig:sgd_geo_false_negatives}. En tout, 12 domaines autres que *Géographie*
sont prédits parmi les 39 faux négatifs, avec une forte prévalence de
l'*Histoire* et de l'*Antiquité*. Ces faux négatifs pour la classe *Géographie*
peuvent se regrouper en trois types.

![Articles étiquetés *Géographie* par l'ENCCRE mais pas par le modèle (faux négatifs)](figure/histogram/classification/SGD+TF-IDF_domainGroup/Géographie/falseNegatives_frequencies.png){#fig:sgd_geo_false_negatives width=100%}

Le premier, celui des «faux multiclasses» correspond au cas des articles ayant
un seul désignant et pour lesquels le modèle n'a attribué que la deuxième ou
troisième plus haute probabilité à la classe qu'il fallait prédire. Ce type de
faux négatif correspond par exemple à l'article INDOUSTAN (L'Encyclopédies, T8,
p.686). Il ne possède que le désignant «Géog.» et fait donc partie du domaine
ENCCRE *Géographie*. La meilleure prédiction pour cet article est *Histoire*
(avec 50% de certitude), et le domaine qu'il fallait prédire, *Géographie* vient
ensuite avec 47,6%. En regardant son contenu on observe, après 5 lignes qui
situent l'«Indoustan» dans l'espace, un paragraphe plus long, de 12 lignes au
total, qui trace un historique de la région. Si le modèle a donc bel et bien
échoué à reproduire la logique éditoriale de l'*EDdA*, sa prédiction est tout à
fait satisfaisante du point de vue de la compréhension du texte qu'on pourrait
attendre d'un lectorat humain et il n'aurait donc pas été surprenant que
l'article soit pourvu d'un désignant pour l'*Histoire* en plus de celui pour la
*Géographie*. Les articles de toponymes de l'*EDdA* comportent très fréquemment
une multitude de contenus qui ne sont pas nécessairement «géographiques», et
cela est particulièrement le cas sous la plume de Jaucourt qui signe cet
article. La prévalence de l'*Histoire* sur la figure
\ref{fig:sgd_geo_false_negatives} est un premier signe de sa proximité avec la
*Géographie*.

Le deuxième type de faux négatifs, celui des «désaccords» inclut des articles
auxquels les auteurs de l'*EDdA* n'avaient à l'origine pas assigné de domaine de
connaissance et que l'[@=ENCCRE] a classés à *Géographie* mais pour lesquels
*Géographie* ne fait pas partie des prédictions les plus probables du modèle.
L'article de Diderot sur les îles des AÇORES (L'Encyclopédies, T1, p.110)
présente d'après le modèle une très faible probabilité de relever de la
*Géographie* (la classe est prédite avec seulement 2.7% de confiance). Les
classes les plus probables d'après le modèle sont *Commerce* (45%), suivie de
*Métiers* (43%) et *Arts et métiers* (8.6%). Et en effet, en allant lire
l'entrée *AÇORES* elle-même, on constate qu'après une seule ligne situant
l'archipel à la fois très grossièrement («de l'Amérique») et trop précisément
(par des coordonnées en longitude et latitude mais sans donner aucune relation à
d'autres terres que le lectorat serait susceptible de connaître), la majeure
partie de l'article est consacrée à décrire en détail son intérêt pour le
commerce, comme un point de passage sur des routes commerciales, et en énumérant
longuement toutes les marchandises qui en proviennent ou y transitent. Sur 1 597
articles *Commerce*, seuls 3 possèdent aussi la classe *Géographie* selon les
sources de domaines (colonne de gauche sur la figure
\ref{fig:classification_errors_situation}). Ce type de faux négatifs révèle en
fait les liens qui existent entre certains contenus, ici la *Géographie* et le
*Commerce*. Alors qu'il n'apparaît pas explicitement dans le «Système figuré» de
l'*EDdA* (voir figure \ref{fig:systeme_figure} p.\pageref{fig:systeme_figure}),
ce lien est pourtant assez fréquent sous la plume de Diderot, qui malgré la
rigueur assumée de son style dans les articles de *Géographie*
[@laramee_production_2017, p.168] intègre volontiers des «considérations sur le
commerce local» [@vigier_articles_2022, p.69].

La dernière sorte de faux négatifs, à l'inverse du premier type (et qu'on pourra
donc naturellement nommer le type des «vrais multiclasses»), regroupe un
ensemble d'articles qui possédaient plusieurs désignants, devenus plusieurs
ensembles de domaines après les travaux de l'[@=ENCCRE] et dont le *domaine
regroupé* unique choisi pour l'étude était *Géographie*. Par exemple GOLGOTHA
relevait initialement de la *Géographie* et de la *Théologie* d'après les
éditeurs, correspondant aux ensembles de domaines *Géographie* et *Religion*
pour l'[@=ENCCRE], mais la seule classe *Géographie* a été retenue comme vérité
pour évaluer le modèle. Les deux classes les plus probables d'après celui-ci
sont *Religion* (60%) et *Histoire* (31%)). De manière surpenante, *Géographie*
n'obtient que 3%. Le nom «Golgotha» réfère à l'endroit où le messie de la
religion chrétienne a été crucifié et l'article, fidèle au reste de la ligne
éditoriale de l'*EDdA*, consiste en une comparaison des écrits des premiers
chrétiens sur l'endroit. Il se termine en rejetant les théories les plus
imaginatives sur l'origine du nom. L'auteur, l'abbé Mallet, tranche en faveur de
la suggestion de Saint Jérôme selon laquelle le Golgotha aurait été nommé ainsi
(*calvaire* en Hébreu) car les crânes étaient abandonnés sur ce lieu d'exécution
traditionnel. Sur cet exemple il semble que l'erreur du modèle révèle surtout en
fait les limites d'un choix automatique fait lors de la préparation du corpus
d'une classe parmi plusieurs. Ce dernier type des «vrais multiclasses» pourrait
certainement intéresser une étude séparée sur les différences entre
contributeurs dans leurs approches à la *Géographie* et aux autres domaines de
connaissance. Pour l'entrée GOLGOTHA, il est ainsi intéressant de noter que son
auteur, l'abbé Mallet est un théologien qui a rédigé de nombreux articles (plus
de 2 000 en tout), mais seulement 7 en *Géographie*. Pour d'autres articles,
l'approche propre à un autre contributeur, le chevalier de Jaucourt, est
développée à la section \ref{sec:biographies} à partir de la page
\pageref{sec:biographies}.

##### Faux positifs

Pour 152 articles, le modèle a prédit *Géographie* comme la classe la plus
probable alors qu'ils ne relevaient de ce domaine ni d'après ses auteurs ni
d'après l'[@=ENCCRE]. L'article ROCHER (L'Encyclopédie, T14, p.313) visible à
la figure \ref{fig:rocher_edda}, classé en *Géographie* par le modèle alors que
son désignant le situe en *Grammaire*, constitue un exemple de faux positif.
Extrêmement bref, il se contente d'établir l'équivalence sémantique entre les
mots *rocher*, *roc* et *roche* et contient un renvoi vers l'entrée *ROC*.

![Article ROCHER, l'*EDdA*, T14, p.313](figure/text/EDdA/rocher_t14_p313.png){#fig:rocher_edda width=60%}

Si l'on tient compte des prétraitements appliqués avant classification, en
particulier l'élimination des mots vides, le modèle a même reçu comme entrée
pour cet article:

::: displayquote
rocher s. m. chose roc roche roc.
:::

L'abondance de termes liés aux propriétés physiques du sol oriente
vraisemblablement le modèle vers la *Géographie* comme classification la plus
probable. Ici encore dans le contexte des faux positifs, le modèle révèle son
utilité pour mettre en lumière des contenus thématiques qui resteraient cachés
en s'en tenant à la seule classification des auteurs, ici du vocabulaire
disciplinaire qui pourrait relever de la géographie (voir section
\ref{sec:contrasts_objects} p.\pageref{sec:contrasts_objects}). En général, les
probabilités de prédiction pour un article donné décroissent très vite pour
atteindre 0 après quelques classes (ce qui traduit la certitude d'après le
modèle que l'article ne relève pas des classes suivantes): il est remarquable
que pour ROCHER le modèle attribue une vraisemblance non nulle à 16 classes
différentes, traduisant l'ambiguïté d'un article très pauvre en éléments
distinctifs et difficile à situer parmi les domaines de connaissances. Après
*Géographie*, le modèle propose ainsi dans l'ordre la *Grammaire* (17%, ce qui
correspondrait au premier type de faux négatifs des «faux multiclasses»
identifiés plus haut si l'on conduisait pour la *Grammaire* l'étude conduite ici
pour la *Géographie*), *Marine* (14%), et *Loi* (13%).

Les faux positifs se répartissent entre 23 domaines différents, soit presque
deux fois plus que pour les faux négatifs. Les domaines impliqués dans les faux
négatifs sont d'ailleurs tous inclus dans ces 23 domaines à l'exception de la
*Minéralogie*, à l'origine d'un seul faux négatif. Leur distribution, visible à
la figure \ref{fig:sgd_geo_false_positives}, révèle une forte prépondérance de
l'*Histoire* et de l'*Antiquité*, les deux classes les plus confondues avec la
*Géographie*. On retrouve également les classes *Marine*, *Religion* et, dans
une moindre mesure, *Histoire naturelle* dont seulement 3 articles ont été pris
pour de la *Géographie* sur les 152 faux positifs — soit 2% — alors que les 3
articles de *Géographie* prédits comme appartenant à cette classe représentaient
7.7% des 39 faux négatifs et classaient l'*Histoire naturelle* en 5^ème^
position des domaines les plus prédits à la place de la *Géographie* par le
modèle. Plusieurs domaines présents mais assez rares parmi les faux négatifs
(dans les 50% les moins représentés) se classent parmi les faux positifs les
plus fréquents (dans les 33% les mieux représentés): *Droit - Jurisprudence* qui
est le 3^ème^ domaine causant le plus de faux positifs pour le modèle,
*Physique*, *Grammaire* et *Belles-lettres*. Deux domaines totalement absents
des faux négatifs, *Agriculture* et *Architecture*, sont plutôt bien représentés
parmi les faux positifs puisqu'ils sont respectivement les 10^ème^ et 11^ème^
domaines les plus fréquents sur 23 domaines au total.

![Articles étiquetés *Géographie* par le modèle mais pas par l'ENCCRE (faux positifs)](figure/histogram/classification/SGD+TF-IDF_domainGroup/Géographie/falsePositives_frequencies.png){#fig:sgd_geo_false_positives width=100%}

Les différences de distribution entre faux positifs et faux négatifs suggèrent
une apparition fréquente de contenus géographiques en dehors du seul domaine de
la *Géographie*, davantage que l'inverse. Ces contenus nourrissent les articles
de nombreux autres domaines, montrant la Géographie comme une science
transverse, nécessaire dans l'articulation du discours des autres sciences.

#### Graphe des Plus Proches Voisins {#sec:graph_model}

L'étude préliminaire qui précède révèle déjà des traits intéressants de la
*Géographie* mais serait trop fastidieuse à conduire sur chacun des 38 *domaines
regroupés* et, surtout, ignorerait des dynamiques complexes impliquant plus de
deux domaines à la fois. Pour approfondir l'exploitation des erreurs du modèle,
il faut des outils permettant d'adopter une approche d'ensemble.

Les matrices et les graphes sont des représentations complémentaires et
équivalentes d'un même objet. À tout graphe dirigé fini et sans arêtes multiples
correspond une matrice d'adjacence pondérée. Réciproquement toute matrice carrée
dont les coefficients peuvent représenter les poids d'un graphe peut être vue
comme sa matrice d'adjacence pondérée (ou matrice des poids). En effet, dans un
graphe de $n$ nœuds n'admettant qu'une seule arête entre deux nœuds donnés, il
peut y avoir au plus $n^2$ arêtes. Si le graphe est complet ou si les
coefficients qui pondèrent ses arêtes peuvent prendre une valeur particulière
pour représenter une absence d'arête (typiquement $0$), alors ces arêtes peuvent
être stockées dans une matrice carrée de taille $n$: sa matrice des poids.
Inversement, une matrice $M$ de taille $n$ permet de construire un graphe en
créant $n$ nœuds puis en ajoutant une arête pondérée du coefficient $M_{i,j}$ du
nœud $i$ vers le nœud $j$ (de même que pour la traduction du graphe en matrice,
en n'ajoutant éventuellement aucune arête si ce coefficient a une valeur
particulière, typiquement $0$) pour tous les coefficients de la matrice $M$.
Cette équivalence est représentée par la figure \ref{fig:mat_graph_equivalence}.

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \[\begin{pmatrix}
      m_{0,0} & m_{0,1} & \dots & m_{0,n}\\
      m_{1,0} & m_{1,1} & \dots & m_{1,n}\\
      \vdots & \ddots & \\
      m_{n,0} & m_{n,1} & \dots & m_{n,n}\\
    \end{pmatrix}\]
    \caption{Les coefficients dans la matrice}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{figure/exampleGraph.png}
    \caption{Les poids des arêtes du graphe}
  \end{subfigure}
  \caption{Représentation schématique de l'équivalence entre matrice et graphe}
  \label{fig:mat_graph_equivalence}
\end{figure}

Il faut remarquer ici la part d'arbitraire qui existe dans les deux opérations
de conversion puisqu'elles reposent sur un choix implicite pour faire
correspondre le sens d'une arête sur le graphe avec l'ordre entre colonne et
ligne dans la matrice. Il aurait tout à fait été possible de stocker en
$M_{j,i}$ le poids de l'arête de $i$ vers $j$ ou, de manière équivalente de
construire à partir de $M_{i,j}$ l'arête de $j$ vers $i$ dans le graphe. La
seule chose qui importe est d'adopter des conventions compatibles (ce qui est le
cas de celles choisies) pour les deux transformations
`matrice`$\rightarrow$`graphe` et `graphe`$\rightarrow$`matrice` afin que leur
composée revienne à la matrice ou au graphe de départ, et pas à la matrice
transposée ou au graphe opposé (le même graphe en renversant le sens des
arêtes).

Ces deux concepts représentent donc d'une façon complémentaire un même type
d'objet: les systèmes dynamiques discrets et linéaires ([@=SDDL]). Ceux-ci
comprennent par exemple les flux sur un réseau, une étape de transformation d'un
modèle physique simplifié ou dans le cas présent la prédiction des classes d'un
ensemble d'articles. Cela est clair en revenant à la définition d'une matrice de
confusion: ses coefficients $m_{i,j}$ représentent la proportion d'objets de la
classe $i$ prédits par le modèle comme appartenant à la classe $j$. Dans
l'évaluation d'un modèle sur un jeu de test, l'état initial est caractérisé par
une certaine distribution d'éléments de chaque classe, qui constitue donc un
premier vecteur. Après la prédiction du modèle, il y a une deuxième distribution
qui correspond à la répartition des classes prédites et peut se représenter par
un deuxième vecteur de même dimension que le premier (le nombre de classes
possibles), et qui est aussi proche du premier que les performances du modèle
sont bonnes. Le rapport entre ces deux vecteurs est la matrice de confusion du
modèle, c'est-à-dire que le nombre d'objets dans une classe prédite (son
cardinal) sera une combinaison linéaire des cardinaux des différentes classes de
départ, pondérés par les coefficients de la matrice de confusion dans la colonne
correspondant à la classe prédite. S'il est vrai que ce système ne possède que
deux états (le modèle donne toujours la même prédiction sur une entrée donnée,
il n'y a pas de sens à lui faire effectuer une nouvelle prédiction), il n'en
constitue pas moins un [@=SDDL] et il est possible d'utiliser les techniques
employées habituellement sur ces objets.

La dualité entre matrices et graphes permet d'explorer les [@=SDDL] en adoptant
alternativement les points de vue d'un graphe ou d'une matrice. Lorsque le
nombre de nœuds et d'arêtes devient trop élevé dans un graphe pour pouvoir
exploiter sa représentation graphique (voire parfois même pour pouvoir
simplement tracer une telle représentation en deux dimensions), l'exploitation
de sa matrice des poids, qui dans tous les cas donne «seulement» un tableau
bidimensionnel permet de prendre le relai. À l'inverse, il peut être compliqué
d'obtenir une intuition de l'évolution d'un système à partir de sa seule matrice
d'adjacence pondérée alors qu'un graphe traduit facilement une idée de
dynamique.

Dans le cadre de cette étude, c'est donc à partir de la matrice de confusion
normalisée, présentée à la figure \ref{fig:confusion_matrix} page
\pageref{fig:confusion_matrix} et notée $C$ dans ce qui suit, que sera étudié le
système correspondant aux prédictions du modèle *SGD+TF-IDF*. Suite à la
remarque précédente, la convention choisie pour le sens des arêtes signifie que
l'analyse est orientée vers ses faux négatifs (les lignes de $C$). La
représentation de la matrice de confusion du modèle sous forme de graphe revient
donc à le voir «déplacer» les articles du *domaine regroupé* auquel ils
appartiennent vers le domaine qu'il considère le plus probable (cette
visualisation comme un flux permet également de comprendre en quoi la situation
est celle d'un [@=SDDL] puisque les analyses de flux en sont des exemples
typiques). La convention contraire (basée sur les colonnes et donc les faux
positifs) aurait signifié que les articles d'un domaine prédit par le modèle
auraient été issus des vrais domaines pointés par les arêtes partant du nœud
correspondant). Elle semble moins intuitive, ce qui justifie la convention
adoptée.

La matrice de confusion d'un modèle parfait est la matrice identité: des $1$ sur
la diagonale principale (du coin supérieur gauche au coin inférieur droit) et
des $0$ partout ailleurs, signifiant que pour chaque classe donnée tous les
articles de cette classe sont correctement identifiés par le modèle («laissés au
même endroit»). Avec l'équivalence discutée tout au long de cette partie, le
graphe correspondant à un tel modèle parfait serait formé d'autant de nœuds
qu'il y a de classes, tous isolés, et ne possédant chacun qu'une unique arête
vers eux-mêmes (une boucle) pondérée de la valeur $1$. Mais le graphe d'un vrai
modèle comme celui étudié est bien plus difficile à représenter, comme le
suggèrent les nombreuses cases colorées hors de la diagonale sur la figure
\ref{fig:confusion_matrix}. Il est d'ailleurs difficile de distinguer
précisément quelles cases sont nulles ou pas à cause des différences d'ordre de
grandeur dans les coefficients. En reprenant les données numériques, 484
cellules sont non nulles sur les 1 444 cellules au total dans la matrice ($38
\times 38$). Ce «bruit de fond» sur la matrice correspondrait donc à un graphe
extrêmement dense, avec $\frac{1}{3}$ des arêtes d'un graphe complet
($\frac{484}{1444} \simeq 0.335$). Il est donc difficile de se donner une
intuition de la dynamique sous-jacente à cette matrice de confusion.

Pour dépasser cette difficulté, on simplifie le système considéré pour ne
considérer que les erreurs les plus significatives du modèle. Cela revient à
considérer la notion de «Plus Proche Voisin» ([@=PPV] au centre du problème des
*K-Nearest Neighbors* (les «K plus proches voisins») sur le graphe des faux
négatifs de sa matrice de confusion. La résolution de ce problème a des
applications dans de nombreux domaines, des réseaux [@guo_link_2023] à la vision
par ordinateur [@bewley_advantages_2013] où il met en jeu des nombres de points
tellement grands qu'il faut des structures de données particulières pour
accélérer la recherche comme les arbres de dimensions K
[@friedman_algorithm_1977] ou trouver des heuristiques pour approcher la
solution plus rapidement. Si la notion existe déjà, elle se trouve appliquée ici
dans le cadre le plus simple possible au point de la rendre à peine
reconnaissable: il ne s'agit de trouver qu'un unique voisin pour chaque nœud du
graphe ($K = 1$) et il n'y a que 38 «points» (les classes des *domaines
regroupés*). De plus, là où la plupart des points considérés dans les problèmes
de *KNN* traités dans la littérature sont multidimensionnels et contiennent de
grandes quantités de données parfois combinées de manière complexe, les seules
grandeurs à comparer dans cette instance du problème sont des nombres flottants
(les scores du modèle pour chaque paire de classe, entre $0$ et $1$) ce qui
permet d'effectuer tous les calculs et de comparer les résultats pour obtenir
*le* plus proche voisin de manière certaine. Ce cas particulier du problème se
distingue aussi par le fait qu'il s'agit habituellement de trouver une distance
minimale alors qu'il faut ici garder la classe dont le score dans la matrice de
confusion est le plus grand.

Appliquée au problème de l'exploitation du graphe des faux négatifs de la
matrice de confusion du modèle, la notion de [@=PPV] permet de considérer une
matrice et un graphe représentant les faux négatifs les plus importants de
chaque classe. Pour souligner la parenté avec le problème plus général, et pour
pouvoir ensuite porter la notion à d'autres objets que des matrices et des
graphes issus des mesures de performance d'un classifieur automatique, ces deux
objets seront nommés respectivement MPPV (Matrice des Plus Proches Voisins) et
GPPV (Graphe des Plus Proches Voisins) du modèle. La MPPV est la matrice
d'adjacence pondérée du GPPV. Puisqu'il est possible de résoudre le problème de
[@=PPV] complètement et de manière naïve dans le cas présent, il est en pratique
plus aisé de commencer par décrire la MPPV à partir de la matrice de confusion
du modèle, puis de la considérer comme une matrice d'adjacence pour générer le
GPPV correspondant. Pour obtenir la MPPV à partir de la matrice de confusion, il
suffit de mettre à 0 toutes les cellules diagonales (celles qui d'après la
remarque ci-dessus encodent les prédictions exactes du modèle qui ne
contiennent donc pas d'information sur ses erreurs) puis de calculer la valeur
maximale sur chaque ligne et, pour une ligne donnée, rendre également nulles
toutes les cellules sauf celle(s) où le maximum a été atteint. Ici encore, la
simplification a lieu par ligne puisque ce sont les lignes qui régissent le
graphe: ce choix correspond à ne garder que l'arête la (ou les arêtes les) plus
forte(s) qui quitte(nt) chaque nœud, c'est-à-dire le domaine qui attire le plus
de faux négatifs du domaine de départ. En appliquant cette procédure à $C$, on
obtient la MPPV pour le modèle *SGD+TF-IDF* présentée à la figure
\ref{fig:nn_confusion_matrix}.

![Matrice des Plus Proches Voisins pour la méthode SGD+TF-IDF sur le jeu de test.](figure/classification/SGD+TF-IDF_domainGroup/confusionMatrixNN.png){#fig:nn_confusion_matrix width=63%}

À partir de cette matrice, il est aisé de construire le GPPV correspondant en
appliquant la procédure utilisée plus haut pour prouver le sens retour dans
l'équivalence entre graphe et matrice, à savoir construire le graphe à partir
des coefficients de la matrice. Le résultat de cette opération appliquée à la
MPPV de la figure \ref{fig:nn_confusion_matrix} est le GPPV du modèle
*SGD+TF-IDF* visible à la figure \ref{fig:nn_confusion_graph}. Ses arêtes
reprennent le schéma de couleur utilisé dans ce manuscrit pour les matrices de
confusion: le mauve y est d'autant plus prononcé que le taux de faux négatifs y
est important. Ainsi, le cas des classes *Minéralogie* et *Arts et métiers*,
très mal reconnues, ressort nettement sur le graphe: plus de la moitié de leurs
articles sont incorrectement prédits par le modèle (en faveur des classes
*Histoire Naturelle* et *Métiers* respectivement).

![Graphe des Plus Proches Voisins pour la méthode SGD+TF-IDF sur le jeu de test.](figure/graph/SGD+TF-IDF_domainGroup/confusionMatrixNN.png){#fig:nn_confusion_graph}

La particularité la plus visible est sans doute la très forte attractivité de la
classe *Métiers*, traduisant la densité relativement élevée de la colonne
correspondant à cette classe sur la MPPV de la figure
\ref{fig:nn_confusion_matrix}. De manière cohérente avec le résultat des
analyses du début de cette section, la *Géographie* pointe sur l'*Histoire* car
ce domaine est le plus fort attracteur de ses faux négatifs. En revanche, il
apparaît que les relations ne sont évidemment pas symétriques car si la plupart
des faux positifs de *Géographie* viennent de l'*Histoire*, le modèle «envoie»
encore plus d'article de ce domaine vers *Droit - Jurisprudence*. Entre ces deux
classes par contre, il y a une réciprocité, car l'*Histoire* est aussi le
domaine qui attire le plus de faux négatifs de *Droit - Jurisprudence*. Une
autre configuration intéressante est celle de la classe *Mathématiques* dont
partent deux arêtes[^2arêtes] (c'est le seul nœud dans ce cas): les classes
*Physique* et *Grammaire* attirent autant de faux négatifs de la classe
*Mathématiques*. Sans cet équilibre, le graphe serait partagé en deux
composantes indépendantes: la partie supérieure de la figure
\ref{fig:nn_confusion_graph} rassemble plutôt des domaines liés à un certain
savoir-faire et la partie inférieure des domaines liés à la connaissance et à la
réflexion. Il n'y a pas d'explication évidente à cette division qui soit liée à
la méthodologie employée: elle n'est probablement pas stricte et certains liens
s'expliqueraient sans doute davantage par des ressemblances de thématiques et de
vocabulaire que par une démarche similaire (on peut penser par exemple au lien
entre *Histoire Naturelle* et *Agriculture*), mais il n'en demeure pas moins
intéressant de remarquer que  la *Physique* et la *Chimie* font toutes deux
partie de la composante supérieure (ce qui est assez surprenant avec une
perspective contemporaine très théorique de ces deux disciplines). La
*Géographie*, quant à elle, se situe bien dans la composante inférieure ce qui
semble traduire — si l'on accepte l'interprétation précédentes des deux
partitions — un domaine qui compile des informations plus qu'il ne décrit des
procédés.

[^2arêtes]: ce fait peut sembler déconcertant mais correspond aux pluriels entre
    parenthèses proposés un peu plus haut: en effet, vu la méthode de
    construction, si la valeur maximale est atteinte par plusieurs arêtes, il
    n'y a aucune raison d'en préférer une et elles sont toutes gardées

En représentant les dynamiques des faux négatifs dans les prédictions d'un
modèle, le GPPV constitue donc déjà une ressource visuelle utile pour identifier
les proximités existant entre les classes. La section suivante poursuit
l'exploration des proximités entre domaines en considérant les [@=PPV] d'autres
graphes.

### Similarités lexicales {#sec:lexical_similarities}

Il est difficile de déterminer les raisons qui amènent le modèle *SGD-TF-IDF* à
commettre les erreurs étudiées dans la section précédente
(\ref{sec:model_errors}). Si l'étude de cas menée à partir de la page
\pageref{sec:geography_errors} sur les erreurs autour de la classe *Géographie*
a mis en évidence des passages consacrés à différentes disciplines, elle ne dit
pas ce qui permet de les identifier. La présente section s'intéresse aux
ressemblances en termes de lexique entre les *domaines regroupés* comme un
facteur explicatif possible. Le but est de déterminer si la structure mise en
évidence précédemment sur le GPPV (voir figure \ref{fig:nn_confusion_graph})
peut être reproduite ou au moins approchée à partir de propriétés lexicales
simples des articles.

Le modèle *SGD+TF-IDF* étudié, comme tout modèle d’AA, base ses prédictions de
classification pour un article sur un ensemble de traits des tokens de cet
article défini empiriquement lors de l’apprentissage mais inconnu pour qui
l’utilise. Cette raison contribue à ce que les modèles d’apprentissage
automatique soient parfois appelés «boîtes noires» et constitue un argument en
faveur de la démarche d’Intelligence Artificielle «Explicable» («XAI» ou
«eXplainable Artificial Intelligence» en anglais), visant à comprendre à quoi
s'intéressent les modèles. Cette préoccupation peut avoir au moins deux buts:
d'une part améliorer les performances des modèles (en préselectionnant les
propriétés à observer pour s'assurer que tous utilisent celles du meilleur
modèle plutôt que de laisser chaque modèle s'en approcher individuellement) et
d'autre part transférer leurs «découvertes» statistiques pour apprendre des
choses sur les traits qui caractérisent un genre ou, dans le cas présent, un
domaine.

En plus des propriétés elles-mêmes (lemme ou forme, avec ou sans [@=POS]), la
façon de les combiner peut également avoir un impact sur les performances du
modèle. Ainsi, la notion de n-gramme propose de remplacer les tokens en entrée
par des séquences brèves, faisant l'hypothèse que des fenêtres de mots contigus
peuvent lever certaines ambiguïtés lexicales et jouer un rôle décisif dans les
prédiction d'un modèle. Des travaux comme @tan_use_2002 ou
@mladenic_feature_2003 étudient comment sélectionner et combiner les propriétés
apportant le plus d'information sur les textes afin de mieux les classer. La
construction des n-grammes elle-même fait l'objet de recherches pour augmenter
son efficacité [@garcia_efficient_2021]. La longueur des n-grammes joue un rôle
important: si les performances s'accroissent en utilisant des n-grammes par
rapport à de simples tokens (c'est-à-dire quand $n > 1$ puisque les tokens
peuvent être vus comme des «séquences de 1 token» donc des 1-grammes), elles
décroissent en revanche quand $n$ devient trop grand. Si toutes les études
menées ne trouvent pas la même longueur seuil exactement, toutes semblent
s'accorder sur sa faible valeur (inférieure à 5). @furnkranz_study_1998 et à sa
suite @tan_use_2002[p.531] estiment que les performances se dégradent au-delà de
3.

Pour cette raison, dans ce qui suit les articles seront représentés tour à tour
par des séquences de leurs tokens de longueur 1, 2 puis 3 pour explorer à quel
point les similarités lexicales des n-grammes obtenus ressemblent aux erreurs du
modèle. D'autres paramètres s'ajoutent à $n$ pour contrôler quelles séquences
vont être considérées: le nombre de n-grammes associés à chaque classe ($k$ dans
ce qui suit) ainsi que la métrique utilisée pour effectuer les comparaisons.

Premièrement, soit $\mathcal{L}$ le lexique de l'*EDdA*, c'est-à-dire l'ensemble
de tous les mots qui apparaissent dans les articles. Pour tout entier $n \in
\mathbb{N}$, un n-gramme est un élément de l'ensemble $\mathcal{L} ^n$, que l'on
peut aussi écrire informellement comme le produit Cartésien répété $\mathcal{L}
\times \mathcal{L} \times \ldots \mathcal{L}$, et ce $n$ fois: en effet, un
n-gramme a $n$ composantes, chacune à valeur dans $\mathcal{L}$ ce qui
correspond à la notion ensembliste de produit Cartésien, ou en logique à la
notion de conjonction: avoir un n-gramme, c'est avoir $n$ mots du lexique à la
fois. Cette étude considère donc suite à la remarque ci-dessus les 1-grammes,
les 2-grammes et les 3-grammes. Pour simplifier les calculs et éviter les
disparités causées par les différences entre les classes en termes de longueur
et de nombre d'articles, elle ne retient que les n-grammes les plus fréquents
pour chaque classe, une même quantité pour toutes les classes, ce qui constitue
le deuxième paramètre de l'expérience présenté plus haut, appelé $k$ et qui
prendra successivement les valeurs 10, 50 et 100 (c'est-à-dire, que chaque
classe sera tour à tour représentée par ses 10 n-grammes les plus fréquents, ses
50 plus fréquents, puis ses 100 les plus fréquents). Utiliser plusieurs valeurs
pour l'expérience permet d'observer l'impact de $k$ et de contrôler dans quelle
mesure cette simplification distord les résultats.

Pour représenter les classes par une combinaison de n-grammes, on considère
maintenant l'espace $\mathbb{R}^{|\mathcal{L}|^n}$ (l'espace vectoriel sur le
corps $\mathbb{R}$ de dimension la taille du lexique $\mathcal{L}$ à la
puissance $n$) — une dimension certes grande devant les espaces vectoriels
manipulés communément en géométrie élémentaire mais encore finie. Une base
naturelle de cet espace est la famille canonique de vecteurs caractéristiques
$\{e_W, W \in \mathcal{L} ^n\}$ correspondant à chaque n-gramme
et dont les composantes sont toutes nulles sauf une, celle qui correspond au
n-gramme représenté par le vecteur considéré. Tant qu'il s'agit de compter des
occurrences de chaque n-gramme, les coefficients ne pourront qu'être entiers
(c'est-à-dire à valeur dans $\mathbb{N}$), mais faire le choix de $\mathbb{R}$
et donc de se placer dans un espace vectoriel plutôt qu'un simple module permet
d'effectuer ensuite des divisions sur les coefficients, utiles pour normaliser
les vecteurs obtenus. Ce formalisme revient à faire des *sac de n-grammes*,
d'une manière similaire à laquelle *BoW* créait des *sac de mots* à partir des
tokens.

Pour un n-gramme $g$ donné, on a besoin de compter son nombre total
d'occurrences parmi tous les articles de la classe $c$, noté $|g|_{c, n}$,
c'est-à-dire qu'on considère la fonction $g \rightarrow |g|_{c, n}$ de l'espace
$\mathcal{L}^n \rightarrow \mathbb{N}$. Par exemple, $\mathrm{|("à",
"plusieurs", "choses")|_{Géographie, 3}}$ représente le nombre d'occurrence du
trigramme $\mathrm{("à", "plusieurs", "choses")}$ dans l'ensemble des articles
classés à *Géographie* par le modèle. Comme chaque classe $c$ sera représentée
par l'ensemble de ses n-grammes les plus fréquents, ce qui sera noté
$\mathcal{G}_{c, n, k}$, on pourra alors lui associer un vecteur $V_{c, n, k}$
dans l'espace vectoriel $\mathbb{R}^{|\mathcal{L}|^n}$ défini ci-dessus. Les
composantes de ce vecteur sont le compte d'occurrences dans la classe avec la
fonction précédente $|\dots|_{c, n}$ pour chacun de ses $k$ n-grammes les plus
fréquents et 0 pour tous les autres n-grammes possibles. Plus formellement, cela
signifie qu'on peut écrire la combinaison linéaire:

$$V_{c, n, k} = \sum_{W \in \mathcal{G}_{c, n, k}} |W|_{c, n} \cdot e_W$$

Représenter ainsi les classes par des vecteurs donne la possibilité d'exprimer
des mesures de similarité à l'aide d'outils comme le produit scalaire et les
notions dérivées de normes et de distance. On définit ainsi le produit scalaire
sur les vecteurs des classes comme la somme des produits des composantes des
vecteurs qu'ils ont en commun:

$$\langle V_{c_i, n, k}, V_{c_j, n, k} \rangle = \sum_{W \in \mathcal{G}_{c_i, n, k} \cap \mathcal{G}_{c_j, n, k}}{|W|_{c_i, n} \times |W|_{c_j, n}}$$

Et, par extension, la norme d'un vecteur de classe est définie par la racine
carrée de son produit scalaire avec lui-même (c'est-à-dire la racine carrée de
la somme des carrés de ses coefficients, soit l'équivalent de la norme
euclidienne manipulée couramment sur les espaces à 2 ou 3 dimensions, mais
généralisée à l'espace de dimension $|\mathcal{L}|^n$ considéré):

$$\|V\| = \sqrt{\langle V, V \rangle}$$

Avec ces définitions, il devient possible de considérer deux métriques. La
première, qu'on appellera la similarité *cardinale* consiste à simplement
compter la proportion de n-grammes que deux classes données ont en commun. En
supposant que $c_i$ et $c_j$ sont deux classes, leur similarité *cardinale*
$\langle c_i, c_j \rangle_{n, k, card}$ peut s'exprimer ainsi (la division par
$k$ sert à normaliser le nombre obtenu pour le ramener entre $0$ et $1$ — deux
ensembles de $k$ éléments ne peuvent pas avoir plus de $k$ éléments en
commun — pour la rendre comparable indifféremment de $k$):

\begin{equation}
    \label{eq:count_similarity}
    \langle c_i, c_j \rangle_{n, k, card} = \frac{\|\mathcal{G}_{c_i, n, k} \cap \mathcal{G}_{c_j, n, k} \|}{k}
\end{equation}

La seconde, qu'on appellera la similarité *scalaire*, s'obtient en calculant
leur produit scalaire, normalisé en le divisant par le produit de leurs normes.
Toujours avec la même notation, cette autre métrique peut s'écrire:

\begin{equation}
    \label{eq:dot_similarity}
    \langle c_i, c_j \rangle_{n, k, scal} = \frac{\langle V_{c_i, n, k}, V_{c_j, n, k} \rangle}{\|V_{c_i, n, k}\| \times \|V_{c_j, n, k}\|}
\end{equation}

À partir de ces deux métriques, il est possible de générer des matrices de
similarité pour chacune des configurations possibles, 18 au total: 3 valeurs
possibles pour le paramètre $n$ (la taille des n-grammes, pouvant être 1, 2 ou
bien 3), 3 pour $k$ (pour rappel 10, 50 ou 100) et 2 métriques à considérer
chaque fois (*cardinale* ou *scalaire*). La figure \ref{fig:similarity_matrices}
reproduit deux d'entres elles correspondant à $k = 100$ et $n = 2$ (100
2-grammes les plus fréquents par classe), pour les deux métriques possibles.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[width=\textwidth]{figure/matrix/lexicalSimilarity/2-grams/keysIntersection_top100.png}
      \caption{Pour la similarité \textit{cardinale}}
      \label{fig:similarity_matrices_count}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[width=\textwidth]{figure/matrix/lexicalSimilarity/2-grams/colinearity_top100.png}
      \caption{Pour la similarité \textit{scalaire}}
      \label{fig:similarity_matrices_dot}
    \end{subfigure}
    \caption{Similarités lexicale des 100 2-grammes les plus fréquents}
    \label{fig:similarity_matrices}
\end{figure}

La première diagonale de ces matrices constitue une ligne de symétrie car la
relation qu'elles représentent est symétrique: pour toute paire de classes $c_i$
et $c_j$, $\langle c_i, c_j \rangle = \langle c_j, c_i \rangle$ (vrai pour
chacune des deux métriques considérées, toutes deux définies à partir
d'opérations symétriques — produits et sommes d'entiers, intersections
d'ensembles). Par contraste, la matrice de confusion du modèle *SGD+TF-IDF* était
asymétrique car le modèle ne confond pas les articles de *Géographie* avec ceux
d'*Histoire* avec le même taux d'erreur qu'il confond ceux d'*Histoire* avec de
la *Géographie* (un fait déjà visible sur le GPPV du modèle à la figure
\ref{fig:nn_confusion_graph} p.\pageref{fig:nn_confusion_graph} et discuté à la
section \ref{sec:model_errors}). Cette différence est une conséquence de la
simplification opérée sur les résultats des prédictions du modèle.

Les matrices sont également de plus en plus creuses à mesure que les paramètres
$n$ et $k$ croissent, car la probabilité que deux classes partagent un même de
leurs n-grammes les plus fréquents décroît alors: d'une part plus un n-gramme
est long, plus il tend à être unique et d'autre part plus la liste de n-grammes
considérée est longue, plus des n-grammes différents ont des chances
d'apparaître.

Enfin, la première métrique, la similarité *cardinale* présente l'avantage d'être
moins «bruitée» que la similarité *scalaire*, c'est-à-dire que la couleur de fond des
matrice de similarités générées est plus claire, comme cela est visible sur la
figure \ref{fig:similarity_matrices_count} (à gauche) par rapport à la figure
\ref{fig:similarity_matrices_dot} (à droite). Cela peut sembler contre-intuitif
si l'on regarde le calcul de la similarité *cardinale* comme simplement un cas
particulier de la similarité *scalaire* dans lequel tous les coefficients des
vecteurs sont mis à 1, au coefficient multiplicatif de leur norme près. En
effet, on pourrait s'attendre à ce que le produit scalaire des vecteurs de deux
classes soit systématiquement très inférieur au décompte de leurs vecteurs de
base en commun, en ce sens qu'avoir des coefficients différents ne peut que
rendre les vecteurs moins colinéaires.

Mais cet effet est vaincu par la distribution de «masse» des coefficients dans
les vecteurs: si deux vecteurs ont seulement très peu de composantes en commun,
mais que leurs coefficients les plus élevés se trouvent précisément sur ces
composantes, alors leurs projections sur le sous-espace des vecteurs qu'ils ont
en commun produira des vecteurs de taille proche de leur taille initiale. Si de
plus les distributions des composantes de ces vecteurs projetés ont des formes
semblables (le même ordre quand on les classe par valeur de leurs coefficients,
et avec le même ratio entre elles), alors leur produit scalaire peut rester
assez proche du produit de leurs normes. Dans ce cas, la similarité *scalaire*
donnera une valeur proche de 1, alors qu'ils ne partageront que quelques
n-grammes, ce qui donnera une similarité *cardinale* très faible. En d'autres
termes, les classes n'ont que peu en commun mais ce qu'elles ont en commun est
précisément ce qui leur est le plus fondamental. Les composantes qu'elles ne
partagent pas ne jouent qu'un rôle tout à fait marginal. Bien entendu, pour que
cet effet s'applique, les deux classes doivent avoir au moins un n-gramme en
commun, sans quoi leur produit scalaire sera simplement nul.

Cette configuration contre-intuitive se produit par exemple avec les 3-grammes
des classes *Histoire* et *Religion*, visibles au tableau
\ref{tab:histoire_religion}. Ces deux classes ne partagent que 25 de leur 100
3-grammes les plus fréquents (ce qui correspond donc à un score de similarité
*cardinale* de 0.25) mais leur produit scalaire normalisé (la similarité
*scalaire*) est de 0.61 car leurs projections sur le sous-espace des 25
3-grammes qu'elles ont en commun restent très proches d'elles (où leurs
projections représentent respectivement 87.8% et 87.3% de leurs normes
initiales).

\begin{table}
    \centering
    \input{table/topNGrams/3-grams/Histoire_Religion_common.tex}
    \caption{Les 3-grammes les plus fréquents communs aux classes \textit{Histoire} et \textit{Religion} avec leurs comptes d'occurrences dans chacune}
    \label{tab:histoire_religion}
\end{table}

Le tableau contient 10 n-grammes provenant d'informations sur l'emploi
grammatical des articles («s.f.pl.» est l'abréviation de «substantif féminin
pluriel», voir la section \ref{sec:encyclopedia_anatomy}
p.\pageref{sec:encyclopedia_anatomy}) et 2 qui sont des signatures ou des
mentions (du même auteur, «c. d. j.» étant certainement l'abréviation du
«Chevalier De Jaucourt»). Si les premiers sont sans doute peu pertinents pour
cette étude (sauf à supposer que les proportions de termes féminins ou pluriels
d'un domaine puissent avoir une signification), les signatures d'auteur peuvent
avoir leur importance: d'une part parce que la capacité d'un même auteur à
intervenir (ou être cité) dans deux domaines peut être un signe de leur
proximité, d'autre part parce que dans ce cas son style personnel peut augmenter
artificiellement leur ressemblance s'il a signé une part suffisamment grande des
articles des deux domaines concernés. La notion de temps long dans le passé est
traduite par 5 autres n-grammes: «avant jesus christ», «depuis long tems» et
«depuis tems -là» (reliquat de «depuis ce tems-là» après élimination des mots
vides, voir la section \ref{classification_datasets}
p.\pageref{classification_datasets}). Les autres n-grammes sont plus
difficile à analyser: 2 montrent la prévalence de termes d'origine grecque, 4
semblent des tournures assez communes, et les 2 dernières concernent à nouveau
des auteurs. Le fait de référer à Montesquieu (auteur de l'«Esprit des Lois»)
par une périphrase pourrait être propre au style d'un contributeur de ces deux
domaines (Jaucourt ?), mais pourrait également être biaisé par l'observation des
3-grammes: «Montesquieu» n'a pas de raison d'être environné toujours des mêmes
mots et son nom a donc très peu de chance d'obtenir assez d'occurrence pour
remonter parmi les 3-grammes les plus fréquents, alors que l'expression
«l'auteur de l'Esprit des Lois», devient après la préparation du corpus le
3-gramme «auteur esprit loi», unique, et donc décompté systématiquement à
chacune de ses occurrences. Enfin le dernier 3-gramme laissé, «trévoux chambers
gramme» correspond vraisemblablement à une bibliographie sommaire et à une
signature maltraitée par le lemmatiseur. En effet deux sources très communément
employées par les auteurs de l'*EDdA* sont le *Dictionnaire universel de
Trévoux*, dont plusieurs éditions ont été imprimées entre 1704 et 1771, et la
*Cyclopaedia* de Chambers, qui a elle aussi connu plusieurs éditions entre 1728
et 1753 et dont la traduction en français était le but initial du projet de
l'*EDdA* (voir section \ref{sec:corpus_edda} p.\pageref{sec:corpus_edda}). Le
mot «gramme», assez inattendu ici, a sans-doute été réintroduit par le
lemmatiseur à la place d'un «g.», lettre employée par l'abbé Mallet, un
contributeur important des domaines *Histoire* et *Religion* pour signer ses
articles.

Il est difficile d'exploiter les matrices de similarités de la figure
\ref{fig:similarity_matrices} au-delà des remarques structurelles déjà faites et
de tirer des n-grammes plus que les quelques remarques qualitatives ci-dessus.
De la même façon que pour l'étude de la matrice de confusion du modèle
*SGD+TF-IDF* menée à la section \ref{sec:graph_model}, leur restriction aux
[@=PPV] permet d'en tirer des graphes mettant plus clairement en évidence les
ressemblances lexicales majeures entre les domaines. C'est ainsi que la matrice
de la figure \ref{fig:similarity_matrices_count} permet après simplification de
générer le graphe de la figure \ref{fig:nn_similarity_graph_count}. La même
procédure a été appliquée à chacune des 18 matrices de similarités correspondant
au 18 configurations possibles (des paramètres $n$, $k$, et de la métrique
considérée) pour générer 18 GPPV de similarités, qui ne sont pas tous reproduits
ici mais qui ont été analysés.

![Graphe des Plus Proches Voisins pour la similarité *cardinale* mesurée sur les 100 2-grammes les plus fréquents de chaque classe.](figure/graph/lexicalSimilarity/2-grams/keysIntersection_top100NN.png){#fig:nn_similarity_graph_count width=85%}

Plusieurs critères distinguent assez nettement tous ces graphes du GPPV pour les
confusions du modèle *SGD+TF-IDF* (voir figure \ref{fig:nn_confusion_graph}
p.\pageref{fig:nn_confusion_graph}). Alors que le GPPV du modèle était connexe
(presque partagé en 2 composantes, rassemblées seulement par la classe
*Mathématiques*), 15 se divisent en au moins 3 composantes (jusqu'à 6 sur celui
de la figure \ref{fig:nn_similarity_graph_count} et 8 pour celui des 10
3-grammes ayant la plus forte similarité *scalaire*), et plusieurs graphes
contiennent même des nœuds isolés, c'est-à-dire des domaines qui n'ont aucun
n-gramme en commun avec aucun autre domaine. Les 3 autres graphes sont tous des
GPPV générés pour la similarité *cardinale*. Tous les graphes pour cette mesure
de similarité, même ceux ayant plus de 3 composantes non-connexes possèdent 2
nœuds ou plus reliés à plusieurs autres nœuds, certains jusqu'à 9 comme la
classe *Militaire* sur le GPPV des 50 3-grammes. Cet effet, très prononcé pour
$k = 10$ s'explique par le faible nombre de valeurs pouvant être prises par les
scores de similarité *cardinale*.

En effet pour chaque paire de nœuds considérée, elle prend une valeur de la
forme $\frac{i}{k}$ avec $i \in \mathbb{N}, i \geq 0, i \leq k$ ($k$ est le
paramètre fixé pour le graphe considéré, pouvant valoir 10, 50 ou 100). Pour un
nœud donné, en calculant sa similarité *cardinale* avec chacun des 37 autres
nœuds, la métrique retournera donc 1 des seulement 11, 51 ou 101 valeurs
possibles. Par application du «principe des tiroirs» il y aura nécessairement
plusieurs nœuds (de destination) avec la même similarité *cardinale* pour ce
nœud considéré pour $k = 10$, des «collisions» dans les valeurs de sortie de la
fonction qui a chacun des 37 autres nœuds attribue sa similarité avec le nœud de
départ considéré. Cette certitude de collision devient seulement un risque, mais
assez élevé, pour les deux autres valeurs de $k$. Si une collision se produit
sur la plus haute valeur atteinte pour ce nœud, alors il y aura autant d'arêtes
sortant de ce nœud que de classes impliquées dans la collision puisque ce sont
ces arêtes avec la plus haute similarité qui sont utilisées pour tracer le
graphe.

Malgré ces réserves, les GPPV de similarités contiennent des motifs similaires à
ceux du GPPV des confusions du modèle de la figure \ref{fig:nn_confusion_graph}
(p.\pageref{fig:nn_confusion_graph}). Par exemple, on constate que la connexion
entre *Mathématiques* et *Physique* est bien présente dans tous les graphes de
similarités sauf 3 (ceux pour $k = 10$, $n = 1$ avec la similarité *scalaire* et
$n = 3$ pour les deux mesures de similarité). Le triangle formé par *Mesure*,
*Monnaie* et *Commerce* se trouve dans tous les graphes sauf ceux des 2-grammes
pour la similarité *scalaire* (soit seulement 3 graphes également). On retrouve
également des proximités fréquentes entre *Médecine*, *Anatomie*, *Pharmacie* et
*Chimie* sur les graphes. En revanche le nœud *Métiers* n'est pas toujours aussi
attracteur, cédant fréquemment sa place à des domaines comme les
*Belles-Lettres*, la *Religion* ou l'*Histoire*. Quant à la *Géographie*, sa
plus forte similarité est avec la classe *Antiquité* (jusqu'à 80% de similarité
*scalaire* pour leur 50 2-grammes les plus fréquents), sauf dans 5 graphes (dont
celui de la figure \ref{fig:nn_similarity_graph_count}) où ses plus fortes
ressemblances vont à l'*Histoire* et aux *Belles-Lettres*. Ces trois classes
figuraient parmi les faux négatifs et les faux positifs les plus fréquents de la
classe *Géographie* (voir la section \ref{sec:model_errors}), mais il est
intéressant de noter que la classe *Histoire* qui apparaissait nettement en tête
des erreurs n'obtient la plus forte similarité avec *Géographie* que sur
relativement peu de GPPV de similarités.

Les erreurs les plus importantes faites par le modèle (correspondant donc au
GPPV de sa matrice de confusion) présentent donc une certaine compatibilité avec
les similarités lexicales qui existent entre les domaines. La ressemblance est
davantage marquée pour les 2- et 3-grammes, et pour certains domaines. Les
erreurs faites à propos de la *Géographie* en particulier ne suivent pas tout à
fait le schéma de ses ressemblances lexicales.

### Application de la centralité à la structure du modèle {#sec:model_centrality}

Les deux sections précédentes tentent de dégager du sens d'objets complexes en
les simplifiant localement (en considérant une colonne et une ligne particulière
ou en limitant les arêtes partant de chaque nœud d'un graphe). La dernière étape
restante pour analyser les liens entre domaines consiste donc logiquement à
essayer de capturer leurs dynamiques d'ensemble. Pour y parvenir, cette section
considère une mesure de centralité sur le graphe défini en considérant la
matrice de confusion du modèle *SGD+TF-IDF*, $C$, comme une matrice d'adjacence
pondérée. Il ne s'agit donc plus du GPPV de la section \ref{sec:graph_model},
engendré par la MPPV, mais de l'objet qui avait été initialement écarté au
profit du GPPV car sa complexité ne permettait pas de le tracer pour l'exploiter
visuellement. Ce graphe, correspondant à la matrice de confusion via
l'équivalence décrite à cette même section \ref{sec:graph_model}, sera dénommé
dans ce qui suit le «graphe de confusion» et noté $\mathcal{G}_C$. En cohérence
avec les choix faits pour les GPPV dans les sections \ref{sec:graph_model} puis
\ref{sec:lexical_similarities}, les arêtes de $\mathcal{G}_C$ représentent les
faux négatifs du modèle (plutôt que les faux positifs).

Si un graphe représente des déplacements sur un réseau, la centralité de vecteur
propre choisie pour cette étude correspond à une situation particulière
d'équilibre des flux. Elle est définie comme une distribution de poids sur les
nœuds caractérisée par le fait d'être inchangée après une étape de déplacement
suivant les coefficients de ses arêtes. Dans le cadre de l'analyse de
$\mathcal{G}_C$, l'interprétation dynamique du processus de prédiction des
classes par le modèle donnée à la section \ref{sec:graph_model} permet de
traduire cette situation d'équilibre sur un ensemble donné d'articles. En
considérant par exemple 1 000 articles de *Géographie*, d'après les scores
mesurés sur le jeu de test le modèle en reconnaîtrait correctement 985 comme
étant de la classe *Géographie*, «presque»[^presque] 4 comme de l'*Histoire*, 2
comme de l'*Antiquité*, etc. Une distribution initiale d'articles exclusivement
sur le nœud *Géographie* semblerait, après le «déplacement» causé par la
prédiction du modèle, s'être étalée sur ses voisins en suivant les taux
d'erreurs du modèle pour cette classe. Si au lieu de choisir 1 000 articles de
la classe *Géographie* la distribution avait comporté 900 articles de
*Géographie* et 100 d'*Histoire*, alors environ 6 articles d'*Histoire* aurait
été pris pour de la *Géographie* par le modèle[^histoire-to-geo]. Leurs
mouvements sur le graphe auraient dans une certaine mesure compensé ceux de la
classe *Géographie* que le modèle aurait pris pour de l'*Histoire*. Une mesure
de centralité des nœuds peut donc dans ce cas s'interpréter comme une proportion
d'articles entre les différents domaines qui «résonne» d'une manière
particulière avec le modèle en lui permettant de ne pas se tromper sur les
quantités: pour un échantillon d'articles choisis dans chaque domaine
proportionnellement à leur centralité, le modèle trouve en sortie exactement le
bon nombre d'articles de chaque domaine (il se trompe sur certains articles de
chaque classe mais dans l'ensemble ses erreurs se compensent).

[^presque]: Après avoir appliqué le modèle une fois, chaque classe ne contiendra
    bien évidemment qu'un nombre entier d'articles, mais les nombres réels issus
    des scores du modèle et qui vont être considérés dans cette partie décrivent
    un comportement statistique (ils ont d'ailleurs été obtenus à l'origine en
    mesurant un nombre entier d'article, les virgules traduisent seulement le
    fait que le choix arbitraire de «1000» articles n'est pas multiple du nombre
    d'articles de *Géographie* présents dans le jeu de test).

[^histoire-to-geo]: Selon les résultats du modèle, encore une fois, cela se
    produit pour $5.84%$ des articles de la classe *Histoire*.

Les poids attribués par la mesure de centralité permettent de se faire une idée
de l'attractivité des nœuds les uns par rapport aux autres car la valeur en un
nœud décrit une situation d'équilibre entre deux paramètres contraires. Elle
peut être d'autant plus élevée (relativement aux autres, c'est-à-dire en
supposant leurs poids fixés) que ce nœud reçoit un flux important des autres
nœuds (en nombre — qu'il y ait beaucoup d'arêtes — ou en volume — des arêtes
avec un coefficient élevé). Cette augmentation du poids est tempérée de manière
linéaire par l'importance du flux qui quitte le nœud considéré (de même, en
nombre ou en volume, ici encore seule la somme des coefficients des arêtes
quittant un nœud donné a une importance): supposant les coefficients de toutes
les autres arêtes fixés, le poids d'un nœud sera d'autant plus faible que ses
arêtes sortantes ont des coefficients importants. Une mesure de centralité
élevée signifiera donc qu'un nœud est assez bien reconnu (flux de faux négatifs
sortant bas) tout en attirant assez de faux négatifs des autres classes. Sur ce
dernier point, c'est le comportement d'ensemble qui compte, le nœud n'a besoin
d'être le plus fort attracteur d'aucun de ses voisins pour avoir une centralité
élevée, ce qui permet de détecter des tendances qui étaient invisibles en
réduisant les graphes à leurs [@=PPV].

Pour formaliser le problème de la recherche d'une telle distribution stable qui
capturerait les influences entre classes, il est utile de représenter les
distributions d'articles par des vecteurs de $\mathbb{R}^{38}$ (cette dimension
correspondant, pour rappel, au nombre de classes des *domaines regroupés*). Une
fois encore, laisser les coefficients prendre comme valeurs des nombres réels
permet de faire la transition d'une expérience initiale sur les *articles* où
seuls des entiers peuvent avoir du sens à un contexte statistique où il est
possible de représenter des distributions moyennes, des probabilités et plus
seulement un vrai échantillon donné d'articles. Avec ce formalisme un peu plus
algébrique[^algébrique], et en gardant comme dans la section précédente (Section
\ref{sec:graph_model}) la notation $C$ pour la matrice de confusion de la figure
\ref{fig:confusion_matrix} (p.\pageref{fig:confusion_matrix}), la définition de
la centralité comme une distribution de poids laissée stable par $C$ se traduit
par l'existence d'un vecteur

[^algébrique]: Il est à noter qu'avec la convention de placer les vraies
    étiquettes de classe en ligne et les étiquettes prédites en colonnes,
    appliquer le modèle correspond à un simple produit matriciel avec le vecteur
    à gauche de la matrice.

\begin{equation}
    \boldsymbol{v} \in \mathbb{R}^{38} \text{\,tel que\,} \boldsymbol{v} \cdot C = \boldsymbol{v}
    \label{eq:left_eigenvector}
\end{equation}

Cette expression coïncide avec la définition d'un vecteur propre (à gauche)
associé à la valeur propre 1. Selon la définition de la matrice de confusion,
chaque rangée représente une distribution de probabilité (la probilité qu'un
article donné issu de la classe correspondante soit prédit par le modèle dans
chacune des classes possibles): c'est-à-dire que $C$ est stochastique à droite.
En tant que telle, la somme des coefficients d'une rangée doit valoir 1. Étant
données les règles de calcul des produits matriciels et en appelant
$\boldsymbol{v_1}$ le vecteur de $\mathbb{R}^{38}$ dont tous les coefficients
sont égaux à 1, la propriété d'être stochastique à droite peut s'écrire:

\begin{equation}
    C \cdot \boldsymbol{v_1} = \boldsymbol{v_1}
    \label{eq:1_is_right_stochastic}
\end{equation}

Cette expression montre que ce vecteur plein de 1 est un vecteur propre de la
matrice $C$, mais seulement à droite. Or, les valeurs propres d'une matrice $M$
sont les racines de son polynôme caractéristique $P_M[\lambda] = det(\lambda Id
\- M)$. Puisque par définition du déterminant, invariant par transposition, on a
$det(M) = det(M^T)$, alors $M$ et $M^T$ ont le même polynôme caractéristique et
de ce fait les mêmes valeurs propres. Ayant trouvé un vecteur propre à droite
pour $C$ associé à la valeur propre 1, on sait qu'il doit nécessairement exister
un vecteur propre (à gauche cette fois) avec la même valeur propre, ce qui
prouve que l'Équation \ref{eq:left_eigenvector} doit admettre au moins une
solution. Ayant prouvé son existence, une manière pratique de calculer cette
solution consiste à utiliser le théorème du cercle de Gershgorin qui, appliqué à
une matrice stochastique, montre que toutes les valeurs propres doivent être
inférieures ou égales à 1 en norme (somme des lignes). Par conséquent, itérer la
matrice $C$ de manière répétée va progressivement éliminer les «petits»
coefficients (ceux strictement inférieurs à 1) en les faisant tendre vers 0
(parce que $\lim_{n \rightarrow \infty} \lambda ^ n = 0$ si $|\lambda| < 1$).
Cela n'est en soi pas suffisant pour garantir la convergence des itérées de $C$
car il pourrait y avoir plusieurs vecteurs propres associés à la valeur 1 si
celle-ci avait une multiplicité $> 1$[^perron]. Mais par contre, s'il y a
convergence, alors ce sera nécessairement vers *le* vecteur propre (dont
l'unicité serait alors prouvée) associé à cette valeur. Calculer les itérées de
$C$ donne par exemple pour les puissances 4, 16, 64 et 256 les matrices visibles
dans la figure \ref{fig:iterates}.

[^perron]: l'application du théorème de Perron-Frœbenius permettrait de lever
    cette réserve en garantissant l'unicité de la valeur propre la plus grande
    en norme mais il s'applique sur des graphes connexes (matrices régulières).
    La densité élevée de $\mathcal{G}_C$ et la présence de boucles sur tous les
    nœuds (le modèle ne maltraite aucun domaine au point de ne reconnaître aucun
    de ses articles) engage à faire le pari de sa connexité, mais cela n'a pas
    été vérifié et le théorème ne peut pas être appliqué en toute rigueur. Le
    pragmatisme commande de passer outre: si les itérées de $C$ convergent,
    l'unicité sera prouvée empiriquement, sans le théorème.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.44\textwidth}
        \centering
         \includegraphics[width=\textwidth]{figure/matrix/SGD+TF-IDF_domainGroup/confusionMatrix/power4.png}
         \caption{$C^4$}
         \label{fig:m4}
    \end{subfigure}
    \begin{subfigure}[b]{0.44\textwidth}
        \centering
         \includegraphics[width=\textwidth]{figure/matrix/SGD+TF-IDF_domainGroup/confusionMatrix/power16.png}
         \caption{$C^{16}$}
         \label{fig:m16}
    \end{subfigure}
    \begin{subfigure}[b]{0.44\textwidth}
        \centering
         \includegraphics[width=\textwidth]{figure/matrix/SGD+TF-IDF_domainGroup/confusionMatrix/power64.png}
         \caption{$C^{64}$}
         \label{fig:m64}
    \end{subfigure}
    \begin{subfigure}[b]{0.44\textwidth}
        \centering
         \includegraphics[width=\textwidth]{figure/matrix/SGD+TF-IDF_domainGroup/confusionMatrix/power256.png}
         \caption{$C^{256}$}
         \label{fig:m256}
    \end{subfigure}
    \caption{Des itérées de la matrice de confusion du modèle, $C$}
    \label{fig:iterates}
\end{figure}

Après un peu plus de cent itérations, la matrice se stabilise et toutes ses
lignes présentent une même distribution. Cette distribution est le vecteur
propre recherché pour $C$ et ses coefficients correspondent à la mesure de
centralité de chaque nœud dans le graphe, explicitée sous forme d'histogramme à
la figure \ref{fig:centralities}. Cette même figure montre que la forme de la
distribution diffère visiblement de celle du nombre d'articles par classe (voir
la figure \ref{fig:distribution_grouped_domains}
p.\pageref{fig:distribution_grouped_domains}, dont la décroissance est bien plus
continue) et présente trois groupes distincts: 1) la classe la plus centrale, 2)
trois classes secondaires sur un pallier, et 3) toutes les autres en une longue
traîne. De plus, alors que les classes ordonnées par centralité ont globalement
des positions proches de leurs rangs par nombre d'articles, les classes les plus
peuplées obtenant dans l'ensemble des scores de centralité plus élevés que
celles les moins peuplées, l'ordre peut être localement assez différent. Par
exemple, la classe *Droit - Jurisprudence*, 2^ème^ classe par fréquence, n'est
que 4^ème^ par centralité, dernière du pallier secondaire. Juste après elle à la
5^ème^ place se trouve la classe *Marine*, loin devant *Grammaire* (au 12^ème^
rang avec une centralité presque 3 fois moindre) alors que cette dernière était
plus fréquente que *Marine*. Il en est de même vers la fin de la distribution:
une classe comme *Minéralogie*, avant-dernière en termes de fréquence n'est que
la 8^ème^ classe la moins centrale, avec une centralité plus de 5 fois
supérieure à celle de l'avant-dernière. Bien que la mesure de centralité des
classes semblent avoir un lien avec leurs tailles, toutes ces différences
montrent que la taille seule ne suffit pas à expliquer en détail la distribution
des centralités. Le modèle semble donc mieux identifier certaines classes grâce
à leurs particularités intrinsèques.

![La distribution par classe de mesures de centralité du graphe du modèle](figure/histogram/matrix/SGD+TF-IDF_domainGroup/centralities.png){#fig:centralities}

En s'intéressant plus particulièrement à la classe *Géographie*, on constate
qu'elle obtient le score le plus élevé ($0.43$). Ce résultat suggère que loin de
jouer un rôle périphérique dans le graphe comme aurait pu le suggérer la figure
\ref{fig:nn_confusion_graph} p.\pageref{fig:nn_confusion_graph}, le domaine se
place au centre du flux des erreurs de classification d'articles. En accord avec
le nombre de faux positifs plus de trois fois supérieur aux faux négatifs mis en
évidence à la section \ref{sec:geography_errors} ($3 \times 39 = 117 < 152$), le
nœud *Géographie* du graphe reçoit un flux des autres nœuds du graphe supérieur
au flux sortant d'après la remarque précédente sur l'interprétation de la
centralité. Cette analyse montre donc une *Géographie* bien reconnue et détectée
par le modèle dans de nombreux autres domaines, signe de l'importance au
XVIII^ème^ siècle de cette science au contact de nombreuses autres.

Au travers des différentes analyses précédentes, cette section amène plusieurs
pistes de recherche à exploiter dans le chapitre \ref{sec:contrasts}. L'analyse
qualitative initiale a montré des adjacences thématiques intéressantes entre la
*Géographie* et des domaines comme l'*Histoire* et le *Commerce*. Les GPPV
suggèrent quant à eux que les confusions du modèle entre la *Géographie* et les
autres classes ne se fondent pas (ou au moins pas uniquement) sur des
ressemblances lexicales (par opposition à des classes comme *Histoire* et
*Métiers* qui attirent de nombreux faux négatifs d'autres classes et sont
fréquemment la cible de nombreuses arêtes dans les GPPV de similarité lexicale).
Au vu des résultats de la dernière section, cela signifierait que les contenus
géographiques se distinguent assez bien de ceux des autres disciplines mais
infusent largement dans l'*EDdA*, bien au-delà de ce qui a été initialement
classé à *Géographie*.

