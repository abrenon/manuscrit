À l'issue du chapitre \ref{sec:corpus}, les textes du corpus sont disponibles
dans une version XML-[@=TEI] ainsi que dans des fichiers texte qui suivent la
convention décrite à la section \ref{sec:text_format} page
\pageref{sec:text_format}. Mais si une structure a été choisie pour représenter
le corpus et ses métadonnées, la valeur à associer à chaque article pour
représenter son domaine de connaissance (voir la section
\ref{sec:knowledge_domains} p.\pageref{sec:knowledge_domains}) reste à
déterminer pour une partie des articles de l'*EDdA* et la totalité de ceux de
*LGE*. Le présent chapitre décrit les efforts menés sur ce versant en le
considérant comme un problème de classification automatique. Il reprend dans une
large mesure et étend des résultats déjà exposés dans @brenon_classifying_2022.

La première étape consiste à comparer les résultats de différentes méthodes de
classification, avant d'étudier à la section \ref{sec:geo_relations} les erreurs
commises par l'une d'entre elles pour essayer de comprendre ce qu'elles peuvent
révéler sur les ressemblances et les différences entre domaines de connaissance.
Enfin, la dernière section \ref{sec:classification_application} décrit
concrètement l'annotation en domaines de connaissance du corpus entier.

S'agissant d'[@=AA], la remarque faite p.\pageref{french_please} à la fin de la
section \ref{french_please} s'applique tout autant dans ce chapitre qu'elle
s'appliquait à la section \ref{sec:EdlA_TAL} de l'état de l'art: devant la
prédominance de l'emploi des noms anglophones pour les méthodes d'[@=AA], même
chez des locuteurs français, une exception locale est faite dans toute cette
section à l'emploi exclusif de termes francophones qui est la règle dans le
reste de cette thèse. Ce choix se justifie par le soucis de ne pas produire de
formes traduites quelque peu artificielles qui n'auraient de toute façon pas
cours hors de ces pages.

