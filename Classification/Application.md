## Annotation des articles {#sec:classification_application}

### Classification non supervisée

En complément des méthodes de classification supervisées, des tentatives ont été
faites pour utiliser de l'apprentissage non supervisé, permettant d'étudier
autrement les relations entre les contenus des articles, les systèmes de
classification et les prédictions générées automatiquement. Des techniques de
*clustering* ont ainsi été employées pour grouper automatiquement les articles
suivant leurs similarités, sans s'attacher aux étiquettes à appliquer à ces
groupes (c'est-à-dire à une quelconque interprétation qu'il faudrait faire de
ces groupes). Cette «similarité» est basée sur un calcul de distance entre les
représentations vectorielles des articles. Pour cette expérience, la méthode des
*K-Means* a été testée avec une vectorisation *TF-IDF*. La première expérience
consiste à entraîner un modèle de *clustering* pour construire 38 classes (le
nombre de domaines regroupés choisis précédemment). Des résultats décevants
amènent à rechercher automatiquement le meilleur nombre de clusters en utilisant
la méthode Silhouette [@shahapure2020cluster]. Les résultats suggèrent 36 comme
le nombre optimal de clusters, mais la répétition de l'expérience précédente
avec ce nombre produit des résultats similaires.

La figure \ref{fig:clustersperclass} montre une carte de chaleur des
distributions normalisées des nombres de clusters trouvés automatiquement avec
la méthode *K-Means* en compartimentant les articles en 36 clusters. La plupart
des clusters comportent un nombre important d'articles de plusieurs domaines et
le cluster n°0 regroupe des articles de quasiment tous les domaines. De plus,
pour beaucoup de clusters, la proportion est élevée et, de la même manière,
beaucoup de domaines se retrouvent étalés sur plusieurs clusters. Cela est
particulièrement vrai pour la *Géographie*, l'*Histoire naturelle* et les *Arts
et métiers*. Bien que les résultats soient complexes, l'analyse des clusters
demeure utile: par exemple le cluster n°22 regroupe des articles étiquetés
*Belles-lettres - Poésie*, *Histoire*, *Médailles*, *Religion* et
*Superstition*. Ces catégories possèdent des similarités thématiques qui
dénotent leur classification initiale dans l'*EDdA* ainsi que celle choisie en
termes de domaine regroupé (elles contiennent fréquemment du contenu à propos
d'histoires réelles mais aussi fictionnelles et mythologiques). De manière
semblable, le cluster n°7 comporte des articles des domaines *Médecine -
Chirurgie*, *Anatomie*, *Physique - \[Sciences physico-mathématiques\]* et
*Pharmacie*. Il semble possible de percevoir une thématique autour du corps et
de la médecine dans cet ensemble de classes. Enfin, le cluster n°30 en
rassemblant des articles de *Commerce* et de *Mesure* suggère une thématique
autour des nombres et d'unités. Quelques clusters restent particulièrement
homogènes et donc proches des classes comme le cluster n°10 composé à 96.46%
d'articles des classes *Géographie* ou le n°34, à 99.05% de l'*Histoire
naturelle*.

![Distributions normalisées des nombres de clusters par classe.](figure/classification/unsupervised/cluster36_tf_idf_per_classes.png){#fig:clustersperclass width=80%}

À l'inverse, la figure \ref{fig:classespercluster} montre la distribution de
domaine regroupés par cluster. Comme sur la figure \ref{fig:clustersperclass},
le cluster n°0 ressort particulièrement en rassemblant plus de 14 000 articles
de quasiment toutes les classes, alors que la très large majorité des autres
comporte moins de 2 000 articles. Cet écart considérable entre ce cluster et les
autres illustre toute la difficulté qu'il y a à trouver automatiquement une
catégorisation satisfaisante des articles du jeu de données. Cette nouvelle
figure montre encore l'hétérogénité des classes de quasiment tous les clusters
(exceptés les cas discutés plus haut) et par là-même la difficulté à donner un
sens aux clusters proposés par *K-Means*, sinon en terme de thématiques pour
quelques uns d'entre eux.

![Nombre d'articles de chaque classe par cluster](figure/classification/unsupervised/classes_per_cluster_cluster36_tf_idf.png){#fig:classespercluster width=80%}

De plus, sur les 38 classes, seules 15 apparaissent comme la classe principale
d'un des clusters (voir le tableau \ref{tab:maxclasspercluster}). Parmi les
clusters, 9 apparaissent relativement homogènes avec une classe principale
«pure» dans ces clusters à plus de 90% (cela est le cas pour les clusters n°1,
6, 10, 13, 15, 19, 31, 33 et 34). Mais pour ces 9 clusters, seules 3 classes
apparaissent comme la classe principale, et la *Géographie* est la classe
principale de 7 d'entre eux. Cela empêche d'interpréter la pureté importante des
clusters comme la correspondance de certains d'entre eux avec des classes
existantes, une hypothèse qui était pourtant à priori intéressante au vu de la
proximité entre le nombre de classes (38) et de clusters (36). De plus, la
classe *Géographie* est présente dans 31 clusters différents, c'est-à-dire la
quasi-totalité d'entre eux, ce qui contredit une interprétation des 7 clusters
comme autant de types d'articles de *Géographie* possibles.

Cluster n° | Classe principale     |  Part    | Cluster n° | Classe principale     |  Part    
----------:|-----------------------|---------:|-----------:|-----------------------|---------:
         0 | Droit - Jurisprudence |  15.85 % |         18 | Métiers               |  20.51 %
         1 | Géographie            |  91.95 % |         19 | Droit - Jurisprudence |  95.50 %
         2 | Droit - Jurisprudence |  71.54 % |         20 | Chimie                |  12.33 %
         3 | Géographie            |  89.03 % |         21 | Histoire naturelle    |  74.48 %
         4 | Antiquité             |  51.38 % |         22 | Histoire              |  16.99 %
         5 | Métiers               |  58.78 % |         23 | Mathématiques         |  30.95 %
         6 | Géographie            |  99.28 % |         24 | Droit - Jurisprudence |  52.67 %
         7 | Médecine - Chirurgie  |  66.10 % |         25 | Métiers               |  51.76 %
         8 | Métiers               |  42.17 % |         26 | Métiers               |  39.85 %
         9 | Géographie            |  15.25 % |         27 | Droit - Jurisprudence |  82.77 %
        10 | Géographie            |  96.46 % |         28 | Histoire naturelle    |  52.82 %
        11 | Maréchage - Manège    |  70.80 % |         29 | Anatomie              |  82.93 %
        12 | Histoire naturelle    |  83.24 % |         30 | Commerce              |  52.82 %
        13 | Géographie            |  96.53 % |         31 | Géographie            |  95.50 %
        14 | Marine                |  74.93 % |         32 | Histoire naturelle    |  65.09 %
        15 | Géographie            | 100.00 % |         33 | Géographie            |  99.47 %
        16 | Grammaire             |  44.65 % |         34 | Histoire naturelle    |  99.05 %
        17 | Blason                |  24.06 % |         35 | Histoire naturelle    |  71.61 %

: Part de la classe la plus représentée dans chaque cluster.
\label{tab:maxclasspercluster}

Il est difficile de tirer des conclusions des résultats de cette expérience de
*clustering* et, avant d'aller plus loin, davantage d'analyses qualitatives sont
nécessaires pour pouvoir décider si l'apprentissage non supervisé peut être
utile comme une méthode complémentaire pour classer automatiquement les articles
de l'*EDdA* et à terme de *LGE*. Le *clustering* est, comme le *topic modeling*
par *LDA* (*Latent Dirichlet Allocation* — allocation latente de dirichlet), une
manière d'organiser le contenu du corpus sans présupposer un ensemble de classes
qui serait *la* manière correcte de structurer la connaissance. Le choix fait
précédemment de reprendre le nombre de domaines regroupés utilisés pour la
classification supervisée pour le nombre de clusters biaisait nécessairement la
découverte de clusters, mais le fait que l'heuristique Silhouette ait trouvé un
nombre proche de clusters (36) demeure troublant. Il pourrait être intéressant
dans de futurs travaux de tester de manière systématique un bien plus grand
nombre de possibilités en partant d'un très petit nombre de clusters et en
augmentant leur nombre tout en évaluant manuellement la pertinence du système
trouvé à chaque étape. Dans le contexte de cette thèse, ces méthodes n'ont pas
permis d'avancées significatives et ne sont pas retenues dans le reste des
analyses.

### Choix d'un classifieur {#sec:classification_choices}

Si le travail de comparaison des méthodes de classification décrit dans cette
partie a été effectué sur le jeu des 38 «domaines regroupés», c'est celui des 17
«superdomaines» (les deux sont décrits à la section
\ref{sec:domains_build_classes}) qui a été plus utilisé pour les études
contrastives du chapitre suivant (voir chapitre \ref{sec:contrasts}). Il a donc
été non seulement nécessaire de choisir une architecture définitive de modèle de
classification, mais aussi de réappliquer les chaînes de traitement utilisées
pour les comparaisons décrites à la section \ref{sec:classifiers_comparison}
p.\pageref{sec:classifiers_comparison} pour la prédiction de superdomaines.

#### Superdomaines {#sec:edda_superdomains_classifier}

Comme pour les domaines regroupés, une annotation en superdomaines des articles
possédant un désignant explicite a pu être obtenue de manière immédiate en
appliquant sur les corpus de 58 509 articles pourvus d'un désignant (voir le
tableau \ref{tab:nb_articles_seuil} à la page \pageref{tab:nb_articles_seuil})
la fonction qui à un désignant normalisé (disponible dans les métadonnées de
l'[@=ENCCRE]) associait son superdomaine. À partir de ce corpus annoté, de
nouveaux modèles de classification utilisant ce jeu des superdomaines ont pu
être entraînés en suivant exactement le même processus que précédemment. Au vu
des résultats de l'étude comparative précédente, seuls des modèles *SGD+TF-IDF*
(figure \ref{fig:edda_superdomain_confusion_matrix_sgd+tf-idf}) et *BERT*
(figure \ref{fig:edda_superdomain_confusion_matrix_bert}) ont été ainsi
entraînés.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \centering
        \includegraphics{figure/classification/SGD+TF-IDF_superdomain.png}
        \caption{Pour la combinaison \textit{SGD+TF-IDF}}
        \label{fig:edda_superdomain_confusion_matrix_sgd+tf-idf}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/classification/BERT_superdomain.png}
        \caption{Pour le modèle \textit{BERT}}
        \label{fig:edda_superdomain_confusion_matrix_bert}
    \end{subfigure}
    \caption{Matrices de confusion des modèles entraînés sur les superdomaines}
    \label{fig:edda_superdomain_confusion_matrix}
\end{figure}

Les performances sont légèrement supérieures à celles obtenues sur les domaines
regroupés, ce qui est cohérent avec la réduction du nombre de classes (moins il
y a de classes, plus il devient difficile, statistiquement, de se tromper). La
combinaison *SGD+TF-IDF* obtient des scores très semblables à ceux du modèle
*BERT* avec des F-mesures respectives de 85% et 88% sur l'ensemble du jeu de
test. Les seuls éléments encore décelables hors de la diagonale sont les 3
colonnes correspondant à l'*Histoire*, les *Métiers* et la *Philosophie* et la
ligne correspondant à la *Politique*, tant sur la figure
\ref{fig:edda_superdomain_confusion_matrix_sgd+tf-idf} que sur la figure
\ref{fig:edda_superdomain_confusion_matrix_bert}. Les 3 colonnes montrent que le
modèle a souvent tendance à confondre un assez grand nombre de classes avec une
des 3 classes *Histoire*, *Métiers* ou *Philosophie*. La ligne montre que la
*Politique* reste très mal reconnue par ces modèles avec le nouveau jeu
d'étiquettes et que ses articles se retrouvent distribués entre un assez grand
nombre de classes dans les prédictions du modèle au lieu d'être correctement
assignés à la classe *Politique*. Le phénomène est naturellement moins prononcé
pour le modèle *BERT* qui obtient dans l'ensemble de meilleurs résultats mais il
reste observable sur les deux matrices.

#### Considérations énergétiques {#sec:edda_classifier_energy}

Les résultats des évaluations ci-dessus étaient dans une certaine mesure
prévisibles et cohérents avec l'état de l'art, mais le choix d'un classifieur
repose généralement sur davantage que la seule comparaison de F-mesures. Si
l'avantage des transformeurs sur les autres méthodes d'[@=AP] ne sont après tout
que de quelques décimales, en revanche cette étude fait ressortir un avantage
décisif en leur faveur: le peu de données d'entraînement qu'ils nécessitent. Les
performances relativement décevantes des méthodes *CNN* et *BiLSTM* peuvent
s'expliquer sans doute en grande partie par la rareté relative[^big-data] des
données d'entraînement et il est donc d'autant plus remarquable de voir les
méthodes basées sur *BERT* s'en sortir aussi bien avec les mêmes quantités de
données. Cela souligne l'intérêt du préentraînement de ces modèles, les
quelques dizaines de milliers d'articles disponibles étant suffisants pour
ajuster finement leurs paramètres.

[^big-data]: par opposition aux mégadonnées qui ont nécessité l'invention de ces
    méthodes d'AP et qui consistent en des ensembles d'au moins plusieurs
    millions voire milliards d'objets

La comparaison de méthodes traditionnelles d'[@=AA] avec ces méthodes d'[@=AP]
plus récentes a une pertinence au-delà de l'intérêt historique si l'on prend en
compte le coût des calculs. Il est bien sûr déjà remarquable de voir des
combinaisons de méthodes comme *SGD+TF-IDF* ou *SVM+TF-IDF* obtenir de meilleurs
résultats que les méthodes d'[@=AP], pourtant bien plus récentes, à l'exception
des transformeurs. Mais cela est d'autant plus impressionnant que leurs besoins
en termes de stockage, de mémoire et de temps de calcul sont minimes comparés
aux méthodes d'[@=AP]. La consommation énergétique occasionnée par
l'entraînement et l'évaluation des modèles *SGD+TF-IDF* et *BERT* sur le jeu
d'étiquettes des superdomaines a été mesurée à l'aide des outils développés par
@lacoste_quantifying_2019. L'opération a été effectué pour les deux modèles sur
la même machine du Centre Blaise Pascal de l'ENS de Lyon
[@quemener_sidussolution_2013] équipée d'une carte GPU de type NVIDIA RTX A2000
12GB et d'un CPU Intel Core i3-2120 d'une fréquence 3.30GHz

L'ensemble de l'expérience a consommé 594 W·h comme le montre le tableau
\ref{tab:energy_consumption}, ce qui représente une quantité d'énergie de
l'ordre de celle utilisée pour faire fonctionner un réfrigérateur pendant une
heure. Cela reste relativement modeste mais cette remarque a le mérite de
matérialiser le coût d'un calcul qui peut trop facilement sembler se produire
«hors du monde» dans un espace numérique sans conséquences, et ce d'autant plus
quand le calcul a lieu sur un serveur distant et n'est pas suivi d'effets
directement perceptibles comme le bruit du ventilateur et la chaleur dégagée par
la machine depuis laquelle il est lancé. D'après l'Agence Européenne pour
l'Environnement, l'électricité en France — où est situé le serveur qui a
effectué le calcul — revenait à 68 g.eqCO~2~ par kW·h en 2022[^EEA]. On peut
donc estimer que 40.4 g.eqCO~2~ ont été rejetés dans le cadre de cette
expérience soit l'équivalent d'environ 6km de voyage en Eurostar pour une
personne d'après les chiffres de la SNCF[^SNCF].

[^EEA]:
    https://www.eea.europa.eu/en/analysis/indicators/greenhouse-gas-emission-intensity-of-1
[^SNCF]:
    https://medias.sncf.com/sncfcom/rse/Methodologie-generale_guide-information-CO2.pdf

+---------------+---------------+----------+---------------+
| Modèle        | *SGD+TF-IDF*  | *BERT*   | Total         |
+===============+==============:+=========:+==============:+
| Prétraitement | 41.3 W·h      |          | 41.3 W·h      |
+---------------+---------------+----------+---------------+
| Vectorisation | 1.59e^-1^ W·h |          | 1.59e^-1^ W·h |
+---------------+---------------+----------+---------------+
| Entraînement  | 4.48e^-1^ W·h | 545 W·h  | 546 W·h       |
+---------------+---------------+----------+---------------+
| Évaluation    | 9.60e^-2^ W·h | 6.83 W·h | 6.93 W·h      |
+===============+===============+==========+===============+
| Total         | 42.0 W·h      | 552 W·h  | 594 W·h       |
+---------------+---------------+----------+---------------+

: Coût en énergie des différentes phases de l'entraînement des modèles
*SGD+TF-IDF* et *BERT*
\label{tab:energy_consumption}

Au-delà de la traduction en un coût physique concret qu'on peut en faire, cette
valeur pose en soi plusieurs problèmes d'interprétation, notamment parce qu'elle
dépend de l'efficacité de la production électrique à un endroit donné à un
moment donné (dans un autre pays, la même expérience n'aura pas le même bilan
carbone et rien ne garantit qu'il sera toujours possible d'effectuer cette
expérience pour le même bilan carbone si les conditions de production venaient à
changer en France) mais aussi parce que la production d'énergie dans certaines
conditions a toujours une capacité finie, et que l'énergie utilisée à une
certaine fin n'est pas disponible pour d'autres applications — si une grande
quantité de personnes venaient à faire ce type d'expérience, l'énergie consommée
pourrait venir à entrer en concurrence avec celle utilisée pour les transports
ou l'industrie, au point de devoir potentiellement importer de l'électricité
produite dans des conditions différentes en termes de bilan carbone
[@lacoste_quantifying_2019 p.4]. Plus que le grammage équivalent CO~2~ de
l'ensemble, il est donc intéressant de considérer la répartition de l'énergie
utilisée entre les différentes phases des différentes méthodes.

D'abord la distinction entre GPU et CPU, bien qu'elle ne soit pas reportée sur
le tableau précédent différencie nettement les deux modèles. La consommation
énergétique attribuée à la carte GPU pour l'entraînement du modèle *BERT*
représente 67.8% de la consommation totale, fraction qui baisse à 27.5% pour le
modèle *SGD+TF-IDF*. Disposer d'une GPU est en pratique indispensable pour
utiliser un modèle *BERT* alors qu'il est tout à fait possible de s'en passer
pour *SGD+TF-IDF*. Mais le point le plus saillant du tableau
\ref{tab:energy_consumption} reste la différence d'ordre de grandeur entre la
consommation de l'assemblage *SGD+TF-IDF* et celle de *BERT*, d'un facteur de
l'ordre de 10. Sur la phase d'évaluation, le facteur est de l'ordre de 70, et
sur la phase d'entraînement il atteint même plus de 1 200. En fait, même le coût
de la préparation du vectoriseur *TF-IDF* additionnée à l'entraînement et
l'évaluation du modèle *SGD* a coûté presque 10 fois moins d'énergie que la
seule évaluation de *BERT* (0.7 W·h contre 6.83 W·h). Enfin, alors que la phase
de prétraitement pour filtrer les mots vides et lemmatiser le reste du texte est
souvent perçue comme un préalable décorrélé de l'apprentissage automatique en
lui-même, le fait que *BERT* puisse s'en passer est en réalité un avantage
important puisque c'est en fait à cette phase que la préparation du modèle
*SGD+TF-IDF* consomme la très large majorité (> 98.3%) de l'énergie qu'il
requiert. Devant ce constat, un modèle *SGD+TF-IDF* a été réentraîné sur le
texte non prétraité, obtenant des résultats tout à fait similaires à ceux
obtenus avec prétraitement[^SGD-with-stopwords], ce qui s'explique par la
capacité de la méthode *TF-IDF* à discriminer les mots présents dans un grand
nombre de documents (pour les mots vides, probablement tous). Le modèle obtenu
occupe seulement 4% de place en plus et son entraînement a consommé 15%
d'énergie en plus (5.15e^-1^ W·h), ce qui amène à s'interroger sur la réelle
pertinence de cette phase de prétraitement pour les modèles *SGD+TF-IDF* quand
le nombre de prédictions à faire est limité.

[^SGD-with-stopwords]: la F-mesure reste à 85%, seuls les scores individuels
    bougent, certaines classes comme la *Politique* étant moins mal reconnues (17%
    contre 8%) alors que d'autres perdent un peu comme le *Commerce* (77% contre
    79%) — la *Géographie* est essentiellement inchangée (97% contre 96%)

Dans le cadre des présents travaux de thèse, il ne s'agit en effet pas de mettre
en place un service de classification mais simplement d'appliquer un jeu
d'étiquettes une seule fois sur un ensemble d'articles en vue de leur
exploitation. Bien entendu, les performances très légèrement — mais
strictement — supérieures de *BERT* incitent à le préférer. Mais surtout, le
nombre d'occurrences où le modèle est utilisé est donc assez limité et dû aux
seules expérimentations de cette étude, et une fois la classification obtenue il
n'a plus besoin de servir. Pour cette raison, c'est effectivement le classifieur
*BERT* qui a été retenu pour étiqueter l'ensemble du corpus malgré les points
soulevés précédemment qui montrent la pertinence de *SGD+TF-IDF*.

Le modèle *BERT* a donc été utilisé pour prolonger la classification en
superdomaines sur les articles de l'*EDdA*. La répartition des articles de
l'*EDdA* à l'issue de cette classification est visible à la figure
\ref{fig:edda_count_by_domain_repartition}. Suite aux regroupements introduits
par les superdomaines toutes les classes à part la *Politique* comportent au
moins 1 000 articles.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.30\textwidth}
        \centering
        \small\input{table/GEODE/EDdA/count_by_domain.tex}
        \vspace{.8cm}
        \caption{Décompte absolu}
        \label{tab:edda_count_by_domain_repartition_numbers}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.69\textwidth}
        \centering
        \includegraphics{figure/repartition/corpus/GEODE/EDdA/count_by_domain.png}
        \caption{Proportions relatives}
        \label{fig:edda_count_by_domain_repartition_pie}
    \end{subfigure}
    \caption{Nombre d'articles par domaine dans l'\textit{EDdA}}
    \label{fig:edda_count_by_domain_repartition}
\end{figure}

### Extension à *LGE* {#sec:classifying_lge}

Pour *LGE* la situation est un peu plus compliquée. Il a été montré à la
sous-section \ref{sec:knowledge_domains} (p.\pageref{sec:knowledge_domains}) que
certains articles étaient pourvus de désignants — bien qu'ils semblent plus
rares que dans l'*EDdA* — mais l'encodage des articles de cette encyclopédie
demeure trop superficiel et n'y donne pas accès (voir la section
\ref{sec:corpus_lge} page \pageref{sec:corpus_lge}). Pour obtenir une
classification de ses articles, il faut donc utiliser les prédictions du modèle
*BERT* discuté précédemment dans cette section. Cela pose naturellement un
problème épistémologique majeur: l'ensemble de classe des superdomaines a été
conçu pour représenter de manière simplifiée les domaines de connaissance du
XVIII^ème^ siècle, sans prise en compte particulière de l'état des sciences au
XIX^ème^ ni même des domaines choisis par les éditeurs de *LGE*. L'application
de ces domaines, de surcroît par un système automatisé, ne peut donc pas se
faire sans une phase d'évaluation tâchant de valider la pertinence des
superdomaines associés aux articles.

Le fait le plus saillant parmi les prédictions du modèle *BERT* sur les articles
de *LGE* est le taux surprenamment élevé d'éléments dans la classe *Commerce*,
de l'ordre de 8%. Ce taux est bien supérieur à celui de la même classe dans
l'*EDdA* (3.9%) ou même à sa fréquence perçue en parcourant les pages de *LGE*.
Un échantillonnage des articles ainsi classés fait ressortir un grand nombre
d'articles très courts sur le type de BOULAINCOURT (La Grande Encyclopédie, T7,
p.655) présenté à la figure \ref{fig:boulaincourt}.

![Article BOULAINCOURT, *LGE*, T7, p.655](figure/text/LGE/boulaincourt_t7.png){#fig:boulaincourt width=60%}

Cet article relève évidemment de la *Géographie* et non pas du *Commerce*. À
l'image de la large majorité des articles de l'échantillon, il comprend
l'abréviation «Com.», qui signifie évidemment «commune» dans le contexte. Le
terme est anachronique dans ce sens au XVIII^ème^ siècle: comme le montre
l'article COMMUNE ou COMMUNES (L'Encyclopédie, T3, p.725), il désigne alors une
association d'habitants d'une localité, et pas la localité elle-même. Cette
entrée possède d'ailleurs le désignant «Jurisp.» et relève clairement du droit
plus que de la géographie. Il est difficile de déterminer avec certitude les
raisons qui ont orienté le choix du modèle mais les occurrences répétées de
«Com.» pouvant évoquer un désignant mal filtré dans ces articles incorrectement
classés en *Commerce* sont pour le moins troublantes. Il n'est pas facile non
plus de tester cette hypothèse en contrôlant la lemmatisation de cette
abréviation puisque *BERT* attend en entrée le texte de l'article. Il ne procède
pas à un découpage en mots en interne (il n'y a donc pas de lemmatisation) mais
se contente de grouper des graphèmes qui apparaissent fréquemment ensemble
(algorithme WordPiece).

De manière pragmatique, la solution la plus immédiate pour corriger le problème
au vu de la régularité de ces articles relativement brefs et possédant le motif
«Com. du dép.» («commune du département», autre anachronisme) sur leur première
ligne est de les identifier à l'aide d'une expression régulière et d'un filtrage
sur leur taille. En effet, le motif peut survenir dans des articles plus
généraux sur un département, comprenant notamment des sections historiques ou
économiques et les erreurs de segmentation (voir la section
\ref{sec:corpus_preprocessing_lge} page \pageref{lge_segmentation}) peuvent
préfixer de tels articles géographiques brefs à d'autres articles sur des sujets
divers. Pour éviter d'introduire trop de bruit dans de futurs usages qui
pourraient être faits de ces articles, un seuil de 50 tokens est fixé
empiriquement après échantillonnage d'articles ayant une occurrence de
l'expression précédente sur leur première ligne.

Plus précisément, le motif est sujet à une certaine variation sans doute en
partie du fait de l'envergure de projet de *LGE* — par application de ce
principe empirique proposé au début du présent chapitre qui veut qu'à cette
échelle tout motif subit des mutations (voir la section \ref{sec:principles}
p.\pageref{sec:principles}) — mais surtout à cause des erreurs d'[@=OCR] qui
fournissent un rendu imprécis des caractères présents sur le papier. Certains
'C' majuscules, peut-être à cause d'une tache sur le papier ont été lus comme
'G', certains '.' à la fin des abréviations ont pu être pris pour des ',' voire
être omis entièrement. Pour ces raisons, l'expression régulière écrite pour
rechercher ce motif a été largement étendue pour s'adapter à tous les cas
rencontrés empiriquement dans les articles comme le montre l'extrait de code
source \ref{lst:com_du_dep_regex}.

\begin{lstlisting}[caption=Expression régulière utilisée pour repérer les
entrées de communes,label=lst:com_du_dep_regex]
[cCG]\(o\(m\|[ri]n\)\{1,2\}\|ant\)[-.,]\? \?d[ue] \?d[ée]p[-,.]\?
\end{lstlisting}

En lisant les blocs à partir des plus profonds pour remonter à l'expression dans
son ensemble, voici comment elle peut s'analyser. D'abord, la lettre 'm' est
parfois perçue par l'[@=OCR] comme la séquence "rn" voire "in", ce qui est à
l'origine de la séquence `\(m\|[ri]n\)` qui sert à capturer «un m» : soit
`R`~`m`~ cette séquence. L'abréviation n'est pas toujours la même et il y a
plusieurs occurrences où les deux 'm' du mot «commune» ont été conservés: c'est
là le sens de `R`~`m`~`\{1, 2\}` qui tolère une séquence de 1 ou 2 fois
l'expression précédente pour absorber les différentes réalisations possibles de
«m» ou «mm». Ce bloc, préfixé du `o` reconnaît évidemment la séquence «omm» (et
les différentes combinaisons qui peuvent le remplacer, donc): ce sera
`R`~`omm?`~ dans ce qui suit. L'ensemble `\(R`~`omm?`~`\|ant\)` reconnaît donc
la séquence précédente ou bien «ant»: cette disjonction a été ajoutée pour
prendre en compte quelques villes qui n'étaient pas mentionnées en tant que
«commune» mais en tant que chef-lieu de canton «Ch.-l. de cant. du dép.» comme
l'article HEYRIEUX (La Grande Encyclopédie, T20, p.60) présenté à la figure
\ref{fig:heyrieux_t20}[^canton]. Le préfixe `[cCG]` sert à capturer un "C", qui
est parfois lu "c" ou "G" par l'[@=OCR], pour former `R`~`Comm?|Cant`~. Le motif
`[-.,]\?`, qui survient juste après `R`~`Comm?|Cant`~ ainsi qu'à la toute fin de
l'expression, permet d'absorber différentes lectures du '.' (point), parfois ","
voire "-", ainsi que son absence potentielle (`\?`). Ce `\?` survient aussi
après les deux espaces qui manquent entre les mots de l'expression dans certains
textes. Les deux derniers ensembles de caractères prennent en compte "du" et
"de" (on trouve «du dép.» ou «de dép.») et bien sûr l'absence possible de
l'accent de «dép.».

[^canton]: Il est difficile de savoir pourquoi ces entrées peuvent être
    également affectées par l'erreur de classification en *Commerce*
    puisqu'elles ne comportent pas le motif «Com.». Cette erreur paraît moins
    systématique que pour les entrées où ce motif apparaît et, s'agissant de
    villes plus importantes (puisque chefs-lieux), celles qui subissent cette
    erreur de classification mentionnent souvent des produits issus de la ville
     — comme les chaussures de la figure \ref{fig:heyrieux_t20} — ce qui
     pourrait fournir une piste d'explication.

![Article HEYRIEUX, *LGE*, T20, p.60](figure/text/LGE/heyrieux_t20.png){#fig:heyrieux_t20 width=60%}

À l'issue de ce traitement, 20 116 articles sont identifiés (soit 14.9% du total
des articles de *LGE*) et la classe *Géographie* leur est assignée manuellement.
Ces articles constituent un sous-corpus des Communes qui ne fait pas l'objet
d'une exploitation particulière dans le cadre de cette thèse mais qui pourrait
intéresser de futures études. Il serait également intéressant d'utiliser ces
articles pour améliorer le modèle de classification mais cela reste à faire.

La classification utilisée pour les articles de *LGE* est donc composite: elle
assigne la classe *Géographie* aux articles du sous-corpus des Communes défini
ci-dessus, et la classe prédite par le modèle *BERT* entraîné pour les
superdomaines sur l'*EDdA* pour les autres articles. Une fois cette définition
posée, il devient possible d'évaluer la qualité de cette annotation afin de
juger de son utilité. Le différentiel conceptuel entre ces classes prévu pour le
XVIII^ème^s. et ces articles rédigés au XIX^ème^s. (voir la section
\ref{sec:structuring_knowledge} p.\pageref{sec:structuring_knowledge}) empêche
naturellement les articles de *LGE* de correspondre parfaitement aux classes
proposées. Ainsi, la présence des biographies discutées précédemment (à la
section \ref{sec:knowledge_domains} p.\pageref{sec:knowledge_domains}) perturbe
évidemment le système de classes des superdomaines modelé sur l'*EDdA* qui les
exclut volontairement; de même des domaines anachroniques comme l'Industrie
n'existent pas parmi les superdomaines. Une telle évaluation ne peut donc pas
exiger une rigueur comparable à celle menée sur les modèles de classification
comparés à la section \ref{sec:classifier_benchmark}
(p.\pageref{sec:classifier_benchmark} et seq.) où il s'agissait essentiellement
de «compter les points», mais doit au contraire se contenter de déterminer si la
classe proposée fait assez sens pour permettre d'opérer des comparaisons
intéressantes dans les analyses contrastives du chapitre suivant.

Dans cette optique, un échantillon de 40 articles pour chacun des 17
superdomaines (soit $n = 680$ articles au total) a été sélectionné aléatoirement
pour une validation manuelle des superdomaines qui leur étaient associés.
Plusieurs problèmes gênent aussi l'évaluation, dont les problèmes de
segmentation qui affectent encore un nombre perceptible d'articles de *LGE*  ce
qui cause la présence d'agrégats de plusieurs articles parmi les textes de
l'échantillon pour lesquels la définition d'une classe fait peu sens. La
présence d'articles qui sont de purs renvois sans contenu propre rend également
difficile la détermination d'une classe sans ambiguïté. Pour toutes ces raisons,
l'évaluation a été plutôt permissive, c'est-à-dire que la «charge de la preuve»
a plutôt pesé sur le rejet de la classification proposée par le modèle que sur
son acceptation : le refus d'une annotation suggérée par le modèle devait être
motivé par au moins un superdomaine qui conviendrait mieux de manière évidente.
Pour un article qui aurait accepté plusieurs classes, la proposition du modèle a
été acceptée si l'une au moins correspondait (même si ça n'était pas forcément
la première mentionnée dans le texte). C'est ainsi que la plupart des
biographies se retrouvent en *Histoire* ou dans la discipline où la personne
s'est illustrée. Pour les renvois, la classe proposée a été acceptée sauf quand
la cible du renvois relevait manifestement d'une autre classe. Pour les
agrégats de plusieurs articles, la classification a été rejetée quand vraiment
aucune des entrées contenues n'avait de rapport avec la classe proposée et
acceptée sinon.

Avec cette convention, la méthode composite (*Géographie* pour les Communes, la
prédiction de *BERT* pour les autres articles) a prédit une classe acceptable
pour 590 articles de l'échantillon, soit 86.8% de la totalité. S'agissant d'une
évaluation binaire, qui dit juste si la classe est acceptable sans fournir de
réponse attendue dans le cas contraire, il n'est pas possible de calculer de
score de rappel ni de générer de matrice de confusion pour observer les biais du
modèle. Le score mesuré correspond à une précision puisqu'il compte le nombre de
vrais positifs divisé par le nombre total d'élements. Le tableau
\ref{tab:lge_bert_evaluation} montre que le modèle obtient une assez bonne
précision sur la plupart des domaines (supérieure à 90% sur 7 domaines). Les
domaines qui mettent le plus le modèle en difficulté sont la *Chasse*, le
*Commerce* et dans une moindre mesure le domaine *Militaire*. La *Politique*,
qui est pourtant si mal reconnue par le modèle dans l'*EDdA* obtient le score
tout à fait surprenant de 95%. D'une part, il est à noter que lors de
l'évaluation sur l'*EDdA*, la précision — certes bien plus faible — était tout
de même de 56%, et c'est le très mauvais rappel sur cette classe qui donnait une
F-mesure aussi faible. D'autre part, les articles de *LGE* classés en
*Politique* sont assez prototypiques, très longs pour la plupart, ou consistent
en des biographies d'hommes politiques (et sans doute un grand nombre d'articles
validés auraient aussi pu être acceptés en *Histoire*, donc il faut garder à
l'esprit que les prédictions du modèle sont par construction acceptables pour un
humain, mais pas nécessairement comparables à celles faites sur l'*EDdA*).

\begin{table}[h!]
    \centering
    \input{table/LGE_evaluation.tex}
    \caption{Scores de précision par domaine obtenus par le modèle \textit{BERT}
    pour la classification en superdomaines sur l'échantillon de \textit{LGE}}
    \label{tab:lge_bert_evaluation}
\end{table}

\label{bernouilli_experiment}Le nombre relativement élevé d'articles
sélectionnés pour cette évaluation permet d'accorder une certaine confiance aux
résultats obtenus. S'agissant d'une évaluation binaire, le processus peut se
modéliser par le tirage sans remise (chaque article ne peut être sélectionné
qu'une fois dans l'échantillon) d'une variable aléatoire booléenne pour chaque
article, pouvant prendre la valeur `vrai` si la classification proposée par le
modèle pour cet article est jugée pertinente et `faux` sinon. Il s'agit de
variables de Bernoulli, non indépendantes en toute rigueur: la probabilité que
la première variable vale `vrai` est égale à la précision du modèle dans son
ensemble, mais en fonction du résultat de ce premier tirage, la probabilité de
succès de la deuxième variable est affectée. Toutefois, les dimensions de
l'expérience font que l'échantillon reste assez petit par rapport à l'ensemble
de la population (100 × 680 < 130k) donc les variables peuvent être considérées
indépendantes (le résultat de chaque tirage affecte peu la proportion d'articles
dont la classification est satisfaisante parmi les articles restants). Le
Théorème Central Limite s'applique donc et permet de modéliser le comportement
de la somme de ces variables par une loi normale.

La moyenne empirique $m$ constitue un estimateur sans biais de la proportion
réelle $p$. C'est donc la valeur la plus probable compte tenu du résultat de
l'expérience, c'est-à-dire que toute autre valeur, prise individuellement, est
moins probable que $m$ mais pas impossible. La vraisemblance de l'estimation
dépend de la taille de l'intervalle que l'on considère autour de $m$. Si l'on
prend un intervalle trop petit (par exemple $10^{-6}\%$) il est plus probable
que $p$ soit hors de cet intervalle que dedans. En particulier, il est vain de
croire que $p$ puisse être exactement égale à $m$ (intervalle de taille nulle):
bien que ce soit la valeur la plus vraisemblable, la probabilité de ce nombre
précis est nulle. L'ensemble des valeurs réelles possibles pour $p$ suit une
distribution en cloche autour de la valeur $m$, la loi normale, dont la
probabilité décroît au fur et à mesure que l'on s'en éloigne. Il faut donc
accepter de perdre en précision pour obtenir un intervalle assez probable pour
connaître $p$ avec une confiance suffisante. La taille du demi-intervalle dépend
de la moyenne empirique $m$, de la taille de l'échantillon $n$ et d'un facteur
multiplicatif lié seulement pour une loi donnée à la précision attendue, son
quantile. Ainsi pour que $p$ ait plus de 95% de chance de faire partie de
l'intervalle proposé (encadrement à 95% de confiance), le quantile $z_{97.5\%}$,
d'ordre $1 - \frac{5}{2} = 97.5\%$ permet d'écrire en reprenant $n$ pour
désigner le nombre total d'article échantillonnés, l'encadrement de la formule
\ref{eq:quality_range_algebraic}.

\begin{equation}
    m - z_{97.5\%} \times \sqrt{\frac{m \times (1- m)}{n}} \le p \le m + z_{97.5\%} \times \sqrt{\frac{m \times (1- m)}{n}}
    \label{eq:quality_range_algebraic}
\end{equation}

Une application numérique à partir des valeurs $m = \frac{590}{680}$, $n = 680$
et $z_{97.5\%} = 1.96$ (donnée par les tables numériques de la loi normale)
permet d'obtenir l'encadrement \ref{eq:quality_range_numerical}. Cet encadrement
signifie que l'étiquetage par le modèle des domaines de connaissance dans *LGE*
a 95% de chances d'avoir une qualité au moins égale à 84.2% et inférieure à
89.3%.

\begin{equation}
    84.2\% \le p \le 89.3\%
    \label{eq:quality_range_numerical}
\end{equation}

La formule \ref{eq:quality_range_algebraic} montre que la largeur de
l'intervalle dépend de la taille de l'échantillon $n$. Par conséquent, la
précision de l'encadrement est moins bonne pour chaque classe individuellement
(pour lesquelles $n = 40$) que pour l'ensemble des articles. La présence du
produit $m \times (1 - m)$ au numérateur du demi-intervalle dans
\ref{eq:quality_range_algebraic} montre également que la précision est meilleure
pour les classes les mieux reconnues (car ce produit toujours positif s'annule
en $0$ et en $1$ et atteint sa valeur maximale — $0.25$ — en $m = 0.5$). Ainsi,
avec le même degré de confiance de 95%, il est par exemple possible d'affirmer
que la précision du modèle sur la classe *Histoire naturelle* est d'au moins
92.7%. Par contraste, la précision la moins bonne mesurée sur la classe *Chasse*
(67.5%) ne peut pas être encadrée aussi finement et pour conserver le même
indice de confiance il n'est possible de fournir que la valeur 52.3% comme borne
inférieure (avec optimisme, le même argument renversé implique que seules des
valeurs au-delà de 82.0% ont moins de 5% de chance d'être la vraie précision du
modèle sur cette classe). Pour la classe *Géographie* qui intéresse
principalement les présents travaux, il est possible d'affirmer à 95% de
confiance que la précision est au moins supérieure à 88.2%. Les bons résultats
du modèle sur la très large majorité des classes — et la *Géographie* en
particulier — permettent de valider l'emploi de cette annotation pour les
analyses conduites tout au long du chapitre \ref{sec:contrasts}. La répartition
des nombres d'articles de *LGE* par superdomaine est donnée par la figure
\ref{fig:lge_count_by_domain_repartition}.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.30\textwidth}
        \centering
        \small\input{table/GEODE/LGE/count_by_domain.tex}
        \vspace{.8cm}
        \caption{Décompte absolu}
        \label{tab:lge_count_by_domain_repartition_numbers}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.69\textwidth}
        \centering
        \includegraphics{figure/repartition/corpus/GEODE/LGE/count_by_domain.png}
        \caption{Proportions relatives}
        \label{fig:lge_count_by_domain_repartition_pie}
    \end{subfigure}
    \caption{Nombre d'articles par domaine dans \textit{LGE}}
    \label{fig:lge_count_by_domain_repartition}
\end{figure}

Cette section conclue les travaux en classification automatique effectués dans
le cadre de cette thèse. La comparaison de classifieurs opérée à la section
\ref{sec:classifiers_comparison} a montré la pertinence de l'emploi d'un modèle
de type *BERT* pour appliquer une classification en domaine aux articles. Plus
inattendu, cette comparaison a aussi révèlé l'intérêt spécifique de méthodes
classiques d'[@=AA] telles que *SVM* et *SGD* associées à une vectorisation
*TF-IDF* sur des volumes de données de l'ordre de grandeur de ceux présents dans
le corpus d'étude. Ces modèles nécessitent en effet relativement «peu» de
données pour donner des résultats exploitables par rapport aux méthodes d'[@=AP]
autres que *BERT*. La sous-section \ref{sec:classification_choices}) a en outre
mis en évidence le fait que *SGD*, certes légèrement moins performant que
*BERT*, s'avère en plus particulièrement efficace du point de vue de la
consommation énergétique. Les études conduites à la section
\ref{sec:geo_relations} sur les erreurs faites par ce modèle sur les articles de
l'*EDdA* suggèrent des pistes intéressantes à explorer dans le chapitre
\ref{sec:contrasts}, notamment sur la présence de contenus non géographiques
dans les articles de la classe *Géographie*. À l'issue de ces travaux de
classification, la totalité des articles du corpus — aussi bien ceux issus de
l'*EDdA* que de *LGE* — a été classée selon ces superdomaines, ce qui rend
possible les études contrastives développées dans le dernier chapitre.

