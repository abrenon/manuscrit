## Comparaison de méthodes de classification automatique {#sec:classifiers_comparison}

Cette première section présente l'étude comparative menée pour choisir un modèle
de classification, en commençant par exposer la démarche suivie avant de décrire
son implémentation (sous-section \ref{sec:comparison_preparation}). La dernière
sous-section (\ref{sec:classifier_benchmark}) présente les résultats obtenus et
discute leur signification tout en gardant à l'esprit le fait que les
performances obtenues par les différentes méthodes présentées ne rejoignent pas
nécessairement les raisons pour lesquelles les éditeurs d'encyclopédies
assignent une classification aux articles.

### Démarche

Pour obtenir une classification complète des articles dans le système des
domaines regroupés choisis à la section \ref{sec:domains_build_classes} (voir p.
\pageref{sec:domains_build_classes}), il suffit pour les articles qui possèdent
déjà un domaine de le convertir vers ce nouveau jeu d'étiquettes, et pour les
autres de prédire une valeur à l'aide d'un modèle de classification automatique.
Pour disposer d'un tel classifieur, il faut un ensemble d'articles possédant
déjà un domaine regroupé assez vaste pour l'entraîner puis tester ses
performances (pour évaluer la qualité de l'étiquetage obtenu). Ce deuxième cas
se ramène donc au premier: il est nécessaire de commencer par appliquer la
correspondance entre le domaine qui leur a été attribué par l'[@=ENCCRE]
\(«ensemble de domaine») et les domaines regroupés. Puisque la seule différence
entre les deux jeux d'étiquettes réside dans le regroupement des différentes
branches professionnelles («métiers du bois», «métiers du papier», etc.) en une
classe *Métiers* unique (voir p.\pageref{sec:domain_groups}), la conversion est
en réalité très simple puisqu'elle consiste à conserver la classe initiale
partout sauf pour les métiers.

Toutefois, les choix de segmentation du texte en articles, légèrement différents
entre l'[@=ENCCRE] \(dont proviennent les annotations en ensemble de domaines)
et l'[@=ARTFL] \(qui a fourni les fichiers encodés des articles, voir la section
\ref{edda_existing_versions} p.\pageref{edda_existing_versions}), compliquent
cette conversion en empêchant d'établir une correspondance parfaite entre les
deux corpus. En effet, les synonymes ne sont pas toujours traitées de la même
façon selon la typographie utilisée dans l'*EDdA*, l'[@=ARTFL] les séparant
parfois en des articles distincts (au sens de sa numérotation à 2 niveaux: tome
et rang de l'article) là où l'[@=ENCCRE] en fait des sous-entrées distinctes au
sein d'un même article (ses textes possèdent des identifiants à 3 niveaux, un
numéro d'entrée venant s'ajouter au numéro d'article). Par conséquent, seuls
69 531 articles ont pu être appairés entre ces deux sources sur les 74 190 au
total présents dans la version de l'œuvre étudiée.

Pour cette raison certains articles sans désignant marqué typographiquement
possèdent un domaine (inféré par l'[@=ENCCRE] d'expressions telles que «en
termes de…» qui remplacent parfois les désignants — voir la section
\ref{sec:edda_domain_expressions} p.\pageref{sec:edda_domain_expressions}); à
l'inverse, des articles pourtant pourvus d'un désignant mais qui n'ont pas pu
être mis en correspondance avec leur équivalent chez l'[@=ENCCRE] peuvent se
retrouver sans domaine et nécessiter ensuite une prédiction du
modèle[^domain-paradox]. Dans les données de l'[@=ENCCRE], 12 635 articles ne
sont pas pourvus d'une classification explicite, mais l'application d'une
classification experte basée sur le contenu des articles parvient à réduire ce
nombre à 2 392. Le jeu de données de l'[@=ARTFL] qui n'a utilisé que les
désignants explicites fournit quant à lui une classification pour 56 688
articles.

[^domain-paradox]: ce point est tout à fait contre-intuitif mais, s'il reste
    heureusement très marginal, il est important de le garder en tête pour
    comprendre pourquoi des prédictions ont pu être réalisées sur des articles
    qui comportent pourtant un ou plusieurs désignants, notamment dans la
    section \ref{sec:parallel_analysis} (p.\pageref{sec:parallel_analysis} et
    seq.)

Cette approche est inspirée de l'étude conduite par @horton2009mining qui
avaient employé un modèle [@=Naive Bayes] Multinomial pour étendre la
classification originale des auteurs. Bien que leur étude ne comporte pas
d'évaluation formelle des performances de leur modèle, il offre une analyse
détaillée d'un échantillon des résultats. Au vu de la vitesse à laquelle l'état
de l'art en apprentissage automatique a bougé au cours de la décennie 2010-2020,
il est intéressant d'actualiser leurs travaux pour y intégrer des modèles de
classification récents, non seulement des méthodes d'[@=AA] classiques mais
aussi des modèles d'[@=AP].

### Choix des modèles et préparation des jeux de données {#sec:comparison_preparation}

Les présents travaux comparent des approches combinant différentes méthodes de
vectorisation et de classification supervisée décrites aux sections
\ref{sec:EdlA_vectorization} et \ref{sec:EdlA_classification} (voir
pp.\pageref{sec:EdlA_vectorization} et seq.) ou issues de méthodes présentées
dans ces sections. Ainsi, plutôt qu'un modèle *LSTM* (voir la section
\ref{deep_learning_classifiers} p.\pageref{deep_learning_classifiers}), c'est sa
variante plus récente et bidirectionnelle *BiLSTM* qui a été testée. Cette étude
vise à tester le plus de combinaisons possibles, mais toutes les combinaisons ne
sont pas réalisables à cause de contraintes spécifiques.

Par exemple, la méthode *[@=Naive Bayes]* nécessite des vecteurs d'entrée sans
valeurs négatives, ce qui impose l'emploi d'une représentation vectorielle *Bag
of Words* ou *TF-IDF* mais empêche l'usage de plongements de mots. De plus,
puisque les modèles de la famille de *BERT* intègrent à la fois la
représentation vectorielle de leurs entrées et les couches de classification,
permettant de travailler directement à partir du texte, ils n'ont pas pu être
combinées à d'autres méthodes de vectorisation. En tenant compte de ces
remarques, les combinaisons testées sont les suivantes:

1. Une vectorisation *BoW* et des algorithmes traditionnels d'AA (*Naive Bayes*,
   *Logistic Regression*, *Random forest*, *SVM* et *SGD*);

2. Une vectorisation utilisant un plongement de mots statique (*Doc2Vec*) et des
   algorithmes d'AA traditionnels (*Logistic regression*, *Random Forest*, *SVM*
   et *SGD*);

3. Une vectorisation utilisant un plongement de mots statique (*FastText\[fr\]*)
   et des algorithmes d'AP (*CNN* et *BiLSTM*);

4. Une approche tout-en-un utilisant des modèles de langue contextuels
   pré-entraînés (*BERT*, *CamemBERT*) dont les poids sont réajustés à la tâche
   dont il est question (*fine-tuning*).

Pour les algorithmes d'[@=AA], la librairie Scikit-learn[^scikit-learn] et plus
précisément *GridSearchCV* a été utilisée pour déterminer les hyperparamètres.
Les résultats de la recherche avec *GridSearchCV* sont disponibles dans le dépôt
git de l'étude[^results]. Dans les expériences d'[@=AP] c'est la version
française de *FastText* [@bojanowski2017enriching] pour les plongements
préentraînés qui a été utilisée pour vectoriser les entrées et les
implémentations des *CNN*s et des *BiLSTM*s proviennent de la librairie Keras.
Le modèle de *CNN* testé emploie une architecture classique avec une couche de
plongement pour des vecteurs de dimension 300 (la taille des vecteurs de mots
pré-entraînés par *FastText*), une couche de convolution avec la fonction
d'activation ReLu, une couche de max-pooling et une couche softmax pour la
sortie. Pour le modèle *BiLSTM*, plusieurs architectures ont été testées et
celle retenue utilise une taille de vecteurs de 300 également, une couche
*BiLSTM* (avec un taux de décrochage de 20%), une couche de max-pooling, deux
couches denses avec la fonction d'activation ReLu avec un taux d'abandon de 50%
entre chaque, et une couche softmax en sortie. Pour ajuster les modèles *BERT*,
les données d'entraînement ont été groupées par lots de huit fichiers (à cause
de contraintes de place en mémoire) pendant quatre époques — @devlin2018bert
recommandent entre deux et quatre époques pour ce type d'opération.

[^scikit-learn]: [https://scikit-learn.org/stable/](https://scikit-learn.org/stable/)
[^results]: [https://gitlab.liris.cnrs.fr/geode/EDdA-Classification/-/tree/master/reports](https://gitlab.liris.cnrs.fr/geode/EDdA-Classification/-/tree/master/reports)

\label{classification_datasets}Deux jeux de données distincts sont utilisés pour
entraîner et évaluer les modèles (respectivement dénommés dans ce qui suit
*train* et *test*). Ces deux jeux de données (voir le tableau
\ref{tab:nb_articles_seuil}) contiennent les articles étiquetés avec l'ensemble
de domaines regroupés déduit de l'étiquetage opéré par l'[@=ENCCRE]. Dans ce qui
suit, traitant d'un problème de classification, ces domaines regroupés ne seront
donc plus vus comme des objets épistémologiques mais comme autant d'étiquettes
abstraites apposées sur des textes: des classes. Pour cette étude, une seule
classe a été retenue par article. En réalité, 3 654 articles de l'*EDdA* (soit
environ 5%) possèdent plusieurs classes. Quelques traitements sont appliqués
avant de pouvoir débuter les expériences proprement dites. D'abord, les articles
sans classe et plus courts que 15 mots sont écartés. Cela produit un jeu de
58 509 articles répartis sur une des 38 classes possibles. Ensuite, les
désignants sont retirés des articles pour éviter que les modèles ne fondent
leurs prédictions que sur ces éléments (ce qui donnerait des scores
artificiellement élevés sur le jeu de validation mais les rendrait bien plus
mauvais pour prolonger la classification sur les articles qui justement n'en
sont pas pourvus). Enfin, la ponctuation et les mots vides — déterminants,
prépositions, pronom etc. en reprenant la distinction de
@lehmann_lexicologie_2018[chap.1 §8] — sont retirés des textes et les mots
restants sont lemmatisés[^lefff]. Cette dernière étape est contournée pour les
modèles de la famille *BERT* (ligne 4. dans la liste précédente) puisque ces
modèles peuvent recevoir en entrée des textes complets et la version des textes
qui leur est présentée, pour l'apprentissage comme pour la prédiction est celle
issue de l'étape précédente à savoir le texte purgé seulement de son éventuel
désignant. Une proportion de 20% est réservée pour le jeu d'évaluation (*test*)
et le reste devient le jeu d'entraînement (*train*).

[^lefff]: les tokens sont identifiés avec la librairie SpaCy et la lemmatisation
    proprement dite est effectuée à l'aide du FrenchLefffLemmatiser
    [@sagot_lefff_2010]

::: {}
  Jeu de donnée           \# articles
  ---------------------- -------------
  corpus entier                 74 190
  corpus prétraité              58 509
  train (tout)                  46 807
  train (max 1 500)             27 381
  train (max 500)               14 058
  test                          11 702

  : Détail du nombre d'articles dans le corpus entier, le corpus pré-traité et
  les jeux d'entraînement et de test.
  \label{tab:nb_articles_seuil} 

:::

Du fait du déséquilibre entre les nombres d'articles des différentes classes, il
apparaît essentiel d'évaluer l'impact de l'utilisation d'échantillons d'articles
pour les classes surreprésentées. On peut faire l'hypothèse que cela va réduire
les biais des modèles en leur faveur. Un paramètre supplémentaire est donc
ajouté aux expériences pour comparer les résultats des modèles entraînés sur des
jeux d'entraînement plus ou moins homogènes. Les classes sont limitées à 500
articles maximum, 1 500 articles maximum ou ne sont pas limitées du tout pour
constituer 3 jeux d'entraînement différents. Ces 3 cas correspondent aux 3
lignes *train* dans le tableau ci-dessus (tableau \ref{tab:nb_articles_seuil}),
qui représentent donc des jeux d'entraînement de respectivement 14 058, 27 381
et 46 807 articles au total.

### Évaluation des performances {#sec:classifier_benchmark}

Les résultats de classification sont évalués en mesurant la précision, le rappel
et la F-mesure (moyenne harmonique de la précision et du rappel) de chaque
méthode. Afin d'obtenir des résultats généraux pour toutes les classes, les
F-mesures moyennes sur les 38 classes pondérées par leur nombre d'articles sont
examinés. Le tableau \ref{tab:result_1} présente les F-mesures obtenues par les
différentes méthodes.

::: {}

: F-mesures moyennes pour les différents modèles entraînés sur un échantillon de 500 articles maximum par classe (1), 1500 maximum par classes (2) et tous les articles disponibles de chaque classe (3) appliqués sur le jeu de test.
\label{tab:result_1}

+---------------------+---------------+--------------------------------+
| Classification      | Vectorisation | F-mesure                       |
|                     |               +----------+----------+----------+
|                     |               |    (1)   |    (2)   |    (3)   |
+=====================+===============+:========:+:========:+:========:+
| Naive Bayes         | BoW           |   0.63   |   0.71   |   0.70   |
|                     +---------------+----------+----------+----------+
|                     | TF-IDF        |   0.74   |   0.69   |   0.44   |
+---------------------+---------------+----------+----------+----------+
| Logistic Regression | BoW           |   0.74   |   0.77   |   0.79   |
|                     +---------------+----------+----------+----------+
|                     | TF-IDF        | **0.77** |   0.79   | **0.81** |
|                     +---------------+----------+----------+----------+
|                     | Doc2Vec       |   0.64   |   0.69   |   0.77   |
+---------------------+---------------+----------+----------+----------+
| Random Forest       | BoW           |   0.57   |   0.54   |   0.16   |
|                     +---------------+----------+----------+----------+
|                     | TF-IDF        |   0.55   |   0.53   |   0.16   |
|                     +---------------+----------+----------+----------+
|                     | Doc2Vec       |   0.63   |   0.66   |   0.60   |
+---------------------+---------------+----------+----------+----------+
| SGD                 | BoW           |   0.70   |   0.73   |   0.75   |
|                     +---------------+----------+----------+----------+
|                     | TF-IDF        | **0.77** | **0.81** | **0.81** |
|                     +---------------+----------+----------+----------+
|                     | Doc2Vec       |   0.68   |   0.72   |   0.76   |
+---------------------+---------------+----------+----------+----------+
| SVM                 | BoW           |   0.71   |   0.75   |   0.78   |
|                     +---------------+----------+----------+----------+
|                     | TF-IDF        | **0.77** |   0.80   | **0.81** |
|                     +---------------+----------+----------+----------+
|                     | Doc2Vec       |   0.68   |   0.74   |   0.78   |
+---------------------+---------------+----------+----------+----------+
| CNN                 | FastText      |   0.65   |   0.72   |   0.74   |
+---------------------+---------------+----------+----------+----------+
| Bi-LSTM             | FastText      | **0.69** | **0.79** | **0.80** |
+---------------------+---------------+----------+----------+----------+
| BERT Multilingue    | \-            | **0.81** | **0.85** | **0.86** |
+---------------------+---------------+----------+----------+----------+
| CamemBERT           | \-            |   0.78   |   0.83   | **0.86** |
+---------------------+---------------+----------+----------+----------+

:::

La méthode *Random Forest* obtient les plus mauvais résultats (entre 16% et 66%)
et ce quelle que soit la méthode de vectorisation et l'échantillonnage utilisés.
Les résultats de la méthode *Naive Bayes* vont de 44% à 74% et présentent une
très forte dépendance à l'échantillonnage pour la vectorisation *TF-IDF*. Les
méthodes *Logistic regression*, *SGD* et *SVM* obtiennent des résultats très
semblables et les meilleurs sont atteints avec la vectorisation *TF-IDF*
(environ 80%). De manière surprenante, le plongement de mot *Doc2Vec* produit
des résultats légèrement inférieurs à la représentation *BoW*. Les scores
augmentent avec la taille de l'échantillonnage, ce qui explique pourquoi cette
approche a besoin d'un grand jeu de données pour bien fonctionner. Les approches
d'[@=AP] utilisant des réseaux de neurones (*CNN* et *BiLSTM*) obtiennent des
scores comparables (entre 65% et 80%) mais restent tout de même en dessous des
meilleures méthodes d'[@=AA] traditionnelles associées à la vectorisation
*TF-IDF*. Le rééquilibrage des classes a un effet négatif plus important que
pour les méthodes traditionnelles à cause de la réduction du jeu de données
qu'il occasionne. Les modèles de langue préentraînés tel que *BERT* Multilingue
et *CamemBERT* obtiennent des résultats encore meilleurs de peu aux méthodes
classiques d'[@=AA], offrant des performances très comparables avec 86% pour la
meilleure F-mesure. Les résultats de *BERT* Multilingue et *CamemBERT* sont très
proches pour ce qui est de la moyenne globale mais diffèrent un peu sur chaque
classe, en fonction de la taille d'échantillonnage.

La figure \ref{fig:F1Scores_BERTvsCAMEMBERT} montre les F-mesures obtenus pour
*BERT* Multilingue (courbe bleue) et *CamemBERT* (courbe orange) pour chacune
des classes (les courbes grises correspondent aux autres méthodes). Les classes
sont triées de gauche à droite par ordre de prévalence dans l'échantillon
(c'est-à-dire dans le même ordre que l'histogramme de la figure
\ref{fig:distribution_grouped_domains} à la page
\pageref{fig:distribution_grouped_domains}). Les deux méthodes obtiennent des
scores similaires pour la plupart des classes et qui ne diffèrent que sur les
classes avec très peu d'articles. Par exemple, la classe *Économie domestique*
n'obtient que 44% avec *BERT* mais 61% avec *CamemBERT*. Pour la *Politique* au
contraire, *BERT* parvient à atteindre 53% alors que *CamemBERT* plonge à 8%; de
même sur la *Minéralogie* (70% contre 37%) et le *Spectacle* (62% contre 0%,
*CamemBERT* faisant l'impasse complète sur cette classe). Mais comme les écarts
les plus importants entre ces résultats sont sur des classes sous-représentées,
l'impact sur la moyenne globale est très petit et se retrouve compensé par des
écarts pourtant plus réduits mais sur des classes plus nombreuses. Cette figure
met aussi en évidence le fait que la classe *Arts et métiers* pose des
difficultés à toutes les méthodes sans exception: toutes obtiennent des scores
inférieurs à 40%, vraisemblablement parce que ce domaine est souvent confondu
avec les *Métiers*. L'ambiguïté du système de domaines et les ressemblances
entre classes sont discutées plus en détail à la section
\ref{sec:geo_relations}. D'une manière générale, augmenter la taille des
échantillons par classe (c'est-à-dire le nombre maximum d'articles pour chacune)
et donc accroître l'écart entre les classes les moins peuplées et celles les
plus peuplées améliore les résultats, mais détériore ceux obtenus sur les
classes moins représentées. Cela suggère qu'il est préférable d'avoir plus de
données même avec des classes déséquilibrées que moins de données avec des
classes bien équilibrées si le but est d'améliorer les résultats pour un grand
nombre de classes différentes.

![F-scores obtenus par BERT Multilingual et CamemBERT sur chaque classe](figure/classification/F1Score/BERTvsCAMEMBERT.png){#fig:F1Scores_BERTvsCAMEMBERT width=80%}

Deux méthodes présentent un comportement différent: *Random Forest* et *Naive
Bayes*, lorsqu'elles sont combinées avec la vectorisation *TF-IDF*. Dans le cas
de *Naive Bayes*, la figure \ref{fig:F1Scores_NB_TF} montre clairement que la
classification obtient de très mauvais résultats sans échantillonnage (courbe
verte), et la stratégie d'échantillonnage a un impact positif sur les
performances. Sans échantillonnage, les performances se dégradent très
rapidement avec la diminution du nombre d'articles par classe et le modèle ne
fonctionne que pour les celles qui sont les plus peuplées (à gauche de la
figure). Le modèle entraîné avec des classes comprenant au plus 1 500 articles
(courbe orange) fonctionne plutôt bien sur toutes les classes ayant plus de 500
articles. Enfin, seul le modèle avec le jeu d'entraînement le plus équilibré
(avec 500 articles au plus par classe, courbe bleue) parvient à classer
correctement la majorité des classes et ses performances ne chutent
dramatiquement que sur les classes avec moins de 200 articles dans toute
l'*EDdA* (de *Mesure* à *Spectacle*).

![F-mesures obtenues par *Naive Bayes + TF-IDF* sur chaque classe avec trois échantillonnages différents](figure/classification/F1Score/NB_TF.png){#fig:F1Scores_NB_TF width=80%}

Parmi les méthodes traditionnelles d'[@=AA], la vectorisation *TF-IDF* obtient
quasi-systématiquement de meilleurs résultats que les *BoW* et *Doc2Vec*. Cela
reste vrai aussi bien sur les F-mesures globales que sur les performances
obtenues sur chaque classe. La figure \ref{fig:F1Scores_SGD} montre ainsi les
scores obtenus par le classifieur *SGD* sur chacune des classes avec les trois
différentes vectorisations testées et dans chaque cas sans échantillonnage. Les
résultats sont très voisins mais le modèle *TF-IDF* (courbe orange) est
légèrement au-dessus pour la plupart des classes.

![F-mesures obtenues par *SGD* sur chaque classe avec trois vectorisations différentes et sans échantillonnage.](figure/classification/F1Score/SGD.png){#fig:F1Scores_SGD width=80%}

Le tableau \ref{tab:res_per_class} et la figure \ref{fig:res_per_class}
présentent les différences de résultats obtenus en termes de F-mesure sur le jeu
de test (sans échantillonnage) pour toutes les classes (triées par le nombre
d'articles dans chacune) pour (1) *SGD+TF-IDF*, (2) *BiLSTM+FastText* et (3)
*BERT* Multilingue. *BERT* obtient une F-mesure supérieure à 70% sur 31 des 38
classes au total que comporte le jeu des domaines regroupés (pour *SGD+TF-IDF*
25 classes, et *BiLSTM+FastText* seulement 19). Les performances du modèle *BERT*
sont inférieures à 50% pour seulement 3 classes (5 avec *SGD+TF-IDF* et 10 avec
*BiLSTM+FastText*). En général, les classes avec le plus d'articles (plus de
1 000) sont très bien reconnues par ces trois modèles. Pour les classes avec le
moins d'articles (moins de 500), il y a une chute significative des
performances. Il y a quelques exceptions comme *Pêche* et *Médailles*. Ces deux
classes ont très peu d'articles dans le jeu d'entraînement (168 et 94
respectivement) mais sont étonnamment bien classées (à 85% et 86% par *BERT*).

::: {}

-------------------------- ------- ------ ------ ------ -------------------------- ----- ------- ------- -------
Domaine regroupé                \#  \(1\)  \(2\)  \(3\) Domaine regroupé             \#  \(1\)   \(2\)   \(3\)
(classe)                                                (classe)                                              
-------------------------- ------- ------ ------ ------ -------------------------- ----- ------- ------- -------
Géographie                   2 621  0.96   0.98   0.99  Chasse                        116  0.87    0.87    0.92

Droit - Jurisp.              1 284  0.88   0.90   0.93  Arts et mét.                  112  0.15    0.27    0.36

Métiers                      1 051  0.79   0.76   0.81  Blason                        108  0.87    0.86    0.89

Histoire naturelle             963  0.90   0.87   0.93  Maréchage                     105  0.83    0.86    0.90

Histoire                       616  0.64   0.64   0.75  Chimie                        104  0.70    0.58    0.77

Médecine                       455  0.83   0.80   0.86  Philosophie                    94  0.75    0.49    0.72

Grammaire                      452  0.58   0.54   0.71  Beaux-arts                     86  0.70    0.62    0.82

Marine                         415  0.83   0.86   0.88  Pharmacie                      65  0.53    0.38    0.63

Commerce                       376  0.71   0.69   0.74  Monnaie                        63  0.63    0.50    0.72

Religion                       328  0.78   0.77   0.84  Jeu                            56  0.84    0.74    0.85

Architecture                   278  0.79   0.74   0.80  Pêche                          42  0.85    0.84    0.85

Antiquité                      272  0.66   0.68   0.74  Mesure                         37  0.35    0.10    0.56

Physique                       265  0.75   0.76   0.82  Économie dom.                  27  0.41    0.48    0.44

Militaire                      258  0.83   0.82   0.88  Caractères                     23  0.61    0.08    0.46

Agriculture                    233  0.68   0.58   0.71  Médailles                      23  0.77    0.70    0.86

Anatomie                       215  0.89   0.84   0.90  Politique                      23  0.15    0.22    0.53

Belles-lettres                 206  0.58   0.41   0.70  Minéralogie                    22  0.38    0.39    0.70

Mathématiques                  140  0.82   0.85   0.89  Superstition                   22  0.72    0.48    0.71

Musique                        137  0.87   0.83   0.88  Spectacle                       9  0.33    0.46    0.61

-------------------------- ------- ------ ------ ------ -------------------------- ----- ------- ------- -------

: F-mesures par classe obtenues sur le jeu de test par les combinaisons *SGD +
TF-IDF* (1), *BiLSTM + FastText* (2) et *BERT* Multilingue (3).
\label{tab:res_per_class}

:::

![F-mesures par classe obtenus sur le jeu de test par les combinaisons *SGD + TF-IDF*, *BiLSTM + FastText* and *BERT* Multilingue.](figure/classification/F1Score/SGD_BiLSTM_BERT.png){#fig:res_per_class width=80%}

Au-delà de l'importance du nombre d'articles par classes, ces résultats
soulignent la difficulté à distinguer entre certaines classes pour des raisons
lexicales ou sémantiques comme cela était le cas pour *Arts et métiers* avec
*Métiers*. Ce domaine, parmi les mieux représentés, semble attirer à lui des
articles de nombreuses classes moins fréquentes mais proches sémantiquement.
Cette hypothèse semble confirmée par la matrice de confusion de la figure
\ref{fig:confusion_matrix}.

![Matrice de confusion matrix pour la combinaison *SGD+TF-IDF* sur le jeu de test.](figure/classification/SGD+TF-IDF_domainGroup/confusionMatrix.png){#fig:confusion_matrix width=63%}

Sur cette figure, une colonne pâle est en effet visible vers la droite. Cette
colonne représente des articles de nombreux domaines pour lesquels le modèle
*SGD+TF-IDF* a prédit à tort la classe *Métiers*. Les deux points plus colorés
de cette colonne hors de la diagonale correspondent aux classes *Arts et
métiers* et *Économie domestique*. Ils sont même les plus colorés des lignes
correspondant à ces classes, ce qui signifie que la majorité des articles de ces
deux classes ont été confondus avec des articles de *Métiers* par le modèle. De
la même manière, *Mesure*, *Minéralogie*, *Pharmacie* et *Politique* ont été
confondues avec *Commerce*, *Histoire naturelle*, *Médecine - Chirurgie* et
*Droit - Jurisprudence* respectivement. Les ressemblances sémantiques entre ces
classes illustrent les difficultés que rencontre un modèle pour choisir la
classe qui convient le mieux à un article donné. Les résultats confirment que là
où il y une forte proximité sémantique le modèle tend à choisir la classe la
mieux représentée dans le jeu de donnée, privilégiant ainsi les domaines
regroupés qui contiennent le plus d'articles.

