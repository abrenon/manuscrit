(use-modules ((geode packages toolbox) #:select (geopyck ghc-geode))
             ((gnu packages commencement) #:select (gcc-toolchain))
             ((gnu packages base) #:select (coreutils findutils glibc-locales gnu-make grep sed))
             ((gnu packages fonts) #:select (font-libertinus font-sarasa-gothic))
             ((gnu packages graphviz) #:select (graphviz))
             ((gnu packages haskell) #:select (ghc))
             ((gnu packages haskell-xyz) #:select (ghc-attoparsec ghc-pandoc pandoc))
             ((gnu packages pdf) #:select (poppler))
             ((gnu packages python) #:select (python))
             ((gnu packages python-science) #:select (python-pandas))
             ((gnu packages python-xyz) #:select (python-matplotlib python-seaborn))
             ((gnu packages texlive) #:select (texlive))
             ((gnu packages wireservice) #:select (csvkit)))

(let ((pandoc-fignos (load "dependencies/pandoc-fignos.scm"))
      (python-pysankey (load "dependencies/python-pysankey.scm")))
  (concatenate-manifests
    (list (packages->manifest
            (list coreutils
                  csvkit
                  findutils
                  font-libertinus
                  font-sarasa-gothic
                  gcc-toolchain
                  geopyck
                  ghc
                  ghc-attoparsec
                  ghc-geode
                  ghc-pandoc
                  (load "locale/fr_FR.UTF-8.scm")
                  gnu-make
                  graphviz
                  grep
                  pandoc
                  pandoc-fignos
                  poppler
                  python
                  python-matplotlib
                  python-pandas
                  python-pysankey
                  python-seaborn
                  sed
                  texlive))
          (manifest
            (list (package->manifest-entry gcc-toolchain "static"))))))
