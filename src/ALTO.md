---
header-includes:
	- \pagestyle{empty}
	- \usepackage{graphicx}
	- \usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	- \geometry{paperwidth=25cm, paperheight=9.1cm}
---

```xml
<Layout>
  <Page ID="P44" PHYSICAL_IMG_NR="44" HEIGHT="4884" WIDTH="3353" ACCURACY="0.7655755"
        QUALITY_DETAIL="1228 words, 6373 chars, 4988 with recognition greater than 50% (78,27%)">
    <TopMargin ID="P44_TM00001" HPOS="0" VPOS="0" WIDTH="3353" HEIGHT="509"/>
    <LeftMargin ID="P44_LM00001" HPOS="0" VPOS="509" WIDTH="433" HEIGHT="3555"/>
    <RightMargin ID="P44_RM00001" HPOS="2661" VPOS="509" WIDTH="692" HEIGHT="3555"/>
    <BottomMargin ID="P44_BM00001" HPOS="0" VPOS="4064" WIDTH="3353" HEIGHT="820"/>
    <PrintSpace ID="P44_PS00001" HPOS="433" VPOS="509" WIDTH="2228" HEIGHT="3555">
      <TextBlock ID="P44_TB00001" HPOS="1435" VPOS="513" WIDTH="201" HEIGHT="50" IDNEXT="P44_TB00002">
        <TextLine ID="P44_TL00001" HPOS="1443" VPOS="521" WIDTH="184" HEIGHT="34">
          <String ID="P44_S00001" HPOS="1443" VPOS="540" WIDTH="44" HEIGHT="8" STYLEREFS="StyleId-0" CONTENT="—" WC="0.1" CC="9"/>
          <SP ID="P44_SP00001" WIDTH="0" HPOS="0" VPOS="0"/>
          <String ID="P44_S00002" HPOS="1514" VPOS="521" WIDTH="39" HEIGHT="34" STYLEREFS="StyleId-1" CONTENT="11" WC="0.255" CC="68"/>
          […]
        </TextLine>
      </TextBlock>
    </PrintSpace>
  </Page>
</Layout>
```
