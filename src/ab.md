---
header-includes:
	- \pagestyle{empty}
	- \usepackage{graphicx}
	- \usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	- \geometry{paperwidth=1.5cm, paperheight=1.3cm, margin=0cm}
---

```xml
<a>
    <b/>
</a>
```
