---
header-includes:
	- \pagestyle{empty}
	- \usepackage{graphicx}
	- \usepackage[left=0cm,top=0cm,right=0cm,nohead,nofoot]{geometry}
	- \geometry{paperwidth=1.7cm, paperheight=.5cm, lmargin=-1.5cm}
---

```xml
<author></author>
```
