#!/usr/bin/env python3
import matplotlib.pyplot as plot
import os
import pandas
import seaborn
import sys

biographies = pandas.DataFrame([
    {'name': "FERTÉ-MILON", 'proportion': 4.5/7.5, 'motivation': 0.9, 'size': 87,
     'dx': 0.003, 'dy': 0.01},
    {'name': "HAIE", 'proportion': 84/115, 'motivation': 0, 'size': 1127,
     'dx': 0.005, 'dy': -0.02},
    {'name': "LEIPSIC", 'proportion': 64/82, 'motivation': 0.06, 'size': 825,
     'dx': -0.005, 'dy': 0.02},
    {'name': "LODEVE", 'proportion': 23/39, 'motivation': 0.35, 'size': 447,
     'dx': 0.003, 'dy': -0.01},
    {'name': "NICE", 'proportion': 39/53, 'motivation': 0.6, 'size': 557,
     'dx': 0.003, 'dy': -0.01},
    {'name': "ROUEN", 'proportion': 613/754, 'motivation': 0, 'size': 7171,
     'dx': 0.005, 'dy': -0.01},
    {'name': "VOORHOUT", 'proportion': 485/487, 'motivation': 1, 'size': 4633,
     'dx': -0.064, 'dy': 0},
    {'name': "WOLSTROPE", 'proportion': 755/756, 'motivation': 1, 'size': 7277,
     'dx': -0.045, 'dy': -0.05},
    ])

def draw_biography_criteria(outputPath):
    os.makedirs(os.path.dirname(outputPath), exist_ok=True)
    plot.figure(figsize=(8,5))
    ax = seaborn.scatterplot(data=biographies, x='proportion', y='motivation',
                        size='size', color='mediumpurple')
    ax.legend(title="Taille des articles")
    for _,b in biographies.iterrows():
        plot.text(b['proportion']+b['dx'],
                  b['motivation'] + b['dy'],
                  b['name'])
    ax.set_xlabel('Proportion de contenu biographique')
    ax.set_xticks(ticks=[0.6, 1], labels=['moyenne', 'élevée'])
    ax.set_ylabel('Motivation de l\'article')
    ax.set_yticks(ticks=[0, 1], labels=['intrinsèque', 'biographique'])
    plot.savefig(outputPath, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    draw_biography_criteria(sys.argv[1])
