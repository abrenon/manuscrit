#!/usr/bin/env python3
from math import cos, pi
import matplotlib.pyplot as plot
import os
import pandas
from random import randrange
import seaborn
import sys

def getCos(legend, points):
    return pandas.DataFrame([{'x': x, 'y': cos(x), 'Légende': legend}
                             for x in points])

def drawSampler(outputPath):
    os.makedirs(os.path.dirname(outputPath), exist_ok=True)
    plot.figure(figsize=(8,4))
    curve = getCos('Vrai signal', [k*pi/600 for k in range(-200, 1500)])
    measure = getCos('Mesures', [randrange(-20, 150)/20 for _ in range(25)])
    seaborn.lineplot(data=curve, x='x', y='y', hue='Légende', palette=['mediumpurple'])
    seaborn.scatterplot(data=measure, x='x', y='y', hue='Légende', palette=['deeppink'])
    plot.savefig(outputPath, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    drawSampler(sys.argv[1])
