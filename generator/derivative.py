#!/usr/bin/env python3
from math import exp
import matplotlib.pyplot as plot
import pandas
import os
import seaborn
import sys

def tangent(x):
    return exp(0.75)*(x+0.25)

def showDelta(ax):
    ax.annotate("",
                xytext=(1.5, exp(1.5)),
                xy=(1.5, tangent(1.5)),
                arrowprops={'arrowstyle': "<->",
                            'color': 'silver',
                            'linestyle': '--'})

def drawDerivative(outputPath):
    os.makedirs(os.path.dirname(outputPath), exist_ok=True)
    plot.figure(figsize=(8,4))
    xs = [i/50 for i in range(0, 100)]
    ax = seaborn.lineplot(x=xs, y=[exp(x) for x in xs], color='mediumpurple')
    seaborn.lineplot(x=xs, y=[tangent(x) for x in xs], color='deeppink')
    showDelta(ax)
    plot.savefig(outputPath, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    drawDerivative(sys.argv[1])

