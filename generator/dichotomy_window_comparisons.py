#!/usr/bin/env python3
from math import ceil, log
import matplotlib.pyplot as plot
import pandas
import os
import seaborn
import sys

def comparisons(n):
    return n*ceil(log(5873/n)/log(2))

def drawDichotomyWindowComparisons(outputPath):
    os.makedirs(os.path.dirname(outputPath), exist_ok=True)
    plot.figure(figsize=(8,4))
    xs = [2**(i/100) for i in range(0, 670)]
    ax = seaborn.lineplot(x=xs, y=[comparisons(x) for x in xs], color='mediumpurple')
    ax.set_xscale('log')
    plot.savefig(outputPath, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    drawDichotomyWindowComparisons(sys.argv[1])

