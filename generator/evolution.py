#!/usr/bin/env python3
import pandas
from pySankey.sankey import sankey
import matplotlib.pyplot as plot
import os
import sys

def nest(path):
    prefix = []
    for i in range(len(path)):
        prefix.append("" if i == 0 else "  "*(i-1) + "└ ")
    return list(map(lambda p: p[0] + p[1], zip(prefix, path)))

def tree(path):
    lines = nest(path)
    maxLength = max(map(len, lines))
    return '\n'.join([l + "   "*((maxLength - len(l))//2) for l in lines])

EDdAClasses = [tree(p)
               for p in [['Histoire', 'Naturelle, Sacrée, Civile'],
                         ['Philosophie', 'Sciences de la nature', 'Mathématiques'],
                         ['Philosophie', 'Sciences de la nature', 'Physique particulière']]]
LGEClasses = ['Histoire & Géographie',
              'Mathématique & Astronomie',
              'Physique & Chimie',
              'Sciences naturelles']

def colorise(l, c):
    return dict(zip(l, c))

colors = dict(
        **colorise(EDdAClasses, ['skyblue', 'mediumpurple', 'orchid']),
        **colorise(LGEClasses, ['mediumturquoise', 'plum', 'deeppink', 'slateblue']))

EDdADomains = [EDdAClasses[x] for x in [0, 0, 1, 1, 1, 2]]
LGEDomains = [LGEClasses[x] for x in [0, 3, 0, 1, 2, 2]]

def drawEvolution(outputPath):
    os.makedirs(os.path.dirname(outputPath), exist_ok=True)
    weights = [2, 1, 1, 2, 1, 1]
    sankey(EDdADomains, LGEDomains, leftWeight=weights, colorDict=colors)
    plot.text(0.1, 1, "Histoire")
    plot.text(0.1, 2.4, "Histoire naturelle")
    plot.text(0.1, 3.6, "Géographie")
    plot.text(0.1, 5.1, "Arithmétique")
    plot.text(0.1, 6.6, "Accoustique")
    plot.text(0.1, 7.7, "Chimie")
    plot.savefig(outputPath, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    drawEvolution(sys.argv[1])
