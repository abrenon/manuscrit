## Tracer les contours de la géographie {#sec:geo_contours}

Cette première section articule deux approches complémentaires. Une première
étude purement quantitative établit le périmètre accordé à la *Géographie* et
aux autres domaines de connaissance dans l'*EDdA* et dans *LGE*. Dans un second
temps, une étude qualitative vient compléter la précédente en regardant
l'évolution du traitement qui est fait d'un ensemble fixe de thématiques par
rapport à la notion de domaine de connaissance.

### La place de la géographie {#sec:geo_metrics}

La revue quantitative qui débute s'intéresse à plusieurs critères, du nombre
d'articles consacrés à chaque domaine aux densités d'entités nommées qu'on peut
y rencontrer.

#### Tailles {#sec:geo_size_metrics}

À l'issue des sections \ref{sec:edda_superdomains_classifier} et
\ref{sec:classifying_lge}, la répartition du nombre d'articles par domaine
était connue. Mais un parcours rapide des pages de l'*EDdA* comme de *LGE*
montre une grande disparité de taille d'une entrée à une autre. Le phénomène a
déjà été décrit par @laramee_production_2017[p.168] pour la géographie dans
l'*EDdA* où il peut au moins s'expliquer par des différences de conceptions très
marquées entre les auteurs quant aux buts de la Géographie, particulièrement
entre Diderot et le chevalier de Jaucourt. Mais il s'observe également pour des
articles d'autres domaines, ainsi que dans toute *LGE*. Il est dès lors naturel
de s'intéresser aux éventuels liens entre taille des articles et domaines de
connaissance.

À plusieurs échelles, plusieurs métriques peuvent être considérées. La mesure
qui se présente le plus immédiatement à l'esprit pour mesurer la taille d'un
ensemble de textes est sans doute le nombre d'unités lexicales qu'il contient.
Sous une apparente simplicité c'est un problème intrinsèquement «délicat» (voir
la section \ref{sec:EdlA_tal_pos} p.\pageref{sec:EdlA_tal_pos}). En diachronie,
la question se complique encore car la segmentation «naturelle» pour les
locuteurs change suivant les époques des parties du corpus. Si le français
classique de l'*EDdA* diffère heureusement assez peu de celui déjà quasi
contemporain de *LGE*, on y trouve encore tout de même des formes similaires à
celles rapportées par @diwersy_ressources_2017 [p.29]. Ainsi, l'adverbe «très»
demeure préfixé à de nombreux adjectifs, bien que séparés d'un tiret[^tiret]
dans des formes comme «très-subtil» à l'article MALEBRANCHISME (L'Encyclopédie,
T9, p.942) ou «très-philosophique» dans l'article DICTIONNAIRE (L'Encyclopédie,
T4, p.958) cité à la section \ref{dalembert_dictionnaire}
(p.\pageref{dalembert_dictionnaire}). À l'inverse certaines formes ne sont pas
encore agglutinées telles que «par ce que» dans l'article ATTAQUES *d'une place*
(L'Encyclopédie, T1, p.829).

[^tiret]: en moyen français on trouve des formes simplement agglutinées comme
    «trèsardent» rapportée par @diwersy_ressources_2017 [p.29]

Les «mots» sont donc une abstraction utile mais inatteignable en pratique: la
linguistique de corpus requiert de faire des choix de segmentation toujours
teintés d'une part d'arbitraire pour produire une séquence de tokens que les
outils numériques sont à même de manipuler. Ceux opérés dans le cadre de ces
travaux suivent simplement les décisions de l'annotateur Stanza qui intègre la
phase de tokenisation (voir la section \ref{sec:corpus_application_formats_text}
p.\pageref{sec:corpus_application_formats_text}). Entraîné pour du français
contemporain, il laisse l'amalgame «très-subtil» en un seul token (qu'il annote
correctement comme un adjectif alors que la forme «très-opiniâtre» qui apparaît
peu après dans le texte est étiquetée en tant que nom commun), et ne reconnaît
pas «parce que», considérant la séquence «par ce que» comme trois tokens
distincts. Le seul choix restant à l'issue des traitements concerne les
amalgames, représentés à l'aide de sous-tokens disponibles dans le format
CoNLL-U. À la suite de [@vigier_autour_2017, p.101], un seul token a été produit
pour les représenter dans des outils permettant des recherches en surface comme
TXM. Les [@=POS] et lemmes affectés à ces tokens sont obtenus en concaténant les
[@=POS] et les lemmes de leurs sous-tokens, séparés par un caractère '+'. C'est
à ces conventions que renvoient les nombres de «mots» considérés dans cette
partie.

Mais la taille des textes peut également être considérée à l'échelle plus fine
des signes, en comptant précisément la place qu'ils occupent et là aussi, cette
intention peut être réalisée au moins de deux manières. L'emploi d'outils
informatiques amène à utiliser la place occupée sur le disque, d'accès immédiat
puisque disponible parmi les métadonnées exposées par le système de fichier au
même titre que son nom ou sa date d'accès, sans qu'il soit même nécessaire d'en
lire le contenu. Du point de vue de la complexité, cette information est donc
accessible en temps constant par rapport à la taille du fichier ($O(1)$). Le
nombre d'octets occupés par un fichier contenant le texte d'un article dépend de
certaines conventions discutées à la section \ref{sec:text_format} notamment
pour l'encodage des caractères ou la représentation des fins de ligne. Si les
fichiers créés pour *LGE* les réalisent par des `"\n"` et sont distribués ainsi,
il n'est pas exclu que certaines plateformes ou éditeurs de texte les remplacent
par des `"\r\n"` en les enregistrant, ce qui changerait leur contenu et donc
leur taille en octets. De plus, si au lieu d'utiliser cette version ils sont
recréés à partir des fichiers ALTO avec `soprano`, les choix de la plateforme
sur laquelle le traitement a lieu pourront de la même façon avoir une influence
sur la taille des fichiers produits. Enfin pour l'*EDdA*, puisqu'il n'est de
toute façon pas possible de redistribuer les fichiers, il n'est pas possible de
garantir les choix d'encodage de l'éventuelle version qui pourrait être
récupérée par quelqu'un tentant de reproduire ces travaux. Le nombre d'octets,
s'il est très simple d'accès, ne constitue donc pas un invariant absolu des
textes.

D'un point de vue plus typographique, la place occupée sur les pages par un
article correspond davantage au nombre de caractères que contient son texte. Ce
dernier est en revanche plus long à calculer puisqu'il faut parcourir tout le
contenu du fichier une fois, la complexité est donc ici linéaire ($O(n)$ où $n$
représente la longueur du fichier). Ce calcul peut toutefois être fait une seule
fois pour chaque article lors de son indexation et ce choix ne pénalise donc pas
les nombreuses comparaisons faites ultérieurement. C'est donc ce nombre de
caractères qui est utilisé à l'échelle des signes, mais il est affecté par les
mêmes paramètres que le décompte des octets pour les mêmes raisons car il est
impossible de se défaire totalement des détails d'implémentation en linguistique
outillée. En outre ce décompte inclut la présence des caractères de fin de
ligne. En effet, il apparaîtrait difficilement justifiable de compter les
espaces, qui prennent de la place entre deux mots sur la même ligne mais pas ces
autres séparateurs alors que deux mots séparés par la fin d'une ligne prennent
visuellement plus de place sur une page qu'un seul mot dont la longueur serait
la somme des longueurs de ces deux mots. Il est également à noter que tous les
fichiers se terminent par une fin de ligne, ce qui ajoute un caractère
supplémentaire au décompte.

#### Nombre de mots {#sec:geography_edge_words_count}

La chose la plus évidente lorsque l'on compare la figure
\ref{fig:edda_words_by_domain_repartition} représentant le nombre de mots par
domaine dans l'*EDdA* avec celui représentant le nombre d'articles pour la même
partition (figure \ref{fig:edda_count_by_domain_repartition} p.
\pageref{fig:edda_count_by_domain_repartition}) est la faible importance
relative des articles de géographie en terme de taille. En nombre d'articles, la
*Géographie* est très nettement le domaine le plus représenté avec plus de 20%
des articles de l'*EDdA* qui lui sont consacrés, soit presque deux fois plus
d'articles que le deuxième domaine le plus représenté, *Droit et Jurisprudence*.
Malgré cette très forte présence, ses articles ne représentent plus que 9.3% en
nombre de mots, ce qui n'en fait que le 4^ème^ domaine le plus volumineux, avec
de surcroît un nombre de mots assez voisin de celui des 5^ème^ et 6^ème^
domaines selon le même critère (respectivement *Physique* et *Droit et
Jurisprudence*). Il est même remarquable que la *Géographie* soit couverte en
moins de mots que la *Philosophie*, à laquelle ne sont consacrés que 5.47% des
articles, soit près de 4 fois moins qu'à la *Géographie*. Les articles de
géographie apparaissent donc comme particulièrement brefs par rapport à ceux des
autres domaines de connaissance.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.30\textwidth}
        \centering
        \small\input{table/GEODE/EDdA/words_by_domain.tex}
        \vspace{.8cm}
        \caption{Décompte absolu}
        \label{table:edda_words_by_domain_repartition_numbers}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.69\textwidth}
        \centering
        \includegraphics{figure/repartition/corpus/GEODE/EDdA/words_by_domain.png}
        \caption{Proportions relatives}
        \label{fig:edda_words_by_domain_repartition_pie}
    \end{subfigure}
    \caption{Nombre de mots par domaine dans l'\textit{EDdA}}
    \label{fig:edda_words_by_domain_repartition}
\end{figure}

Cette caractéristique est encore plus visible sur la figure
\ref{fig:edda_words_by_domain_distribution} qui représente la distribution des
nombres de mots par article dans chacun des domaines pour l'*EDdA*. Le centre de
masse de la *Géographie* y est particulièrement décalé vers les petites valeurs
de nombre de mots et, alors que l'échelle logarithmique rend la plupart des
distributions assez plates, les faisant ressembler à des crayons avec un long
corps et une mine plus ou moins affûtée, la distribution de la *Géographie*
présente une forme assez particulière, directement décroissante (une «mine»
gigantesque sans «corps»). La pente de décroissance est plus ou moins visible
selon les domaines mais la plupart présentent au moins cet effet de rupture de
pente (la «mine» dans la métaphore précédente), par exemple les domaines
*Histoire* et *Militaire*. Seul le domaine *Métier* semble comme la *Géographie*
décroître constamment, mais sa pente est tout de même moins forte que celle de
la *Géographie* et il possède bien plus d'articles avec un nombre élevé de mot
(la différence devient flagrante au-dessus de quelques centaines de mots). La
figure \ref{fig:edda_words_by_domain_distribution} montre la raréfaction des
articles de *Géographie* en raison logarithmique de leur taille (la mine présente
des bords bien droits caractéristiques d'une relation linéaire). Il y a environ
10 fois moins d'articles de *Géographie* de longueur 100 que de longueur 10.

![Distribution des nombres de mots par article au sein des différents domaines de l'*EDdA*](figure/distribution/GEODE/EDdA/words_by_domain.png){#fig:edda_words_by_domain_distribution}

Il est naturel après avoir fait cette remarque de se demander si cette signature
particulière persiste 130 ans plus tard. La taille de la *Géographie*, dans
*LGE* aussi, est bien moins importante en nombre de mots qu'en nombre
d'articles. Elle représente en effet une part encore plus importante du nombre
d'articles (37.8% soit plus d'un tiers alors qu'elle ne représentait «que»
20.74% des articles de l'*EDdA*), mais cette fois encore cela ne suffit pas à en
faire le domaine le plus volumineux: malgré cette surreprésentation accrue la
*Géographie* n'arrive qu'à la deuxième place des disciplines occupant le plus de
mots en comprenant «seulement» 22.87% du texte. Là encore, les articles de
*Géographie* apparaissent donc relativement brefs par rapport à ceux des autres
disciplines.

Il faut toutefois remarquer que les proportions d'articles classés en *Histoire*
et en *Géographie* semblent en contradiction avec les intentions des auteurs de
*LGE* dans l'Avant-Propos (La Grande Encyclopédie, T1, p.XI) mentionnées à la
section \ref{lge_preface_domains} (p.\pageref{fig:lge_editors_domains})
puisqu'il y est question de 15% du volume consacré à *Histoire et Géographie*,
alors que la seule *Géographie* prédite par le modèle en totalise déjà une fois
et demie plus. S'il faut bien sûr garder en tête que l'ensemble de 17 domaines
utilisés dans cette étude ne correspond pas au découpage en 15 «sciences» de
l'Avant-Propos, il reste vrai que le modèle a néanmoins prédit qu'un article sur
deux appartenait à la *Géographie* ou à l'*Histoire*, et devrait donc être
compté dans *Histoire et Géographie*, ce qui ferait un volume bien trop élevé.
Parmi les raisons qui pourraient expliquer cet écart se trouve l'inadéquation
potentielle d'un modèle entraîné sur l'*EDdA* pour traiter des textes de *LGE*.
Un modèle d'apprentissage automatique intègre un certain nombre de biais
statistiques du corpus sur lequel il s'entraîne et a tendance à les reproduire
sur les données sur lesquelles on l'applique. Toutefois, le déséquilibre prédit
en faveur de la *Géographie* est encore plus important que celui constaté dans
l'*EDdA*, et de plus l'*Histoire* était moins présente que la *Géographie* alors
qu'elle l'est encore davantage dans les prédictions du modèle sur *LGE*. Si les
erreurs du modèle pourraient sans aucun doute gonfler un peu les chiffres, elles
paraissent difficilement pouvoir changer 15% en 50%. Une autre explication
vraisemblable pourrait résider dans la difficulté à décider à l'avance avec
assez de précision du contenu d'une œuvre aussi vaste qu'une encyclopédie,
impliquant autant de contributeurs sur une période aussi longue. D'ailleurs, le
même Avant-Propos annonce 25 tomes de 1200 pages chacun, quand on sait qu'il y
en a eu finalement 31 au total, dont le dernier dépasse allègrement les 1300
pages. Mais là encore, même si l'*Histoire et Géographie* était effectivement
prévue pour être la science occupant le plus de place dans *LGE*, on a peine à
croire qu'elle ait pu plus que tripler de volume sous les plumes de ses
contributeurs. Il n'est pas possible de conclure avec les données disponibles
quant aux raisons de cette différence, mais c'est un élément à garder en tête
qui sera utile tout au long des réflexions de ce chapitre.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.69\textwidth}
        \centering
        \includegraphics{figure/repartition/corpus/GEODE/LGE/words_by_domain.png}
        \caption{Proportions relatives}
        \label{fig:lge_words_by_domain_repartition_pie}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.30\textwidth}
        \centering
        \small\input{table/GEODE/LGE/words_by_domain.tex}
        \vspace{.8cm}
        \caption{Décompte absolu}
        \label{table:lge_words_by_domain_repartition_numbers}
    \end{subfigure}
    \caption{Nombre de mots par domaine dans \textit{LGE}}
    \label{fig:lge_words_by_domain_repartition}
\end{figure}

Quoi qu'il en soit, en s'intéressant aux distributions d'articles en fonction de
leur nombre de mots, la figure \ref{fig:lge_words_by_domain_distribution} montre
cette fois un profil assez différent et beaucoup moins marqué où davantage de
disciplines voient leurs articles diminuer en nombre avec l'augmentation de leur
taille mais avec des pentes bien moins prononcées. Ainsi, si elles semblent
s'accentuer légèrement à partir de 1 000 mots, les pentes de décroissance en
fréquence de l'*Histoire* et de l'*Histoire naturelle* sont assez droites et
ressemblent globalement à celle de la *Géographie*, dont la pente, si elle
semble là aussi régulière, est bien moins marquée, signe d'un étalement bien
plus progressif. De plus, les nombres de mots minimum par domaine varient bien
plus (ce qui se traduit sur la figure \ref{fig:lge_words_by_domain_distribution}
par le fait que les «crayons» sont bien moins alignés à gauche qu'ils ne
l'étaient pour l'*EDdA* à la figure
\ref{fig:edda_words_by_domain_distribution}). Il y a donc plus d'articles de
géographie longs dans *LGE* qu'il n'y en avait dans l'*EDdA* et surtout, la
répartition en nombre de mots des articles ressemble davantage à celle des
autres disciplines. Cela suggère que le style des articles de *Géographie* perd
de sa spécificité.

![Distribution des nombres de mots par article au sein des différents domaines de *LGE*](figure/distribution/GEODE/LGE/words_by_domain.png){#fig:lge_words_by_domain_distribution}

#### Longueur moyenne des mots

L'intérêt de mesurer aussi les tailles d'articles à l'échelle des signes comme
décrit plus haut est de pouvoir calculer la longueur moyenne des mots de chaque
domaine pour vérifier s'il existe un lien de dépendance entre celle-ci et les
domaines. Il paraît plutôt vraisemblable que le rapport entre nombre de lettres
et nombre de mots, c'est-à-dire en fait la longueur moyenne d'un mot, soit une
constante propre à la langue et que la mesure ne puisse varier que très peu sur
des échantillons assez grands. Le tableau
\ref{table:words_characaters_ratio_by_domain} montre qu'au contraire ce rapport
n'est pas constant et qu'il atteint son minimum pour la *Géographie* dans
l'*EDdA*.

\begin{table}[h!]
    \centering
    \input{table/GEODE/averageWordLengths.tex}
    \caption{Longueur moyenne des mots par domaine pour chacune des deux encyclopédies étudiées}
    \label{table:words_characaters_ratio_by_domain}
\end{table}

Les articles de *Géographie* y sont donc particulièrement courts, non seulement
en nombre de mots mais même, typographiquement, en place occupée sur les pages
de l'*EDdA*. La présence dans ces articles d'abréviations notamment pour les
points cardinaux («N.», «S.») et les coordonnées («Long.», «lat.») contribue
sans doute à abaisser un peu la moyenne dans ce domaine mais ne suffit
probablement pas à expliquer la brièveté particulière des articles de
*Géographie* car des abréviations d'une lettre se rencontrent également dans les
autres domaines. Outre les initiales de noms de personnes qui concernent toutes
les lettres et peuvent apparaître dans de nombreux contextes, la séquence «S.»
peut aussi représenter le mot «Saint» et se rencontre alors très fréquemment
dans toute l'*EDdA*, notamment en *Histoire* et en *Religion* où elle a
respectivement 1 861 et 1 409 occurrences (il y en a 1 877 en *Géographie*).

Dans *LGE*, la longueur moyenne des mots dans les articles augmente globalement
pour atteindre 5.1 soit presque deux écarts-types de plus que la moyenne dans
l'*EDdA*, ce qui montre un mouvement général de complexification du discours
avec l'emploi plus fréquent de mots techniques plus longs quels que soient les
domaines. Cette nouvelle valeur est même supérieure à la longueur moyenne des
mots dans tous les domaines de l'*EDdA* sauf la *Politique*. La *Géographie*
n'échappe pas à cet allongement du vocabulaire. Si elle reste plus d'un
écart-type sous la moyenne de longueur des mots dans *LGE* (4.97 contre 5.1)
elle perd tout de même sa place de domaine aux mots les plus brefs, détrônée de
peu par la *Musique* et les *Belles-lettres*. Mais surtout, la longueur moyenne
de son vocabulaire, 4.97, est supérieure à celle dans l'ensemble de l'*EDdA*,
tout domaine confondu.

\label{lge_abbreviation}Cela est d'autant plus remarquable que les abréviations
se multiplient pourtant dans *LGE*. Parmi les 20 lemmes les plus spécifiques de
la *Géographie* dans cette œuvre figurent cinq abréviations: «hab.» (habitant),
«dép.» (département), «arr.» (arrondissement), «cant.» (canton) et «kil.»
(kilomètre), toutes spécifiques «à saturation» (voir la section
\ref{textometry_specificity} p.\pageref{textometry_specificity}) et de surcroît
exclusives à la *Géographie* (elles sont sous-spécifiques de tous les autres
domaines). Par comparaison, les 40 lemmes les plus spécifiques de la
*Géographie* dans l'*EDdA* ne contenaient que quatre abréviations de mots pleins
(pour «Sud», «ancien(ne)?», «Longitude» et «latitude», les autres abréviations
sont ou bien les désignants du domaine, ou des signatures d'auteurs). Pour que
la longueur moyenne des mots dans les articles de *Géographie* parviennent à
croître autant entre l'*EDdA* et *LGE*, il est donc raisonnable de faire
l'hypothèse de l'apparition dans ce domaine d'un grand nombre de termes
techniques beaucoup plus longs à même de contrebalancer cet effet, ce qui
corrobore également l'hypothèse de sa disciplinarisation.

#### Annotation géo-sémantiques {#sec:geo_named_entities}

Intuitivement, la Géographie en tant que science descriptive de l'espace
terrestre renvoie à la notion de lieu et en particulier de lieu nommé. Le
concept d'Entité Nommée ([@=EN]) paraît donc un angle d'approche tout à fait
approprié pour étudier les articles encyclopédiques du corpus et on peut faire
l'hypothèse que ces entités seront particulièrement présentes dans les articles
de *Géographie*.

Ce concept apparu dans les années 1990 généralise la notion de noms propres à
tout groupe de mots renvoyant à des objets nommés, uniques [@nadeau_survey_2007,
p.3] \(ou supposés tels dans le contexte). Central dans les tâches d'extraction
d'information, il se définit en creux comme la matière précieuse à extraire qui
justifie de s'intéresser à ces tâches. Il s'agit de les trouver dans des textes
tels que des articles de presse ou des rapports d'activité; de les classer
suivant une typologie préétablie en relation avec l'usage que l'on souhaite
faire du document; de les rattacher sans ambiguïté à des entrées présentes dans
des bases de connaissance et enfin de découvrir les relations qu'elles
entretiennent [@ehrmann_named_2016, p.3350]. Si elles semblent inclure dès le
début les expressions numériques comme les dates ou les quantités pourvu d'une
unité [@sekine_extended_2002], la définition et l'organisation de classes assez
complètes fait l'objet de travaux conséquents qui aboutissent à la publication
de jeux d'étiquettes complexes [@sekine_definition_2004] jusqu'à des schémas
d'annotation [@rosset_entites_2011]. L'extraction des relations spatiales entre
entités ou *Spatial Role Labeling*, une tâche introduite par
@kordjamshidi_spatial_2011 pour SemEval2012 est une opération difficile à cause
de l'ambiguïté des prépositions et du vocabulaire dans de nombreux
langages — illustrée par exemple pour l'anglais par
@kordjamshidi_spatial_2010[p.3].

Mais les discours géographiques, au-delà de leur densité en [@=EN], présentent
aussi un intérêt dans la manière dont ils les mettent en relation ainsi que dans
le vocabulaire qu'ils utilisent pour les catégoriser. On peut ainsi penser, dans
le contexte précis de ce corpus d'étude à la différence significative entre des
termes comme «paroisse» et «commune». C'est pourquoi une annotation
géo-sémantique riche a été retenue plutôt qu'une annotation en entités nommées
classiques pour examiner l’hypothèse formulée au début de cette sous-section. Le
corpus a été annoté avec un modèle spaCy spancat personnalisé[^modèle]
[@moncla_spacy_2024]. Ce modèle a pour tâche de catégoriser les entités ou spans
(ensemble de tokens) avec imbrications possibles selon une version simplifiée du
schéma d’annotation décrit dans @moncla_multilayer_2015. Le schéma retenu pour
l’annotation du jeu de données GeoEDdA [@moncla_geoedda_2024] qui a servi pour
l’entraînement et l’évaluation de ce modèle d'annotation comporte 12 étiquettes:

[^modèle]:
    [https://huggingface.co/GEODE/fr_spacy_custom_spancat_edda](https://huggingface.co/GEODE/fr_spacy_custom_spancat_edda)

- 3 types de noms propres: lieux (`NP-Spatial`), personnes (`NP-Person`) et autres
  (`NP-Misc`)
- 2 types de noms communs: lieux (`NC-Spatial`) et personnes (`NC-Person`)
- 1 pour les relations spatiales (`Relation`)
- 1 autre pour les coordonnées géographiques (`LatLong`)
- 1 marqueur pour les vedettes d'articles (`Head`)
- 1 pour leurs désignants (`Domain-mark`)

À ces neuf types atomiques s'ajoutent trois types d'Entités Nommées Étendues
([@=ENE]), reprenant le concept défini par @gaio_extended_2017:

- 1 de lieux (`ENE-Spatial`)
- 1 de personnes (`ENE-Person`)
- 1 non classées (`ENE-Misc`)

Ces entités permettent de capturer et de structurer le contexte local des
entités nommées (NP-\*). Elles sont construites à partir des NC-\* et des NP-\*
(ou d’une autre ENE-\*) et peuvent également inclure les Relations. De manière
systématique le type de l'ENE (Spatial, Person, Misc) hérite du type du NC-\*
sur lequel elle est bâtie. Ainsi la figure \ref{fig:ene_edda_strongoli} montre
comment un `NC-Spatial` peut se combiner avec un `NP-Spatial` pour former une
`ENE-Spatial`. En effet, une «ville d'Italie» est bien une ville, donc l'ENE
dans son ensemble désigne une ville et est donc de type spatial. Dans ce cas le
nom propre impliqué est aussi spatial donc il serait possible de croire que
c'est le `NP` qui gouverne le type de l'`ENE` ou qu'un `NP` et un `NC` doivent
s'accorder pour former une `ENE` mais un exemple comme «place de la Libération»
permet de dissiper ces mauvaises intuitions: la «Libération» est un nom propre
renvoyant à un événement (elle serait ici annotée `NP-Misc`), «place» est un
`NC-Spatial` et l'ensemble est bien une `ENE-Spatial` (une fois encore de
manière évidente parce que la «place de la Libération» est en particulier une
place, donc bien un lieu). Ayant choisi de n'annoter que les noms communs de
lieux et de personnes, les `ENE-Misc` sont dans ce schéma d'annotation
dépourvues de `NC` (on pourrait ainsi annoter «le débarquement de Normandie»
`ENE-Misc` car il s'agit d'un événement, «débarquement» ne serait pas annoté
mais «Normandie» constitue évidemment un `NP-Spatial`). Enfin, les `ENE` peuvent
à leur tour se combiner entre elles comme cela est également visible à la figure
\ref{fig:ene_edda_strongoli} (où elles atteignent quatre niveaux de profondeur).

![Annotations de l'article Strongoli (L'Encyclopédie, T15, p.547)](figure/annotation/strongoli.png){#fig:ene_edda_strongoli width=75%}

La figure \ref{fig:ene_edda} représente les densités des différents types
d'entités par domaine dans l'*EDdA*, c'est-à-dire pour chaque domaine de
connaissance le nombre d'entités d'un des types décrits ci-dessus divisé par la
taille de ce domaine. Les colonnes ne peuvent pas se sommer directement puisque
les cellules de la matrice représentent des densités dans des domaines qui n'ont
pas les mêmes nombres de mots. L'histogramme en haut de la figure représente
donc leurs sommes pondérées, c'est-à-dire en fait les décomptes d'entités d'un
type donné dans toute l'*EDdA*, divisés par le nombre de mots de cette
encyclopédie.

![Densité d'entités dans l'*EDdA* par domaine et par type](figure/entities/EDdA_density.png){#fig:ene_edda width=90%}

Les sommes horizontales s'effectuent au contraire sans problème, mais il faut
garder à l'esprit qu'elles représentent un nombre d'entités et pas un nombre de
mots. D'abord, il n'y a pas de correspondance un-à-un entre les mots et les
entités, même atomiques, ce qui est visible par exemple à la figure
\ref{fig:ene_edda_strongoli} dans «petite ville»: le syntagme nominal entier a
été étiqueté `NC-Spatial` alors qu'il comporte deux mots. Ensuite, la nature
imbriquée des ENE entraîne que les mêmes mots individuels peuvent participer à
plusieurs entités. Ainsi, la séquence «royaume de Naples» de la même figure
\ref{fig:ene_edda_strongoli} contribue à trois entités: une pour «royaume»,
`NC-Spatial`, une pour «Naples», `NP-Spatial`, et une pour l'ensemble de
l'expression, `ENE-Spatial`. Le fait que l'expression comporte également trois
mots n'a pas de lien numérique avec ce fait, et sa participation à trois autres
`ENE` dans lesquelles elle est imbriquée en profondeur fait que la densité
d'entités rapportée à ce passage serait même supérieure à une entité par mot. Le
compte obtenu pour chaque article rapporté à son nombre de mot, division
nécessaire pour rendre compte fidèlement de la distribution des entités sans
biais dûs à sa taille, ne représente donc pas une proportion de mots qui
seraient des entités mais simplement une densité abstraite pour chacun et qui
n'a pas réellement d'interprétation intuitive directement visible dans les
articles.

Le trait le plus visible de la figure \ref{fig:ene_edda} est évidemment la
prépondérance de la *Géographie* par opposition aux autres domaines dans la
répartition des entités. Les plus présentes sont celles de lieu : `ENE-Spatial`,
`NC-Spatial` et surtout `NP-Spatial` qui atteint environ 5%, ce qui est
supérieur à la densité moyenne de tous les types d'entités confondus dans
l'ensemble de l'*EDdA* ($\Sigma$, valant 4.6%). La ligne *Géographie* dans son
ensemble totalise environ 16% d'entités et se dégage très nettement des autres
domaines, puisque seules les classes *Histoire* et *Religion* arrivent à
dépasser $\Sigma$ de peu, avec une densité plus de deux fois moindre. Les ENE
sont dans leur ensemble assez rares mais la catégorie `ENE-Spatial` atteint tout
de même une densité assez élevée dans la classe *Géographie*, de l'ordre de
celle de `NC-Spatial` ce qui est cohérent avec la remarque faite ci-dessus sur
le fait qu'une `ENE-Spatial` était censée être bâtie autour d'un `NC-Spatial`.
L'inverse n'est pas vrai, ce qui se voit au fait que la colonne `NC-Spatial` est
légèrement plus foncée sur la ligne *Géographie* que celle de `ENE-Spatial`.
Cette différence s'accentue pour tous les autres domaines: là où le rapport
`NC-Spatial` / `ENE-Spatial` (calculé à partir des données numériques de la
matrice, difficile à évaluer à partir des seules couleurs de la figure) atteint
son minimum, 1.1, en *Géographie*, il monte ensuite à 2.5 en *Commerce*,
deuxième domaine pour lequel ce rapport est le plus bas et s'élève même à plus
de 10 en *Agriculture*. Bien qu'encore atténuée, une répartition similaire
concerne les `Relation`s, présentes quasi-exclusivement en *Géographie*. Si des
éléments spatiaux sont donc présents à l'intérieur de syntagmes nominaux dans
les autres domaines, la *Géographie* se caractérise dans l'*EDdA* par leur mise
en relation au sein des composés structurés que constituent les ENE.

Les autres types d'entités particulières, celles de personnes, présentent une
distribution beaucoup plus homogène entre les domaines. Les `NC-Person` sont
ainsi bien plus uniformément réparties que les `NC-Spatial`, et les `NP-Person`
atteignent une densité proche de 2%, son maximum, dans trois domaines. Il est à
noter que ce dernier type d'[@=EN] est bien représenté en *Géographie*, où il
très légèrement plus fréquent qu'en *Histoire* alors que les [@=EN] de type
*NP-Spatial* étaient bien moins fréquents en *Histoire* qu'en *Géographie*. Les
entités de personnes n'apparaissent donc pas comme caractéristiques d'un domaine
en particulier mais s'observent avec des combinaisons de densités variées entre
les `NC-Person`, `NP-Person` et `ENE-Person` en fonction des domaines.

La figure \ref{fig:ene_lge} qui montre les mêmes statistiques mais pour *LGE*
offre un aspect tout à fait différent. La *Géographie* y conserve la place de
domaine le plus dense en entités mais son score est bien plus proche de ceux de
l'*Histoire*, des *Beaux-arts* et des *Belles-lettres* les trois autres domaines
à dépasser la densité moyenne à l'échelle de toute cette seconde encyclopédie.
Cette proximité se retrouve dans les lignes de la matrice qui sont bien plus
uniformes: celle de la *Géographie* ne ressort pas nettement des autres et
présente des similitudes fortes avec celles des trois autres domaines les plus
denses. La seule particularité réellement identifiable reste au niveau des types
utilisés pour les lieux: `ENE-Spatial` et `NC-Spatial`, bien que très atténués
par rapport à l'*EDdA* se dégagent de la ligne *Géographie* et la distinguent
des autres semblables. La *Géographie* conserve la plus haute densité de
`NP-Spatial` mais est considérablement rattrapée par la plupart des domaines.
Très schématiquement, là où la figure \ref{fig:ene_edda} pour l'*EDdA* était
marquée par la ligne *Géographie*, la figure \ref{fig:ene_lge} de *LGE* se
présente plus verticalement et se structure le long de la colonne `NP-Spatial`.
Le résultat sur l'histogramme du haut de la figure est net: cette classe creuse
l'écart et s'élève nettement au-dessus des autres à près de 3% de densité dans
la totalité de *LGE*, soit environ deux fois et demie plus que dans l'*EDdA*. La
densité totale d'entités dans cette encyclopédie s'est beaucoup accrue,
atteignant 7.5% soit plus d'une fois et demie celle dans les pages de l'*EDdA*.

![Densité des ENE dans *LGE* par domaine et par type d'ENE](figure/entities/LGE_density.png){#fig:ene_lge width=90%}

Il y a donc dans ces encyclopédies deux mouvements simultanés et contraires au
niveau des entités entre le XVIII^ème^ et le XIX^ème^ siècle. D'une part les
entités atomiques se font plus fréquentes, surtout les noms propres, et se
répandent dans de nombreux autres domaines que la *Géographie*, y compris les
`NP-Spatial` détectés autant dans des domaines où ils peuvent être attendus (le
*Commerce* mais aussi l'*Histoire naturelle* quand on tient compte de ce que la
discipline doit aux grandes explorations du siècle précédent) que dans d'autres
où ils le seraient moins comme les *Beaux-arts*, la *Musique* ou même la
*Religion*. Concernant les entités de lieu, le mouvement se limite aux seuls
noms propres. Les `NC-Spatial` tout comme les `ENE-Spatial`, bien moins
fréquents, restent davantage caractéristiques de la *Géographie*, qui se
distingue par contraste des autres domaines en conservant l'usage de ces noms
communs qui deviennent un vocabulaire disciplinaire. Malgré cette
spécialisation, le deuxième mouvement à l'œuvre voit la *Géographie* se
normaliser et ressembler davantage aux autres domaines pour les entités
détectées. La décroissance de sa densité en entités est probablement surestimée
au moins en partie à cause des abréviations évoquées précédemment dans cette
section p.\pageref{lge_abbreviation}, très présentes dans les articles de
*Géographie* de *LGE* et assez mal repérées par le modèle. Mais au-delà de ce
seul indicateur, la différence de densités de `NP-Spatial` entre la *Géographie*
et les autres domaines s'estompe également, la rapprochant des profils des
autres disciplines. Les `ENE-Spatial` sont moins concentrées, elles forment des
arbres moins profonds (c'est-à-dire moins d'`ENE-x` imbriquées comme pouvaient
l'être celles de la figure \ref{fig:ene_edda_strongoli} imbriquées sur quatre
niveaux). L'`ENE-Spatial` la plus profonde trouvée dans *LGE* a ainsi une
profondeur de quatre là où la profondeur maximale dans l'*EDdA* s'élevait à
sept. La très grande majorité des ENE trouvées sont très plates et la profondeur
moyenne dans ces encyclopédies est de l'ordre de grandeur de $10^{-2}$ mais
celle hors de la classe *Géographie* dans l'*EDdA* est de $8\times10^{3}$ alors
qu'elle s'élève à $1\times10^{-1}$ en *Géographie*; dans *LGE*, la profondeur
moyenne hors *Géographie* est de $4\times10^{-3}$ contre $9\times10^{-3}$. Les
articles de *Géographie* dans *LGE* sont donc non seulement moins denses en
entités que ceux de l'*EDdA* mais ils font l'objet d'une mutation plus profonde:
les concentrations de noms propres (dont surtout des toponymes) s'effacent pour
laisser se développer un discours plus structuré et plus similaire à celui des
autres disciplines: la Géographie ne sert plus tant à nommer des référents qu'à
donner des informations sur eux.

### Des articles qui changent de domaine {#sec:parallel_corpus}

Les considérations précédentes fournissent des mesures «en gros» et se
focalisent sur les groupes d'articles rassemblés par domaines en essayant de
mesurer leurs variations de taille entre l'*EDdA* et *LGE*. La représentation
sous-jacente correspond à des «essaims» d'articles qui grossiraient et se
réduiraient au fil des ajouts ou des suppressions d'articles dans un domaine
donné ainsi que des variations des volumes de textes consacrés aux différents
articles de ce domaine. Cette sous-section se propose au contraire de suivre une
approche relativement orthogonale en prenant un ensemble fixe d'entrées existant
aux deux époques pour observer comment celles-là changent.

#### Constitution d'un sous-corpus

La première difficulté consiste donc à établir des correspondances entre les
articles des deux encyclopédies, c'est-à-dire à définir une notion d'équivalence
entre deux articles pourtant issus de deux encyclopédies distinctes et dont les
différences seules nous intéressent. Étant donné le caractère qualitatif plus
que quantitatif de cette étude (l'ensemble d'entrées suivies en détail n'a pas
besoin d'être aussi grand que possible puisque de toute façon par construction
il ne représentera pas la totalité des articles), l'algorithme le plus naïf est
utilisé en constituant des paires d'articles de la manière suivante: une paire
est constituée d'un article de l'*EDdA* et d'un article de *LGE* ayant la même
vedette qui doit de plus se trouver être unique dans chacune des encyclopédies
prise séparément.

Les vedettes sont comparées en utilisant l'égalité usuelle des chaînes de
caractères mais sans tenir compte de la casse étant donné que la typographie est
un peu irrégulière dans l'*EDdA* et que les conventions typographiques varient
de toute façon légèrement entre les deux encyclopédies. Cette règle relativement
stricte n'est pourtant pas suffisante pour garantir que les paires sont toutes
bien fondées : au lieu qu'elles renvoient bien au «même» objet (concept, lieu,
etc.) il se pourrait qu'une entrée vienne à disparaître de la première
encyclopédie pour être remplacée dans la suivante par une autre avec la même
vedette mais sans lien sémantique avec l'entrée initiale. Si la vedette commune
à une paire d'entrées est un nom propre, il s'agira de vérifier qu'elles
décrivent bien la même entité; pour les vedettes relevant des autres catégories
morphosyntaxiques, comme le proposent @lehmann_lexicologie_2018[chap.5 §11-13]
l'étymologie complétée de critères sémantiques lorsqu'une évolution forte a
éloigné les termes permettront de distinguer les polysémies, intéressantes à
étudier, des simples homonymies et ce même si le sens utilisé dans une des
encyclopédies est absent dans l'autre. Toute la difficulté de constitution de ce
sous-corpus, nommé «Parallèle» et noté $\mathcal{P}$ dans ce qui suit, réside
dans ce problème de bon appariement — distinguer les *vraies* paires des
*fausses* sources de bruit dans le corpus. Par construction, $\mathcal{P}$
comprend un nombre pair d'articles, autant de l'*EDdA* que de *LGE* puisqu'il
est obtenu en sélectionnant des couples d'articles. Les figures
\ref{fig:adige_edda} et \ref{fig:adige_lge} présentent un exemple de paire issue
du sous-corpus Parallèle. Cette paire est une vraie paire car les articles, bien
que très différents ne serait-ce que par leur longueur, renvoient bien au même
cours d'eau.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/text/EDdA/adige_t1}
        \caption{Article ADIGE dans l'\textit{EDdA}, T1, p.138}
        \label{fig:adige_edda}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/text/LGE/adige_t1}
        \caption{Article ADIGE dans \textit{LGE}, T1, p.572}
        \label{fig:adige_lge}
    \end{subfigure}
    \caption{Les articles sur le fleuve ADIGE, un exemple de paire issue du sous-corpus Parallèle}
    \label{fig:adige_pair}
\end{figure}

L'approche ci-dessus basée sur la vedette plutôt que le contenu des articles est
en un sens trop restrictive et peut être critiquée pour plusieurs raisons.
D'abord, des objets dont le nom aurait changé entre les XVIII^ème^ et XIX^ème^
siècles seront exclus du sous-corpus par la condition choisie alors que le
renommage d'entrées pourrait aussi être un aspect intéressant à étudier. Ce cas
de figure se rencontre par exemple avec la capitale impériale japonaise à l'ère
Tokugawa, la ville dont le nom moderne est Kyōtō en trancription Hepburn. On
trouve la ville dans l'*EDdA* à l'entrée «MÉACO ou MIACO» (L'Encyclopédie, T10,
p.218) — correspondant au japonais みやこ, soit «miyako» en transcription
Hepburn, un ancien nom de la ville signifiant simplement «capitale» — alors que
dans *LGE* publiée après la restauration impériale de 1868 (l'année de début de
publication de l'œuvre, 1885, correspond à l'an 18 de l'ère Meiji) la même ville
est décrite par l'entrée «KIYOTO ou MIAKO» (La Grande Encyclopédie, T21, p.545).
Outre la prise en compte nécessaire des graphies multiples (ce qui en théorie ne
paraît pas revêtir un problème conceptuel particulier mais en pratique peut
s'avérer une tâche infinie nécessitant sans cesse de raffiner la grammaire
employée pour analyser les vedettes au fur et à mesure qu'elle bute sur de
nouveaux cas particuliers), la variation orthographique aurait caché cette paire
pourtant valide avec le parti pris de ne pas comparer le contenu des articles.
Ensuite, une comparaison des contenus des articles permettrait de maintenir le
bruit dans les paires à un niveau acceptable tout en utilisant une comparaison
plus souple sur les vedettes, par exemple une égalité basée sur un seuil pour
une distance usuelle sur les chaînes de caractères comme la distance de
Levenshtein ou le coefficient de Sørensen-Dice, ce qui aurait pu rattraper
l'écart précédent entre «MIAKO» et «MIACO», illustration que l'orthographe
change au cours du temps en particulier pour les transcriptions de noms de lieux
étrangers. Enfin, il peut exister des groupes d'entrées homonymes dans une des
encyclopédies au sein desquelles une entrée pourrait être mise en correspondance
avec une entrée de l'autre encyclopédie, appartenant possiblement elle aussi à
un groupe homonyme si la vedette était fortement polysémique aux deux époques.
L'heuristique choisie les écarte plutôt que d'essayer de trouver comment les
mettre en correspondance.

Ce choix se défend pourtant déjà en ce qu'il s'agit ici de favoriser la
simplicité par rapport à l'exhaustivité. Il apparaît peu pertinent de rechercher
à considérer précisément l'ensemble de tous les objets décrits à la fois dans
l'*EDdA* et *LGE* comme s'il avait une qualité particulière qui ne tiendrait pas
aux hasards éditoriaux des deux entreprises. En pratique, cet ensemble peut être
remplacé par n'importe lequel de ses sous-ensembles pourvu qu'il soit assez
large pour être représentatif. Mais surtout, imposer des restrictions trop
strictes sur le contenu des articles empêcherait d'observer des changements
majeurs dans la manière dont un même objet serait traité entre l'*EDdA* et
*LGE*, par exemple un territoire qui changerait de pays et ne serait donc plus
décrit en rapport aux mêmes entités administratives comme ce fut le cas de
l'Alsace entre le milieu du XVIII^ème^ siècle et la fin du XIX^ème^ siècle, ou
une évolution scientifique et technique qui ferait qu'on ne parle plus du tout
d'un même sujet avec les mêmes termes. L'article SAVONE est particulièrement
intéressant pour cette raison. D'un descriptif des ordres religieux de la ville,
de son commerce moribond et de ses liens avec des autres villes, italiennes
exclusivement, qui est suivi d'une biographie du pape Jules II dans l'*EDdA*
(L'Encyclopédie, T14, p.722), la ville paraît transformée dans *LGE* (La Grande
Encyclopédie, T29, p.624), l'article bien plus bref insiste sur la force de son
commerce notamment de fer et de pétrole, sur son aciérie, et la met en relation
avec Marseille. Une comparaison basée sur le contenu aurait pu juger les deux
articles trop différents.

Si la pertinence de la règle basée sur les vedettes a été justifiée, il n'en
demeure pas moins que les objections levées précédemment démontrent l'intérêt
d'une approche basée sur une mesure de similarités des contenus des articles. Il
s'agit donc de trouver une manière de les combiner pour améliorer la précision
de la règle de sélection initiale. Le problème majeur de l'approche basée sur le
contenu des articles réside dans la part d'arbitraire qui demeure dans le choix
d'un seuil au-delà duquel deux articles sont considérés comme différents et qui,
sauf à trouver une métrique particulièrement bonne et dont l'existence n'est pas
prouvée, excluera nécessairement des vraies paires tout en incluant des fausses.
Pour cette raison, le sous-corpus $\mathcal{P}$ est donc constitué en appairant
d'abord automatiquement les articles avec la règle simple basée sur les vedettes
puis filtré de manière semi-automatique en utilisant une métrique basée sur le
contenu seulement pour guider la validation, purement manuelle, des paires. Ce
processus de sélection se base sur les données, sans à priori: le seuil est fixé
en fonction des vraies et fausses paires trouvées parmi les articles.

L'application de la règle sur les vedettes génère une liste de 11 746 articles,
c'est-à-dire 5 873 paires. Cette liste contient bien sûr des fausses paires,
comme celle pour la vedette ABACO qui est donnée pour un synonyme désuet
d'«arithmétique» dans l'*EDdA* (L'Encyclopédie, T1, p.6) mais désigne un
architecte italien dans *LGE* (La Grande Encyclopédie, T1, p.11) — il y a en
réalité trois entrées homonymes dans l'œuvre mais des problèmes de segmentation
des articles de cette œuvre en ont masqué deux. Le nombre de paires trouvées est
relativement faible au regard des 74 190 articles que comprend au total l'*EDdA*
(moins de 8%) mais reste tout de même un peu trop élevé pour les évaluer
manuellement. La stratégie composite définie au paragraphe précédent prend alors
tout son intérêt en utilisant les similarités de contenus des articles pour les
ordonner et accélérer la sélection. En effet, en appliquant une métrique de
similarité cosinus basée sur le plongement vectoriel des textes avec
*SentenceBERT* [@reimers_sentencebert_2019], on constate une forte dépendance de
la véracité des paires au score qu'elles obtiennent. Ainsi les cinq candidates
les mieux notées selon cette métrique traitent toutes des mêmes objets:
CONTEMPLATION d'un concept théologique et mystique, ACUTANGLE d'un cas
particulier de triangle, SYNE d'un mois éthiopien, TRIHEMITON d'un intervalle en
musique grecque jusqu'à CONGRUE qui n'est qu'un renvoi dans les deux
encyclopédies mais vers le même terme de Jurisprudence. Au contraire parmi les
cinq les moins bien notées se trouvent PRESTON, une ville dans l'*EDdA* contre
un renvoi biographique dans *LGE*, SERY respectivement un rongeur et une commune
ardennaise et MARTIALE qui définit une fleur au XVIII^ème^ et une loi au
XIX^ème^ (malgré leur étymon commun — le dieu romain Mars — il n'y a plus de
lien sémantique entre les deux et la polysémie est devenue homonymie). Les deux
autres sont une paire valide pour CAP-BRETON (mais au contenu peu comparable
puisqu'il s'agit d'un simple renvoi dans l'*EDdA* mais d'une entrée géographique
complète dans *LGE*) et l'autre, FILLES est due à une erreur de segmentation
dans un article sur l'école dans *LGE*.

Ces exemples montrent que cette simple étude pourrait être énormément
approfondie en travaillant sur la comparaison des vedettes (en incluant
notamment une composante phonémique à la métrique utilisée pour valuer
différemment l'écart entre MIACO et MIAKO et celui entre MIACO et MIANO) et en
résolvant les renvois pour effectuer les comparaisons de similarité sur le
contenu des articles complets. Mais dans le cadre de ces travaux où elle n'a
pour but que de mettre en évidence des tendances d'évolution entre disciplines,
il ne reste qu'à fixer le seuil de similarité pour retenir les paires et former
$\mathcal{P}$. Et puisque les deux extrémités du classement ne sont pas
violemment en contradiction avec cette hypothèse, on va supposer que le lien de
dépendance entre qualité et score est relativement monotone (plus le score est
élevé, plus la paire a de chance d'être vraie) et procéder par dichotomie sur la
liste des paires candidates classées par ordre croissant de similarité.

Pour éviter d'accorder trop d'importance à une éventuelle paire trop bien ou
trop mal classée, on procède en gardant une fenêtre pour la comparaison. En
effet non seulement les cinq premières les mieux notées sont valides mais c'est
également le cas des cinq suivantes. Une fenêtre de dix entrées est donc
utilisée pour trouver le point au-delà duquel plus d'une paire sur dix est
fausse, dans l'espoir d'atteindre ainsi une qualité d'au moins 90% de vraies
paires dans $\mathcal{P}$. Puisque la plage de recherche initiale est de 5 873
paires, en considérant des fenêtres de dix éléments le processus s'arrête dans
le pire des cas au bout de $\lceil log_2(5873 / 10)\rceil = 10$ étapes.
L'évaluation d'une fenêtre peut s'arrêter dès que deux fausses paires ont été
trouvées, mais dans le cas le plus défavorable (trouver exclusivement des vraies
paires), il faudrait donc au pire 100 comparaisons. Le processus représente un
compromis entre fiabilité statistique, qui requiert d'augmenter la taille de la
fenêtre et demande davantage de comparaisons et rapidité qui est optimale quand
on regarde seulement un article à la fois («fenêtre» de taille un) comme
l'illustre la figure \ref{fig:dichotomy_window_comparisons} (l'effet de
«décrochage» est bien sûr dû à la fonction partie entière supérieure dans la
formule précédente).

![Nombre total de comparaisons requis dans le pire des cas en fonction de la taille de la fenêtre utilisée](figure/dichotomy_window_comparisons.png){#fig:dichotomy_window_comparisons width=60%}

Au milieu des paires candidates classées par similarité, entre les rangs 2931 et
2940 on trouve une unique fausse paire, celle pour SPIEGELBERG — une contrée
allemande (L'Encyclopédie, T15, p.461) et un gynécologue (La Grande
Encyclopédie, T30, p.388). Toutes les autres sont au moins en lien, l'acception
moderne contenant l'acception ancienne comme c'est le cas pour BARRAGE où *LGE*
contient une sous-section sur son acception commerciale dans l'ancien régime (La
Grande Encyclopédie, T5, p.469) qui est la seule définie dans l'*EDdA*
(L'Encyclopédie, T2, p.90) qui a été écrit à cette époque. Le taux de similarité
rapporté avec la mesure choisie est supérieur à 56.9% (valeur pour la 2931^ème^
paire). On réitère donc le procédé entre les rangs 1461 et 1470 qui ont une
similarité cosinus d'au moins 42% et contiennent au moins deux fausses paires.
Puisqu'ils contiennent trop de bruit, on remonte donc le seuil en allant entre
les rangs 2 194 et 2 203 (similarité > 50.3%) qui eux ne contiennent qu'une fausse
paire et ainsi de suite. Le processus se poursuit jusqu'à ce que deux fenêtres
se rejoignent ce qui arrive comme prévu à la dixième étape. On passe alors
toutes les fausses paires jusqu'à la première vraie (pour ne pas ajouter
délibérément des paires que l'on sait fausses à $\mathcal{P}$), celle de rang
2 167 qui avait obtenu un score de similarité de 50.0%. On les retient toutes
depuis celle-ci jusqu'à la fin de la liste, ce qui donne $5872 - 2167 + 1 =
3706$ paires d'articles. Il est à noter qu'au cours du processus, une autre
série de 10 paires sans aucune homonymie a été trouvée entre les rangs 2 171 et
2 180. Ce contre-exemple à l'hypothèse de monotonie de la qualité en fonction du
score est toutefois rassurant sur la qualité des rangs sélectionnés et la
pertinence de la limite.

#### Analyses du sous-corpus Parallèle {#sec:parallel_analysis}

Au cours du processus précédent de validation de fenêtres, quatre fenêtres
positives ont été mises en évidence: deux sans aucune fausse paire, et deux avec
une unique fausse paire. Il aurait bien évidemment été très tentant d'utiliser
ces échantillons déjà validés pour estimer la qualité du sous-corpus
sélectionné. C'est toutefois impossible pour des raisons méthodologiques car ces
quatre fenêtres ne constituent pas un échantillon aléatoire d'éléments
indépendants. En effet, la nature du processus de sélection même fait que les
fenêtres ont été considérées en fonction des résultats de l'évaluation des
autres déjà validées. Il est donc nécessaire de prélever un nouvel échantillon
pour vérifier si la qualité a une chance d'être proche des 90% espérés
initialement. Comme cette fois les $n$ paires prélevées sont indépendantes, et
qu'on évalue sur chacune une propriété booléenne (elles peuvent être soit vraies
soit fausses), elles constituent elles aussi des variables aléatoires de
Bernoulli, comme c'était le cas pour les articles de *LGE* étiquetés en domaine
par le modèle *BERT* et dont il s'agissait de contrôler la qualité à la section
\ref{sec:classifying_lge} page \pageref{bernouilli_experiment}. Le même cadre
théorique s'applique donc et un raisonnement tout à fait semblable peut se
développer. Le Théorème Central Limite peut à nouveau s'appliquer à condition
que la taille de l'échantillon reste suffisamment faible devant celle de
l'ensemble de la population: le rapport de 100 considéré comme suffisant impose
donc que l'échantillon comporte moins de $5873/100 < 59$ paires. À cette
condition, il est à nouveau possible d'utiliser la loi normale pour modéliser le
comportement de ces variables.

Un échantillon de 50 paires a donc été extrait aléatoirement puis vérifié et ne
comportait que deux fausses paires. La première est SPIEGELBERG discutée plus
haut (qui s'est retrouvée par hasard dans l'échantillon). La seconde concerne
les entrées PLUVIERS, toutes deux en *Géographie* mais ne désignant pas la même
ville: il s'agit dans l'*EDdA* d'une ville de la Beauce connue maintenant sous
le nom de Pithiviers (L'Encyclopédie, T12, p.805), mais de l'ancien nom d'une
commune de la Dordogne désormais appelée Piégut pour *LGE* (La Grande
Encyclopédie, T26, p.1146). De même que lors de l'échantillonnage précédent de
la section \ref{sec:classifying_lge}, on utilise la moyenne empirique pour
estimer la qualité $q$ dans $\mathcal{P}$ valant $m = \frac{48}{50} = 0.96$.
L'application numérique \ref{eq:parallel_corpus_quality_range_numerical} de la
borne inférieure de la formule \ref{eq:quality_range_algebraic}
p.\ref{eq:quality_range_algebraic} permet d'affirmer avec moins de 5% de risque
d'erreur que la qualité réelle dans le sous-corpus Parallèle est d'au moins

\begin{equation}
    m - z_{97.5\%} \times \sqrt{\frac{m \times (1- m)}{n}} = 90.6\%
    \label{eq:parallel_corpus_quality_range_numerical}
\end{equation}

Maintenant que la qualité de ce sous-corpus est établie, il est possible de se
poser la question de sa représentativité. La figure
\ref{fig:parallel_ratio_by_domain_by_work} montre que la répartition du nombre
d'articles par classe au sein du sous-corpus Parallèle est plutôt semblable à
celle trouvée dans toute l'*EDdA* (voir la figure
\ref{fig:edda_count_by_domain_repartition_pie}
p.\pageref{fig:edda_count_by_domain_repartition_pie}), ce qui est cohérent avec
le fait que cette encyclopédie, la plus réduite en nombre d'articles joue en
quelque sorte le rôle de «facteur limitant» dans cette réaction d'appariement.
Les principales différences notables sont l'augmentation des parts de la
*Géographie* et de la *Médecine* aux dépens surtout du *Droit Jurisprudence* et
des *Métiers*. Ces changements sont tous conformes à la variation qui existe
entre l'*EDdA* et *LGE* prises dans leur totalité ce qui peut s'interpréter
comme une influence de cette dernière au cours du processus de sélection.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/repartition/corpus/Parallel/EDdA/count_by_domain.png}
        \caption{Dans la moitié issue de l'\textit{EDdA}}
        \label{fig:parallel_edda_by_domain}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/repartition/corpus/Parallel/LGE/count_by_domain.png}
        \caption{Dans la moitié issue de \textit{LGE}}
        \label{fig:parallel_lge_by_domain}
    \end{subfigure}
    \caption{Répartition des nombres d'articles par domaines au sein du sous-corpus Parallèle}
    \label{fig:parallel_ratio_by_domain_by_work}
\end{figure}

Au-delà leur forte ressemblance, il est à noter que les proportions diffèrent
entre les deux moitiés du corpus, ce qui montre immédiatement qu'un même objet
peut intéresser des disciplines différentes suivant l'époque à laquelle on lui
consacre un article. Et le phénomène n'est pas du tout marginal puisque sur les
3 706 paires, 927 — soit tout juste plus d'un quart d'entre elles (25.01%) — ont
des domaines différents, ce qui permet de rejeter l'explication qui attribuerait
ces différences aux erreurs d'appariement (il y a moins de 5% de risque qu'il y
en ait seulement 371[^371] dans $\mathcal{p}$, et la probabilité diminue
fortement avec l'augmentation du nombre donc il serait d'autant plus improbable
que ces erreurs expliquent ces différences presque deux fois et demie plus
nombreuses).

[^371]: soit 10% des articles de $\mathcal{p}$, ce qui serait équivalent à dire
    que la qualité — mesurée empiriquement à 96% — serait en fait inférieure à
    90%, ce qui d'après définition de $z_{97.5\%}$ a moins de 5% de chance
    d'être vrai

La figure \ref{fig:parallel_changes} présente ces changements: pour une paire
donnée, le domaine en ordonnée est celui de l'article dans l'*EDdA*, alors qu'en
abscisse est représenté celui dans *LGE*. Le premier élément visible est une
diagonale relativement forte — mais pas homogène — qui matérialise les 74.99% de
paires dont le domaine n'a pas changé. Sur cette diagonale, les points
particulièrement colorés sont ceux des domaines *Géographie*,
*Histoire-naturelle* et dans une moindre mesure *Beaux-arts*, *Droit
Jurisprudence*, *Militaire*, *Musique* et *Médecine*. La colonne et la ligne du
domaine *Géographie* sont remarquablement peu visibles hors de leur
intersection, ce qui montre que ce domaine est celui dont statistiquement les
articles ont le moins changé de catégorie entre les deux époques. Par contraste
la colonne *Histoire*, très ténue mais nettement visible hors de la diagonale
montre qu'un nombre conséquent d'articles d'autres domaines sont passés vers la
catégorie *Histoire* dans *LGE* ce qui se manifestait dans la figure
\ref{fig:parallel_lge_by_domain} par la nette augmentation de la proportion
d'*Histoire* par rapport à la même figure pour l'*EDDA* (figure
\ref{fig:parallel_edda_by_domain}).

![Évolution des domaines des articles dans le sous-corpus Parallèle](figure/classification/parallelCorpus.png){#fig:parallel_changes width=63%}

En allant regarder dans le détail ce qui se passe pour les articles de
*Géographie*, c'est-à-dire en s'intéressant à la ligne et la colonne de ce
domaine, très pâles sur la figure \ref{fig:parallel_changes}, il ressort qu'il y
a légèrement plus d'entrées qui étaient rattachées à cette discipline dans
l'*EDdA* qui en changent dans *LGE* que l'inverse (56 contre 54). Leur nombre
total relativement faible, 110, rend peu pertinente une étude statistique de
leur répartition, mais permet d'en faire une revue qualitative exhaustive. La
figure \ref{fig:parallel_geography_differences} montre que la plupart des
changements de domaine pour des articles de *Géographie* concernent
l'*Histoire*, mais dans les deux sens (aussi bien des articles de *Géographie*
dans l'*EDdA* qui sont passé à l'*Histoire* dans *LGE* que le contraire).

\begin{figure}[h!]
    \centering
    \begin{subfigure}[t]{0.6\textwidth}
        \centering
        \includegraphics{figure/evolution/Parallel/geo_in_LGE.png}
        \caption{Part des domaines dans l'\textit{EDdA} des articles passant à la \textit{Géographie} dans \textit{LGE}}
        \label{fig:parallel_geography_differences_not_to_geo}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.6\textwidth}
        \centering
        \includegraphics{figure/evolution/Parallel/geo_in_EDdA.png}
        \caption{Part des domaines dans \textit{LGE} des articles issus de la \textit{Géographie} dans l'\textit{EDdA}}
        \label{fig:parallel_geography_differences_geo_to_not}
    \end{subfigure}
    \caption{Changements de domaines d'articles impliquant la \textit{Géographie} dans le corpus Parallèle, de l'\textit{EDdA} (à gauche) à \textit{LGE} (à droite)}
    \label{fig:parallel_geography_differences}
\end{figure}

Si l'ordre de grandeur du nombre de paires dont les domaines diffèrent prouve
qu'elles ne peuvent toutes s'expliquer par les seules erreurs d'appariement, en
revanche ce type d'erreur est évidemment très surreprésenté dans une sélection
de paires d'articles aux domaines différents. Les erreurs apportent bien sûr peu
d'information mais sont relativement peu nombreuses. On dénombre ainsi 15 paires
dont les articles ne renvoient pas aux mêmes objets et qui n'auraient donc
idéalement pas dûes être incorporées au sous-corpus $\mathcal{P}$ mais qui ont
réussi à obtenir un score assez élevé pour être conservées par le processus
précédent. À ces 15 paires s'ajoutent 12 autres dont la classe d'une des entrées
a été mal prédite par le modèle. Parmi elles, six ne sont en fait que des
renvois, pour lesquels la prédiction du domaine est évidemment très incertaine
puisque le modèle ne peut se baser que sur la vedette de l'article pointé par le
renvoi ce qui est rarement suffisant. Une troisième source d'erreurs est
l'imperfection de la segmentation dans *LGE* (voir la sous-section
\ref{sec:corpus_preprocessing_lge} p.\pageref{lge_segmentation}) qui a par
exemple mis l'entrée sur COMMUNAY (ville de l'Isère) avec l'entrée sur les Biens
COMMUNAUX, faisant passer à *Géographie* dans *LGE* une vraie paire dont les
deux entrées en correspondance relevaient du domaine *Droit Jurisprudence*. Ce
type d'erreurs concerne cinq paires seulement.

Ayant écarté ces 32 erreurs, les 78 autres paires se révèlent bien plus
intéressantes. Elle peuvent essentiellement se répartir en trois groupes
correspondant à trois mouvements distincts: la géographie abandonne d'abord les
contenus imaginaires, elle acquiert ensuite des connaissances nouvelles — en
particulier économiques et industrielles — sur les territoires qu'elle décrit et
enfin cède à d'autres domaines des connaissances spécifiques qu'ils expliquent
mieux grâce à de nouvelles théories ou de nouveaux faits importants, et qui
relevaient en quelque sorte de la *Géographie* «par défaut» dans l'*EDdA*. Ces
mouvements, décrits plus en détail dans les paragraphes suivants, peuvent
conduire à des différences de domaine entre les entrées dans l'*EDdA* et dans
*LGE* au sein de vraies paires, aussi bien au profit qu'au détriment de la
*Géographie* selon que le domaine a été inféré par le modèle pour la moitié
issue de l'*EDdA* ou bien décidé par ses éditeurs et selon que le contenu dans
*LGE* en est resté proche ou non.

En ce qui concerne le premier de ces mouvements, la présence dans l'*EDdA* de
désignants tels que «Géog. anc.» (ex. PEDUM, L'Encyclopédie, T12, p.238),
«Géogr. \& Myth.» (BATHOS, L'Encyclopédie, T2, p.141) et même «Géog. sacrée»
(MASSADA, L'Encyclopédie, T10, p.176) est en soi assez édifiante. Le discours
sur des lieux et des peuples historiques voire imaginaires relève dans ses pages
de la *Géographie*, comme l'illustrent les articles HYPERBORÉENS
(L'Encyclopédie, T8, p.405), JUTURNA (L'Encyclopédie, T9, p.102) ou OGYGIE
(L'Encyclopédie, T11, p.429) reproduit à la figure \ref{fig:ogygie_edda}. Les
articles correspondant dans *LGE* (La Grande Encyclopédie, respectivement T20
p.481, T21 p.362 et T25 p.298 visible à la figure \ref{fig:ogygie_lge})
conservent un contenu proche et se retrouvent donc classés en *Histoire* ou en
*Histoire Naturelle*. À l'inverse, des articles comme ERYMANTHE (La Grande
Encyclopédie, T16, p.223) se concentrent sur les données factuelles de ces lieux
réels et relèguent leur dimension mythologique à des remarques largement
anecdotiques. Le modèle le classe donc à *Géographie*, alors que son traitement
dans l'*EDdA* (L'Encyclopédie, T5, p.918) s'éloigne assez des articles de
géographie type du XVIII^ème^ siècle pour inclure des éléments mythologiques et
recevoir du modèle la classe *Histoire*. On peut observer les signes de la même
évolution pour des paires dont l'article dans l'*EDdA* est pourvu d'un
désignant. Ainsi le traitement des entrées CLAIRETS (L'Encyclopédie, T3, p.500
et La Grande Encyclopédie, T11, p.528) et STONEHENGE (L'Encyclopédie, T15, p.535
et La Grande Encyclopédie, T30, p.520) est assez proche et, d'un point de vue
moderne bien plus historique que géographique dans les deux œuvres. Les deux
articles dans l'*EDdA* sont pourvus de désignants qui les classent en
*Histoire* mais le modèle, trop entraîné à voir cette sorte de contenu
historique dans des articles de *Géographie* a tout de même prédit cette classe
sur leurs successeurs dans *LGE*.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/text/EDdA/ogygie_t11_p429}
        \caption{Article OGYGIE dans l'\textit{EDdA}, T11, p.429}
        \label{fig:ogygie_edda}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/text/LGE/ogygie_t25_p298}
        \caption{Article OGYGIE dans \textit{LGE}, T25, p.298}
        \label{fig:ogygie_lge}
    \end{subfigure}
    \caption{Les articles sur l'île OGYGIE dans le sous-corpus Parallèle}
    \label{fig:ogygie_pair}
\end{figure}

Le deuxième mouvement concerne des paires pour lesquelles le modèle n'a pas
reconnu une *Géographie* qui a trop évolué et intègre des informations trop
nouvelles par rapport à celles qu'elle comportait au XVIII^ème^ siècle, par leur
forme ou leur contenu. Le groupe correspondant contient par exemple des paires
comme HAMEAU dont l'article dans l'*EDdA* (L'Encyclopédie, T8, p.34) ne donne
pas de définition mais se concentre sur l'étymologie du terme qu'il illustre par
ses occurrences dans un grand nombre de toponymes; son désignant le classe
explicitement à *Géographie*. L'entrée a évolué dans *LGE* (La Grande
Encyclopédie, T19, p.789) pour employer un certain nombre de termes formels
comme «circonscription territoriale», «paroissial» ou «fraction de commune» et
référencer par leurs numéros des articles du code civil et du code forestier si
bien que le modèle ne reconnaît plus la discipline et propose la classe *Droit
Jurisprudence*. Le même genre d'évolution semble être à l'œuvre dans le cas de
la paire VILLEPREUX (L'Encyclopédie, T17, p.282 et La Grande Encyclopédie, T31,
p.1007), visible à la figure \ref{fig:villepreux_pair}, deux entrées très
simples et «clairement» géographiques pour le lectorat contemporain. Le modèle
échoue pourtant à reconnaître le domaine dans la moitié issue de *LGE*, sans
doute à cause de la présence d'éléments anachroniques pour l'*EDdA* où il a été
entraîné comme «chem. de fer» ou l'adjectif «professionnelle» qui n'est attesté
qu'à partir de 1842\. Il est intéressant de remarquer que cet article débute par
le motif des communes identifié à la section \ref{sec:classifying_lge}
p.\pageref{lst:com_du_dep_regex}, mais ne fait pas partie des articles
identifiés à cause de sa longueur[^longueur], précisément due à ces phrases
nominales en fin d'article qui mentionnent des infrastructures présentes sur la
commune. Ces informations supplémentaires qui font que l'article n'a pas été
repéré directement comme un article de commune (et n'a donc pas été annoté
*Géographie*) n'ont pas permis au modèle d'identifier de la géographie car elles
mentionnent des objets trop différents de ceux existant au XVIII^ème^ siècle.

[^longueur]: pour rappel, un seuil de 50 tokens avait été imposé pour diminuer
    les faux-positifs et ne pas risquer d'annoter *Géographie* trop d'articles
    simplement amalgamé avec un article bref à cause des problèmes de
    segmentation de `soprano` (voir section \ref{sec:corpus_preprocessing_lge}
    p.\pageref{lge_segmentation})

\begin{figure}[h!]
    \centering
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/text/EDdA/villepreux_t17_p282.png}
        \caption{Article VILLEPREUX dans l'\textit{EDdA}, T17, p.282}
        \label{fig:villepreux_edda}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/text/LGE/villepreux_t31_p1007.png}
        \caption{Article VILLEPREUX dans \textit{LGE}, T31, p.1007}
        \label{fig:villepreux_lge}
    \end{subfigure}
    \caption{Les articles VILLEPREUX dans le sous-corpus Parallèle}
    \label{fig:villepreux_pair}
\end{figure}

Enfin, l'évolution des connaissances ou l'arrivée de faits nouveaux a permis à
d'autres disciplines de s'emparer de thématiques qui relevaient de la géographie
dans l'*EDdA*. Là où l'entrée VOLCAN dans l'*EDdA* (L'Encyclopédie, T17, p.445)
se cantonne par exemple à une description externe du concept avant de lister les
plus connus à différents endroits du globe ce qui justifie pleinement son
classement en *Géographie* par les éditeurs, l'article correspondant dans *LGE*
(La Grande Encyclopédie, T31, p.1104) en fait une description bien plus
structurelle, et introduit des termes comme «lave», «cheminée», «explosion»,
absents de la précédente ce qui conduit le modèle à le classer à *Physique*. Les
trajectoires paires PORPHYRITE (L'Encyclopédie, T13, p.127 et La Grande
Encyclopédie, T27, p.328) et PURBECK (L'Encyclopédie, T13, p.576 et La Grande
Encyclopédie, T27, p.964) sont assez semblables mais avec la classe *Histoire
naturelle* et dans deux directions opposées. L'entrée PORPHYRITE de l'*EDdA*
décrit le lieu d'origine d'une pierre particulière; l'entrée correspondante dans
*LGE* est un long article de géologie (désignant «Pétrogr.») sur la pierre
elle-même. La figure \ref{fig:purbeck_pair} illustre le cas inversé de PURBECK:
l'article est en *Histoire naturelle* dans l'*EDdA* où il décrit la pierre et
mentionne l'endroit d'où elle provient dans le Dorset, alors que *LGE* couvre
seulement la géographie de l'île dont elle est issue, et renvoie à un article
séparé pour expliquer ses caractéristiques géologiques: le modèle le classe en
*Géographie*. Il est aussi intéressant de voir qu'un certain nombre d'entrées
géographiques assez brèves sur des fiefs dans l'*EDdA* — par exemple BRAGANCE
(L'Encyclopédie, T2, p.392) ou INFANTADO (L'Encyclopédie, T8,
p.697) — deviennent dans *LGE* des articles plus longs sur les familles
possédant ces terres. Le choix éditorial de ne pas consacrer d'articles à des
personnes (plus amplement discuté dans la section \ref{sec:biographies}) suffit
bien sûr à expliquer pourquoi ces articles portent sur les terres plutôt que sur
les familles qui y règnent, mais leurs contreparties dans *LGE*, toutes deux
classées en *Histoire*, sont l'occasion de décrire en détail la lignée de la
maison de BRAGANCE (La Grande Encyclopédie, T7, p.959) en la mettant à jour et
pour INFANTADO (La Grande Encyclopédie, T20, p.768) de raconter la vie d'un
homme d'état espagnol qui s'est illustré pendant la première moitié du XIX^ème^
siècle.

\begin{figure}[h!]
    \centering
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/text/EDdA/purbeck_t13_p576.png}
        \caption{Article PURBECK dans l'\textit{EDdA}, T13, p.576}
        \label{fig:purbeck_edda}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figure/text/LGE/purbeck_t27_p964.png}
        \caption{Article PURBECK dans \textit{LGE}, T27, p.964}
        \label{fig:purbeck_lge}
    \end{subfigure}
    \caption{Les articles PURBECK dans le sous-corpus Parallèle}
    \label{fig:purbeck_pair}
\end{figure}

Pour terminer, un dernier exemple éclairant de ce troisième mouvement concerne
les deux paires ARCTIQUE et ANTARCTIQUE à la frontière entre *Géographie* et
*Physique*. Dans l'*EDdA*, ANTARCTIQUE (L'Encyclopédie, T1, p.491) et ARCTIQUE
(L'Encyclopédie, T1, p.621) sont tous deux signés par d'Alembert, auteur (outre
de nombreux articles de mathématiques évidemment) de plusieurs articles de
*Géographie* construits sur une approche mathématique des concepts. La
*Géographie* dans cette œuvre recouvre en effet encore les techniques
géométriques utilisées pour mesurer la Terre, ce qui s'illustre bien par exemple
dans l'article LATITUDE[^latitude] (L'Encyclopédie, T9, p.302) du même auteur.
L'entrée ANTARCTIQUE possède le double désignant «Astr. \& Géog.», ce qui
rappelle la place de la *Géographie* au sein de la *Cosmographie* dans le
«Systême Figuré» (voir la section \ref{sec:structuring_knowledge} à la page
\pageref{sec:domains_geography}). De même, l'article ARCTIQUE est situé «en
Astronomie» par son auteur qui définit la vedette en termes de cercles et de
sphères. Les deux articles sont classés comme on peut s'y attendre à *Physique*.

[^latitude]: Cet article contient dès le premier paragraphe l'expression «arc
    méridien», puis d'Alembert introduit des «cercles parallèles» dont il
    considère l'«intersection» dans le deuxième pour dans le troisième inviter
    son lectorat à considérer «un nombre infini de grands cercles».

La fin du XIX^ème^ siècle est marquée par de grandes explorations, entre autres
vers les pôles qui deviennent peu à peu perceptibles en tant que territoire
plutôt que comme de simples abstractions géométriques[^polaire] ce qui conduit à
traitement très différent des deux mêmes entrées dans *LGE*. Si ANTARCTIQUE (La
Grande Encyclopédie, T3, p.135) reste dans une certaine mesure similaire (on y
parle d'«axe», de «cercle» et de «parallèle» dans un «I. ASTRONOMIE»), il y est
tout de même question de «région», de «monde», et un «II. Géographie» qui
consiste en un renvoi vers l'entrée OCÉAN suffit à ce que le modèle classe
l'article à *Géographie*. La bascule est encore bien plus nette avec l'ARCTIQUE
(La Grande Encyclopédie, T3, p.774) qui, après une phrase introductive pour
définir l'Arctique en termes astronomiques, prend un tournant nettement
géographique («En géographie») et utilise le point cardinal «Nord» avec et sans
abréviations, ainsi que des termes comme «terre», «océan» qui permettent au
modèle de le classer aussi en *Géographie*. Dans l'exemple de ces deux paires
d'articles, on voit la géographie perdre ce qui relevait de la géométrie et se
spécialiser pour garder ce qui lui est propre.

[^polaire]: De manière adjacente, l'article POLAIRE (La Grande Encyclopédie,
    T27, p.54) illustre bien cette tendance en consacrant plusieurs colonnes aux
    expéditions successives vers le pôle Nord ainsi qu'une carte en couleur sur
    une double page (un feuillet collé entre les pp.72 et 73) qui montre en
    détail toutes les îles et côtes de la zone et marque les points atteints par
    les différentes expéditions, jusqu'au tracé complet de l'expédition de
    Nansen en 1895. Le pôle Sud, encore bien moins accessible, est évoqué plus
    brièvement mais une carte lui est tout de même consacrée (p.59), moins
    précise et en noir et blanc mais reportant aussi les avancées majeures vers
    ce pôle.

L'approche particulière de cette sous-section, en s'intéressant à des articles
précis plutôt qu'à des domaines dans leur ensemble a permis de mener une études
plus qualitative. Les observations qui y ont été faites mettent en évidence des
différences significatives dans les rôles et les méthodes de la géographie entre
les articles dans l'*EDdA* et ceux dans *LGE*, dont le contenu s'individualise
entre les deux époques par rapport à celui des autres disciplines.

Mise en regard de la sous-section précédente sur la place accordée aux discours
géographiques, des évolutions subies par la géographie commencent à apparaître.
L'intervalle de temps est d'abord marqué par un fort accroissement de la
volumétrie consacrée à la classe *Géographie*, avec un plus grand nombre
d'articles, dont la plupart restent extrêmement brefs mais également une
diversification des profils d'articles avec l'apparition d'articles bien plus
longs que ceux consacrés à cette discipline dans l'*EDdA*. Parallèlement, elle
se spécialise et gagne en technicité, utilisant au passage davantage de mots
plus longs et structurant les informations au delà de simplement concentrer des
noms de lieux et de personnes. Elle cède par ailleurs à d'autres disciplines des
thématiques qui lui revenaient par défaut et donne bien plus d'informations plus
détaillées sur celles qu'elles conserve.

