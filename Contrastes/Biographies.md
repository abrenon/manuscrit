## La biographie cachée {#sec:biographies}

Un parti pris éditorial remarquable de l'*EDdA* est de ne pas avoir inclus de
biographies dans ses pages, choix assez emblématique du Siècle des Lumières et
qui correspond bien à l'idéal humaniste et démocratique de diffusion du savoir
pour sa portée libératrice intrinsèque [@dantuono_democratie_2018, p.99] plutôt
que pour mettre en valeur des récits héroïques individuels. Les auteurs le
justifient très simplement dans le Discours Préliminaire de l'*EDdA* en écrivant
qu'un autre dictionnaire, celui de Moreri, contient déjà des notices sur les
«noms de Rois, de Savans, & de Peuples» (L'Encyclopédie, T1, p.xlj).

Les pages de l'*EDdA* citent tout de même bien sûr des figures historiques
autant qu'elles éclairent l'histoire des disciplines où elles se sont
illustrées. Ainsi, l'article «MÉDECINS ANCIENS» (L'Encyclopédie, T10, p.276)
offre un historique détaillé de la médecine illustré par les contributions de
nombreux médecins célèbres depuis l'antiquité. Certains des paragraphes
contiennent, en sus de la bibliographie et de la liste des apports de la
personne qu'ils concernent, quelques éléments biographiques comme leur lieu et
éventuellement année de naissance ou de mort. Mais de nombreuses biographies que
l'on pourrait qualifier de «clandestines» ont également été rapportées dans la
*Géographie* par exemple par @roe_discourses_2016[p.5] qui décrivent comment
l'entrée sur le village de Wolstrope sert en fait de prétexte pour introduire
une biographie de Newton. Particulièrement fréquentes sous la plume du chevalier
de Jaucourt qui a été le contributeur le plus important de la *Géographie* dans
l'*EDdA* signant 57.2% des articles du domaine d'après
@laramee_production_2017[p.166], de nombreuses autres biographies ont été
rapportées dans des articles de *Géographie*, que ce soit plus loin par
@laramee_production_2017[p.169] ou par @vigier_articles_2022[p.70].

Il est donc impossible d'envisager correctement la Géographie dans les pages de
l'*EDdA* sans s'intéresser aussi à la place des biographies. Il faut d'abord
déterminer si elles entretiennent une relation particulière avec les articles de
géographie, ou si ces inclusions relèvent simplement des pratiques
encyclopédiques de l'époque. Deux attitudes peuvent s'envisager face à la
présence de ces textes: on peut ou bien les considérer comme des îlots de
contenu non disciplinaires à l'intérieur des articles et les détournant de leur
vocation première[^discordia] ou bien considérer au contraire que puisqu'ils
font partie des articles qui ont été écrits, ils sont constitutifs des discours
de l'époque. Quoi qu'il en soit, une conséquence importante des choix éditoriaux
des encyclopédistes pour les présents travaux est que le modèle utilisé n'a pas
pu apprendre à reconnaître les biographies car elles ne sont pas identifiées
explicitement dans les pages de l'*EDdA* et constituent donc un angle mort
important du modèle et par conséquent des résultats qu'il est possible
d'observer en l'utilisant.

[^discordia]: et donc, dans le cas précis de la *Géographie*, comme autant de
    pommes de la discorde entre Diderot et Jaucourt

Et la question est d'autant plus pertinente dans une perspective diachronique,
puisque *LGE* comporte un très grand nombre de notice bibliographiques qui sont
quant à elles explicitement consacrées à une personne sous forme d'entrées
indépendantes dont la vedette est le nom de famille. La figure
\ref{fig:zschimmer_t31} le rappelle, la figure \ref{fig:loveling_lge}
l'illustrait déjà à la page \pageref{fig:loveling_lge}. Faut-il donc penser que
la Biographie aurait pris son indépendance de la Géographie entre le
XVIII^ème^s. et le XIX^ème^s. ou plutôt y voir la preuve que la biographie a une
existence propre et n'aurait jamais dû envahir l'espace consacré à cette science
?

![Article ZSCHIMMER dans *LGE*, T31, p.1338](figure/text/LGE/zschimmer_t31.png){#fig:zschimmer_t31 width=60%}

De plus, l'existence de la biographie comme domaine de connaissance en soi au
même titre que les Mathématiques, la Médecine ou la Géographie ne relève pas de
l'évidence. En revenant comme à la section \ref{sec:knowledge_domains} (voir p.
\pageref{sec:knowledge_domains}) à l'avant-propos de *LGE* (La Grande
Encyclopédie, T1, p.XI) on constate qu'il n'est fait aucune mention de la
Biographie parmi les sciences citées alors qu'elles sont tellement omniprésentes
qu'une page ouverte au hasard en contient statistiquement au moins une — le plus
souvent même plusieurs — pourvu qu'elle ne soit pas à l'intérieur d'un
article-essai s'étendant sur plusieurs pages.

On pourrait s'attendre à voir une biographie rattachée au domaine dans lequel
s'est illustrée la personne à laquelle elle est consacrée, comme c'est par
exemple le cas de celle sur SHAKESPEARE (La Grande Encyclopédie, T29, p.1142)
pour laquelle le modèle prédit la classe «Belle-lettres», mais on constate en
pratique qu'un grand nombre d'entre elles ont reçu la classe «Histoire». Ce
choix n'est pas dénué de sens si l'on considère la forte composante narrative
des biographies, écrites au passé, comportant fréquemment des références à des
dates d'événements et à d'autres personnages historiques que l'on retrouve
habituellement dans les articles d'Histoire. Cette remarque apporte d'ailleurs
un premier éclairage possible sur le constat dressé précédemment à la section
\ref{sec:geography_edge_words_count} sur la disproportion des articles
d'«Histoire et Géographie» dans *LGE* d'après le modèle par rapport aux autres
domaines. En effet, le très grand nombre de biographies et la très forte
tendance du modèle à les considérer comme des articles d'Histoire pourrait être
une bonne piste pour expliquer leur nombre anormalement élevé. Un corollaire
amusant de ces constats est qu'une façon efficace de sélectionner une biographie
au hasard dans *LGE* consiste à choisir le premier article classé «Histoire»
dont le titre n'est ni un nom commun ni un nom de roi, de pape ou de personnage
mythologique.

Cette section ne propose pas de solution définitive à la question de la place
des biographies dans les articles de Géographie mais tente de formuler assez
précisément le problème pour commencer à y réfléchir et suggère quelques
critères simples basés sur une combinaison d'observations qualitatives et
quantitatives pour aller au-delà du seul constat: «Jaucourt a piraté la
Géographie de l'*EDdA* pour écrire des Biographies».

### Deux lemmes inattendus

Les réflexions préliminaires de cette section amènent d'abord à examiner les
relations entre la biographie et les différents domaines de connaissance pour
déterminer si les contenus biographiques sont particulièrement présent dans la
Géographie ou si le fait que les exemples connus de biographie dans l'*EDdA*
apparaissent dans ses articles ne relève que du hasard. En partant du cas de
WOLSTROPE (L'Encyclopédie, T17, p.630), qui utilise sa ville de naissance pour
parler de Newton, il est naturel de prendre la notion de ville comme point de
départ. L'étude des cooccurrents syntaxiques offre la possibilité de voir les
constructions dans lequelles le terme «ville» est utilisé.

Dans le Lexicoscope [@kraif_lexicoscope_2016], une requête sur le lemme «ville»
(formulée en langage TQL[^tql] dans le code source \ref{lst:tql_ville}) peut
être utilisée pour mesurer les scores d'associations avec ce lemme des
différents tokens possibles.

\begin{lstlisting}[caption=Requête TQL sur le lemme «ville» annoté \texttt{NOUN}
(nom commun), label=lst:tql_ville]
<l=ville,c=NOUN,#1>
\end{lstlisting}

[^tql]:
    [http://phraseotext.univ-grenoble-alpes.fr/lexicoscope_beta/doc/Reference%20TQL.fr.pdf](http://phraseotext.univ-grenoble-alpes.fr/lexicoscope_beta/doc/Reference%20TQL.fr.pdf)

La figure \ref{fig:ville_lemma_geode} représente l'histogramme des dix mesures
les plus élevées. Sur cette figure, le déterminant «ce» apparaît comme
cooccurrent principal de «ville». Il est intéressant de constater qu'il obtient
même un score d'association (22 958) supérieur à celui de l'adjectif «natal»
(18 773): alors que la nature d'un déterminant fait qu'il peut potentiellement
être utilisé avec n'importe quel nom commun (pas seulement ville), l'ensemble
des noms que peut qualifier un adjectif donné est bien plus restreint par des
considérations sémantiques. En effet, «ce» peut déterminer aussi bien «ville»
que «procédé» par exemple, mais la séquence «procédé natal» n'a aucun sens
évident et donc virtuellement aucune chance d'apparaître dans un texte, à part
sous forme de contre-exemple comme dans cette phrase. Deux autres mots outils
figurent parmi les cinq premiers cooccurrents. Ce sont le déterminant «le»
(intéressant par comparaison avec «ce») et la préposition «dans» qui suggère
l'emploi du concept de ville comme d'un cadre narratif dans lequel se produit
les événements qui intéressent les articles. Un deuxième adjectif, «petit»,
obtient également un score proche: l'asymétrie que crée cette observation par
rapport au contraire «grand» semble suggérer qu'on qualifie bien plus volontiers
une ville de «petite» que de «grande» dans les pages des encyclopédies du
corpus.

![Les 10 cooccurrents syntaxiques principaux du lemme «ville» annoté comme `NOUN` (nom commun)](figure/histogram/textometry/ville.png){#fig:ville_lemma_geode}

Le Lexicoscope permet ensuite de générer automatiquement une requête qui intègre
un des cooccurrents parmi ces résultats. Cette fonctionnalité correspond à une
étape du procédé itératif permettant de construire progressivement un [@=ALR].
La nouvelle requête obtenue pour le premier résultat, le déterminant «ce», est
reproduite à l'extrait de code \ref{lst:tql_ce_ville}.

\begin{lstlisting}[caption=Requête TQL sur la mise en relation syntaxique
quelconque du lemme «ce» annoté comme \texttt{DET} et du lemme «ville» annoté
comme \texttt{NOUN}, label=lst:tql_ce_ville]
<l=ce,c=DET,#2>&&<l=ville,c=NOUN,#1>::(.*,1,2)
\end{lstlisting}

Son exécution sur le même corpus aboutit à la figure
\ref{fig:ce_ville_lemma_geode} qui retrouve la préposition «dans», mais cette
fois avec la plus forte mesure d'association. Déjà présente sur la figure
\ref{fig:ville_lemma_geode}, la requête \ref{lst:tql_ville} ne pouvait par
construction que rendre compte des intéractions de «ce» et de «dans» avec
«ville» de manière séparée. Cette nouvelle mesure montre qu'en réalité les trois
éléments apparaissent fréquemment ensemble dans le syntagme «dans cette ville».
La préposition «de» suit une trajectoire semblable puis les mesures
d'associations chutent très rapidement à moins de 10% de la valeur obtenue pour
la préposition «dans», obtenues par un ensemble de verbes et de noms.

![Les 10 cooccurrents syntaxiques principaux du motif formé d'une relation syntaxique quelconque entre le lemme «ce» annoté comme `DET` et le lemme «ville» annoté comme `NOUN`](figure/histogram/textometry/ce_ville.png){#fig:ce_ville_lemma_geode}

En suivant ce nouveau coocurrent «dans» on construit la requête
\ref{lst:tql_dans_ce_ville}, dont un équivalent sous forme d'arbre syntaxique en
dépendances est visible à la figure \ref{fig:dans_cette_ville_tree} pour plus de
clarté. Les astérisques sur les deux arêtes partant du nœud `ville_NOUN`
représentent l'absence de contrainte sur les relations qui relient «ville»
respectivement à «dans» et à «ce». En pratique, on peut s'attendre à ce que ces
relations soient réalisées par un `case` pour «dans» et par un `det` pour
«ce»[^UDdeps] mais le motif demeure flexible sur ce point. De plus, travaillant
au niveau de la syntaxe et pas de la réalisation de surface des mots, la requête
inclut des résultats comme «dans ces deux villes» à l'article ARABIE
(L'Encyclopédie, T1, p.570) ou «dans cette dernière ville» à l'article DAEHLING
(La Grande Encyclopédie, T13, p.749).

[^UDdeps]:
    [https://universaldependencies.org/u/dep/index.html](https://universaldependencies.org/u/dep/index.html)

\begin{lstlisting}[caption=Requête TQL sur le motif syntaxique «dans cette ville»,
label=lst:tql_dans_ce_ville]
<l=dans,c=PREP,#3>&&<l=ce,c=DET,#2>&&<l=ville,c=NOUN,#1>::(.*,1,2)(.*,
1,3)
\end{lstlisting}

![Représentation sous forme d'arbre de syntaxe en dépendance du motif défini par la requête \ref{lst:tql_dans_ce_ville}](figure/syntax/dans_ce_ville.png){#fig:dans_cette_ville_tree width=20%}

En recherchant ce troisième motif, on obtient la figure
\ref{fig:dans_ce_ville_lemma_geode} qui représente les dix coocurrents de «dans
cette ville» avec la plus forte mesure d'association. Sur cette figure, il n'y a
quasiment plus que des verbes et des adjectifs. Le phénomène précédent de
remontée d'un lemme déjà observé au cours du processus se produit à nouveau avec
l'adjectif «dernier» qui n'obtenait que la troisième place sur la figure
\ref{fig:ce_ville_lemma_geode} mais devient le cooccurrent principal de «dans
cette ville». De la même manière, le verbe «mourir» prend le deuxième rang,
alors qu'il n'était que sixième pour «cette ville» seulement. De plus, le
troisième rang est occupé par le verbe «naître» très lié sémantiquement. Absent
jusque-là des histogrammes, il est cependant proche de l'adjectif «natal»
discuté lors de l'analyse de la figure \ref{fig:ville_lemma_geode} et renvoie
directement au procédé utilisé dans l'article WOLSTROPE, à savoir utiliser la
ville de naissance d'une personnalité pour en donner un récit de sa vie.

![Les 10 cooccurrents syntaxiques principaux de «dans» (`ADP`), «ce» (`DET`), et «ville» (`NOUN`)](figure/histogram/textometry/dans_ce_ville.png){#fig:dans_ce_ville_lemma_geode}

Cette observation cruciale présente donc ces deux lemmes inattendus comme des
objets incontournables pour poursuivre l'exploration. Dans TXM
[@heiden_txm_2010], on partitionne d'abord les textes par domaines, en
commençant par se restreindre à l'*EDdA*.

### L'influence des domaines {#sec:biography_domains}

Puisque des éléments biographiques apparaissent dans un article d'histoire de la
médecine, la question qui se présente ensuite naturellement est de savoir si des
articles consacrés exclusivement à une unique figure historique sont une
spécificité de la Géographie ou s'il est possible d'en trouver dans des articles
d'autres disciplines. Malgré le choix éditorial de ne pas mettre en avant de
figure individuelles, le Discours Préliminaire de l'*EDdA* (L'Encyclopédie, T1,
p.xlj) fait mention de nombre de «génies» qui ont façonné les domaines où ils se
sont illustrés. L'attention particulière qui leur est portée dans ce Discours
Préliminaire — et le fait que Newton dont on sait qu'il y a une biographie dans
les pages de l'*EDdA* soit parmi eux — laisse à penser qu'ils font de bons
candidats pour trouver d'éventuelles biographies.

En cherchant leurs noms parmi les vedettes de l'*EDdA*, on a la surprise de
trouver celui de Locke à l'article «LOCKE, Philosophie de» (L'Encyclopédie, T9,
p.625). Malgré sa vedette, l'article ne se limite pas à la philosophie de Locke
et, sur toute sa première moitié, donne un récit de la vie du philosophe riche
en dates, lieux, et mentions d'autres personnages qui débute par sa naissance et
s'achève par sa mort. Il apparaît tout à fait comparable à l'article WOLSTROPE
(L'Encyclopédie, T17, p.630) qui, bien que trois fois plus long environ (7277
mots contre 2659) contient lui aussi environ une moitié d'éléments
biographiques, le reste de l'article concernant directement les travaux de
Newton, ses théories et jusqu'à l'exposition des travaux de ses précurseurs
comme le paragraphe sur les lois de Kepler.

En plus de constituer un premier exemple de biographie individuelle hors de la
Géographie (le désignant de l'article est «Hist. de la Philosoph. moder» et le
modèle le classe à Philosophie), cette entrée dévoile une stratégie pour en
trouver d'autres: on peut trouver des informations biographiques sur une
personne dans un article motivé par la discussion de sa philosophie. Et cette
stratégie s'avère payante puisque l'on en trouve neuf de cette manière,
présentés dans le tableau \ref{table:edda_biographies_in_philosophy}. Certains
contiennent davantage de récits et de détails personnels (LÉIBNITZIANISME en est
particulièrement riche), d'autres sont des attaques assez virulentes contre le
philosophe qu'ils décrivent (surtout CAMPANELLA) mais tous contiennent à la fois
des éléments biographiques et des détails sur ses travaux. On remarque de plus
qu'aucun article n'est de Jaucourt, et que les articles JORDANUS BRUNUS,
LÉIBNITZIANISME, LOCKE, PLATONISME et PYTHAGORISME sont tous les cinq signés par
Diderot. Ce fait est d'autant plus remarquable quand on considère que la
«sécheresse» du style de Diderot dans les articles de Géographie — dont il a dû
se défendre dans l'article ENCYCLOPÉDIE[^defense] — est souvent opposé à celui
de Jaucourt, fourmillant et érudit, quand il s'agit de décrire la «contrebande»
biographique de ce dernier. De plus, ayant publié une biographie de Leibnitz en
1734 [@ferenczi_chevalier_2017, p.87], le chevalier de Jaucourt était
particulièrement bien placé pour l'écrire dans l'*EDdA*; il se contentera
pourtant à l'article LEIPSIC (L'Encyclopédie, T9, p.380) de le nommer et de
citer Voltaire à son sujet avant de renvoyer son lectorat à l'article
LÉIBNITZIANISME. Ces observations montrent donc non seulement qu'il y a des
biographies individuelles hors des articles de Géographie (dans la Philosophie),
mais qu'en plus il n'y avait pas non plus de «tabou» biographique que Jaucourt
aurait enfreint seul et de son propre chef.

[^defense]: «À ceux qui l’auraient désirée moins sèche : qu’il était nécessaire
    de s’en tenir à la seule connaissance géographique des villes qui fût
    scientifique, à la seule qui nous suffirait pour construire de bonnes cartes
    des temps anciens» (L'Encyclopédie, T5, p.635)

Philosophe | Vedette             | Tome | Page | #article
-----------|---------------------|-----:|-----:|---------:
Bacon | BACONISME ou PHILOSOPHIE DE BACON | 2 | 8 | 92
Campanella | CAMPANELLA | 2 | 576 | 5039
Jérôme Cardan | CARDAN | 2 | 675 | 5624
René Descartes | CARTÉSIANISME | 2 | 716 | 5851
Giordano Bruno | JORDANUS BRUNUS, \textsc{Philosophie de} | 8 | 881 | 3608
Leibnitz | LÉIBNITZIANISME ou PHILOSOPHIE DE LÉIBNITZ | 9 | 369 | 1730
John Locke | LOCKE, \textsc{Philosophie de} | 9 | 625 | 2934
Platon | PLATONISME ou \textsc{Philosophie de Platon} | 12 | 745 | 3325
Pythagore | PYTHAGORISME ou \textsc{Philosophie de Pythagore} | 13 | 614 | 2470

: Coordonnées d'articles de philosophie de l'*EDdA* contenant des éléments biographiques \label{table:edda_biographies_in_philosophy}

Puisque les biographies de l'*EDdA* ne sont pas identifiées explicitement, il
n'est pas possible d'en dresser une liste exhaustive et de dire avec certitude
qu'une biographie est absente de ses pages, mais il semble bien plus difficile
d'en trouver ailleurs qu'en Philosophie. En reprenant les noms du Discours
Préliminaire, on remarque que pour chaque philosophe de la liste ci-dessus,
l'article ayant le plus grand nombre d'occurrences de ce nom (ou au moins un des
plus grands comme pour Bacon où il arrive «seulement» au 3^ème^ rang)
contiennent l'article qui traite conjointement de sa philosophie et de sa vie.
En appliquant la même heuristique à Boërhaave, Boyle et Huyghens les résultats
sont différents. L'article ayant le plus d'occurrences de Boërhaave (plus
précisément de `[word="Bo[eë]rhaave"]`, la requête [@=CQL] utilisée pour ramener
ces résultats[^boerhaave]) est l'article de Médecine CRISE (L'Encyclopédie, T4,
p.471) qui ne le mentionne que pour ses contributions sur le sujet (avec 29
occurrences), mais le deuxième avec le plus de mentions est un article de
Géographie signé par Jaucourt: l'article VOORHOUT (L'Encyclopédie, T17, p.468)
qui offre un nouvel exemple de biographie «complète» dans un article prétexte
comme l'était WOLSTROPE. Le nom de Boërhaave apparaît dès la première phrase en
termes laudatifs et l'intégralité de l'article se partage entre les détails de
sa vie personnelle et professionnelle. Au lieu que sa vie soit racontée dans un
article sur ses contributions principales au domaine comme c'était le cas pour
les philosophes, sa biographie est «déguisée» en article de Géographie.

[^boerhaave]: l'orthographe des noms propres peut parfois varier dans les pages
    de l'*EDdA* surtout quand il y a des diacritiques

Pour Boyle et Huyghens, les articles avec le plus grand nombre d'occurrences de
leur noms sont purement techniques et portent exclusivement sur leurs
thématiques de recherches, respectivement la Chimie — AIR (L'Encyclopédie, T1,
p.225) et CHYMIE (L'Encyclopédie, T3, p.408) — et la Physique et les
Mathématiques — «Figure de la Terre» (L'Encyclopédie, T6, p.749), CYCLOIDE
(L'Encyclopédie, T4, p.590), PENDULE (L'Encyclopédie, T12, p.293) et «Ressort
spiral» (L'Encyclopédie, T14, p.188). Mais, en sélectionnant dans la liste des
résultats non plus les articles ayant le plus d'occurrences mais ceux en
Géographie, émergent alors les articles LISMORE (L'Encyclopédie, T9,
p.574) — ville d'origine de Boyle — et «HAIE (la)» (L'Encyclopédie, T8,
p.23) — celle de Huyghens. Les deux sont également de Jaucourt mais à la
différence de VOORHOUT il s'agit plutôt de biographies «partielles». L'article
sur LISMORE contient un paragraphe entier sur la ville, son importance politique
(faible) et sa position, et même la biographie de Boyle dans le second
paragraphe est introduite par rapport au contexte historique de la ville, ce qui
fait que Boyle occupe strictement moins que la moitié du texte. Quant à celui
sur La Haye, en plus de détailler l'étymologie du nom de la ville, son histoire
et sa place contemporaine dans le pays et l'Europe, il cite non seulement
Huyghens mais également Jacques Golius, Jean Meursius, Frédéric Ruysch, Albert
Henri de Sallengre et Jean Second. L'article s'achève même par une
«biographie-mystère» d'un «monarque célèbre» mais pas nommé, dans laquelle il
n'est pas difficile de reconnaître Guillaume III d'Orange-Nassau. Une fois
encore, si ces trois exemples ne constituent pas une démonstration définitive
que les savants hors de la Philosophie n'ont pas leur biographie dans l'article
disciplinaire décrivant leur doctrine, ils semblent toutefois bien le suggérer,
et si des biographies d'autres savants moins mis en avant dans le Discours
Préliminaire et moins mentionnés en général dans les pages de l'*EDdA* venaient
à être mise au jour dans des articles de spécialité, il s'agirait surtout de
comprendre pourquoi Boërhaave, Boyle et Huyghens n'ont pas eu le même honneur.

En admettant cette inférence, il semble donc y avoir un régime différent pour
les philosophes par rapport aux autres savants. Cela pourrait s'expliquer par
les différences entre disciplines: là où les physiciens travaillent généralement
sur des problèmes variés, les théories philosophiques qui par nature tendent à
décrire le monde dans son ensemble sont plus enclines à former des «systèmes»
caractéristiques de leur auteur et difficiles à séparer d'eux. Il est sans doute
malaisé de bien distinguer aussi nettement les disciplines à une époque de
polymathie encore assez générale mais il semble tout de même y avoir une
importance particulière accordée à la Philosophie. Alors que Newton est surtout
considéré actuellement comme un physicien, sa contribution est particulièrement
valorisée dans les pages de l'*EDdA* et mise en regard de celles de Descartes et
de Leibnitz qu'il a affronté sur certains problèmes. Les trois savants se
retrouvent dans des contextes similaires, et ont des coocurrents très proches:
des mots comme «analyse», «philosophie», «calcul», «principes» et «démontrés» et
même plus directement leurs noms (par exemple `"Newton"` et `"Descartes"` sont
dans les cinq coocurrents les plus forts de `"Leibnitz"` et il en va de même
pour les deux autres combinaisons qu'on peut former de leurs trois noms). Et on
constate même qu'à la différence des autres physiciens il possède lui aussi un
article à son nom: NEWTONIANISME (L'Encyclopédie, T11, p.122), reformulé en
«Philosophie Newtonienne». Il semble que ce terme, bien qu'il soit explicité de
plusieurs façons («physique expérimentale», «mécanique & mathématique», «partie
de la Physique»), soit surtout utilisé pour souligner l'ampleur du saut
conceptuel que représente la démarche de Newton par rapport à ses prédécesseurs.
L'article est très différent des articles de doctrines philosophiques discutés
plus haut ne serait-ce que par son lexique: des mots comme «âme» et «Dieu» qui
apparaissent fréquemment chez les autres philosophes sont absents de l'article
NEWTONIANISME, et le mot «corps» par exemple qui était opposé à âme et discuté
dans un rapport à soi-même chez eux y devient un terme de physique: «corps
physique», «corps céleste», «corps pesans» (sic). Ces différences sont
particulièrement visibles sur le diagramme d'[@=AFC] de la figure
\ref{fig:newton_vs_philosophers} représentant les 65 lemmes les plus fréquents
parmi les textes de doctrines philosophiques (ceux du tableau
\ref{table:edda_biographies_in_philosophy}, le nuage de point rouge sur la
figure) auxquels est rajouté NEWTONIANISME (le point `EDdA_11_679` très
nettement à part sur la figure). Newton est donc mis au même niveau que les
grands philosophes alors que le contenu de ses travaux est tout à fait
différent. La vraie question qui se pose n'est donc pas «pourquoi les
biographies sont-elles cachées dans la Géographie ?» car elles ne le sont pas
toutes, mais bien plutôt «pourquoi celle de Newton n'a-t-elle pas pu figurer
dans son propre article ?». Faut-il y voir un lien avec les reproches faits à
Newton mentionnés dans le Discours Préliminaire et la manière dont il y est
opposé à Descartes, jusqu'à suggérer qu'il était difficile en France de s'en
déclarer partisan, comme a pu le faire Maupertuis «le premier qui ait osé parmi
nous se déclarer ouvertement Newtonien» ?

![Analyse Factorielle des Correspondances pour les articles de systèmes philosophiques dans l'*EDdA*, y compris NEWTONIANISME](figure/textometry/newton_vs_philosophers.png){#fig:newton_vs_philosophers}

### Deux critères utiles

#### Proportion de contenu biographique

Si on revient à l'article WOLSTROPE qui est en grande partie à la base de toutes
les réflexions de cette section, la situation est limpide car Jaucourt ne fait
pas le moindre effort pour déguiser sa biographie: le bourg n'est cité qu'en
relation à Isaac Newton dès la première phrase et les deux seules informations,
lapidaires, que l'on pourrait qualifier de géographiques à son propos tiennent
dans les huit premiers mots, en un syntagme nominal et un complément
circonstanciel: «bourg d'Angleterre, dans le comté de Lincoln».  Aucunes
coordonnées, aucune distance ni même direction à partir d'une autre ville ou
point remarquable du territoire anglais.

Mais la situation est quand même toute autre avec l'article LODEVE
(L'Encyclopédie, T9, p.627). L'article commence par un paragraphe entier qui en
plus de donner une précision d'ordre administrative («un évêché suffragant»)
dresse un rapide historique de la ville depuis son nom latin attesté par Pline
jusqu'à donner le gentilé de ses habitants pour se conclure par un concentré
d'informations sur le paysage et le climat («sec et stérile»), l'activité
économique de la ville («manufactures de draps & de chapeaux») et sa situation
par rapport aux cours d'eau et reliefs («sur la Lergue, au pié des Cévennes»)
avant de la situer en distance par rapport à cinq villes différentes classées
par ordre croissant de distance et d'importance dont les deux dernières avec en
prime une direction sur la rose des vents. Et de se conclure, de surcroît, par
des coordonnées en longitude et latitude, basées vraisemblablement sur le
méridien de l'île de Fer (longitude $\geq$ 20 pour un lieu en France
métropolitaine). C'est quand même se donner beaucoup de peine pour seulement
parler du cardinal de Fleury. D'autant plus que, non seulement la biographie du
cardinal occupe déjà moins de lignes que le seul premier paragraphe de
l'article, mais en plus il n'est pas le seul à être mentionné. Une phrase
présente deux cardinaux nés à Lodève, et chacun a droit a son propre paragraphe.
On est donc manifestement en présence de deux phénomènes très différents.

Parmi les articles cités par @vigier_articles_2022, SOULIERS (L'Encyclopédie, T15,
p.406) est bien plus proche du modèle de WOLSTROPE quoique la première phrase
réussisse à s'achever avant de citer Antoine Arena alors que VALOGNE
(L'Encyclopédie, T16, p.824) bien qu'il soit beaucoup plus clairement consacré à
Jean de Launoi que LODEVE ne peut l'être au cardinal de Fleury commence tout de
même par un paragraphe géographique complet. Dans le même cas, on peut également
citer l'article NICE (L'Encyclopédie, T11, p.131) qui accorde une très large
place à Cassini mais seulement après avoir parlé de la ville elle-même. Il
semble donc y avoir au moins un spectre continu de possibilités entre les deux
tendances extrêmes en termes de proportion de contenu que représenteraient
WOLSTROPE — biographie «complète» — et LODEVE — biographie «partielle» — qui
constitue un premier critère pour caractériser les biographies de l'*EDdA*.

#### Motivation de l'article

Les biographies «cachées» dans la Géographie rendent la navigation
particulièrement difficile puisqu'elles sont inaccessibles à moins de connaître
une «clef» particulière: le lieu de naissance de la personne recherchée. Pour en
trouver, une stratégie consiste à penser à des personnages historiques dont on
espère qu'il pourrait y avoir une biographie, à trouver leur lieu de naissance
dans une autre source et à consulter l'article du lieu en question. On trouve
ainsi par exemple l'article \textsc{Ferté-Milon} (L'Encyclopédie, T6, p.556) qui
est en fait le lieu de naissance de l'auteur Racine (figure
\ref{fig:edda_ferte_milon}). En plus de faire étrangement écho à la remarque
ci-dessus sur la prédominance de la Philosophie — on reproche quand même à
demi-mot à Racine de n'avoir été «que» poête et pas philosophe — l'article très
bref signé par Jaucourt pose un défi d'interprétation. En effet, l'auteur semble
dire que le seul intérêt de la ville — et par là même de l'article — réside dans
la mention de Racine («uniquement remarquable par»). Mais il est dit bien peu
de choses sur le tragédien: sa date de mort et l'âge qu'il avait alors, le
domaine dans lequel il a exercé et le succès qu'il y a rencontré. Même ces 4
lignes qui précèdent la remarque lui reprochant de ne pas avoir fait de
philosophie ne parviennent pas à lui être entièrement consacrées puisqu'il y est
également fait mention de Corneille. Sur les sept lignes et demie que comprend
l'article, cela ne fait donc qu'une grosse moitié du volume de l'article
réellement dédiée à Racine. Le critère portant sur la proportion de l'article
dédiée à la biographie est donc d'une utilité très limitée à l'échelle d'un
article aussi court.  Jaucourt parvient quand même à glisser des coordonnées
géographiques avant sa signature, on pourrait presque se demander s'il lui
importe de donner de l'information sur le lieu, malgré sa remarque initiale
dépréciative sur l'intérêt de ce dernier.

![Article "Ferté-Milon" *EDdA*, T6](figure/text/EDdA/ferté-milon_t6.png){#fig:edda_ferte_milon width=60%}

Ce trait dépréciatif se rencontre d'ailleurs dans plusieurs biographies cachées
dans la Géographie par exemple dans VOORHOUT «village de Hollande […] mais
village illustré» (l'opposition avec la conjonction de coordination «mais»
semble montrer que village revêt ici un caractère négatif) ou dans
\textsc{Villa-nueva} (L'Encyclopédie, T17, p.274) «qui n'est connu que pour
avoir donné la naissance à Michel Servet» dans une autre biographie complète
cachée, qui ne parle en fait pas d'autre chose que de ce personnage historique.
D'ailleurs, les recherches avec la stratégie illustrée ci-dessus pour Racine
montrent assez vite qu'il est d'autant plus probable de trouver une biographie
complète que le lieu de naissance de la personne est petit. Une ville de très
grandes dimensions comme Paris est bien sûr le lieu de naissance de nombreux
personnages historiques importants, mais ce fait ne permet pas d'accéder à leurs
biographies, car l'article Paris pourtant long de 23 272 mots n'en contient
aucune ! Après avoir écarté l'évidence dans son deuxième paragraphe «Elle a
produit plus de grands personnages […] que toutes les autres villes de France
[…]», l'article passe sous silence le fait qu'il n'en donnera pas. Une
explication parfois avancée pour la présence de biographies dans les articles de
Géographie est une tendance de Jaucourt à laisser son érudition prendre la
place: c'est pourtant lui qui signe cet article, et qui y consacre le dernier
quart (600 lignes !) pour dresser un parallèle entre Paris et l'Athènes antique.
L'érudition, le style personnel de Jaucourt et la simple longueur des articles
ne suffisent donc pas à prédire la présence de passages biographiques.

En essayant de trouver un juste milieu entre la Ferté-Milon et Paris, pour
regarder des villes de taille intermédiaire, on trouve alors très facilement un
grand nombre de biographies : l'entrée pour TOURS (L'Encyclopédie, T16, p.490)
en contient 8 dont deux frères qui sont décrits indépendamment, celle pour
TROYES (L'Encyclopédie, T16, p.719) en contient 9 sans liens entre les
personnes, ROUEN atteint le total de 24. À chaque fois le motif est semblable:
une suite de biographies succintes sans liens entre elles et présentée
implicitement comme un passage obligé des articles («je passe aux simples hommes
de lettre natifs de», «l'abondance m'oblige de m'arrêter à cette liste», «je ne
me propose que d'indiquer ici les principaux»), ce qui est renforcé par l'usage
fréquent de l'ordre alphabétique. Ce que disent en négatif ces énumérations,
c'est qu'elles sont là parce que le lectorat de ces articles s'attendent à les
trouver, plus que pour l'intérêt de la vie individuelle de chaque personne. Ce
qui ne signifie pas qu'elles doivent être anecdotiques: les exemples de la
section \ref{sec:biography_domains} contiennent des savants majeurs de leurs
domaines cités brièvement dans les villes qui les ont vu naître: même Leibnitz,
dont la vraie biographie est à l'article LÉIBNITZIANISME, est mentionné dans
l'article LEIPSIC, en même temps qu'une remarque éclairante sur la motivation
profonde de ces biographies: «Leibnitz seul auroit suffi pour donner du relief à
Leipsic sa patrie». Ces énumérations sont là pour l'anecdote, elles ornent,
donnent davantage d'intérêt aux lieux traités. En cela elle font partie du
discours géographique de l'époque.

Toutes ces remarques convergent donc pour dégager un dernier critère à appliquer
aux biographies trouvées dans les articles de Géographie: celui de la motivation
«biographique» ou «intrinsèque» de l'article, c'est-à-dire à quel point il ne
doit son existence qu'à la personne dont il contient la biographie ou bien au
contraire à quel point le passage biographique ne fait qu'apporter une
information à propos d'un endroit sur lequel il y aurait eu de toute façon une
entrée. La figure \ref{fig:biography_criteria} illustre les deux critères
dégagés ci-dessus en plaçant les articles les plus emblématiques discutés au fil
de cette section. Chaque axe de la figure correspond à un des critères: celui
des abscisses à la proportion de contenu biographique dans l'article calculée
comme le rapport du nombre de lignes consacrées à des personnages historiques
sur le nombre total de lignes de l'article; celui des ordonnées à une note
quelque peu arbitraire tâchant de rendre compte des arguments discutés pour
chaque article, notamment de la quantité et de la précision du contenu
géographique, des remarques dépréciatives éventuelles des auteurs, du nombre de
biographies présentes dans le même article et de l'importance relative de la
ville dont il est possible de se faire une idée par exemple en cherchant le
nombre d'occurrences dans l'ensemble du texte de l'*EDdA*.

![Visualisation d'articles de l'*EDdA* discutés dans la section suivant les deux critères identifiés](figure/biography_criteria.png){#fig:biography_criteria width=80%}

S'il ressort donc de ce qui précède que la Géographie dans l'*EDdA* a
effectivement servi dans une certaine mesure à abriter des textes purement
biographiques pour diverses raisons en revanche il apparaît clair que dans le
contexte de l'époque, une certaine part de contenu biographique était au moins
acceptable sinon attendue dans un article de Géographie. C'est d'ailleurs
peut-être ce fait qui a rendu possible l'opération de dissimulation des
biographies: en effet, comment celles-ci auraient pu être cachées dans les
articles de Géographie si elles avaient choqué par leur présence ?

La situation est tout autre à la fin du XIX^ème^ siècle dans les pages de *LGE*
où les biographies ont leur place attitrée. Isaac Newton a alors son propre
article «NEWTON (Sir Isaac)» (La Grande Encyclopédie, T24, p.1048) dans lequel
il est simplement fait mention de son lieu de naissance, désormais orthographié
«Whoolstorpe» et sans article. Un autre «article-prétexte», celui de Boërhaave,
suit le même chemin et fait l'objet d'une entrée (La Grande Encyclopédie, T7,
p.42) qui mentionne «Woorhout», sans entrée associée. Pour les autres articles,
on constate que VALOGNES (La Grande Encyclopédie, T31, p.683) ne cite plus Jean
de Launoi alors que que LODÈVE (La Grande Encyclopédie, T22, p.404) retient une
simple mention du cardinal de Fleury à l'issu d'un paragraphe sur les bâtiments
religieux de la ville, sans date ni caractère narratif. Pour une ville de taille
intermédiaire comme TROYES (La Grande Encyclopédie, T31, p.432), une section
\textsc{Grands Hommes} est présente, au même niveau que les sections
\textsc{Monuments}, \textsc{Histoire} et \textsc{Évêques de Troyes}. Cette
partie se trouve à la toute fin de l'article, juste avant la bibliographie,
confirmant son caractère plus pittoresque. Elle consiste en une énumération de
noms et d'années de décès, parfois sans même de titre ni de profession. Évoquer
les personnes qui ont vécu à un endroit donné reste donc une des fonctions
attendues de la Géographie dans *LGE*, tandis qu'une frontière plus nette se
dessine entre la biographie en tant que telle et ce qui n'est plus qu'anecdote.

