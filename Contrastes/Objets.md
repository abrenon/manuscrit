## Les objets de la Géographie {#sec:contrasts_objects}

Le but de cette section est de déterminer de quoi il est question quand on parle
de Géographie. À quels sujets s'intéresse la discipline ? Quels concepts
met-elle en jeu ? Il est remarquable à ce sujet de constater par exemple que
l'article «CHEMIN, ROUTE, VOIE» (*EDdA*, T3 article 1353) qui décrit
(historiquement) des voies de communication et les situe par rapport à des
villes, des axes important de territoires, notamment des fleuves, ne relève pas
de la géographie comme une lecture moderne des termes pourrait le laisser
supposer mais de la grammaire. Comme le suggère la vedette composée de trois
synonymes, son désignant est bien «Gram. Synon.», «Grammaire» et «Synonymie» ce
qui donne l'idée d'une géographie encore constituée par un ensemble de faits et
de données sur les lieux mais sans intérêt pour les moyens de communication ou
bien sans réflexion théorique sur elle-même.

### Notion de mot-classifieur

```cqp
<text>[word="\*"]? [word!="\*"] [type!="(N|V)"]* [type="N" & (lemma="(fleuve|torrent|ruisseau|lac|marais|étang|baie|golfe)" | word="rivieres?")]
```

-> en fait marche plus du tout, c'était l'annotation Perdido que j'ai plus dans
TXM

~~~cqp
<text> [word="[*«]"]? [word="(\w|[-'.;:ÀàÊêÉéÊêÈèËëÔôÖöÛûÜüÎîÏïÇç])+"] []* [pos="NOUN" & word=".*[^.]"]
~~~

ramène 72858 lignes

=> aurait bien aidé pour le corpus Parallèle

### Concepts mobilisés

- quels sont-ils ?
- comparaison / analyse
- lesquels ont un article ?

