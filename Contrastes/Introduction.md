Ce dernier chapitre qui s'ouvre propose une méthodologie fondée sur une analyse
du corpus à différents niveaux de granularité pour montrer la pertinence des
données préparées dans le cadre de cette thèse. Le chapitre \ref{sec:corpus}
fournit un corpus structuré, annoté en syntaxe et développe la notion de domaine
de connaissance, centrale pour la classification qui est appliquée aux articles
au chapitre \ref{sec:domains_classification}. Ces efforts amènent deux
directions de travail pour le présent chapitre.

En gardant bien à l'esprit les réserves émises à l'Introduction sur la
différence entre d'une part la Géographie en tant que discipline et d'autre part
les discours qui peuvent relever de la géographie (voir \ref{sec:intro_strategy}
p.\pageref{sec:intro_strategy}), la conduite d'analyse contrastives à l'échelle
des articles permet de mettre en lumière des différences en diachronie entre
l'*EDdA* et *LGE* ainsi qu'entre domaines de connaissances. Une étude plus fine
des textes fondée sur des phénomènes au niveau des phrases, notamment
syntaxiques, révèle des mélanges thématiques au sein des articles et permet de
préciser la place des discours géographiques.

La première étape de cette étude consiste à estimer les variations de la place
accordée à la discipline géographique dans les œuvres du corpus au travers de
mesures des articles de *Géographie* aux XVIII^ème^ et XIX^ème^ siècles. La
section \ref{sec:contrasts_objects} s'intéresse ensuite au vocabulaire des
concepts manipulés dans les articles de *Géographie*. Enfin la section
\ref{sec:biographies} étudie le cas de la relation particulière qu'entretiennent
géographie et biographies.

