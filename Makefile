DOCUMENT = Manuscrit
HEADER = $(DOCUMENT).yml
CHAPTERS = Introduction ÉdlA Corpus Classification Contrastes Conclusion
SOURCES = $(CHAPTERS:%=%/text.sh)
BIBLIOGRAPHY = biblio.bib
GLOSSARY = Glossaire
SNIPPETS = $(wildcard src/*.md)
GRAPHS = $(wildcard src/*.gv)
ALL_MARKDOWN = find $(CHAPTERS) -type f -name '*.md' -exec cat '{}' \;
FIGURE_FILES = $(shell $(ALL_MARKDOWN) | sed -n 's@.*[({]\(figure/.*.\(png\|jpe?g\)\)[)}].*@\1@p')
TABLE_FILES = $(shell $(ALL_MARKDOWN) | sed -n 's@.*\input{\(table/.*.tex\)}@\1@p')
WITH_STRUCTURE = $(foreach F,$(1),$(dir $(F))) $(1)
FIGURES = $(call WITH_STRUCTURE,$(FIGURE_FILES))
TABLES = $(call WITH_STRUCTURE,$(TABLE_FILES))
CSL = apa.csl
CUSTOM_FILTER = filter/with-glossary
FILTERS = pandoc-fignos $(CUSTOM_FILTER)
LUA_FILTERS = ./filter/with-bibliography.lua ./filter/with-folio.lua
WITH_FILTERS = $(FILTERS:%=--filter %) $(LUA_FILTERS:%=--lua-filter %)
PANDOC_OPTIONS = --pdf-engine=xelatex
FIGURE_GEOPYCK_OPTIONS = --cmap Purples --maxWidth 13
CONFUSION_MATRIX = geopyck drawMatrix $(FIGURE_GEOPYCK_OPTIONS)

DEPENDENCIES=$(CUSTOM_FILTER) $(HEADER) $(FIGURES) $(TABLES) $(BIBLIOGRAPHY)

.SECONDEXPANSION:

sources = $(1)/text.sh $(shell find $(1) -type f -name '*.md') $(shell find $(1) -type f -name '*.md' -exec sed -n 's|.*\[@=\([^]]\+\)\].*|\1|p' '{}' \; | sort -u | xargs -I XXX echo $(GLOSSARY)/XXX.md)
chapter-sources = $(call sources,$*)

JOIN = <(hammer join -O ARTICLES $(1))
> = $(wordlist 2,$(words $^),$^)
WITH_METADATA = $(call JOIN,$(word 2,$(1)) <($(SELECT) $(ARTICLES),$(WORDS),$(DOMAIN) $(word 1,$(1))))
METADATA = $(call WITH_METADATA,$^)

SELECT = cut -d$$'\t' -f # valid as long as there's no \t in the cells
ARTICLES = 1-3
NAME = 5
WORDS = 9
DOMAIN = 12
GROUPED_DOMAIN = 13

all: $(DOCUMENT).pdf

$(CHAPTERS:%=%.pdf):

$(DOCUMENT).pdf: $(DOCUMENT).sh $(foreach chapter,$(CHAPTERS),$(call sources,$(chapter))) $(DEPENDENCIES)
	./$(DOCUMENT).sh | pandoc $(PANDOC_OPTIONS) $(WITH_FILTERS) -o $@

%.pdf: %/text.sh $${chapter-sources} $(DEPENDENCIES)
	$< | pandoc $(PANDOC_OPTIONS) $(WITH_FILTERS) -o $@

figure/%.png: src/%.gv
	dot -Tpng $< -o $@

figure/%.pdf: src/%.md
	pandoc --highlight-style src/highlight-style.theme $< -o $@

figure/%.png: generator/%.py
	$< $@

figure/classification/%.png: data/classification/%.csv
	$(CONFUSION_MATRIX) $< $@

figure/classification/%.png: data/classification/%.tsv
	$(CONFUSION_MATRIX) $< $@

data/matrix/%/confusionMatrix.json: data/classification/%/labels.txt data/classification/%/results.tsv
	geopyck confusionMatrix --labels $< $> $@

# The trailing '0' underneath forces make to generate the Nearest Neighbours
# matrix in a second step and it works because 10, 50, and 100 all end with a 0
data/matrix/lexicalSimilarity/%0.json: data/topNGrams/$$(*D)/
	F="$(*F)0"; geopyck lexicalSimilarities $< --labels data/classification/domainGroup.txt --top "$${F##*_top}" --metric "$${F%%_*}" $@

data/topNGrams/%_common.tsv: data/topNGrams/$$(*D)/
	F="$(*F)"; ./visualisation/CommonNGrams.py $< "$${F%%_*}" "$${F##*_}" $@

%NN.json: %.json
	geopyck confusionMatrix --nearestNeighbour $< $@

%.json: $$(*D).json
	F="$(*F)"; ./visualisation/IterateMatrix.py $< "$${F##power}" $@

figure/matrix/%.png: data/matrix/%.json
	$(CONFUSION_MATRIX) $< $@

figure/graph/%.png: data/matrix/%.json
	geopyck graph $(FIGURE_GEOPYCK_OPTIONS) $< $@

figure/classification/%.png: data/classification/%/results.csv data/classification/%/labels.txt
	$(CONFUSION_MATRIX) $< --labels data/classification/$*/labels.txt $@

figure/classification/%.png: data/classification/%/results.tsv data/classification/%/labels.txt
	$(CONFUSION_MATRIX) $< --labels data/classification/$*/labels.txt $@

figure/classification/parallelCorpus.png: data/corpus/Parallel/pairs.tsv
	$(CONFUSION_MATRIX) <(sed -e '0,/domain_EDdA/{s//truth/}' -e '0,/domain_LGE/{s//answer/}' $<) $@

data/corpus/domainGroup_frequencies.tsv: data/corpus/metadata.tsv
	./visualisation/SortedFrequencies.py <($(SELECT) $(ARTICLES),$(GROUPED_DOMAIN) $<) $@ domainGroup

%_frequencies.tsv: $$(subst _frequencies,,$$(*)).tsv
	./visualisation/SortedFrequencies.py $< $@ domainGroup

%/centralities.tsv: %/confusionMatrix.json
	./visualisation/EigenvectorCentrality.py $< $@

figure/histogram/%.png: data/%.tsv
	./visualisation/BarPlot.py $< $@

%/results.tsv: data/corpus/metadata.tsv %/predictions.tsv
	sed '1 s/domain/truth/' $(METADATA) > $@

.PRECIOUS: %/
%/:
	mkdir -p $@

.PRECIOUS: %/falseNegatives.tsv
%/falseNegatives.tsv: $$(*D)/results.tsv
	D="$(*D)"; csvsql -t --query "SELECT answer as $${D##*_} FROM results WHERE truth = '$(*F)' AND answer != truth" $> | csvformat -T > $@

.PRECIOUS: %/falsePositives.tsv
%/falsePositives.tsv: $$(*D)/results.tsv
	D="$(*D)"; csvsql -t --query "SELECT truth as $${D##*_} FROM results WHERE answer = '$(*F)' AND answer != truth" $> | csvformat -T > $@

data/corpus/GEODE.tsv: data/corpus/metadata.tsv
	$(SELECT) $(ARTICLES) $< > $@

data/corpus/%.tsv: data/corpus/$$(*D).tsv
	grep "^\(work\|$(*F)"$$'\t'"\)" $< > $@

data/corpus/Parallel/pairs.tsv: data/corpus/metadata.tsv data/corpus/Parallel/EDdA.tsv data/corpus/Parallel/LGE.tsv
	hammer join -O name $(foreach tsv,$>,$(call JOIN,$(tsv) <($(SELECT) $(ARTICLES),$(NAME),$(DOMAIN) $<))) -r '%s_EDdA,%s_LGE' -o $@

data/corpus/Parallel/geo_in_%.tsv: data/corpus/Parallel/pairs.tsv
	csvsql -I -t --query "SELECT domain_EDdA as 'from', domain_LGE as 'to' FROM pairs WHERE domain_EDdA $(if $(subst LGE,,$*),=,!)= 'Géographie' AND domain_LGE  $(if $(subst LGE,,$*),!,=)= 'Géographie'" $< | csvformat -T > $@

table/LGE_evaluation.tex: data/classification/LGE_evaluation.tsv
	./visualisation/Repartition.py $< $@ score domain Précision

figure/repartition/corpus/%.png: data/corpus/%.tsv
	./visualisation/Repartition.py $< $@ count domain

figure/repartition/corpus/%.png: data/corpus/metadata.tsv data/corpus/$$(*D).tsv
	./visualisation/Repartition.py $(METADATA) $@

figure/entities/%_density.png: data/corpus/ENE/%.tsv data/corpus/metadata.tsv data/corpus/GEODE/%.tsv
	./visualisation/EntitiesDensity.py $< $(call WITH_METADATA,$>) $@

figure/distribution/%.png: data/corpus/metadata.tsv data/corpus/$$(*D).tsv
	F=$(*F); ./visualisation/SizeDistribution.py $(METADATA) $${F#*by_} $${F%_by*} $@

figure/evolution/%.png: data/corpus/%.tsv
	./visualisation/Evolution.py $< $@

table/%.tex: data/corpus/metadata.tsv data/corpus/$$(*D).tsv
	./visualisation/Repartition.py $(METADATA) $@

table/GEODE/averageWordLengths.tex: data/corpus/metadata.tsv data/corpus/GEODE/EDdA.tsv data/corpus/GEODE/LGE.tsv
	./visualisation/AverageWordLengths.py $(foreach tsv,$>,$(call JOIN,$(tsv) $<)) $@

table/%.tex: data/%.tsv
	./visualisation/LaTeXTable.py $< $@

%.png: %.pdf
	pdftocairo -png -singlefile -r 400  $< -o $(basename $@)

filter/%: filter/%.hs
	ghc --make $^
