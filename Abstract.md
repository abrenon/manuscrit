Dans le cadre du projet GEODE du LabEx ASLAN, cette thèse étudie en diachronie
les discours géographiques dans deux encyclopédies françaises. L'*Encyclopédie,
Dictionnaire Raisonné des sciences, des arts et des métiers* (*EDdA*),
emblématique du siècle des Lumières, constitue la borne la plus ancienne de
l'intervalle. Celui-ci s'étend jusqu'à la fin du XIX^ème^ siècle, représenté par
*La Grande Encyclopédie, Inventaire raisonné des Sciences, des Lettres et des
Arts* (*LGE*). Au travers de trois contributions principales, les travaux
abordent des thématiques telles que la préparation des données et l'encodage
en particulier, la classification automatique et les analyses textométriques.

En confrontant des méthodes simples de la théorie des graphes à des remarques
structurelles sur les éléments présents dans les encyclopédies, la première
étude souligne les différences fondamentales qui existent entre dictionnaires et
encyclopédies en termes de structures et aboutit à la proposition d'un encodage
XML-TEI pour représenter une encyclopédie, qui est appliqué sur la première
édition numérique de *LGE*.

La comparaison de méthodes de classifications permet ensuite de choisir un
modèle pour associer un domaine de connaissance à chaque article du corpus, ce
qui rend possible la conduite d'analyses contrastives dans la dernière partie de
la thèse. Un examen attentif des erreurs commises par un des classifieurs sur
l'*EDdA* révèle des ressemblances existant entre les domaines et montre une
Géographie très au contact des autres sciences au XVIII^ème^ siècle.

Pour finir, les deux œuvres sont comparées pour mettre en relief les changements
survenus dans la manière d'écrire la Géographie. Un premier développement
quantifie puis caractérise la variation dans les articles du domaine (à l'aide
de statistiques sur le nombre de mots et leurs longueurs puis en étudiant les
mouvements d'entrées entre domaines) Les apports de la textométrie sont enfin
utilisés pour approfondir la compréhension des liens entre biographies et
discours géographiques.
