(use-modules ((gnu packages python-build) #:select (python-setuptools))
             ((gnu packages python-science) #:select (python-pandas))
             ((gnu packages python-xyz) #:select (python-matplotlib python-seaborn))
             ((guix build-system python) #:select (python-build-system))
             ((guix build-system python) #:select (pypi-uri))
             ((guix download) #:select (url-fetch))
             ((guix licenses) #:select (gpl3))
             ((guix packages) #:select (base32 origin package)))

(package
  (name "python-pysankey")
  (version "0.0.1")
  (source
   (origin
     (method url-fetch)
     (uri (pypi-uri "pySankey" version))
     (sha256
      (base32 "1h3lnkg9az8b16nsi2b91d312wgqg4m4nri6ganq6s4j88if0l0c"))))
  (build-system python-build-system)
  (native-inputs (list python-setuptools))
  (propagated-inputs (list python-matplotlib
                       python-pandas
                       python-seaborn))
  (arguments
   (list #:tests? #f))
  (home-page "https://github.com/anazalea/pySankey")
  (synopsis "Make simple, pretty Sankey Diagrams")
  (description "Make simple, pretty Sankey Diagrams")
  (license #f))
