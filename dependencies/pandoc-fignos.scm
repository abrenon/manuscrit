(use-modules ((gnu packages python-xyz) #:select (python-psutil))
             ((gnu packages textutils) #:select (python-pandocfilters))
             (guix build-system python)
             ((guix download) #:select (url-fetch))
             ((guix licenses) #:select (gpl3))
             (guix packages))

(let
  ((python-pandoc-xnos (load "pandoc-xnos.scm")))
  (package
    (name "python-pandoc-fignos")
    (version "2.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pandoc-fignos" version))
        (sha256
          (base32 "0jc8glwkhwxi4qc3jh1ssgvrw2jhf9gxv470kwp42948wkmfsn3h"))))
    (build-system python-build-system)
    (propagated-inputs (list python-pandoc-xnos))
    (home-page "https://github.com/tomduck/pandoc-fignos")
    (synopsis "Figure number filter for pandoc")
    (description "Figure number filter for pandoc")
    (license gpl3)))
