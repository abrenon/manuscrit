[ -n "${HEADER_INCLUDED}" ] || source ./header.sh 2

echo "# ${1}"
echo '\etocsettocstyle{\rule{\linewidth}{\tocrulewidth}\vskip0.5\baselineskip}{\rule{\linewidth}{\tocrulewidth}}'
echo '\localtableofcontents'
echo '\pagestyle{regularchapter}'
