*Traitement Automatique des Langues*, est la partie de l'[@=IA] qui s'intéresse
au langage. Il consiste à utiliser un ordinateur pour effectuer des tâches sur
des textes écrits en langues naturelles comme par exemple leur traduction dans
une autre langue, l'écriture de résumé ou, d'une manière générale l'extraction
d'information sémantique comme les chaînes de coréférence, les entités nommées
ou les émotions exprimées par un texte.
