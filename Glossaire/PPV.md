le problème des *K Plus Proche Voisin* (*KNN* pour *K Nearest Neighbors* en
anglais) est un problème général d'[@=IA] consistant à trouver les points les
plus proches d'un point donné dans un espace. D'une grande complexité dans le
cas général, de nombreuses techniques permettent d'accélérer sa résolution, qui
trouvent des applications des réseaux à la vision par ordinateur. Dans cette
thèse, la notion de *PPV* est utilisée pour simplifier des matrices afin de
tracer des graphes exploitables visuellement.
