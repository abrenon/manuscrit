*Intelligence Artificielle*, terme parapluie traduisant davantage un projet
qu'une réalité mesurable objectivement, est l'ensemble des techniques permettant
à un ordinateur de produire un comportement assez complexe pour résoudre des
tâches traditionnellement liées aux capacités intrinsèques de l'espèce humaine:
langage, reconnaissance d'image, généralisation et adaptation à de nouvelles
situations.
