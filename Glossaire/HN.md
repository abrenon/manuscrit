*Humanités Numériques* ou *Digital Humanities* en anglais, sont la rencontre
d'approches et de méthodes issues des technologies de l'information et des
problématiques traditionnellement réservées aux sciences dites «humaines et
sociales», aux arts et aux lettres. Le terme intègre le souhait que cette
rencontre soit, plus qu'une simple application technique, un véritable dialogue
entre disciplines.
