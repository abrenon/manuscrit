*Optical Layout Recognition*, reconnaissance optique de la disposition de la
page, est le procédé par lequel un logiciel découpe une page en blocs
géométriques, afin de pouvoir distinguer les parties de texte, les images et les
données tabulaires, permettant à terme d'inférer un ordre de lecture. Cette
étape est préalable à l'OCR, qu'elle «guide» en quelque sorte vers les zones de
la page où il est susceptible de trouver du contenu à reconnaître. Le problème
fondamental sous-jacent que tente de résoudre un système d'OLR est celui du
passage de la page — objet bidimensionnel qu'un humain peut parcourir à loisir
dans le sens qu'il souhaite — au fichier — abstraction intrinsèquement linéaire
des systèmes informatiques et dont toutes portions distinctes peuvent être
ordonnées, l'une étant *avant* l'autre.

