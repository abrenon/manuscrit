en français «modélisation de thème/sujet» est un ensemble de techniques
d'apprentissage non-supervisé qui permettent de faire émerger des thématiques
d'un corpus en opérant des regroupements entre les différents textes qui le
composent.
