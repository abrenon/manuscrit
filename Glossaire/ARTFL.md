l'American and French Research on the Treasury of the French Language
(«Recherche américaine et française sur les trésors de la langue française») est
une collaboration du laboratoire ATILF, du CNRS et de l'Université de Chicago.
Le projet donne accès à un vaste corpus de textes anciens en français.
[https://artfl-project.uchicago.edu/](https://artfl-project.uchicago.edu/).
