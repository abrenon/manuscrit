*Universal Dependencies*, sont une convention d'annotation grammaticale
comprenant des jeux d'étiquettes à plusieurs niveaux (morphosyntaxe, morphologie
et syntaxe en dépendance) conçu pour être commun à un grand nombre des langues
humaines les plus étudiées, ce qui permet notamment de pouvoir travailler sur
des corpus multilingues
([https://universaldependencies.org/](https://universaldependencies.org/)).
