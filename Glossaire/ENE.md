*Entité Nommée Étendue* désigne un groupe de mots structuré en une construction
qui dépasse le cadre des [@=EN] pour inclure les éléments présents autour:
principalement des noms communs et des relations. Les ENE se bâtissent
récursivement, pouvant inclure d'autres ENE, pour constituer de petits arbres
autours des entités nommées.

Ainsi, «le kiosque dans le parc Sainte-Marie» peut s'analyser comme une ENE de
lieu, constituée d'un nom commun de lieu qui lui donne son type (c'est un
kiosque), d'une relation spatiale «dans» (traduisant l'inclusion), et d'une ENE:
«le parc Sainte-Marie». À son tour, celle-ci se décompose en un nom commun de
lieu (c'est un parc), et un nom propre, «Sainte-Marie», qui sans l'apport des
ENE aurait probablement été la seule [@=EN] simple retenue pour toute
l'expression. Il est intéressant de voir que le nom propre apporte pourtant peu
d'information en lui-même, et qu'il n'est possible de percevoir sa dimension
spatiale qu'avec l'ajout du nom commun «parc» (il existe peut-être par exemple
une «communauté Sainte-Marie» qui pourrait être une organisation et pas un lieu)
ce qui souligne l'utilité des ENE pour la désambiguïsation et les tâches de
compréhension automatique.
