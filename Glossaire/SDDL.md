*Système Dynamique Discret Linéaire*: un système est *dynamique* si une fonction
décrit sont évolution dans le temps. Il est *discret* s'il évolue par états
successifs qui peuvent être indicés par des nombres entiers (par opposition à
*continu* où le temps est représenté par une variable réelle). La résolution
d'un système continu impose parfois de le discrétiser pour le simplifier et
pouvoir approcher son évolution en la modélisant par des petites étapes pendant
lesquels les paramètres sont fixés. Enfin, il est *linéaire* si sa fonction
d'évolution l'est.

En un sens, un *SDDL* est donc la généralisation d'une suite géométrique
($u_{n+1} = r \times u_n$) à un système qui ne peut pas être représenté par un
seul nombre mais nécessite d'en observer plusieurs simultanément, dans un
vecteur $U_n$ dont chaque étape s'obtient par un produit matriciel (opération
linéaire) $U_{n+1} = M \cdot U_n$.
