*Convolutional Neural Network* est une architecture de réseaux de neurones dans
laquelle l'information se déplace dans une seule direction, des entrées vers les
sorties, et avec une interconnexion peu dense entre les premières couches qui
privilégie la localité des intéractions pour aggréger progressivement
l'information. Cette caractéristique les rend particulièrement efficaces pour
traiter des images.
