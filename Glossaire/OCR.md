*Optical Character Recognition*, reconnaissance optique de caractères, est le
procédé par lequel un logiciel extrait du texte, c'est-à-dire une suite de
caractères compréhensibles par la machine et traitables ensuite par des moyens
automatiques, à partir d'une image.

