*Apprentissage Profond* est un sous-domaine de l'[@=AA] comprenant toutes les
architectures de réseaux de neurones dont le grand-nombre de couches permet
l'émergence de comportements complexes utiles pour résoudre des problèmes tels
que le traitement du signal (en particulier l'analyse d'images) et de la langue
naturelle (pour laquelle les *Transformer* dominent l'état de l'art).
