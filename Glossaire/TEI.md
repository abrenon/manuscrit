la *Text Encoding Initiative*, est un consortium fondé en 1994 qui développe et
maintient un ensemble ouvert de recommandations pour encoder des textes
numériques. Ces recommandations définissent un standard, le XML-TEI, qui est un
dialecte du méta-langage de balises XML.
