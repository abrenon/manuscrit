*Apprentissage Automatique* est un domaine scientifique visant à programmer un
ordinateur pour découvrir automatiquement les paramètres clefs dans la
résolution d'un problème, plutôt que de traduire dans son langage des règles
expertes issues de considérations théoriques préalables sur le problème à
résoudre. C'est un sous-domaine de l'[@=IA].
