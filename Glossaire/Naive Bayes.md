ou *classification bayésienne «naïve»* désigne une approche probabiliste des
tâches de classification consistant à apprendre une estimation de la probabilité
qu'un vecteur d'entrée donnée reçoive une certaine classe, en opérant la
simplification consistant à supposer que les coordonnées du vecteur sont
indépendantes entre elles. Le classifieur obtenu associe ensuite à chaque
vecteur d'entrée la classe ayant la plus haute probabilité.
