*Part Of Speech*, couramment abrévié en POS correspond au français «Partie de
Discours» qui est une autre façon de désigner la catégorie morphosyntaxique des
mots d'une phrase. C'est un concept essentiel en textométrie, tant pour formuler
des requêtes que dans l'agrégation des résultats.
