*Recurrent Neural Network* est une architecture de réseaux de neurones
présentant une boucle de rétro-action entre ses entrées et ses sorties ce qui
lui permet de conserver une mémoire des premiers éléments rencontrés et la rend
relativement indépendante du paramètre temporel. Cette caractéristique la rend
particulièrement efficace à traiter des entrées sérielles.
