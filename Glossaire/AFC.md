*Analyse Factorielle des Correspondances* est une technique de visualisation
développée par le mathématicien Jean-Paul Benzécri qui permet d'observer des
mots et les textes dont ils sont issus dans un même espace à deux dimensions
pour observer les proximités thématiques entre eux. Cette représentation est
obtenue à l'aide d'une décomposition matricielle de l'écart entre la
distribution des mots dans les textes observée empiriquement et une distribution
homogène.
