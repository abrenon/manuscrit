*Entité Nommée* (*Named Entity* en anglais) désigne une unité d'information dans
un texte, souvent un mot ou un groupe de quelques mots qui renvoient à un objet
unique dans le contexte. Plus large que la notion de nom propre, il englobe par
exemple les noms de personne, d'organisation ou d'endroit, les dates ou les
grandeurs pourvues d'une unité.
