*Latent Dirichlet Allocation*, procédé de réduction de dimension permettant de
mettre en évidence des classes de ressemblances présentes dans les données
d'entrée. C'est une technique de [@=Topic Modeling].
