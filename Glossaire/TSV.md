*Tab-Separated Values*, «valeurs séparées par des tab(ulation)s» est un format
de données tabulaires (où chaque donnée trouve sa place à l'intersection d'une
colonne et d'une ligne). Comme les autres langages SSV (*Something-Separated
Values*), il possède un caractère d'échappement pour pouvoir inclure le
séparateur (ou ce caractère d'échappement lui-même) dans le contenu de n'importe
quelle cellule.

En pratique tant que les données ne comportent pas le caractère tabulation (ce
qui est le cas pour des métadonnées brèves et souvent numériques) le décodage de
ce format est trivial, nécessitant seulement de découper les lignes suivant les
tabulations. Il faut en revanche se méfier de l'opération inverse, consistant à
produire des fichiers en séparant seulement les cellules par des tabulations
sans prendre en compte le caractère d'échappement (guillemets doubles d'après la
RFC 4180): si une cellule contient ce caractère, alors le fichier obtenu ne
serait plus un TSV valide et ne serait pas lu correctement par les outils pour
ce format (typiquement, des tableurs).
