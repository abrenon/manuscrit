*Corpus Query Langage*, est un langage de requête développé à l'université de
Stuttgart et implémenté dans le Corpus Query Processor (CQP) par l'outil *CWB*
(Corpus Workbench, [https://cwb.sourceforge.io/](https://cwb.sourceforge.io/)).
Il permet de représenter des contraintes sur du texte lemmatisé et annoté en
morphosyntaxe en définissant des contraintes sur différentes dimensions des
tokens (forme, lemme, partie de discours) qui peuvent être ensuite combinées
logiquement et séquentiellement de manière analogue à ce que permettent les
expressions régulières.
