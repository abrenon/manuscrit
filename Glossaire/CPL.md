*Caractères Par Ligne*, désigne en typographie le nombre de caractère en fonte
de largeur fixe que peut afficher un dispositif mécanique ou électronique comme
une machine à écrire ou un télétype. Ce nombre, dont la valeur est généralement
voisine de 80, joue un rôle clef pour comprendre les choix de mise en forme du
texte sur des supports numériques.
