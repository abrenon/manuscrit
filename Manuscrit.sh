#!/bin/sh

. ./header.sh

cat <<EOF

\newgeometry{top=3cm,bottom=2.5cm,inner=3cm,outer=2.5cm}

\input{template/ED.tex}
\pagestyle{specialchapter}

\normalsize
\normalfont
\rmfamily

\chaptermark{Table des Matières}
\tableofcontents
\clearpage
EOF

Introduction/text.sh
ÉdlA/text.sh
Corpus/text.sh
Classification/text.sh
Contrastes/text.sh
Conclusion/text.sh

cat <<EOF

\newpage
\pagestyle{specialchapter}

\listoffigures
\chaptermark{Table des Figures}

\listoftables
\chaptermark{Liste des Tableaux}

EOF
