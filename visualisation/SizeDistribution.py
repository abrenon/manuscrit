#!/usr/bin/env python3

import color
from GEODE import domains, legend, tabular
import matplotlib.pyplot as plot
import seaborn
import sys

defaultFronts = {
        'work': 'Œuvre',
        'domain': 'Domaine',
        'characters': 'nombre de caractères',
        'bytes': 'nombre de caractères',
        'tokens': 'nombre de tokens',
        'words': 'nombre de mots'}

def splitAssoc(s):
    l = s.split('=')
    if len(l) == 1 and l[0] in defaultFronts:
        return (l[0], defaultFronts[l[0]])
    else:
        return (l*2)[:2]

def sizeDistribution(inputTSV, key, metric, outputPath):
    measures = tabular(inputTSV)
    fig = plot.subplots(figsize=(12, 12))
    backKey, frontKey = splitAssoc(key)
    backMetric, frontMetric = splitAssoc(metric)
    short = dict(zip(domains, legend(domains, 13)))
    ax = seaborn.violinplot(data=measures,
                            x=backMetric,
                            y=backKey,
                            formatter=lambda x: short[x],
                            legend=False,
                            hue=backKey,
                            palette=color.ful('domain'),
                            cut=0,
                            order=sorted(measures[backKey].unique()))
    ax.set_xlabel(f'Logarithme du {frontMetric}')
    ax.set_ylabel(frontKey)
    ax.set_xscale('log')
    plot.savefig(outputPath, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    sizeDistribution(*sys.argv[1:])
