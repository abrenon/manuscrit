#!/usr/bin/env python3

from color import highlight, intensity, rainbow
from GEODE import articleKey, eneLabels, legend, tabular
import matplotlib.pyplot as plot
from matplotlib.gridspec import GridSpec, SubplotSpec
from matplotlib.ticker import PercentFormatter
import pandas
import seaborn
import sys

roundPercents = PercentFormatter(xmax=1, decimals=0)

def ticks(l):
    return [i+0.5 for i in range(len(l))]

def count2D(measures, metadata):
    byDomain = measures.merge(metadata, on=articleKey).groupby('domain')
    return byDomain[eneLabels].sum()

def normalise(counts, metadata):
    sizes = metadata.groupby('domain')['words'].agg('sum')
    return pandas.DataFrame({t: counts[t] / sizes for t in counts.columns})

def plotMain(ax, densities):
    cbar_kws = {'use_gridspec':False, 'location':'left', 'anchor': (-3.5, 0.5),
                'format':roundPercents}
    seaborn.heatmap(densities, ax=ax, cbar_kws=cbar_kws, cmap=intensity,
                    xticklabels=legend(densities.columns, 9),
                    yticklabels=legend(densities.index, 13))
    ax.set_ylabel(None)

def plotBar(ax, densities):
    ax.cla()
    ax.grid(axis='y', zorder=0)
    ax.bar(ticks(densities.index), densities, color=highlight, zorder=3)
    ax.spines[['left']].set_visible(True)
    ax.tick_params(axis='x', bottom=False, labelbottom=False)
    ax.yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))

def plotVBar(ax, densities, sigma):
    ax.cla()
    ax.grid(axis='x', zorder=0)
    ax.barh(ticks(densities.index), densities, color=rainbow, zorder=3)
    ax.spines[['bottom']].set_visible(True)
    ax.tick_params(axis='y', left=False, labelleft=False)
    ax.xaxis.set_major_formatter(roundPercents)
    ax.axvline(sigma, color=highlight, zorder=4)
    ax.text(sigma + 0.01, 0, "Σ")

def plotTotal(ax, sigma):
    ax.spines[['top', 'right', 'bottom', 'left']].set_visible(False)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.text(0.1, 0.5, f"Σ = {round(100*sigma,1)}%")

def jointHeatmap(byDomainEvent, byEvent, x, y):
    g = seaborn.JointGrid()
    sigma = byEvent.sum()

    plotBar(g.ax_marg_x, byEvent)
    plotVBar(g.ax_marg_y, byDomainEvent.sum(axis='columns'), sigma)
    plotMain(g.ax_joint, byDomainEvent)

    topRight = GridSpec(1, 1, left=0.85, right=0.95, bottom=0.85, top=0.95)
    plotTotal(g.fig.add_subplot(SubplotSpec(topRight, 0)), sigma)

    g.fig.set_size_inches(6, 8)
    g.fig.subplots_adjust(hspace=0.05, wspace=0.02)

def drawHeatmap(measuresTSV, metadataTSV, outputPNG):
    measures = tabular(measuresTSV)
    metadata = tabular(metadataTSV)
    counts = count2D(measures, metadata)
    jointHeatmap(normalise(counts, metadata),
                 counts.sum(axis='rows') / metadata['words'].sum(),
                 x='event',
                 y='domain')
    plot.savefig(outputPNG, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    drawHeatmap(*sys.argv[1:])
