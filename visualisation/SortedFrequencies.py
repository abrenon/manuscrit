#!/usr/bin/env python3

from GEODE import tabular
import sys

def ticks(l):
    return [i+0.5 for i in range(len(l))]

def sortedHistogram(inputTSV, outputTSV, column):
    rows = tabular(inputTSV)
    groups = rows.groupby(column).size().sort_values(ascending=False)
    groups.name = 'Fréquence'
    groups.to_csv(outputTSV, sep='\t')

if __name__ == '__main__':
    sortedHistogram(*sys.argv[1:])

