#!/usr/bin/env python3

from GEODE import tabular, toTSV
from sys import argv

def load(rootPath, name):
    return tabular(f"{rootPath}/{name}.tsv").rename(columns={'frequency': name})

def commonNGrams(rootPath, name1, name2):
    data1 = load(rootPath, name1)
    data2 = load(rootPath, name2)
    common = data1.merge(data2, on='ngram', how='inner')
    return common.rename(columns={'ngram': 'n-gramme'})

if __name__ == '__main__':
    toTSV(argv[4], commonNGrams(argv[1], argv[2], argv[3]), sortBy=None)
