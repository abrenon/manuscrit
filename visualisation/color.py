from matplotlib.pyplot import cm
from GEODE import domains, eneLabels
from seaborn import color_palette

intensity = 'Purples'
highlight = 'mediumpurple'

def wide_sample(f, size):
    return [f(i/(size-1)) for i in range(size)]

def shift(offset, l):
    return l[offset:] + l[:offset]

rainbow = shift(4, wide_sample(cm.Spectral, len(domains)))

def ful(key):
    if key == 'domain':
        return dict(zip(domains, rainbow))
    elif key == 'event':
        return dict(zip(eneLabels, wide_sample(cm.magma, len(eneLabels))))

def ize(key):
    palette = ful(key)
    if palette is None:
        palette = color_palette(intensity)
        return lambda name: palette[hash(name) % len(palette)]
    else:
        return lambda name: palette[name]
