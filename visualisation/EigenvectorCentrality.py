#!/usr/bin/env python3

from GEODE import toTSV
from GEODE.Visualisation.ConfusionMatrix import getConfusionMatrix
from IterateMatrix import iterate
import pandas
import sys

def eigenVector(inputJSON, outputTSV):
    longEnough = 1000
    m = getConfusionMatrix(inputJSON)
    centralities = pandas.Series(data=iterate(m, longEnough)['matrix'][0],
                                 index=m['labels'],
                                 name='Centralité')
    centralities.sort_values(ascending=False, inplace=True)
    centralities.index.name = 'domainGroup'
    centralities.to_csv(outputTSV, sep='\t')

if __name__ == '__main__':
    eigenVector(*sys.argv[1:])
