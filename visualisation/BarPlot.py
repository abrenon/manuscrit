#!/usr/bin/env python3

from color import highlight
from GEODE import legend, tabular
from math import floor, log
import matplotlib.pyplot as plot
import numpy
from seaborn import barplot
import sys

def ticks(l):
    return [i+0.5 for i in range(len(l))]

def scientificBeyoundE2(x):
    if x - floor(x) == 0:
        return str(int(x))
    else:
        return ("%.3f" if floor(log(x) / log(10)) > -3 else "%.2e") % x

def barPlot(inputTSV, outputPNG):
    rows = tabular(inputTSV)
    series = rows.set_index(rows.columns[0])[rows.columns[1]]
    plot.figure(figsize=(12,3))
    ax = barplot(series, color=highlight, zorder=2)
    ax.grid(axis='y', zorder=0)
    ax.bar_label(ax.containers[0], rotation=45, fmt=scientificBeyoundE2)
    ax.set_xticks(range(0, len(series)), legend(series.index, 13))
    ax.spines[['left', 'top', 'right']].set_visible(False)
    plot.xticks(rotation=45, ha='right')
    ax.set_xlabel(None)
    ax.set_ylabel(rows.columns[1])
    plot.savefig(outputPNG, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    barPlot(*sys.argv[1:])
