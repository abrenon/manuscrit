#!/usr/bin/env python3

from GEODE import tabular
import pandas
import sys

def averageWordsLengthByDomain(rows):
    return {k: v['characters'].sum()/v['words'].sum()
            for k,v in rows.groupby('domain')}

laTeXTemplate = """\
\\begin{{tabular}}{{l r r}}
  \\toprule
  Domaine & dans l'\\textit{{EDdA}} & dans \\textit{{LGE}}\\\\
  \\midrule
{domainRows}
  \\midrule
{statRows}
  \\bottomrule
\\end{{tabular}}
"""

def twoDigits(f):
    return str(round(f, 2))

def rowToLaTeX(row):
    values = [row[0], twoDigits(row[1]['edda']), twoDigits(row[1]['lge'])]
    return f"  {' & '.join(values)} \\\\"

def rowsToLaTeX(rows):
    return '\n'.join([*map(rowToLaTeX, rows)])

def toLaTeX(byDomain, outputTEX):
    stat = byDomain.describe()
    rows = {
        'domainRows': rowsToLaTeX(byDomain.iterrows()),
        'statRows': rowsToLaTeX([('Moyenne', stat.loc['mean']),
                                ('Écart-type', stat.loc['std'])])}
    with open(outputTEX, 'w') as file:
        print(laTeXTemplate.format(**rows), file=file)

def drawWordsLength(eddaMeta, lgeMeta, outputTEX):
    edda = tabular(eddaMeta)
    lge = tabular(lgeMeta)
    byDomain = pandas.DataFrame({'edda': averageWordsLengthByDomain(edda),
                                 'lge': averageWordsLengthByDomain(lge)})
    toLaTeX(byDomain, outputTEX)

if __name__ == '__main__':
    drawWordsLength(*sys.argv[1:])
