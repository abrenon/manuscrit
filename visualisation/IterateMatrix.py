#!/usr/bin/env python3

from GEODE import JSON
from GEODE.Visualisation.ConfusionMatrix import getConfusionMatrix
import numpy.linalg as alg
from sys import argv

def iterate(m, n):
    return {'matrix': alg.matrix_power(m['matrix'], n).tolist(),
            'labels': m['labels']}

if __name__ == '__main__':
    JSON.save(iterate(getConfusionMatrix(argv[1]), int(argv[2])), argv[3])
