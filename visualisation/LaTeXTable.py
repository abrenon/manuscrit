#!/usr/bin/env python3

from GEODE import tabular, toTSV
from sys import argv

laTeXTemplate = """\
\\begin{{tabular}}{{{columns}}}
  \\toprule
{header}
  \\midrule
{rows}
  \\bottomrule
\\end{{tabular}}
"""

def aligned(columns):
    return ' '.join(['l'] + ['r']*(len(columns) - 1))

def rowToLaTeX(row):
    return f"  {' & '.join(row)} \\\\"

def autoCell(x):
    if type(x) == int or type(x) == float:
        return '\\num{' + str(x) + '}'
    else:
        return str(x)

def defaultFormatter(t):
    return [autoCell(c) for c in t.values]

def toLaTeX(data, output, formatter=defaultFormatter):
    rows = [rowToLaTeX(formatter(r[1])) for r in data.iterrows()]
    with open(output, 'w') as file:
        print(laTeXTemplate.format(columns=aligned(data.columns),
                                   header=rowToLaTeX(data.columns),
                                   rows='\n'.join(rows)),
              file=file)

if __name__ == '__main__':
    toLaTeX(tabular(argv[1]), argv[2])
