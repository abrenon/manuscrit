#!/usr/bin/env python3

from GEODE import legend, tabular
import sys

def getMeasure(name):
    if name == 'count':
        return len
    else:
        return lambda d: d[name].sum()

def toGraph(groups, key, output):
    import color
    import matplotlib.pyplot as plot
    fig = plot.subplots(figsize=(13, 8))
    plot.pie(groups,
             labels=legend(groups.index, 13),
             colors=[*map(color.ize(key), groups.index)],
             autopct='%1.1f%%',
             pctdistance=0.9)
    plot.savefig(output, dpi=300, bbox_inches='tight')

laTeXTemplate = """\
\\begin{{tabular}}{{l r}}
  \\toprule
  Domaine & {columnName} \\\\
  \\midrule
{rows}
  \\bottomrule
\\end{{tabular}}
"""

def rowToLaTeX(pair):
    name, size = pair
    return f"  {name} & \\num" + "{" + str(size) + "} \\\\"

def toLaTeX(groups, output, columnName):
    rows = '\n'.join([*map(rowToLaTeX, groups.sort_values().items())])
    with open(output, 'w') as file:
        print(laTeXTemplate.format(rows=rows, columnName=columnName), file=file)

def drawRepartition(inputTSV, outputPath, metric, key, columnName='Taille'):
    rows = tabular(inputTSV)
    if 'count' not in rows and metric == 'count':
        rows['count'] = 1
    groups = rows[[key, metric]].groupby(key)[metric].agg('sum')
    if outputPath[-4:] == '.png':
        toGraph(groups, key, outputPath)
    else:
        toLaTeX(groups, outputPath, columnName)

def fail(message):
    print(message, file=sys.stderr)
    sys.exit(1)

def autoParameters(outputPath):
    from os.path import basename
    stem = basename(outputPath)[:-4]
    parts = stem.split('_by_')
    if len(parts) == 2:
        return parts
    else:
        fail("Could not infer parameters automatically from output file name")

if __name__ == '__main__':
    args = sys.argv[1:]
    if len(args) == 2:
        args += autoParameters(args[1])
    elif len(args) in [4, 5]:
        pass
    else:
        fail("Expects 2, 4 or 5 arguments")
    drawRepartition(*args)
