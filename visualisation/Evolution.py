#!/usr/bin/env python3

import color
from GEODE import tabular
from pySankey.sankey import sankey
import matplotlib.pyplot as plot
import sys

def card(s):
    return s.unique().size

def drawEvolution(data, outputPath):
    groups = data.groupby(['from', 'to']).size().rename('weight').reset_index()
    sankey(groups['from'],
           groups['to'],
           leftWeight=groups['weight'],
           colorDict=color.ful('domain'),
           rightColor=card(groups['from']) < card(groups['to']))
    plot.savefig(outputPath, dpi=300, bbox_inches='tight')

if __name__ == '__main__':
    drawEvolution(tabular(sys.argv[1]), sys.argv[2])
