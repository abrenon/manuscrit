# Méthodes et outils pour l'étude diachronique des discours géographiques dans deux encyclopédies françaises

Le
[manuscrit](https://perso.liris.cnrs.fr/abrenon/no-backup/Thèse/Manuscrit.pdf)
se compile en local avec la commande `make`.

## Avec guix

```sh
guix shell --container --preserve=LANG -m manifest.scm -- make
```

### Reproductibilité

Compilé avec la version: `42e6cafdbeada03cdb144f544d093f84639613aa` de `guix`.
Version exacte avec

```sh
guix time-machine --channels=channels.scm -- shell --container --preserve=LANG -m manifest.scm -- make
```

## Sinon

La liste exacte des dépendances est au format pour `guix` dans le fichier
`manifest.scm`. Dans un environnement où tout est installé, tapper `make`
devrait marcher.
