## Plan de l'étude

Les trois apports principaux de cette thèse concernent la préparation du corpus,
un modèle de classification automatique et une étude des contenus géographiques
à l'aide d'outils de textométrie. Ils correspondent à trois composantes
présentes dans de nombreux projets en [@=HN]: le travail d'ingénierie nécessaire
à la mise en forme des données, l'application de méthodes d'[@=AA] et enfin la
conduite d'analyses expertes propres à une discipline, ici de la linguistique
outillée puisqu'il s'agit de données textuelles.

### Organisation {.unnumbered .unlisted}

Hormis le chapitre consacré à l'état de l'art, le manuscrit est construit en
suivant ces trois versants. S'il existe une relative dépendance entre ces
composantes, le découpage des chapitres vise à leur conserver une certaine
autonomie, bien que les premières contributions présentées soient davantage
préparatoires et laissent progressivement la place aux analyses des données et
métadonnées produites.

Le chapitre \ref{sec:EdlA} dresse un portrait du domaine des Humanités
Numériques. Il commence par décrire les formats utilisés, avant d'expliciter le
lien privilégié qu'entretiennent les œuvres lexicographiques et la linguistique.
Enfin, il décrit et replace dans leur contexte historique deux familles
d'approches statistiques, en présentant les outils et les techniques couramment
utilisées par chacune.

La majeure partie du travail de mise en forme et d'encodage du corpus fait
l'objet du chapitre \ref{sec:corpus}. La description détaillée des deux œuvres
étudiées et de la structure des contenus que l'on peut y trouver y est suivie
d'une discussion des efforts entrepris pour représenter le corpus et les
métadonnées qui lui sont associées. Le chapitre introduit également la notion de
domaine de connaissance des articles, centrale dans le lien entre encyclopédies
et géographie et sur laquelle repose un des partitionnements du corpus utilisés
dans les études contrastives.

L'importance de cette notion justifie la place centrale qui lui est accordée au
chapitre \ref{sec:domains_classification} en tant que focale de tâches de
classification. Après la description de l'entraînement de modèles d'[@=AA]
capables de prédire le domaine de connaissance d'articles des deux encyclopédies
étudiées à partir de leur contenu, le chapitre décrit l'application du
classifieur retenu à la totalité du corpus afin de pouvoir ajouter aux
métadonnées des articles le domaine dont ils relèvent.

Enfin, le chapitre \ref{sec:contrasts} constitue l'aboutissement des deux
précédent et se consacre aux analyses contrastives. Il commence par cerner la
place accordée aux discours géographiques, de manière tout à fait quantitative
d'abord puis, en s'intéressant aux transferts d'articles entre classes en
diachronie, de façon plus qualitative. La dernière étude qu'il contient observe
l'hétérogénéité des articles en termes de domaine de connaissance en prenant le
cas notoire des biographies dans les articles de géographie.

### Contributions {.unnumbered .unlisted}

Les recherches présentées dans ce manuscrit se situent à l'interface de
l'informatique et des sciences du langage mais la répartition des trois
contributions entre ces deux sciences reste nettement en faveur de la première.

#### Version numérique structurée de *LGE* {.unnumbered .unlisted}

Présentée au chapitre \ref{sec:corpus}, la publication de la première version
numérique complète de *LGE* structurée en XML-TEI représente la première
contribution de cette thèse. Au-delà de l'objet produit, l'originalité de ce
travail réside dans l'emploi de méthodes de graphes pour évaluer les
possibilités offertes par un schéma XML.

Si les premiers succès sur ce versant avaient été obtenus dès 2021 à l'issue du
projet DISCO-LGE[^DISCO-LGE], les analyses conduites sur *LGE* dans le cadre des
présents travaux nécessitaient une amélioration de la qualité. La nouvelle
version apporte plusieurs progrès à l'encodage de l'œuvre. D'abord, elle gagne
en précision sur la segmentation des articles. Elle s'accompagne également de
métadonnées permettant la navigation et le retour au texte.

[^DISCO-LGE]: [https://www.collexpersee.eu/projet/disco-lge/](https://www.collexpersee.eu/projet/disco-lge/)

#### Un modèle de classification en domaine de connaissance {.unnumbered .unlisted}

La deuxième contribution, développée dans le chapitre
\ref{sec:domains_classification}, concerne la tâche de classification
automatique. Elle débute par une étude comparative de méthodes d'[@=AA] pour
prédire le domaine d'articles encyclopédiques qui actualise et complète un
travail similaire réalisé par @horton2009mining en considérant notamment des
méthodes d'Apprentissage Profond ([@=AP]).

Ensuite, au lieu de réduire les prédictions incorrectes des modèles à un horizon
technique à dépasser, elle observe que ces erreurs ne ressemblent pas à un bruit
aléatoire mais relèvent dans la plupart des cas d'une certaine logique. Elle les
utilise comme un point d'entrée possible vers les articles en appliquant des
méthodes algébriques — en particulier la notion de centralité — pour mettre en
évidence des proximités thématiques ou stylistiques entre les domaines de
connaissance.

#### Biographies et discours géographiques {.unnumbered .unlisted}

Enfin le chapitre \ref{sec:contrasts}, apporte des éléments nouveaux pour
comprendre la place des biographies dans les articles de *Géographie*. En effet,
il n'y a pas officiellement de notice biographique dans les pages de l'*EDdA* et
la biographie, genre discursif, ne s'intègre pas au «Systême» des domaines de
connaissance des Encyclopédistes.

Plusieurs biographies comme celle de Newton sont pourtant bien connues des
spécialistes de l'*EDdA*, spécifiquement sous la plume du Chevalier de Jaucourt,
contributeur incontournable des entrées géographiques. En mettant en évidence un
régime particulier pour les philosophes puis en définissant deux critères sur
les articles contenant des biographies, l'étude propose un modèle progressif
pour rendre compte des liens qui unissent les biographies et la Géographie.

### Choix {.unnumbered .unlisted}

La rédaction de ce manuscrit a fait l'objet d'un certain nombre de choix et de
partis pris qu'il est bon d'avoir en tête avant d'entamer sa lecture et qui
tiennent en partie à son caractère autoréférentiel. Au contact du genre
encyclopédique, il a fini par en prendre certaines caractéristiques.

Le texte comporte des renvois fréquents, non seulement à des figures mais
également à des sections ou à des passages précis dans le texte pour éviter de
répéter des explications. La version PDF favorise naturellement une lecture
transversale selon ces renvois grâce aux liens hypertextes qu'elle comporte;
pour faciliter une navigation semblable dans l'exemplaire papier, la plupart des
renvois précisent le numéro de la page où se trouve la figure ou la section
mentionnée. Ces numéros de page sont parfois omis pour les renvois «proches» à
l'intérieur d'une même section ou à quelques pages d'intervalle.

En écrivant cette thèse il est en outre apparu que plusieurs sigles et
acronymes, par exemple des noms d'organisations ou de techniques, revenaient
assez souvent pour justifier la création d'un court
\hyperlink{glossary}{Glossaire} alphabétique des termes du manuscrit. À leur
première occurrence, les termes sont explicités, leurs initiales étant précisées
entre parenthèses. Les occurrences suivantes n'utilisent plus que la version
courte sous forme de lien hypertexte également. Ces liens, à la différence des
renvois précédents, ne sont jamais accompagnés de la page où se trouve l'entrée:
tout acronyme qui ne serait pas développé juste avant son utilisation est à
chercher dans le \hyperlink{glossary}{Glossaire} à partir de la page
\pageref{glossary}. La version numérique est donc là encore avantagée puisque de
nombreux lecteurs PDF affichent une prévisualisation de la cible d'un lien au
survol avec la souris, ce qui suffit le plus souvent pour consulter ces
définitions assez brèves. Le format PDF est donc celui recommandé pour lire
cette thèse.

Le manuscrit ne possède en revanche pas d'Annexe alors que cela est courant dans
ce type de document pour présenter des tableaux de données, des algorithmes
utilisés, etc. À la place, puisqu'un certain nombre de traitements des données
et de génération des figures est effectué automatiquement au moment de la
génération de ce texte, le code source[^manuscrit] utilisé tient lieu d'annexe
(voir la section \ref{sec:reproducibility} sur la reproductibilité
page \pageref{sec:reproducibility} à ce sujet).

[^manuscrit]:
    [https://gitlab.liris.cnrs.fr/abrenon/manuscrit](https://gitlab.liris.cnrs.fr/abrenon/manuscrit)

\label{french_please}Enfin, du point de vue du langage, le texte reçoit par sa
composante informatique l'influence de l'anglais, systématique dans cette
discipline quelle que soient les langues maternelles des équipes qui publient
alors que le français semble plus présent dans les publications de la communauté
linguistique francophone. Le français est la langue du corpus, et le manuscrit a
été rédigé en français. Pour la cohérence de l'ensemble, les termes français et
les acronymes correspondants ont été préférés à ceux d'origine anglaise même
d'un emploi répandu en France pour le vocabulaire technique, tant que cela ne
donnait pas lieu à des termes trop artificiels. Certaines initiales peuvent
toutefois paraître obscures quand l'habitude des termes anglophones a popularisé
les leurs à la place d'un terme francophone pourtant utilisé. C'est le cas par
exemple d'[@=AA] pour Apprentissage Automatique (l'expression développée est
parfois utilisée en français mais l'abréviation ML pour «Machine Learning» est
bien plus courante dans la littérature que «[@=AA]») ou pour [@=HN] qui peut
surprendre par rapport à DH pour «Digital Humanities» alors qu'«Humanités
Numériques» se rencontre couramment. Par soucis d'homogénéité, ce sont pourtant
les versions francophones qui ont été préférées; dans tous les cas, le
\hyperlink{glossary}{Glossaire} est là (p.\pageref{glossary}) si besoin. Pour
certains termes comme les noms de méthodes de classification automatique où
aucune traduction ne parvient à s'imposer, des concessions à ce principe ont
été faites.

