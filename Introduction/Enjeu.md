## Réflexions prospectives {#sec:intro_forethoughts}

### Problématique {#sec:intro_issue .unnumbered .unlisted}

Le lien privilégié qui unit les encyclopédies à la Géographie amène à
s'interroger sur les effets qu'ont pu avoir dans ce type d'ouvrages les
changements subis par la discipline après le siècle des Lumières. Puisqu'il y a
lieu de faire l'hypothèse que ces mutations ont eu des conséquences sur la
manière dont on écrit la Géographie, il faut se donner les moyens d'observer
leurs effets, ce qui constitue l'enjeu principal de cette thèse.

La question que soulève immédiatement une telle problématique concerne donc les
moyens d'accéder aux discours géographiques présents dans les encyclopédies du
corpus. De nombreux projets en [@=HN] ont déjà développé des outils pour traiter
des textes antérieurs au XX^ème^ siècle; d'autres s'intéressent à l'importance
d'un ensemble d'articles pour une discipline en particulier mais sans intégrer
d'analyse de discours et sans les opposer aux autres sciences. Ici, l'objectif
est à la fois de pouvoir traiter les textes des encyclopédies dans leur ensemble
à l'aide de méthodes automatiques et, simultanément, d'être à tout moment
capable de restreindre l'étude à des sous-corpus pertinents ou d'adopter une
démarche contrastive, principalement selon deux axes. Le premier, celui du champ
disciplinaire, repose sur la possibilité d'identifier des discours géographiques
par opposition à d'autres qui ne relèveraient pas du même domaine. Le deuxième
axe est temporel puisqu'il faut comparer les discours du genre encyclopédique à
deux époques: le XVIII^ème^ d'une part et le tournant des XIX^ème^ et XX^ème^
siècles d'autre part.

Cette thématique de recherche mobilise plusieurs des apports de l'informatiques
aux [@=HN]. Il est d'abord nécessaire de déterminer un encodage qui convienne à
la fois à l'*EDdA* et à *LGE* malgré leur différences, de façon à pouvoir
regrouper les deux œuvres au sein d'un même corpus et leur appliquer les mêmes
traitements puis les mêmes analyses. Cette tâche de normalisation, avec celle
d'organisation des textes et des métadonnées représente un important travail
d'ingénierie des données. À ces efforts s'ajoutent ceux à fournir pour
identifier les discours pouvant relever de la Géographie. La «cartographie» des
sciences entreprise par les encyclopédistes se matérialise par la répartition
des articles entre les différentes sciences en fonction des concepts dont ils
traitent. À cette échelle, associer un même domaine de connaissance à des
articles des deux encyclopédies prend la forme d'un problème de classification.
Plus près des phrases, l'annotation automatisée des textes et l'écriture de
requêtes basées sur des critères lexicaux ou syntaxiques, faisant appel à la
fois à l'informatique et aux sciences du langage, permet de mettre en évidence
des motifs utiles.

### La Géographie et ses traces {#sec:intro_strategy .unnumbered .unlisted}

Au niveau des articles entiers, une évidence apparente doit toutefois être
critiquée sous peine de fragiliser la démarche dans son ensemble: celle de
l'identification implicite entre d'une part la discipline "géographie" et
d'autre part l'ensemble des articles assignés à cette discipline par le choix
éditorial des encyclopédistes. Si la première peut être initialement définie en
tant qu'un ensemble vivant de pratiques et de savoirs, elle renvoie
intuitivement à une notion plus vaste et plus immatérielle. Au contraire la
seconde, purement arbitraire, n'a d'existence que concrète et contingente au
travers des articles qui la constituent. Ces articles ne sont que l'echo de la
discipline, arrivant nécessairement en retard par rapport à ses progrès: ils
sont écrits après réverberation dans un ou plusieurs des dictionnaires
universels dans lesquels les encyclopédistes ont puisé leurs sources, parfois
d'après des récits de voyages passés. Une expression du XVIII^ème^ siècle
illustre parfaitement cette source d'information: «terme de relation»
[@quemada_dictionnaires_1968, p.309], qui qualifie certaines entrées dans
l'*EDdA* et sous-entend le mot «voyage». Il s'agit en effet du vocabulaire
fréquemment utilisé lorsqu'un texte relate un voyage, le plus souvent une
francisation d'un mot de la langue parlée dans le pays visité comme PILAU
(L'Encyclopédie, T12, p.618) emprunté au Turc pour parler de la cuisson du riz.

L'objet qui intéresse fondamentalement cette thèse est bien sûr la Géographie en
tant que discipline (qu'on distinguera typographiquement en conservant la
majuscule) mais il est par nature insaisissable. Les présents travaux
s'efforcent de s'en rapprocher en étudiant les discours géographiques dans les
encyclopédies, c'est-à-dire la trace laissée par la discipline dans ces textes
(pour laquelle on réservera l'emploi sans marquer l'initiale). De la même
manière, le terme de «discipline» sera pris dans le sens large de «science»
alors que l'expression «domaine de connaissance» renverra plus spécifiquement à
un choix éditorial de découpage dans une encyclopédie particulière.

La situation est tout à fait analogue à celle en traitement du signal d'un
processus d'échantillonnage par lequel on tente de connaître une fonction
continue du temps à partir d'un ensemble fini de mesures, comme représenté à la
figure \ref{fig:sampling}. Tout l'enjeu est alors de savoir si les points
échantillonnés sont assez représentatifs et couvrent suffisamment l'intervalle
complet pour rendre fidèlement compte du phénomène observé. Mais là où en
systématisant l'échantillonnage suivant une grille régulière on parvient à
connaître et limiter la quantité d'information perdue grâce à aux résultats des
travaux de @nyquist_certain_1928 repris et étendus par
@shannon_communication_1949 [^nyquist-shannon], la sélection opérée par les
choix éditoriaux des encyclopédistes ne constitue pas un échantillonnage car,
n'ayant pas été faite dans le but de fournir de l'information sur la discipline
elle-même, elle est nécessairement irrégulière et probablement lacunaire. C'est
la raison pour laquelle les mesures sont réparties aléatoirement sur la figure
\ref{fig:sampling} plutôt qu'espacées entre elles d'un pas constant comme cela
aurait été le cas sur un graphe similaire destiné à illustrer un vrai processus
d'échantillonnage, par exemple d'un signal accoustique. Le problème est donc de
travailler à partir d'une «trace» de la Géographie, passée et finie, sans
pouvoir limiter ni même seulement connaître la quantité d'information perdue par
rapport au «signal» que constituerait la discipline géographique. C'est pourtant
le parti qu'il faut prendre, en l'absence d'un meilleur, tout en gardant cette
distinction à l'esprit.

[^nyquist-shannon]: le théorème de Nyquist-Shannon dit qu'en échantillonnant à
    la fréquence F on peut récupérer sans déformation toutes les composantes de
    fréquence $< \frac{F}{2}$ dans le signal initial

![Échantillonnage d'un processus sinusoïdal par un ensemble de points répartis aléatoirement](figure/sampling.png){#fig:sampling width=60%}

### Faire correspondre des époques {#sec:mapping_ages .unnumbered .unlisted}

À la lumière de la remarque ci-dessus, apparaît un deuxième questionnement sur
l'identité des objets à comparer. En effet, puisqu'il est impossible d'accéder
directement à la Géographie de chaque époque et qu'il faut se contenter de ses
traces sous forme d'ensembles d'articles, il est naturel de s'interroger sur le
bien-fondé d'une mise en regard de ces ensembles au seul prétexte qu'ils sont
estampillés *Géographie* chacun à leur époque. Intuitivement, l'identité paraît
évidente car c'est bien la même discipline qui a évolué continûment d'une époque
à l'autre, et les encyclopédistes de chaque époque ont choisi les articles
qu'ils considéraient relever de cette même discipline.

Mais en toute rigueur, avec les seuls éléments disponibles — c'est-à-dire des
collections d'articles non seulement finies mais surtout disjointes, sans
continuité temporelle — il se pourrait tout à fait que cette identité de noms ne
soit qu'une coïncidence. D'ailleurs, l'ensemble des catégories qui partitionne
les articles diffère à chaque époque. Parfois, cela est dû à l'apparition d'une
discipline entière comme par exemple «Industrie» dans *LGE* qui aurait été
anachronique dans la deuxième moitié du XVIII^ème^ siècle avant la révolution
industrielle. Des redécoupages sont également à l'œuvre: ainsi, là où la
*Géographie* se retrouvait dans le «Systême Figuré» des encyclopédistes des
Lumières aux côté de l'*Uranographie* et de l'*Hydrographie* dans la
*Cosmographie*, au sein des sciences mathématiques[^uranographie], elle se
retrouve associée à l'*Histoire* dans l'Avant-Propos de *LGE* qui en dresse le
projet (La Grande Encyclopédie, T1, p.XI), comme l'illustre la figure
\ref{fig:evolution}[^chronologie]. D'ailleurs, si la liste des collaborateurs du
projet n'associe pas chacun à une discipline précise, il est à noter que sur les
12 membres dont la qualité contient le mot «géographie», la moitié exactement
d'entre eux sont des enseignants, agrégés ou professeurs, à la fois d'histoire
et de géographie puisque les deux disciplines sont rassemblées à l'agrégation de
1831 à 1943, date à laquelle celle de géographie a été créée. Ce rapprochement
est tout à fait cohérent avec le rôle que la Géographie joue à cette époque dans
la constitution d'une identité nationale.

[^uranographie]: le Systême est surtout un manifeste dont les auteurs ont su
    s'émanciper là où il le fallait mais il témoigne en revanche assez bien des
    idéaux taxonomiques de ses auteurs

[^chronologie]: dans le préambule de l'*EDdA* déjà, un paragraphe dressait un
    parallèle entre *Chronologie*, «science des tems» et *Géographie* celle «des
    lieux». C'est peut-être là déjà un des premiers mouvements de ce qui
    deviendra l'*Histoire-Géographie* de *LGE*, mais les Encyclopédistes
    n'identifient pas *Chronologie* et *Histoire*, puisqu'ils font de celle-ci
    la mère de celle-là, attribuant en parallèle la même relation de parenté à
    la *Géographie* et à l'*Astronomie* respectivement.

![Exemple schématique de rebrassage des domaines de connaissance](figure/evolution.png){#fig:evolution width=80%}

Pour reprendre la métaphore géographique de d'Alembert, les frontières des
sciences bougent et se redécoupent donc et les présents travaux visent entre
autres à rendre compte de ces mouvements. Pour avoir une chance d'atteindre leur
objectif, ils devront donc éviter l'écueil consistant à prendre pour acquise la
correspondance de deux ensembles d'articles au seul prétexte qu'ils sont
regroupés sous la même étiquette à deux époques différentes et mettre d'abord en
évidence assez de ressemblance pour s'assurer qu'il s'agit bien des mêmes
objets. C'est seulement après avoir vérifié cette égalité «au premier ordre» que
pourront se justifier des comparaisons ultérieures raffinant l'étude et la
pertinence d'éventuelles différences trouvées. De plus, ils ne pourront pas se
contenter de demeurer à l'intérieur des collections d'articles censés
représenter la Géographie à chaque époque et devront nécessairement prendre en
compte également ce qu'il y a «autour».

Mais puisqu'il faut — une fois les réserves levées — identifier la *Géographie*
de l'*EDdA* et celle de *LGE*, il n'y a pas de sens à utiliser des jeux
d'étiquettes différents aux deux époques pour représenter les domaines de
connaissance. Et si c'est bien la nécessité technique qui motive initialement ce
choix, les remarques ci-dessus montrent cette approche pragmatique sous un
nouveau jour. Certes, appliquer la grille de lecture d'une époque à une autre va
causer des erreurs, mais leur mesure attentive éclairera ces changements, alors
qu'essayer de réconcilier deux réalités de toute façon disjointes n'aurait
laissé aucune prise à l'analyse.

