## Aux sources de cette thèse {#sec:intro_cadre}

### Le genre encyclopédique {.unnumbered .unlisted}

Si l'on en croit André Marcel Berthelot, la Géographie serait la science
encyclopédique par excellence:

> Le géographe aborde successivement le domaine de plusieurs sciences définies ;
> il en prend les résultats et les place dans sa description synthétique :
> astronomie, physique, chimie, géologie, botanique, zoologie, anthropologie,
> linguistique, sociologie, statistique, démographie, histoire, toutes les
> branches des connaissances humaines lui apportent leur contingent de faits
>
> -- La Grande Encyclopédie, T18, p.767

Cet extrait de l'article GÉOGRAPHIE qu'il rédige pour *La Grande Encyclopédie,
Inventaire raisonné des Sciences, des Lettres et des Arts par une Société de
savants et de gens de lettres* — *LGE* dans le reste de cette thèse — rappelle
en effet l'«enchaînement de toutes les connaissances» à l'origine du mot
«encyclopédie». Plus d'un siècle après la fin de la publication de
l'*Encyclopédie, Dictionnaire raisonné des sciences, des arts et des métiers,
par une Société de Gens de lettres* — *EDdA* dans le reste de cette
thèse — l'«esprit encyclopédique» qui a présidé à l'apparition du genre
encyclopédique au tournant des XVII^ème^ et XVIII^ème^ siècles
[@macary_dictionnaires_1973, p.145] semble donc continuer à inspirer les
auteurs.

L'*EDdA* a en effet eu de nombreux précurseurs, au rang desquels on peut compter
le *Dictionnaire Universel* de Furetière [@galleron_tenir_2022], le
*Dictionnaire François-Latin* publié à Trévoux [@le_guern_dictionnaire_1983] et
la *Cyclopedia* de Chambers dont l'*EDdA* ne devait initialement être qu'une
traduction [@kafker_andre_francois_2016]. Mais la portée de cet esprit
encyclopédique se mesure davantage à la descendance fournie qu'il a laissée: le
*Supplément* à l'*EDdA* [@hardestydoig_notices_1990], l'*Encyclopédie
Méthodique* de Panckoucke [@groult_introduction_2019] puis dans le courant du
XIX^ème^ siècle le *Grand Dictionnaire Universel* de Larousse et *LGE*
[@jacquet_pfau_actualiser_2022], et cela rien qu'en France. Ailleurs dans le
monde, l'*Encyclopedia Britannica*, la *Brockhaus Enzyklopädie* et
l'*Encyclopedia Americana* entre autres contribuent à diffuser cet esprit. Les
encyclopédies restent des objets pertinents au XXI^ème^ siècle, ayant atteint
une portée universelle comme le montre le succès de projets comme *Wikipédia*.

Le genre encyclopédique fait l'objet de nombreuses études depuis la thèse de
@quemada_dictionnaires_1968, notamment en diachronie. Le XVIII^ème^ siècle est
marqué par une augmentation de la taille des équipes éditoriales ainsi que par
des rectifications et des augmentations des contenus comme celles observables
entre le *Furetière* et le *Trévoux* [@macary_dictionnaires_1973, pp.150-155].
@rey_professionnalisation_2022 montre que ce mouvement se prolonge au début du
XIX^ème^ siècle par une professionnalisation de la rédaction d'encyclopédies et
il est naturel de se demander ce que devient ce mouvement au tournant des
XIX^ème^ et XX^ème^ siècles.

### La Géographie, une science en recomposition {#sec:geo_intro .unnumbered .unlisted}

Le lien entre la Géographie et les encyclopédies n'a pourtant pas toujours
autant relevé de l'évidence que ne pourrait le laisser croire la citation de
Berthelot. Les contenus géographiques sont très peu présents dans le *Furetière*
[@galleron_tenir_2022 p.38; @vigier_articles_2022 p.60] et ne font vraiment
leur entrée dans les encyclopédies qu'avec la deuxième édition du *Trévoux*
(1721). Dans l'Avertissement des Éditeurs (L'Encyclopédie, T3, p.xj), d'Alembert
doit même défendre le choix d'avoir inclus des articles de Géographie dans
l'*EDdA* alors que «plusieurs personnes ont pensé que les articles de Géographie
étoient de trop dans ce Livre».

Le XVIII^ème^ siècle voit émerger l'association de la Géographie aux cartes
[@verdier_diffusion_2015] alors que l'activité des géographes semblait pouvoir
s'en dispenser auparavant. La carte apparaît alors comme une simple technique de
visualisation, intéressant davantage les mathématiciens pour les constructions
géométriques qu'elles requièrent. L'article CARTE (L'Encyclopédie, T2, p.706) le
montre assez clairement et, bien qu'il ne comporte pas de signature, il est
d'ailleurs communément attribué à d'Alembert, un mathématicien. La valeur du
travail du géographe réside dans l'exactitude de ses relevés, à partir desquels
pourront être construites les cartes. Il n'y a d'ailleurs pas de cartes dans
l'*EDdA* et Diderot se satisfait d'une géographie «sèche», «scientifique», «la
seule qui nous suffiroit pour construire de bonnes cartes des tems anciens, si
nous l'avions, & qui suffira à la postérité pour construire de bonnes cartes de
nos tems» ainsi qu'il l'écrit à l'article ENCYCLOPÉDIE (L'Encyclopédie, T5,
p.646). Cette thématique de la carte reste pourtant présente dans l'œuvre et
permet d'introduire une métaphore frappante que d'Alembert file dans le Discours
Préliminaire des Éditeurs (L'Encyclopédie, T1, p.xv): l'Encyclopédie décrit les
différentes sciences comme autant de «pays» qu'elle situe les uns par rapport
aux autres, permettant d'«entrevoir même quelquefois les routes secrètes qui les
rapprochent». Ses articles sont des «cartes» qui couvrent en détail des portions
de la «mappemonde» que constitue le «Systême Figuré des connoissances humaines»,
un «arbre encyclopédique» matérialisé sous forme d'une gravure au tome 1^er^
dans le but de montrer une vue d'ensemble des liens entre sciences.

\label{geo_nations}La relation entre Géographie et encyclopédies à l'origine de
cette réflexion est donc en réalité à double-sens: à la Géographie comme science
encyclopédique, nourrie de toutes les autres, s'ajoute la pratique
encyclopédique comme une «géographie des sciences» dont elle révèle le paysage.
Cependant, au-delà de cette relation, la Géographie est soumise aux XVIII^ème^
et XIX^ème^ siècles à des forces transformatrices profondes.
@chevalier_quatre_1997 [p.4] distingue quatre pôles du savoir géographique,
opposant les géographies scolaires et universitaires mais aussi la géographie
grand public et la géographie appliquée. L'École des géographes forme depuis
1797 des topographes [@verdier_diffusion_2015 p.41] pour dresser des cartes
utiles aux états-majors dans les nombreux conflits qui parsèment le XIX^ème^
siècle. Avec la période révolutionnaire, l'école reçoit la charge de former des
citoyens dont les connaissances doivent progressivement intégrer de la
géographie [@chevalier_geographie_2013, §2; @todorov_enseigner_2018, §15]. La
géographie est aussi mise à contribution, associée à l'histoire, pour développer
le sentiment national qui émerge au XIX^ème^ siècle non seulement en France dès
1833 et jusqu'aux célèbres lois «Jules Ferry» qui mettent en avant la puissance
coloniale française dans les programmes de géographie
[@chevalier_geographie_2013, §3] mais aussi ailleurs en Europe, comme en Italie,
en Allemagne ou en Hollande [@todorov_enseigner_2018, §§1 et 9]. Vers la fin du
XIX^ème^ siècle la Géographie se hisse enfin au rang de véritable discipline
scientifique grâce à des auteurs comme Vidal de la Blache.

### Des thématiques faites pour l'informatique {.unnumbered .unlisted}

La conduite d'investigations sur des objets à la fois massifs et historiques
comme peuvent l'être les œuvres encyclopédiques évoquées soulève cependant un
certain nombre de difficultés, notamment techniques. La volumétrie
particulièrement élevée de ce type d'ouvrage représente un premier frein
évident: sans moyens automatiques, il est en pratique impossible de rechercher
des phénomènes linguistiques, de les cataloguer exhaustivement ou même de
naviguer rapidement de l'un à l'autre. Les encyclopédies exacerbent bien sûr ce
problème mais il apparaît néanmoins dans de nombreux projets dès qu'il s'agit de
travailler sur un corpus d'œuvres ou de productions langagières assez large pour
prétendre à une certaine représentativité. Pour les travaux portant sur les
siècles passés, le caractère historique des documents étudiés ajoute encore des
contraintes: la fragilité des exemplaires s'oppose à toute consultation
intensive tout en exigeant des précautions techniques lourdes pour ne pas les
abîmer; leur rareté empêche également toute étude simultanée par les membres du
projet — à fortiori dans le cadre de collaborations internationales. Ces
quelques raisons suffisent amplement à justifier l'intérêt de numériser les
œuvres et de les exploiter via des moyens électroniques. Leur dématérialisation
permet en outre de les partager sans limite au sein de la communauté
scientifique mais également de les faire connaître au grand public. Les intérêts
des sciences humaines et celles de l'information convergent donc naturellement,
un mouvement à l'origine des «Humanités Numériques» ([@=HN]). L'ordinateur y
apporte des possibilités techniques originales au service des questionnements
des autres disciplines mais ne se limite pas à un rôle de soutien technique: ces
thématiques nouvelles créent aussi en retour des problématiques de recherche
intéressantes pour l'informatique elle-même.

La mise à disposition des équipes de grandes quantités de données et parfois de
logiciels d'analyses en ligne requiert des infrastructures complexes qui
soulèvent déjà des questions d'ingénieries non triviales liées entre autres au
stockage, à la vitesse d'accès aux données ou même à la prise en charge du
travail collaboratif. Les efforts entrepris sur ces sujets ont donné naissance à
des organisations comme Ortolang [@pierrel_ortolang_2016] ou Huma-Num
[@larrousse_humanum_2023] en France, et comme CLARIN
[@dejong_interoperability_2020] à l'échelle européenne. L'augmentation du nombre
de plateformes de ce type et la nécessité de pouvoir les interconnecter dans des
réseaux comme le European Open Science Cloud [@didonato_social_2020] amène à
devoir développer des formats ouverts et normaliser les données et métadonnées.
L'apport des sciences du numérique ne se résume donc pas à l'implémentation de
services mais inclut aussi la spécification de langages formels utilisables pour
encoder les données, processus aboutissant à la production de standards comme la
XML-TEI [@ide_text_1995], omniprésente en [@=HN].

De plus, le recours à des outils numériques pour le traitement et l'analyse des
données fait peser une responsabilité accrue sur ces derniers: leur qualité et
leur précision deviennent deviennent des enjeux pour la solidité des
connaissances produites. C'est ainsi que s'engagent de véritables courses en vue
de repousser l'état de l'art, par exemple de la reconnaissance optique de
caractères [@patel_optical_2012; @wick_comparison_2018] ou du repérage et de la
classification d'entités nommées [@nadeau_survey_2007; @humbel_named_2021].
Parfois, des progrès significatifs ne peuvent être obtenus qu'au prix de
l'adoption d'approches radicalement différentes comme ont pu l'être les méthodes
d'Apprentissage Automatique ([@=AA]) par rapport aux méthodes symboliques. Ces
ruptures technologiques peuvent s'accompagner d'un bouleversement des usages
(concepts observables, métriques d'évaluations) qui amène en retour à repenser
le rapport aux objets examinés pour comprendre le sens du résultat d'un calcul,
comme par exemple une prédiction faite par un classifieur automatique. Cet enjeu
est en lien avec la visualisation des données, qui peut demander des efforts de
développements logiciels mais interroge surtout sur l'interprétation pouvant
être faite des concepts statistiques ou informatiques mobilisés. Enfin, la
question des chaînes de traitement peut paraître secondaire, ne constituant
qu'un assemblage «évident» ou même «nécessaire» des outils requis par un projet
donné mais suscite en réalité de nombreuses réflexions sur les formats des
données ainsi que sur l'équilibre à trouver entre commodité d'utilisation,
flexibilité et réutilisabilité des états intermédiaires. Si de très nombreux
projets en [@=HN] semblent partager une structure commune allant de la
numérisation de données à l'application d'outils linguistiques automatisés
[@jentsch_text_2020], la diversité des besoins spécifiques d'analyse ou des
formats requis en entrée ou en sortie de chaîne pour chaque projet complique en
réalité le réemploi de chaînes existantes. Quelle que soit la direction
envisagée, les thématiques d'étude choisies apparaissent donc potentiellement
fructueuses pour la recherche informatique.

### Le projet GEODE {.unnumbered .unlisted}

Au-delà des résultats d'analyse auquels ils parviennent, l'enjeu des présents
travaux repose donc davantage sur l'exploration des solutions techniques à
disposition ainsi que des manières dont elles peuvent être combinées pour
répondre à des besoins d'analyse correspondant aux pratiques des [@=HN]. Cette
thèse s'inscrit en effet dans le cadre du projet GEODE[^geode] qui adopte cette
approche interdisciplinaire. Pour ce faire, il étudie un corpus de quatre
encyclopédies de la deuxième moitié du XVIII^ème^ au XXI^ème^ siècle. Pour
respecter la règle de @bender2019rule et mentionner explicitement les langues
concernées par cette étude, il faut préciser qu'elle s'intéresse seulement à des
encyclopédies en français, émaillées ici et là de mots et parfois de citations
complètes en latin et en grec pour les plus anciennes d'entre elles. Autour de
ces textes, le projet réunit des spécialistes de linguistique, d'informatique,
de géographie et d'histoire.

[^geode]: [https://geode-project.github.io/](https://geode-project.github.io/)

La première des œuvres du corpus de GEODE dans l'ordre chronologique est
l'*EDdA*, publiée de 1751 à 1772 et témoin de la deuxième moitié du XVIII^ème^
siècle. Le deuxième jalon de l'étude est posé par *LGE* qui permet d'observer la
toute fin du XIX^ème^ siècle, de 1885 à 1902. L'étape suivante dans la
progression est marquée par l'*Encyclopedia Universalis*, lancée en 1966.
L'édition versée au corpus est la 23^ème^, datant de 2018 et tenue à jour de
l'évolution des connaissances scientifiques et des événements historiques les
plus récents. Toutefois certains articles tel que CONSCIENCE ou LIBERTÉ signés
respectivement par Ey (décédé en 1977), et Ricœur (en 2005) sont nécessairement
plus anciens et font de cette œuvre une bonne représentante de contenus
encyclopédiques de la fin du XX^ème^ siècle et du début du XXI^ème^. La dernière
œuvre utilisée est la version francophone de *Wikipédia*, qui par sa nature
collaborative et vivante est mise à jour en continu et contient donc des états
de langues tout à fait contemporains. Les travaux conduits dans le cadre de
cette thèse se sont concentrés sur le premier intervalle allant de l'*EDdA* à
*LGE* et au cours duquel se sont produites les transformations de la Géographie
décrites par les spécialistes de la discipline dans les références évoquées
p.\pageref{geo_nations}.

