{-# LANGUAGE OverloadedStrings #-}
import Control.Applicative ((<|>), many)
import Control.Monad.Reader (MonadReader(..), ReaderT(..), asks, runReaderT)
import Control.Monad.State (MonadState(..), gets, modify, runStateT)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Attoparsec.Text (char, inClass, parseOnly, string, takeWhile1)
import Data.Char (isAlphaNum, isUpper, toLower)
import Data.Default (def)
import Data.Map as Map (Map, insert, member, toList)
import Data.Text as Text (Text, concat, map, toTitle, unpack)
import Data.Text.IO as Text (readFile)
import System.Exit (die)
import System.FilePath ((</>), (<.>))
import Text.Pandoc
  ( Block(..), Format(..), Inline(..), Meta(..), Pandoc(..), ReaderOptions(..)
  , lookupMeta, nullAttr, pandocExtensions, readMarkdown, runIOorExplode )
import Text.Pandoc.JSON (ToJSONFilter(..))
import Text.Pandoc.Walk (query, walkM)
import Text.Printf (printf)
import Text.Read (readMaybe)

main :: IO ()
main = toJSONFilter withGlossary

type Glossary = Map Text [Block]
data KnownFormat = Latex | Html deriving (Read, Show)
type RawMaker = (Text, KnownFormat -> [Text]) -> Inline
data Config = Config
  { rawMaker :: RawMaker
  , title :: Text
  , level :: Int
  , root :: FilePath }

withGlossary :: Maybe Format -> Pandoc -> IO Pandoc
withGlossary cliFormat document@(Pandoc meta _) =
  either die (runReaderT $ walkM edit document) $ getConfig rawMaker meta
  where
    edit body =
      runStateT (walkM findDefinitions body) mempty >>= addGlossarySection
    readFormat format@(Format f) =
      (format,) <$> readMaybe (Text.unpack $ toTitle f)
    rawMaker =  maybe (Str . fst) rawInline (cliFormat >>= readFormat)
    rawInline (format, knownFormat) (_, code) =
      RawInline format (Text.concat $ code knownFormat)

getConfig :: RawMaker -> Meta -> Either String Config
getConfig format meta = Config format
  <$> (getMeta "glossary-title" <> Right "Glossary")
  <*> ((read . Text.unpack <$> getMeta "glossary-level") <> Right 1)
  <*> (Text.unpack <$> getMeta "glossary-root")
  where
    getMeta s = maybe (Left $ msg s) (Right . query toText) $ lookupMeta s meta
    toText (Str t) = t
    toText Space = " "
    toText _ = ""
    msg = printf "The '%s' metadata parameter required by filter 'with-glossary' must be defined"

findDefinitions :: (MonadIO m, MonadReader Config m, MonadState Glossary m) =>
  Inline -> m Inline
findDefinitions (Str s) = either (liftIO . die) toInline $ parseOnly str s
  where
    str = many (Right <$> term <|> Left <$> (string "[" <|> takeWhile1 (/= '[')))
    term = string "[@=" *> takeWhile1 (not . inClass "]") <* char ']'
    toInline = fmap group . mapM (either (pure . Str) linkTo)
    group l = if length l == 1 then l !! 0 else Span nullAttr l
    linkTo t = gets (member t) >>= loadDef t

findDefinitions i = pure i

loadDef :: (MonadIO m, MonadReader Config m, MonadState Glossary m) =>
  Text -> Bool -> m Inline
loadDef t True = raw link t
loadDef t _ = do
  Pandoc _ definition <- getDocument =<< asks ((</> unpack t <.> "md") . root)
  modify $ insert t [] -- set a (temporary) empty definition for the term to avoid loops
  modify . insert t =<< walkM findDefinitions definition
  loadDef t True
  where
    getDocument path =
      liftIO (Text.readFile path >>= runIOorExplode . readMarkdown options)
    options = def { readerExtensions = pandocExtensions }

addGlossarySection :: MonadReader Config m => ([Block], Glossary) -> m [Block]
addGlossarySection (blocks, definitions)
  | null definitions = pure $ blocks
  | otherwise = (blocks <>) <$> sequence [asks header, chaptermark, body]
  where
    header (Config {level, title}) =
      Header level ("glossary", ["unnumbered"], []) [Str title]
    chaptermark = Plain . pure <$> (raw sectionTitle =<< asks title)
    body = DefinitionList <$> (mapM entryOf $ Map.toList definitions)
    entryOf (key, definition) = (,[definition]) <$> sequence [raw target key]

labelFor :: Text -> Text
labelFor t = "glossary-" <> Text.map normalize t
  where
    normalize c
      | isUpper c = toLower c
      | isAlphaNum c = c
      | otherwise = '_'

raw :: MonadReader Config m => (Text -> KnownFormat -> [Text]) -> Text -> m Inline
raw maker t = asks (($ (t, maker t)) . rawMaker)

target :: Text -> KnownFormat -> [Text]
target t Latex = ["\\hypertarget{", labelFor t, "}{", t, "}"]
target t Html = ["<span id=\"", labelFor t, "\">", t, "</span>"]

link :: Text -> KnownFormat -> [Text]
link t Latex = ["\\hyperlink{", labelFor t, "}{", t, "}"]
link t Html = ["<a href=\"#", labelFor t, "\">", t, "</a>"]

sectionTitle :: Text -> KnownFormat -> [Text]
sectionTitle t Latex = ["\\chaptermark{", t, "}"]
sectionTitle _ Html = [] -- does not apply so not implemented
