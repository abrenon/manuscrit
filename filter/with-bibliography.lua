function getField(field, default)
	if field == nil then
		return default
	else
		return field[1].text
	end
end

function Pandoc(doc)
	level = getField(doc.meta['bibliography-level'], 1)
	entry_spacing = getField(doc.meta['bibliography-spacing'], 0)
	section_title = getField(doc.meta['bibliography-title'], "Bibliography")
	doc.blocks:extend({pandoc.Header(
		level,
		section_title,
		{id = 'bibliography', class = 'unnumbered'}
	), pandoc.RawInline(
		"latex", ("\\chaptermark{" .. section_title .. "}")
	), pandoc.Div(
		{},
		{id = 'refs', ['entry-spacing'] = entry_spacing}
	)})
	return pandoc.utils.citeproc(doc)
end
