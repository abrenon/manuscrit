function Pandoc(doc)
	doc.blocks:extend({
        pandoc.RawInline('latex', '\\newpage'),
        pandoc.RawInline('latex', '\\sffamily'),
        pandoc.RawInline('latex', '\\pagestyle{empty}'),
        pandoc.RawInline('latex', '\\input{template/folio.tex}')
    })
    return doc
end
